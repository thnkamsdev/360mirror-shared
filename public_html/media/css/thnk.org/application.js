// import Behaviour from 'lib/Behaviour';

// // REGISTER BEHAVIOUR
// Behaviour.register('a', Link);

// document.addEventListener('DOMContentLoaded', () => {
//   Behaviour.behave(document.body);
// });

/*
import 'vendor/owl.carousel'
import 'lazy-video'
import 'popup'

var overlay;

$(window).load(function() {
  $(".carousel").owlCarousel({
    loop: true,
    autoHeight: true,
    // margin: 20,
    autoWidth: true,
    center: true,
    nav: true,
    // stagePadding: 40,
    navText: ["<div class='prev-arrow'>","<div class='next-arrow'>"],
    onInitialized: setOwlSlideClass,
    responsive: {
      0 : {
        stagePadding: 40,
        autoWidth: true,
        // items: 1,
      },

      634 : {
        stagePadding: 0,
        autoWidth: true,
      }
    }
  });

  // force show arrows
  //$('owl-nav').removeClass('disabled');
})

function setOwlSlideClass(event){

  $('.owl-item:not(.class-added)').each(function(){
    $(this).addClass( 'owl-item--'+$(this).children().attr('data-type') ).addClass('class-added');
  })

  setTimeout(function(){

    // $(window).on('resize', resizeQuoteSlides);

    $(event.target).trigger('refresh.owl.carousel');

  },100);

}

function resizeQuoteSlides() {
  $('.owl-item--quote').css( { 'width' : $('.carousel').width() } );
}
*/

$(document).ready(function() {

  /*if( $('.block.program-info').length ) initProgramToggles();

  if( $('.cta-overlay').length ) initCtaOverlay();*/

  $('#share-fb').on('click', share_fb);
  $('#share-twitter').on('click', share_tw);
  $('#share-linkd').on('click', share_linkd);

  $('.header__menu').on('click', openMobileMenu);

  $('.header__nav__item--dropdown').on('click', openDropdown);

  /*$('.apply__button').on('click', openApply);
  $('.apply__form__cross').on('click', openApply);*/

  /*changeHeader();

  $(window).on('scroll', changeHeader);*/

  $( "body" ).on( "autocompleteselect", '.facetwp-autocomplete', function( event, ui ) {
    console.log('clicked!');
  } );


});

/* 
function initProgramToggles(){

  $('.block').on('click', '.pilar__heading', toggleProgram);
  $('.block').on('click', '.pilar__item__header', togglePilar);

}


function toggleProgram(){

  $(this).parent('.pilar__info').toggleClass('is-open');

}

function togglePilar() {

  $(this).parent().parent('.pilar__item').toggleClass('is-open');

}

function initCtaOverlay() {
  $('.cta-overlay button').on('click', function(e){ e.preventDefault(); });
  $('.cta-overlay__toggle').on('click', toggleOverlay);
  $('.cta-overlay__cross').on('click', toggleOverlay);
  $('.cta-overlay__next, .cta-overlay__submit').on('click', advanceApplyForm);
  $('.cta-overlay input[type="radio"]').on('change', advanceApplyForm);
  $('.cta-overlay__close').on('click', function(){

    const animateOpen = new TimelineLite();
    const overlay = $('.cta-overlay');

    $('.cta-overlay fieldset.is-active').removeClass('is-active').hide();
    // $('.cta-overlay fieldset[data-set="1"]').addClass('is-active').show();

    overlay.removeClass('is-open');
    animateOpen.
    to('#overlay-apply fieldset', 0.3, { opacity: 0, transform: "translate3d(0, 20px, 0)" },).
    to('#overlay-apply', 0.3, { bottom: 50, transform: "translate3d(0, 100%, 0)"}, "+0.3").
    to('#overlay-apply', 0.2, { width: 200 }, '+0.6');

  });

}

function toggleOverlay() {

  const button = $(this);
  const overlay = button.parent();
  const animateOpen = new TimelineLite();
  $('body').toggleClass('stop-scroll')

  if(overlay.hasClass('is-open')) {

    $('.cta-overlay').css('max-height', em(10));
    $('.cta-overlay__content').css('display', 'none');

    overlay.removeClass('is-open');
    animateOpen.
    to('#overlay-apply fieldset', 0.3, { opacity: 0, transform: "translate3d(0, 20px, 0)" },).
    to('#overlay-apply', 0.3, { bottom: 50, transform: "translate3d(0, 100%, 0)"}, "+0.3").
    to('#overlay-apply', 0.2, { width: 200 }, '+0.6');

  } else {

     $('.cta-overlay').css('max-height', em(90));
     $('.cta-overlay__content').css('display', 'block');

    let overlayW = $(window).width() - 20;
    overlay.addClass('is-open');
    if(overlay.find('fieldset.is-active').length < 1) {
      $('.cta-overlay fieldset[data-set="1"]').addClass('is-active').show();
    }
    animateOpen.
    to('#overlay-apply', 0.2, { width: overlayW }).
    to('#overlay-apply', 0.3, { bottom: '-10px', transform: "translate3d(0, 0, 0)"}, '+0.2').
    to('#overlay-apply fieldset', 0.3, { opacity: 1, transform: "translate3d(0, 0, 0)" }, '+0.6');

  }

}

function em(input) {
  var emSize = parseFloat($("body").css("font-size"));
  return (emSize * input);
}

function advanceApplyForm() {

  let cur = parseInt($('.cta-overlay fieldset.is-active').attr('data-set')) + 1;
  $('.cta-overlay fieldset.is-active').removeClass('is-active').hide();
  $('.cta-overlay fieldset[data-set="'+cur+'"]').show().addClass('is-active');


}
*/

function share_tw(c) {
  var left = (screen.width/2)-(626/2); var top = (screen.height/2)-(436/2);
  console.log(this)
  window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436,top='+top+',left='+left);return false;
}

function share_fb(c) {
  var left = (screen.width/2)-(626/2); var top = (screen.height/2)-(436/2);
  window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436,top='+top+',left='+left);return false;
}


function share_linkd(c) {
  var left = (screen.width/2)-(626/2); var top = (screen.height/2)-(436/2);
  window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436,top='+top+',left='+left);return false;
}

function openDropdown() {

  if ($(this).children(".nav__dropdown-container").hasClass('nav__dropdown-container--open')) {
    $(this).children(".nav__dropdown-container").toggleClass('nav__dropdown-container--open')
    $(this).toggleClass('open');
    $('body').removeClass('header-open');
  } else {
    $(".nav__dropdown-container").removeClass('nav__dropdown-container--open')
    $('.header__nav__item').removeClass('open');
    $(this).children(".nav__dropdown-container").addClass('nav__dropdown-container--open');
    $(this).addClass('open');
    $('body').addClass('header-open');
  }

}


function openMobileMenu() {

  $('.header').toggleClass('header--open')
  $('body').toggleClass('stop-scroll')


}

/*
function openApply() {
  // only is it is a form
  // could be a button too
  if ($(this).parent().find('form').length) {
    $(this).parent('.apply__form').toggleClass('apply__form--open')
  }
}

function changeHeader() {
  var ScrollTop = parseInt($(window).scrollTop());

  if (ScrollTop > 220) {
    $('.header').addClass('header--scrolled')
  } else {
    $('.header').removeClass('header--scrolled')
  }
}
*/