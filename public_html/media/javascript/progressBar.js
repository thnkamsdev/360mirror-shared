function init(){
    $('progress').hide();

    var left = $('.two').offset();
    var color =$('input[name="color"]').val();
    if(color != ''){
        $("<style type='text/css'> progress.fixedColor::-webkit-progress-value{background:"+color+";} </style>").appendTo("head");
        $("progress").addClass("fixedColor");
    }else{
        var color = '#ec1c24';
        $("<style type='text/css'> progress.fixedColor::-webkit-progress-value{background:"+color+";} </style>").appendTo("head");
        $("progress").addClass("fixedColor");
    }
    fixHeight();

    $('span.radio').click(function(){
        check = true;

        var valueC = $(this).next().val();
        if(valueC == 9)
            valueC = 0;
        $(this).parent().parent().parent().find('div:not(:first-child)').find('span').not(':last').fadeOut();
        $(this).parent().parent().parent().find('progress').fadeIn();

        hei = $(this).parent().parent().parent().height();
        newhei = $(this).parent().parent().parent().find('.seven').find('span').offset();
        newhei.left = left.left;
        console.log(newhei);


        if(valueC != 0){
            $(this).parent().parent().parent().find('progress').animate({ value: valueC*20 }, 500);
            $(this).parent().parent().parent().find('progress').addClass('index');
            $(this).parent().parent().parent().children('.two, .three, .four, .five, .six, .seven').addClass('try');
        }else{
            $(this).parent().parent().parent().find('progress').replaceWith( " <progress class=\"disable\" value=\"100\" max=\"100\"></progress>" );
            $(this).parent().parent().parent().find('progress').addClass('index');
            $(this).parent().parent().parent().children('.two, .three, .four, .five, .six, .seven').addClass('try');
        }
        console.log(newhei);
        $(this).parent().parent().parent().find('progress').offset(newhei);
        $(this).parent().parent().parent().next().css('margin-top',"-50px");
        callF();
    });

function callF(){

    $('.demonBox').find('ul').find('li:not(:first-child)').each(function(){
        if($(this).children('progress').is(':visible')){
            $(this).children('div:not(:first-child)').hover(
                function() {
                    $( this ).addClass( "hover" );
                    $(this).click(function(){
                        if($(this).parent().find('progress').hasClass('disable'))
                            $(this).parent().find('progress').removeClass('disable');
                        $(this).find('input').attr('checked', 'checked');
                        var val = $(this).find('input').val();
                        if(val == 9){
                            $(this).parent().find('progress').replaceWith( " <progress class=\"disable\" value=\"100\" max=\"100\"></progress>" );
                            var hei2 = $(this).parent().find('.seven').find('span').offset();
                            hei2.left = left.left;
                            $(this).next().next().offset(hei2);
                        }else{
                            $(this).parent().find('progress').addClass("fixedColor");
                            $(this).parent().find('progress').animate({ value: val*20 },{queue: false, duration:500});
                        }
                    });
                }, function() {
                    $( this ).removeClass( "hover" );
                }
            );
        }
    });

}

function fixHeight(){

    $('body').find('.bar').each(function(a,b){
        var hei = $(this).parent().height();
        $(this).parent().children('.two, .three, .four, .five, .six, .seven').height(hei);
    });

}

}

window.addEventListener('load',init,false);
