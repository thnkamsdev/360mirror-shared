jQuery(document).ready(function($) {

	/**
	 * Toggle the information panel
	 * @return {void}
	 */
	$('#information_toggle').click(function() {
		
		var $information = $('.information');
		var $howitworks = $('.howitworks');
		
		if ($information.hasClass('collapsed')) {

			// Slide down
			$howitworks.addClass('halfMargin');
			$information.slideDown().removeClass('collapsed');
			$(this).addClass('toggled');

			// Show the slide
			questionSlideTimeout = showCurrentSlide();

		} else {

			// Slide up
			$information.slideUp().addClass('collapsed');
			$(this).removeClass('toggled');
			$howitworks.removeClass('noMargin');

			// Kill the delay
			clearTimeout(window.questionSlideTimeout);
		}
	});

	/**
	 * Toggle the login panel
	 * @return {void}
	 */	
	$('#login_toggle').click(function() {
		jQuery.fn.center = function () {
			this.css("position","absolute");
			this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
			this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
			return this;
		}
		// Center and fade stuff
		$('.introduction').css("opacity", "0.2");
		$('.information').slideUp().addClass('collapsed');
		$('.information').removeClass('toggled');
		$('.howitworks').removeClass('noMargin');
		$('.homeBox .loginBox').center();
		$('.homeBox .loginBox').fadeIn();
	});
	$('.loginBox .close').click(function(event) {
		event.preventDefault();
		// Fade stuff
		$('.introduction').css("opacity", "1");
		$('.homeBox .loginBox').fadeOut();
	});

	/**
	 * Start up the slider
	 */
	$('.questionslides li').last().addClass('shown');

	/**
	 * Link feedback form
	 */
	// $('#footer .feedback').click(function(event) {
	// 	event.preventDefault();
	// 	if (!webreepEngine) {
	// 		return;
	// 	}
	// 	webreepEngine.Events._showFeedbackForm();
	// });

	/**
	 * Check if we need to show the login
	 */
	if (window.location.hash && window.location.hash.indexOf('showlogin') >= 0) {
		$('#login_toggle').click();
	}

	/**
	 * Track GA more smartly
	 */
	$('a[target="_blank"]').click(function(){

		// Check if the link is staying within this domain
		var url = $(this).attr('href');
		var domain = 'http://mirror.thnk.org';
		if (url.indexOf(domain) >= 0) {
			_gaq.push(['_trackPageview', '/_blank/' + url.substr(domain.length)]);

		} else {

			// Check if its a # link
			if (url.indexOf('#') === 0) {

				var title = $(this).attr('rel');
				if (!title || title === '') {

					// Dont track the # link if we dont have any info
					return;
				}

				// Track the button
				_gaq.push(['_trackPageview', '/button/' + title]);
				return;
			}

			_gaq.push(['_trackPageview', '/outbound/' + url]);
		}
	});
	$('a:not([href^="http://mirror.thnk.org"])').click(function(){

		// Check if its a # link
		var url = $(this).attr('href');
		if (url.indexOf('#') === 0) {

			var title = $(this).attr('rel');
			if (!title || title === '') {

				// Dont track the # link if we dont have any info
				return;
			}

			// Track the button
			_gaq.push(['_trackPageview', '/button/' + title]);
			return;
		}

		// The link is going outbound, track it
		_gaq.push(['_trackPageview', '/outbound/' + url]);
	});

	// Listen to form submits
	$('input[type="submit"]').click(function(){

		// Find the form submit url
		var url = $(this).parents('form').attr('action');
		url = url.replace('http://mirror.thnk.org/', '');

		// The link is going outbound, track it
		_gaq.push(['_trackPageview', '/formsubmit/' + url]);
	});
});

/**
 * Create the question slider on the more information panel
 */
var currentFocusSlide = 0;
var questionSlideTimeout = -1;

/**
 * Swap the question slides
 * @return {void}
 */
function showCurrentSlide() {

	$('.questionslides li.shown').fadeOut(function() {
		$('.questionslides li').eq(currentFocusSlide).fadeIn().addClass('shown');
	}).removeClass('shown');
	
	return setTimeout(function() {
		currentFocusSlide = (currentFocusSlide + 1) % $('.questionslides li').length;
		questionSlideTimeout = showCurrentSlide();
	}, 6000);
}