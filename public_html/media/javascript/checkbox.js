function init(){
		$('input[name*=category]').each(function(index) {
			if($(this).attr('checked') === false){
				var take = $(this).attr('name');
				take = take.replace('category', "questions");
				$('input[name*='+take+']').attr('disabled', true);
			}
		});

		$(".checkEnable").change(function() {
		var name = $(this).attr('name');
		name = name.replace('category', "questions");
			if ($(this).attr('checked') === true)
				$('input[name*='+name+']').not(".checkEnable").attr('disabled', false);
			else
				$('input[name*='+name+']').not(".checkEnable").attr('disabled', true);
		});
		
	
}

window.addEventListener('load',init,false);

