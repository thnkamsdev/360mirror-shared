<?php
	$id = $_POST['ajaxUserId'];
	$env = $_POST['ajaxEnv'];
	$client_id = $_POST['ajaxClientId'];

	switch ($env) {
		case 'local':
			$servername = "localhost";
			$username = "root";
			$password = "root";
			$dbname = "360mirror";
			break;
		case 'thnkdev':
			$servername = "37.97.165.16";
			$username = "dbdev.360mirror";
			$password = "vVHFm6gouMXi6Jidnd";
			$dbname = "360mirror";
			break;
		default:
			$servername = "localhost";
			$username = "360mirror";
			$password = "OLskx5h4jwMeL62y";
			$dbname = "360mirror";
			break;
	}

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	if (isset($_POST['checkboxvalue']) && isset($_POST['ajaxRole']) ){
		$checkBoxVal = $_POST['checkboxvalue'];
		$role = $_POST['ajaxRole'];

		define('USER_CLIENT_ADMIN', 3);
		define('USER_CLIENT_COACH', 4);
		define('USER_PARTICIPANT', 5);

		if ($checkBoxVal < 0 || $checkBoxVal > 1) {
			$checkBoxErr = "The checkbox value is not allowed";
			return $checkBoxErr;
		}

		switch ($role) {
		case 'participantRole':
			if ($checkBoxVal == 0){
				$sql = "DELETE FROM THNK_users_roles WHERE user_id=".$id." AND role_id=".USER_PARTICIPANT." AND client_id=".$client_id." ";
			}elseif($checkBoxVal == 1){
				$sql = "INSERT INTO THNK_users_roles (user_id, role_id, client_id) VALUES (".$id.", ".USER_PARTICIPANT.", ".$client_id.")";
			}else{
				$sql = "Error updating record: " . $conn->error;
			}
			break;
		case 'coachRole':
			if ($checkBoxVal == 0){
				$sql = "DELETE FROM THNK_users_roles WHERE user_id=".$id." AND role_id=".USER_CLIENT_COACH." AND client_id=".$client_id." ";
			}elseif($checkBoxVal == 1){
				$sql = "INSERT INTO THNK_users_roles (user_id, role_id, client_id) VALUES (".$id.", ".USER_CLIENT_COACH.", ".$client_id.")";
			}else{
				$sql = "Error updating record: " . $conn->error;
			}
			break;
		case 'clientRole':
			if ($checkBoxVal == 0){
				$sql = "DELETE FROM THNK_users_roles WHERE user_id=".$id." AND role_id=".USER_CLIENT_ADMIN." AND client_id=".$client_id." ";
			}elseif($checkBoxVal == 1){
				$sql = "INSERT INTO THNK_users_roles (user_id, role_id, client_id) VALUES (".$id.", ".USER_CLIENT_ADMIN.", ".$client_id.")";
			}else{
				$sql = "Error updating record: " . $conn->error;
			}
			break;
		default:
			$sql = "Error updating record: " . $conn->error;
		}

	}elseif(isset($_POST['ajaxThisVal']) && isset($_POST['ajaxTypeField'])){
		$val = $_POST['ajaxThisVal'];
		$type = $_POST['ajaxTypeField'];

		switch ($type) {
		case 'firstname':
			if (!preg_match("/^[a-zA-Z ]*$/",$val)) {
			  $nameErr = "Only letters and white space allowed";
			  return $nameErr;
			}
			$sql = "UPDATE THNK_users SET ".$type."='".rtrim($val)."' WHERE id=".$id."";
			break;
		case 'lastname':
			if (!preg_match("/^[a-zA-Z ]*$/",$val)) {
			  $nameErr = "Only letters and white space allowed";
			  return $nameErr;
			}
			$sql = "UPDATE THNK_users SET ".$type."='".rtrim($val)."' WHERE id=".$id."";
			break;
		case 'email':
			if (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
			  $emailErr = "Invalid email format";
			  return $emailErr;
			}
			$sql = $conn->query("SELECT * FROM THNK_users WHERE email='".rtrim($val)."'");
			$numrows = $sql->fetch_assoc();
			if($numrows > 0){
				$sql = "Error updating record: The email already exists.";
				break;
			}else{
				$sql = "UPDATE THNK_users SET ".$type."='".rtrim($val)."' WHERE id=".$id."";
				break;
			}
		case 'company':
			$sql = "UPDATE THNK_users SET ".$type."='".rtrim($val)."' WHERE id=".$id."";
			break;
		default:
			$sql = "Error updating record: " . $conn->error;
		}
	}else{
		$sql = "Error updating record: " . $conn->error;
	}

	if ($conn->query($sql) === TRUE) {
	    echo "Record updated successfully";
	} else {
	    echo "Error updating record: " . $conn->error . "in the environment " .$env;
	}

	$conn->close();

	return true;
?>
