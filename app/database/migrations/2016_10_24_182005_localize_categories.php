<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class LocalizeCategories extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categories', function (Blueprint $table)
		{
			$table->renameColumn('name', 'name_enUS');
			$table->renameColumn('description', 'description_enUS');
			$table->renameColumn('short', 'short_enUS');

			// These shouldn't be text (but rather string/varchar), but the original
			// ones were text, so I went from there.
			$table->text('name_esES')->after('name');
			$table->text('description_esES')->after('description');
			$table->text('short_esES')->after('short');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categories', function (Blueprint $table)
		{
			$table->renameColumn('name_enUS', 'name');
			$table->renameColumn('description_enUS', 'description');
			$table->renameColumn('short_enUS', 'short');

			$table->dropColumn('name_esES');
			$table->dropColumn('description_esES');
			$table->dropColumn('short_esES');
		});
	}

}
