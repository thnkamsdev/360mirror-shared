<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class GenerateDefaultSlugs extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$client_ids = array_pluck(DB::table('users_roles')->select('client_id')->distinct()->get(), 'client_id');

		// Generate slugs for all clients
		foreach ($client_ids as $client_id)
		{
			$client = User::find($client_id);
			if ( ! $client)
			{
				continue;
			}

			DB::table('users')->where('id', '=', $client_id)->update([
				'slug' => THNK\Mirror\Client\Client::generateSlugFromName($client->company),
			]);
		}

		// Make sure no role is linked to client 0
		DB::table('users_roles')->where('client_id', '=', '0')->update(['client_id' => 105]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('users')->update(['slug' => '']);
	}

}
