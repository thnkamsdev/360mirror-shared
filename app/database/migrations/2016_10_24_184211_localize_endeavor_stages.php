<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class LocalizeEndeavorStages extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('endeavor_stages', function (Blueprint $table)
		{
			$table->renameColumn('name', 'name_enUS');
			$table->text('name_esES')->after('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('endeavor_stages', function (Blueprint $table)
		{
			$table->renameColumn('name_enUS', 'name');
			$table->dropColumn('name_esES');
		});
	}

}
