<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LocalizeEndeavorTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('endeavor_types', function(Blueprint $table)
		{
			$table->renameColumn('name', 'name_enUS');
			$table->text('name_esES')->after('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('endeavor_types', function(Blueprint $table)
		{
			$table->renameColumn('name_enUS', 'name');
			$table->dropColumn('name_esES');
		});
	}

}
