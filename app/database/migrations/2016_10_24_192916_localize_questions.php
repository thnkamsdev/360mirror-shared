<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LocalizeQuestions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('questions', function(Blueprint $table)
		{
			$table->renameColumn('name', 'name_enUS');
			$table->renameColumn('description', 'description_enUS');

			// These shouldn't be text (but rather string/varchar), but the original
			// ones were text, so I went from there.
			$table->text('name_esES')->after('name');
			$table->text('description_esES')->after('description');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('questions', function(Blueprint $table)
		{
			$table->renameColumn('name_enUS', 'name');
			$table->renameColumn('description_enUS', 'description');

			$table->dropColumn('name_esES');
			$table->dropColumn('description_esES');
		});
	}

}
