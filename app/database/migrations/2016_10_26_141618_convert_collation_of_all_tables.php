<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ConvertCollationOfAllTables extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// We cannot use a regular DB::statement because of query buffering
		$databaseName = Config::get('database.connections.' . Config::get('database.default') . '.database');
		DB::connection()
		  ->getPdo()
		  ->exec('ALTER DATABASE ' . $databaseName . ' CHARACTER SET utf8 COLLATE utf8_unicode_ci');

		// Convert all tables
		$tables = array_pluck(DB::select('SHOW TABLES'), 'Tables_in_360mirror');
		foreach ($tables as $table)
		{
			DB::statement('ALTER TABLE ' . $table . ' CONVERT TO CHARACTER SET utf8');
		}

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// There is no way back ;-)
	}

}
