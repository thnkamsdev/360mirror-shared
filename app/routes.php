<?php

/**
 * Init the locale here since the routes depend on it
 * If the first segment is not a valid language, it probably has no language at all
 * @var $locale \THNK\Mirror\Localization\Locale
 */
$locale = \THNK\Mirror\Localization\Facades\Localization::current();
$localePrefix = in_array(Request::segment(1), Localization::getURLPrefixes()) ? $locale->getURLPrefix() : '';

/**
 * No language or route is specified, redirect to the default language root.
 */
Route::get('/', ['as' => 'domain', function() use ($locale) {
	Session::reflash();

	return Redirect::to($locale->getURLPrefix());
}]);

/**
 * Wrap all routes in a language prefix
 */
Route::group(['prefix' => $localePrefix, 'before' => 'locale.force_prefix'], function()
{

	//ADMIN ROUTES
	Route::group(array('before' => 'typeAdmin'), function()    {
		Route::get('/clients', array('before' => 'auth', 'as' => 'clients_index', 'uses' => 'ClientsController@getClients'));

		Route::get('/clients/edit/{id}', array('before' => 'auth', 'as' => 'clients_edit_id_get', 'uses' => 'ClientsController@getEdit'));
		Route::post('/clients/edit/{id}', array('before' => 'auth', 'as' => 'clients_edit_id_post', 'uses' => 'ClientsController@postEdit'));

		Route::get('/clients/edit', array('before' => 'auth', 'as' => 'clients_edit', 'uses' => 'ClientsController@getEdit'));
		Route::post('/clients/edit', array('before' => 'auth', 'as' => 'clients_edit', 'uses' => 'ClientsController@postEdit'));

		Route::get('/clients/getQuestions/{id}', array('before' => 'auth', 'as' => 'clients_questions', 'uses' => 'ClientsController@getQuestions'));
		Route::post('/clients/getQuestions/{id}', array('before' => 'auth', 'as' => 'clients_questions_post', 'uses' => 'ClientsController@postQuestions'));

		Route::get('/clients/getQuestion/{id}', array('before' => 'auth', 'as' => 'clients_question', 'uses' => 'ClientsController@getQuestion'));
		Route::post('/clients/getQuestion/{id}', array('before' => 'auth', 'as' => 'clients_question_post', 'uses' => 'ClientsController@postQuestion'));

		Route::get('/clients/getCategory/{id}', array('before' => 'auth', 'as' => 'clients_category_edit_get', 'uses' => 'ClientsController@getCategory'));
		Route::post('/clients/getCategory/{id}', array('before' => 'auth', 'as' => 'clients_category_edit_post', 'uses' => 'ClientsController@postCategory'));

		Route::post('/clients/postOrderQuestions', array('before' => 'auth', 'uses' => 'ClientsController@postOrderQuestions'));
		Route::get('/clients/postOrderQuestions', array('before' => 'auth', 'uses' => 'ClientsController@postOrderQuestions'));

		Route::get('/clients/getCoaches/{id}', array('before' => 'auth', 'as' => 'clients_coach_id', 'uses' => 'ClientsController@getCoaches'));
		Route::post('/clients/getCoaches/{id}', array('before' => 'auth',  'uses' => 'ClientsController@postCoaches'));

		Route::get('/clients/getResendCoachCredentials/{client_id}/{id}', array('before' => 'auth', 'uses' => 'ClientsController@getResendCoachCredentials'));

		Route::get('/preview', array('before' => 'auth', 'as' => 'preview-template', 'uses' => 'PdfController@preview'));

		//Route::get('/test/remindparticipantsnonactivity',  array('before' => 'auth',  'uses' => 'TestController@getRemindParticipantsNonActivity'));

	});

	//ADMIN & CLIENT ROUTES
	Route::group(array('before' => 'typeAdminClient'), function() {
		Route::post('/export/', array('before' => 'auth', 'as' => 'data_export', 'uses' => 'DataExportController@downloadAsExcel'));
	});

	//ADMIN & CLIENT & COACH ROUTES
	Route::group(array('before' => 'typeAdminClientCoach'), function() {
		Route::get('/clients/getResendParticipantCredentials/{client_id}/{id}', array('before' => 'auth', 'uses' => 'ClientsController@getResendParticipantCredentials'));

		Route::get('/clients/getAdmin/{id}', array('before' => 'auth', 'as' => 'clients_admin_id', 'uses' => 'ClientsController@getAdmin'));
		Route::post('/clients/getAdmin/{id}', array('before' => 'auth',  'uses' => 'ClientsController@postAdmin'));
		Route::get('/clients/getAdmin/{id}/export', array('before' => 'auth',  'uses' => 'ClientsController@getDataExport'));
	});

	//ADMIN & CLIENT & COACH & PARTICIPANT ROUTES
	Route::group(array('before' => 'typeAdminClientCoachParticipant'), function() {
		Route::get('/clients/logout/{id}',  array('before' => 'auth', 'as' => 'test_logout', 'uses' => 'UserController@logout'));

		Route::get('/surveys/{user_id}', array('before' => 'auth', 'as' => 'surveys_get', 'uses' => 'UserController@getSurveys'));
		Route::post('/surveys/{user_id}', array('before' => 'auth', 'as' => 'surveys_post', 'uses' => 'UserController@postSurveys'));
	});

	//ADMIN & COACH ROUTES
	Route::group(array('before' => 'typeAdminCoach'), function() {
		Route::post('/clients/getTest', array('before' => 'auth', 'uses' => 'ClientsController@postClientTestUser'));
	});

	//ADMIN & COACH & PARTICIPANT ROUTES
	Route::group(array('before' => 'typeAdminCoachParticipant'), function() {
		Route::get('/test/myprofile/{client_id}/{id}',  array('before' => 'auth', 'as' => 'test_profile_get', 'uses' => 'ProfileController@getProfile'));
		Route::post('/test/myprofile/{client_id}/{id}',  array('before' => 'auth', 'as' => 'test_profile_post', 'uses' => 'ProfileController@postProfile'));

		Route::post('/test/generate_own_report/{id}', array('before' => 'auth', 'as' => 'generate_own_report', 'uses' => 'PdfController@selfReport'));
		Route::post('/test/generate_full_report/{id}', array('before' => 'auth', 'as' => 'generate_full_report', 'uses' => 'PdfController@fullReport'));

		Route::get('/endeavor/{client_id}/{id}', array('before' => 'auth', 'as' => 'endeavor_index_get', 'uses' => 'EndeavorController@getIndex'));
		Route::post('/endeavor/{client_id}/{id}', array('before' => 'auth', 'as' => 'endeavor_index_post', 'uses' => 'EndeavorController@postIndex'));
	});

	//ADMIN & PARTICIPANT ROUTES
	Route::group(array('before' => 'typeParticipant'), function() {
		Route::get('/dashboard/{client_id}', array('before' => 'auth', 'as' => 'dashboard_get', 'uses' => 'DashboardController@getIndex'));
		Route::post('/dashboard/{client_id}', array('before' => 'auth', 'as' => 'dashboard_post', 'uses' => 'DashboardController@postIndex'));
		Route::get('/dashboard/index/{client_id}/{id}', array('before' => 'auth', 'as' => 'dashboard_index_get', 'uses' => 'DashboardController@getIndex'));
		Route::post('/dashboard/index/{client_id}/{id}', array('before' => 'auth', 'as' => 'dashboard_index_post', 'uses' => 'DashboardController@postIndex'));

		Route::get('/test/invite_respondents/{client_id}/{id}', array('before' => 'auth', 'as' => 'test_edit_respondents_get', 'uses' => 'TestController@getInviteRespondents'));
		Route::post('/test/invite_respondents/{client_id}/{id}', array('before' => 'auth', 'as' => 'test_edit_respondents_post', 'uses' => 'TestController@postInviteRespondents'));

		Route::get('/test/view_text', array('before' => 'auth', 'as' => 'test_view_text', 'uses' => 'TestController@getViewText'));
		Route::get('/test/view_text_thnk', array('before' => 'auth', 'as' => 'test_view_text_thnk', 'uses' => 'TestController@getViewTextTHNK'));
		Route::get('/test/view_text_storypanda', array('before' => 'auth', 'as' => 'test_view_text_storypanda', 'uses' => 'TestController@getViewTextSP'));
		Route::get('/test/view_text_siemens', array('before' => 'auth', 'as' => 'test_view_text_siemens', 'uses' => 'TestController@getViewTextSiemens'));
		Route::get('/test/view_text_olx', array('before' => 'auth', 'as' => 'test_view_text_olx', 'uses' => 'TestController@getViewTextOLX'));

		Route::get('/test/edit/{client_id}', array('before' => 'auth', 'as' => 'test_edit', 'uses' => 'TestController@getTest'));
		Route::post('/test/edit/{client_id}', array('before' => 'auth', 'as' => 'test_edit_post', 'uses' => 'TestController@postTest'));
		Route::get('/test/edit/{client_id}/{id}', array('before' => 'auth', 'as' => 'test_edit_existing', 'uses' => 'TestController@getTest'));
		Route::post('/test/edit/{client_id}/{id}', array('before' => 'auth', 'as' => 'test_edit_existing_post', 'uses' => 'TestController@postTest'));
	});

	//NO AUTH ROUTES
		Route::get('/', 'UserController@getIndex');
		Route::post('/', 'UserController@postIndex');

		Route::get('/impact', 'UserController@getIndexSP');
		Route::post('/impact', 'UserController@postIndex');

		Route::get('/thnk', 'UserController@getIndexTHNK');
		Route::post('/thnk', 'UserController@postIndex');

		Route::get('/questmirror', 'UserController@getIndexQM');
		Route::post('/questmirror', 'UserController@postIndex');

		Route::get('/endeavorscan', 'UserController@getIndexES');
		Route::post('/endeavorscan', 'UserController@postIndex');

		Route::get('/runwaymirror', 'UserController@getIndexRM');
		Route::post('/runwaymirror', 'UserController@postIndex');
		Route::get('/scaleupnation', 'UserController@getIndexRM');
		Route::post('/scaleupnation', 'UserController@postIndex');

		Route::get('/intro/{id}/{md5}', array('as' => 'feedback_intro_get', 'uses' => 'TestController@getTestRespondent'));
		Route::post('/intro/{id}/{md5}', array('as' => 'feedback_intro_post', 'uses' => 'TestController@postTestRespondent'));

		Route::get('/feedback/{id}/{md5}', array('as' => 'feedback_get', 'uses' => 'TestController@getTestRespondent'));
		Route::post('/feedback/{id}/{md5}', array('as' => 'feedback_post', 'uses' => 'TestController@postTestRespondent'));

		Route::get('/feedback/thanks', array('as' => 'feedback_thanks', 'uses' => 'TestController@getThanksRespondent'));
		Route::get('/feedback/thnk', array('as' => 'feedback_thnk', 'uses' => 'TestController@getThanksTHNK'));

		Route::get('/password/reset', array('uses' => 'RemindersController@getRemind', 'as' => 'password_remind'));
		Route::post('/password/reset', array('uses' => 'RemindersController@postRemind', 'as' => 'password_request'));
		Route::get('/password/reset/{token}', array('uses' => 'RemindersController@getReset', 'as' => 'password_reset'));
		Route::post('/password/reset/{token}', array('uses' => 'RemindersController@postReset', 'as' => 'password_update'));

		Route::get('/password/thnk', array('uses' => 'RemindersController@getRemind', 'as' => 'password_remind_thnk'));
		Route::post('/password/thnk', array('uses' => 'RemindersController@postRemindTHNK', 'as' => 'password_request_thnk'));
		//MAIL LINK NOT WORKING Route::get('/password/thnk/{token}', array('uses' => 'RemindersController@getReset', 'as' => 'password_reset_thnk'));
		//MAIL LINK NOT WORKING Route::post('/password/thnk/{token}', array('uses' => 'RemindersController@postReset', 'as' => 'password_update_thnk'));

		Route::get('/password/impact', array('uses' => 'RemindersController@getRemind', 'as' => 'password_remind_impact'));
		Route::post('/password/impact', array('uses' => 'RemindersController@postRemindImpact', 'as' => 'password_request_impact'));

		Route::get('/password/questmirror', array('uses' => 'RemindersController@getRemind', 'as' => 'password_remind_thnkv3'));
		Route::post('/password/questmirror', array('uses' => 'RemindersController@postRemindQM', 'as' => 'password_request_thnkv3'));

		Route::get('/password/endeavorscan', array('uses' => 'RemindersController@getRemind', 'as' => 'password_remind_thnkv3_endeavor'));
		Route::post('/password/endeavorscan', array('uses' => 'RemindersController@postRemindES', 'as' => 'password_request_thnkv3_endeavor'));

		Route::get('/test/remindparticipantsnonactivity',  array('uses' => 'TestController@getRemindParticipantsNonActivity'));

	//NOT READY
		Route::get('/clients/parseOrders', array('before' => 'auth', 'as' => 'parse_orders', 'uses' => 'ClientsController@parseOrders'));
});