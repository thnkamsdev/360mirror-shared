<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
  //
});


App::after(function($request, $response)
{
  //
});

Route::filter('typeAdmin', function()
{
  if(is_null(Auth::user())){
    $message = "Security error #g8rSloul";
    return Redirect::to(translate_url('/'))->with(['message' => $message]);
  }else{ 
    if ( !Auth::user()->hasRole('USER_THNK_ADMIN') ) {
      $message = "Security error #G3n4auws";
      return Redirect::to(translate_url('/'))->with(['message' => $message]); 
    }
  }
});

Route::filter('typeAdminClient', function()
{ 
  if(is_null(Auth::user())){
    $message = "Security error #2ZD9NEGs";
    return Redirect::to(translate_url('/'))->with(['message' => $message]);
  }else{
      if ( !Auth::user()->hasRole('USER_THNK_ADMIN') && !Auth::user()->hasRole('USER_CLIENT') && !Auth::user()->hasRole('USER_CLIENT_ADMIN') ) {
      $message = "Security error #99j5Y0Lj";
      return Redirect::to(translate_url('/'))->with(['message' => $message]);
    }
 }
});
Route::filter('typeAdminClientCoach', function()
{ 
  if(is_null(Auth::user())){
    $message = "Security error #poC842FW";
    return Redirect::to(translate_url('/'))->with(['message' => $message]);
  }else{
    if ( !Auth::user()->hasRole('USER_THNK_ADMIN') && !Auth::user()->hasRole('USER_CLIENT') && !Auth::user()->hasRole('USER_CLIENT_ADMIN') && !Auth::user()->hasRole('USER_CLIENT_COACH') ) {
      $message = "Security error #5B3Pqp3u";
      return Redirect::to(translate_url('/'))->with(['message' => $message]);
    }
  }
});
Route::filter('typeAdminClientCoachParticipant', function()
{ 
  if(is_null(Auth::user())){
    $message = "Security error #7P44B1aM";
    return Redirect::to(translate_url('/'))->with(['message' => $message]);
  }else{
    if ( !Auth::user()->hasRole('USER_THNK_ADMIN') && !Auth::user()->hasRole('USER_CLIENT') && !Auth::user()->hasRole('USER_CLIENT_ADMIN') && !Auth::user()->hasRole('USER_CLIENT_COACH') && !Auth::user()->hasRole('USER_PARTICIPANT') ) {
      $message = "Security error #518hZVWC";
      return Redirect::to(translate_url('/'))->with(['message' => $message]);
    }
  }
});
Route::filter('typeAdminCoach', function()
{ 
  if(is_null(Auth::user())){
    $message = "Security error #1aE8Y25u";
    return Redirect::to(translate_url('/'))->with(['message' => $message]);
  }else{
    if ( !Auth::user()->hasRole('USER_THNK_ADMIN') && !Auth::user()->hasRole('USER_CLIENT_COACH') ) {
      $message = "Security error #P9kp0way";
      return Redirect::to(translate_url('/'))->with(['message' => $message]);
    }
  }
});
Route::filter('typeAdminCoachParticipant', function()
{ 
  if(is_null(Auth::user())){
    $message = "Security error #5z5eTNRr";
    return Redirect::to(translate_url('/'))->with(['message' => $message]);
  }else{
    if ( !Auth::user()->hasRole('USER_THNK_ADMIN') && !Auth::user()->hasRole('USER_CLIENT_COACH') && !Auth::user()->hasRole('USER_PARTICIPANT') ) {
      $message = "Security error #q9700w8B";
      return Redirect::to(translate_url('/'))->with(['message' => $message]);
    }
  }
});
Route::filter('typeClientCoach', function()
{ 
  if(is_null(Auth::user())){
    $message = "Security error #W9iHVXjB";
    return Redirect::to(translate_url('/'))->with(['message' => $message]);
  }else{
    //ALERT! -> THNK ADMIN IS NOT IN THIS FILTER
    if ( !Auth::user()->hasRole('USER_CLIENT') && !Auth::user()->hasRole('USER_CLIENT_ADMIN') && !Auth::user()->hasRole('USER_CLIENT_COACH') ) {
      $message = "Security error #q56AzL8r";
      return Redirect::to(translate_url('/'))->with(['message' => $message]);
    }
  }
});
Route::filter('typeParticipant', function()
{ 
  if(is_null(Auth::user())){
    $message = "Security error #10fbUGya";
    return Redirect::to(translate_url('/'))->with(['message' => $message]);
  }else{
    // THNK ADMIN ADDED TO PARTICIPANT ROUTES
    if ( !Auth::user()->hasRole('USER_PARTICIPANT') && !Auth::user()->hasRole('USER_THNK_ADMIN') ) {
      $message = "Security error #2nwqmOdi";
      return Redirect::to(translate_url('/'))->with(['message' => $message]);
    }
  }
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('/');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to(translate_url('/'));
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
