<?php

use THNK\Mirror\Localization\LocalizedModel;

class EndeavorStage extends Eloquent {

    use LocalizedModel;

	const CONCEPTING        = 1;
    const INCUBATION        = 2;
    const OPERATIONALIZING  = 3;
    const EXPANSION         = 4;

    public function endeavors()
    {
        return $this->hasMany('Endeavor');
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->getLocalized('name');
    }

    /**
     * @param string $value
     */
    public function setNameAttribute($value)
    {
        $this->setLocalized('name', $value);
    }

}