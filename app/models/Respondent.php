<?php

class Respondent extends Eloquent {

    protected $fillable = array('name', 'email', 'respondentgroup_id');

    public function respondentgroup() {
        
        return $this->belongsTo('Respondentgroup');
    }


    public function create_test($client_id, $test_id, $use_email, $custom_email = "") {

        $user = Auth::user();

        $test = new Test();
        $test->test_id              = $test_id;
        $test->type                 = Test::TEST_BY_RESPONDER;
        $test->respondent_id        = $this->id;
        $test->teststatus_id        = Teststatus::NOT_STARTED;

        $test->save();

        /*
         * Send invite
         */
		if($custom_email == ""){
            $data = array();
            $data['md5_email'] = md5($this->email);
            $data['respondent'] = $this;
            $data['user'] = $user;
            $data['test'] = $test;

            $client = User::find($client_id);
            $typeInvite = 'general';
            $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $user);
            Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
                $user = Auth::user();
                $message->to($this->email, $this->name)->from($dataMail['from'], $dataMail['firstname'])->replyTo($dataMail['email'], $dataMail['firstname'])->subject($dataMail['subject']);
                $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
            });
    
        }else{
            $data = array();
            $data['md5_email'] = md5($this->email);
            $data['respondent'] = $this;
            $data['user'] = $user;
            $data['test'] = $test;
            $data['custom_text'] = $custom_email;

            $client = User::find($client_id);
            $typeInvite = 'custom';
            $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $user);
            Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
                $user = Auth::user();
                $message->to($this->email, $this->name)->from($dataMail['from'], $dataMail['firstname'])->replyTo($dataMail['email'], $dataMail['firstname'])->subject($dataMail['subject']);
                $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
            });
            
		}
    }


    public static function getRespondentsByMainTest($test_id) {

        $respondents =      DB::table('respondents')
                        ->join('tests', 'tests.respondent_id', '=', 'respondents.id')
                        ->select('respondents.*')
                        ->where('tests.test_id', '=', $test_id)
                        ->orderby('respondentgroup_id')
                        ->get();

        return $respondents;
    }

}
