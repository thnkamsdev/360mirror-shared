<?php

class CategoryQuestionUserTest extends Eloquent {

    protected $table = 'category_question_user_test';
 
    
    public function test()
    {
        return $this->belongsTo('Test');
    }
    
    
    public function question()
    {
        return $this->belongsTo('Question');
    }
    
    public static function getCategoryQuestionUserTest($question_id, $test_id) {
        
        $category_question_user_test = CategoryQuestionUserTest::where('question_id', '=' , $question_id)
                                                ->where('test_id', '=' , $test_id)
                                                ->first();
        
        if (!isset($category_question_user_test)) {
            $category_question_user_test = new CategoryQuestionUserTest;
        }
        
        return $category_question_user_test;
    }
    
    public static function getCategoryQuestionUserTestOpenQuestion($category_id, $test_id) {
        
        $category_question_user_test = CategoryQuestionUserTest::where('category_id', '=' , $category_id)
                                                ->where('test_id', '=' , $test_id)
                                                ->first();
        
        if (!isset($category_question_user_test)) {
            $category_question_user_test = new CategoryQuestionUserTest;
        }
        
        return $category_question_user_test;
    }    
}
