<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;
use THNK\Mirror\Client\Client;
use THNK\Mirror\Localization\Facades\Localization;

class User extends Eloquent implements UserInterface, RemindableInterface {

    protected $fillable = array('email', 'password', 'firstname', 'lastname', 'gender', 'dob', 'country', 'company', 'THNKParticipant', 'program');

    public static $rules = array(
            'email'             =>  'required|email', //required|email|unique:users for new users
            'password'          =>  'required|min:8',
            'password_confirm'  =>  'required|same:password',
            'firstname'         =>  'required|alpha_spaces',
            'lastname'          =>  'required|alpha_spaces',
            'gender'            =>  'required',
            'dob'               =>  'required|date|date_format:"Y-m-d"',
            'country'           =>  'required',
            'company'           =>  'required',
            //'THNKParticipant'   =>  'required',
            //'program'           =>  'required'
            );

    public static function rulesUnique ($id=0) {
        return [
            'email' => 'required|email|unique:users' . ($id ? ",$id" : '')
        ];
    }

    /**
     * user role constants
     */
    const USER_THNK_ADMIN    = 1;
    const USER_CLIENT        = 2;
    const USER_CLIENT_ADMIN  = 3;
    const USER_CLIENT_COACH  = 4;
    const USER_PARTICIPANT   = 5;

	/**
     * The app uses the password reminders.
     *
     */
    use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    public function categories()
    {
        return $this->belongsToMany('Category');
    }

    /*public function userfunction()
    {
        return $this->belongsTo('Userfunction');
    }*/

    public function relatedUser() {
        return $this->belongsTo('User');
    }

    public function categoryQuestionUser()
    {
        return $this->hasMany('CategoryQuestionUser');
    }

	/**
	 * @return \THNK\Mirror\Client\Client
	 * @throws \THNK\Mirror\Client\InvalidClientIDException
	 */
    public function getClient()
    {
        if ( ! $this->client)
        {
            $clientId = Request::get('client_id');
            if ( ! $clientId)
            {
	            $firstRole = $this->roles->first();
	            $clientId = $firstRole->pivot->client_id;
            }

	        $this->client = $clientId === 0 ? Client::createDefault() : new Client($clientId);
        }

        return $this->client;
    }

    public function getFirstCategory($client_id) {
        // Get Categories
        if (isset($client_id) && $client_id > 0) {
            $client =  User::find($client_id);
            $categories = $client->categories;
        } else {
            $categories = $this->categories;
        }

        foreach ($categories as $category) {
            break;
        }
        return $category;
    }

    public function getLastCategory($client_id) {
        // Get Categories
        if (isset($client_id) && $client_id > 0) {
            $client =  User::find($client_id);
            $categories = $client->categories;
        } else {
            $categories = $this->categories;
        }

        foreach ($categories as $category) {
            ;
        }
        return $category;
    }

    public function getProgress($current_category_id, $client_id) {
        // Get Categories
        if (isset($client_id) && $client_id > 0) {
            $client =  User::find($client_id);
            $categories = $client->categories;
        } else {
            $categories = $this->categories;
        }

        $i = 0;
        foreach ($categories as $category) {

            if ($category->id == $current_category_id) {
                break;
            }
            $i++;
        }
        return $i;
    }

    public function getNumberOfCategories($client_id) {
        // Get Categories
        if (isset($client_id) && $client_id > 0) {
            $client =  User::find($client_id);
            $categories = $client->categories;
        } else {
            $categories = $this->categories;
        }

        return count($categories);

    }
    public function getCategories($client_id) {
        // Get Categories
        if (isset($client_id) && $client_id > 0) {
            $client =  User::find($client_id);
            $categories = $client->categories;
        } else {
            $categories = $this->categories;
        }

        return $categories;

    }

	public function get_data($test_id){
		$tests = DB::table('category_question_user_test')
                ->select('*')
                ->where('test_id', '=', $test_id)
                ->get();

        return $tests;
	}

    public function get_category_questions($category_id, $client_id) {
        // Get Questions per Category
        if (isset($client_id) && $client_id > 0) {
            $user_id = $client_id;
        } else {
            $user_id = $this->id;
        }

        $category_question_users = CategoryQuestionUser::join('questions', 'category_question_user.question_id', '=', 'questions.id')
            ->select('category_question_user.*')
            ->where('category_question_user.category_id', '=', $category_id)
            ->where('user_id', '=', $user_id)
            ->orderBy('category_question_user.order')
            ->get();

        return $category_question_users;
    }

    public function getTestsByClient() {

        $tests = DB::table('users')
                    ->join('tests', 'users.id', '=', 'tests.user_id')
                    ->select('*')
                    ->where('tests.client_id', '=', $this->id)
                    ->get();

        return $tests;

    }

    /**
     * @param int $clientId
     *
     * @return \User[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function allForClient($clientId)
    {
        return self::join('users_roles', function ($join) use ($clientId) {
                $join->on('users.id', '=', 'users_roles.user_id');
            })
            ->where('users_roles.client_id', '=', (int) $clientId)
            ->orderBy('users.firstname', 'ASC')
            ->get();
    }

    /**
     * The app uses the remember_token.
     *
     */
    public function getRememberToken()
    {
    return $this->remember_token;
    }

    public function setRememberToken($value)
    {
    $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
    return 'remember_token';
    }


    /*
    |--------------------------------------------------------------------------
    | USER ROLES
    |--------------------------------------------------------------------------
    */

    /**
     * Get the roles a user has
     */
    public function roles()
    {
        return $this->belongsToMany('Role', 'users_roles')->withPivot('client_id');
    }

    /**
     * Find out if User is currently active, based on if has any roles
     *
     * @return boolean
     */
    public function isActive()
    {
        $roles = $this->roles->toArray();
        return !empty($roles);
    }

    /**
     * Find out if user has a specific role
     *
     * @return boolean
     */
    public function hasRole($check)
    {

        return in_array($check, array_fetch($this->roles->toArray(), 'name'));
    }

    /**
     * Find out if user has a specific role in the selected client
     *
     * @return boolean
     */
    public function hasRoleInClient($role_id, $client_id, $user_id = null)
    {
        if ( ! $user_id)
        {
            $user_id = $this->id;
        }

        $roleInClient = DB::table('users_roles')
                        ->select('*')
                        ->where('users_roles.client_id', '=', (int)$client_id)
                        ->where('users_roles.role_id', '=', (int)$role_id)
                        ->where('users_roles.user_id', '=', (int)$user_id)
                        ->get();
        $num_rows = count($roleInClient);

        return $num_rows >= 1;
    }

    /**
     * Get key in array with corresponding value
     *
     * @return int
     */
    private function getIdInArray($array, $term)
    {
        foreach ($array as $key => $value) {
            if ($value == $term) {
                return $key;
            }
        }

        throw new UnexpectedValueException;
    }

    /**
     * Add roles to user
     */
    public function addRole($title)
    {
        $assigned_roles = array();

        $roles = array_fetch(Role::all()->toArray(), 'name');

        switch ($title) {
            case 'USER_THNK_ADMIN':
                $assigned_roles[] = $this->getIdInArray($roles, 'USER_THNK_ADMIN');
                break;
            case 'USER_CLIENT':
                $assigned_roles[] = $this->getIdInArray($roles, 'USER_CLIENT');
                break;
            case 'USER_CLIENT_ADMIN':
                $assigned_roles[] = $this->getIdInArray($roles, 'USER_CLIENT_ADMIN');
                break;
            case 'USER_CLIENT_COACH':
                $assigned_roles[] = $this->getIdInArray($roles, 'USER_CLIENT_COACH');
                break;
            case 'USER_PARTICIPANT':
                $assigned_roles[] = $this->getIdInArray($roles, 'USER_PARTICIPANT');
                break;
            default:
                throw new \Exception(trans('userModel.exception'));
        }

        $this->roles()->attach($assigned_roles);
    }


    /*
    |--------------------------------------------------------------------------
    | CLIENTS SECTION
    |--------------------------------------------------------------------------
    */

    /**
     * user client constants
     */
    const HALCYON            = 101;
    const SECONDMIRROR       = 105;
    const TESTTHNK           = 133;
    const TESTDEFAULT        = 142;
    const FIRSTMIRROR        = 151;
    const GGFANDTHNK         = 153;
    const IMPACT             = 1394;
    const GGF                = 1446;
    const SCHUSTERMAN        = 1491;
    const GGFTROSTEN         = 1545;
    const BAYER              = 1586;
    const QUEST_MIRROR       = 1635;
    const ENDEAVOR_SCAN      = 1675;
    const GGF_SPA            = 1693;
    const THNK_GUEST         = 1761;
    const DSIL               = 1792;
    const RUNWAY_MIRROR      = 1927;
    const NRC                = 2091;
    const NRC2018            = 2384;
    const NRC20182           = 2588;
    const OLX                = 2129;
    const OLX2018            = 2344;
    const OLX20182           = 2539;
    const SIEMENS            = 2283;
    const SIEMENSHEALTH      = 2445;
    const LEADERSBRAZIL      = 2560;
    const GGFLSCORP          = 2943;

    /**
     * Get PDF path
     * @param  string $pdfType  full or self
     * @param  int    $thnkUser 1 indicates THNK programs
     * @param  int    $user     client id
     * @return string           PDF path
     */
    public function getUserPDF($pdfType, $thnkUser, $user)
    {

		return $this->getClient()->getPDFPath($pdfType);
    }

    /**
     * Get mail data for coach invites
     * @param  string $inviteType  type indicates template
     * @param  int    $thnkUser    1 indicates THNK programs
     * @param  int    $user        client id
     * @param  object $participant existing user, 0 for new user
     * @return array               mail send info
     */
    public function getMailInvite($inviteType, $thnkUser, $user, $participant)
    {
        if( count( (array)$participant) != 0 ){
            switch ($user) {
                case User::IMPACT:
                    switch ($inviteType) {
                        case 'coach':
                           $data = array( 'template' => 'emails.invite_new_coach_storypanda', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'Impact@360Mirror.org', 'subject' => 'Coaching invitation THNK Impact Assessment' );
                            break;
                        case 'reminder':
                           $data = array( 'template' => 'emails.invite_participate_reminder_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'Impact@360Mirror.org', 'subject' => 'No action from respondents in past 2 weeks' );
                            break;
                        case 'respondentDone':
                           $data = array( 'template' => 'emails.respondent_finished_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'Impact@360Mirror.org', 'subject' => 'You received input on your Impact Assessment' );
                            break;
                        case 'allDone':
                           $data = array( 'template' => 'emails.all_finished_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'Impact@360Mirror.org', 'subject' => 'Your Impact Assessment is completed' );
                            break;
                        case 'general':
                           $data = array( 'template' => 'emails.invite_general_storypanda', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'Impact@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Impact Assessment" );
                            break;
                        case 'custom':
                           $data = array( 'template' => 'emails.invite_custom_storypanda', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'Impact@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Impact Assessment" );
                            break;
                        default:
                            $data = array( 'template' => 'emails.invite_new_seat_storypanda', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'Impact@360Mirror.org', 'subject' => 'Invitation THNK Impact Assessment' );
                            break;
                    }
                    return $data;
                case User::SIEMENS:
                    switch ($inviteType) {
                        case 'coach':
                           $data = array( 'template' => 'emails.invite_new_coach_siemens', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coach your participants' );
                            break;
                        case 'reminder':
                           $data = array( 'template' => 'emails.invite_participate_reminder_siemens', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'No action from respondents in past 2 weeks' );
                            break;
                        case 'respondentDone':
                           $data = array( 'template' => 'emails.respondent_finished_siemens', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'You received input on your 360 Mirror' );
                            break;
                        case 'allDone':
                           $data = array( 'template' => 'emails.all_finished_siemens', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Your 360 Mirror is completed' );
                            break;
                        case 'general':
                           $data = array( 'template' => 'emails.invite_general_siemens', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Leadership skills" );
                            break;
                        case 'custom':
                           $data = array( 'template' => 'emails.invite_custom_siemens', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Leadership skills" );
                            break;
                        default:
                            $data = array( 'template' => 'emails.invite_new_seat_siemens', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Create your 360 Mirror' );
                            break;
                    }
                    return $data;
                case User::OLX2018:
                case User::OLX20182:
                case User::OLX:
                    switch ($inviteType) {
                        case 'coach':
                           $data = array( 'template' => 'emails.invite_new_coach_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coach your participants' );
                            break;
                        case 'reminder':
                           $data = array( 'template' => 'emails.invite_participate_reminder_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'No action from respondents in past 2 weeks' );
                            break;
                        case 'respondentDone':
                           $data = array( 'template' => 'emails.respondent_finished_olx', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'You received input on your 360 Mirror' );
                            break;
                        case 'allDone':
                           $data = array( 'template' => 'emails.all_finished_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Your 360 Mirror is completed' );
                            break;
                        case 'general':
                           $data = array( 'template' => 'emails.invite_general_olx', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => "Please review OLX LAP " . $participant->firstname . " " . $participant->lastname . "'s Leadership skills" );
                            break;
                        case 'custom':
                           $data = array( 'template' => 'emails.invite_custom_olx', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => "Please review OLX LAP " . $participant->firstname . " " . $participant->lastname . "'s Leadership skills" );
                            break;
                        default:
                            $data = array( 'template' => 'emails.invite_new_seat_olx', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Create your 360 Mirror' );
                            break;
                    }
                    return $data;
                case User::LEADERSBRAZIL:
                    switch ($inviteType) {
                        case 'coach':
                           $data = array( 'template' => 'emails.invite_new_coach_thnkv3', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coach your participants' );
                            break;
                        case 'reminder':
                           $data = array( 'template' => 'emails.invite_participate_reminder_thnkv3', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'No action from respondents in past 2 weeks' );
                            break;
                        case 'respondentDone':
                           $data = array( 'template' => 'emails.respondent_finished_leadersinbrazil', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Feedback sobre sua avaliação 360 Mirror' );
                            break;
                        case 'allDone':
                           $data = array( 'template' => 'emails.all_finished_leadersinbrazil', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Seu relatório 360 MIRROR foi concluído' );
                            break;
                        case 'general':
                           $data = array( 'template' => 'emails.invite_general_leadersinbrazil', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => "Convite para dar feedback na Pesquisa de Liderança 360" );
                            break;
                        case 'custom':
                           $data = array( 'template' => 'emails.invite_custom_leadersinbrazil', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => "Convite para dar feedback na Pesquisa de Liderança 360" );
                            break;
                        default:
                            $data = array( 'template' => 'emails.invite_new_seat_leadersinbrazil', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Convite para concluir sua pesquisa de liderança 360' );
                            break;
                    }
                    return $data;
                case User::DSIL:
                case User::NRC:
                case User::NRC2018:
                case User::NRC20182:
                case User::THNK_GUEST:
                case User::SIEMENSHEALTH:
                case User::QUEST_MIRROR:
                    switch ($inviteType) {
                        case 'coach':
                           $data = array( 'template' => 'emails.invite_new_coach_thnkv3', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'QuestMirror@360Mirror.org', 'subject' => 'Coach your participants' );
                            break;
                        case 'reminder':
                           $data = array( 'template' => 'emails.invite_participate_reminder_thnkv3', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'QuestMirror@360Mirror.org', 'subject' => 'No action from respondents in past 2 weeks' );
                            break;
                        case 'respondentDone':
                           $data = array( 'template' => 'emails.respondent_finished_thnkv3', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'QuestMirror@360Mirror.org', 'subject' => 'You received input on your Quest Mirror' );
                            break;
                        case 'allDone':
                           $data = array( 'template' => 'emails.all_finished_thnkv3', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'QuestMirror@360Mirror.org', 'subject' => 'Your Quest Mirror is completed' );
                            break;
                        case 'general':
                           $data = array( 'template' => 'emails.invite_general_thnkv3', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'QuestMirror@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Leadership skills" );
                            break;
                        case 'custom':
                           $data = array( 'template' => 'emails.invite_custom_thnkv3', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'QuestMirror@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Leadership skills" );
                            break;
                        default:
                            $data = array( 'template' => 'emails.invite_new_seat_thnkv3', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'QuestMirror@360Mirror.org', 'subject' => 'Create your Quest Mirror' );
                            break;
                    }
                    return $data;
                case User::ENDEAVOR_SCAN:
                    switch ($inviteType) {
                        case 'coach':
                           $data = array( 'template' => 'emails.invite_new_coach_thnkv3_endeavor', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'EndeavorScan@360Mirror.org', 'subject' => 'Coach your participants' );
                            break;
                        case 'reminder':
                           $data = array( 'template' => 'emails.invite_participate_reminder_thnkv3_endeavor', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'EndeavorScan@360Mirror.org', 'subject' => 'No action from respondents in past 2 weeks' );
                            break;
                        case 'respondentDone':
                           $data = array( 'template' => 'emails.respondent_finished_thnkv3_endeavor', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'EndeavorScan@360Mirror.org', 'subject' => 'You received input on your Endeavor Scan' );
                            break;
                        case 'allDone':
                           $data = array( 'template' => 'emails.all_finished_thnkv3_endeavor', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'EndeavorScan@360Mirror.org', 'subject' => 'Your Endeavor Scan is completed' );
                            break;
                        case 'general':
                           $data = array( 'template' => 'emails.invite_general_thnkv3_endeavor', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'EndeavorScan@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Endeavor Project" );
                            break;
                        case 'custom':
                           $data = array( 'template' => 'emails.invite_custom_thnkv3_endeavor', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'EndeavorScan@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Endeavor Project" );
                            break;
                        default:
                            $data = array( 'template' => 'emails.invite_new_seat_thnkv3_endeavor', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'EndeavorScan@360Mirror.org', 'subject' => 'Create your Endeavor Scan' );
                            break;
                    }
                    return $data;
                case User::RUNWAY_MIRROR:
                    switch ($inviteType) {
                        case 'coach':
                           $data = array( 'template' => 'emails.invite_new_coach_runway_mirror', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'ScaleUpNation@360Mirror.org', 'subject' => 'Coach your participants' );
                            break;
                        case 'reminder':
                           $data = array( 'template' => 'emails.invite_participate_reminder_runway_mirror', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'ScaleUpNation@360Mirror.org', 'subject' => 'No action from respondents in past 2 weeks' );
                            break;
                        case 'respondentDone':
                           $data = array( 'template' => 'emails.respondent_finished_runway_mirror', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'ScaleUpNation@360Mirror.org', 'subject' => 'You received input on your ScaleUpNation Mirror' );
                            break;
                        case 'allDone':
                           $data = array( 'template' => 'emails.all_finished_runway_mirror', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'ScaleUpNation@360Mirror.org', 'subject' => 'Your ScaleUpNation Mirror is completed' );
                            break;
                        case 'general':
                           $data = array( 'template' => 'emails.invite_general_runway_mirror', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'ScaleUpNation@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s ScaleUpNation Mirror" );
                            break;
                        case 'custom':
                           $data = array( 'template' => 'emails.invite_custom_runway_mirror', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'ScaleUpNation@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s ScaleUpNation Mirror" );
                            break;
                        default:
                            $data = array( 'template' => 'emails.invite_new_seat_runway_mirror', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => 'ScaleUpNation@360Mirror.org', 'subject' => 'Create your ScaleUpNation Mirror' );
                            break;
                    }
                    return $data;
                case User::SCHUSTERMAN:
                case User::BAYER:
                default:
                    switch ($inviteType) {
                        case 'coach':
                            if($thnkUser == 1){
                                $data = array( 'template' => 'emails.invite_new_coach_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coach your participants' );
                            }else{
                                $data = array( 'template' => 'emails.invite_new_coach', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => trans('userModel.getMailInvite.coach') );
                            }
                            break;
                        case 'reminder':
                            if($thnkUser == 1){
                                $data = array( 'template' => 'emails.invite_participate_reminder_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'No action from respondents in past 2 weeks' );
                            }else{
                                $data = array( 'template' => 'emails.invite_participate_reminder', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => trans('userModel.getMailInvite.reminder') );
                            }
                            break;
                        case 'respondentDone':
                            if($thnkUser == 1){
                                $data = array( 'template' => 'emails.respondent_finished_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'You received input on your 360 Mirror' );
                            }else{
                                $data = array( 'template' => 'emails.respondent_finished', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => trans('userModel.getMailInvite.respondentDone') );
                            }
                            break;
                        case 'allDone':
                            if($thnkUser == 1){
                                $data = array( 'template' => 'emails.all_finished_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Your 360 Mirror is completed' );
                            }else{
                                $data = array( 'template' => 'emails.all_finished', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => trans('userModel.getMailInvite.allDone') );
                            }
                            break;
                        case 'general':
                            if($thnkUser == 1){
                                $data = array( 'template' => 'emails.invite_general_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Leadership skills" );
                            }else{
                                $data = array( 'template' => 'emails.invite_general', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => trans('userModel.getMailInvite.general.0') . " " . $participant->firstname . " " . $participant->lastname . " " . trans('userModel.getMailInvite.general.1') );
                            }
                            break;
                        case 'custom':
                            if($thnkUser == 1){
                                $data = array( 'template' => 'emails.invite_custom_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => "Please review " . $participant->firstname . " " . $participant->lastname . "'s Leadership skills" );
                            }else{
                                $data = array( 'template' => 'emails.invite_custom', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => trans('userModel.getMailInvite.general.0') . " " . $participant->firstname . " " . $participant->lastname . " " . trans('userModel.getMailInvite.general.1') );
                            }
                            break;
                        default:
                            if($thnkUser == 1){
                                $data = array( 'template' => 'emails.invite_new_seat_thnk', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => 'Create your 360 Mirror' );
                            }else{
                                $data = array( 'template' => 'emails.invite_new_seat', 'email' => $participant->email, 'firstname' => $participant->firstname, 'from' => '360Mirror@360Mirror.org', 'subject' => trans('userModel.getMailInvite.default') );
                            }
                            break;
                    }
                    return $data;
            }
        }else{
            switch ($user) {
                case User::IMPACT:
                    if($inviteType == 'coach'){
                        $data = array( 'template' => 'emails.invite_new_coach_storypanda', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coaching invitation THNK Impact Assessment' );
                    }else{
                        $data = array( 'template' => 'emails.invite_new_seat_storypanda', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Invitation THNK Impact Assessment' );
                    }
                    return $data;
                case User::SIEMENS:
                    if($inviteType == 'coach'){
                        $data = array( 'template' => 'emails.invite_new_coach_siemens', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coach your participants' );
                    }else{
                        $data = array( 'template' => 'emails.invite_new_seat_siemens', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Create your 360 Mirror' );
                    }
                    return $data;
                case User::OLX2018:
                case User::OLX20182:
                case User::OLX:
                    if($inviteType == 'coach'){
                        $data = array( 'template' => 'emails.invite_new_coach_thnkv3', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coach your participants' );
                    }else{
                        $data = array( 'template' => 'emails.invite_new_seat_olx', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Create your 360 Mirror' );
                    }
                    return $data;
                case User::LEADERSBRAZIL:
                    if($inviteType == 'coach'){
                        $data = array( 'template' => 'emails.invite_new_coach_thnkv3', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coach your participants' );
                    }else{
                        $data = array( 'template' => 'emails.invite_new_seat_leadersinbrazil', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Convite para concluir sua pesquisa de liderança 360' );
                    }
                    return $data;
                case User::DSIL:
                case User::NRC:
                case User::NRC2018:
                case User::NRC20182:
                case User::THNK_GUEST:
                case User::SIEMENSHEALTH:
                case User::QUEST_MIRROR:
                    if($inviteType == 'coach'){
                        $data = array( 'template' => 'emails.invite_new_coach_thnkv3', 'from' => 'QuestMirror@360Mirror.org', 'subject' => 'Coach your participants' );
                    }else{
                        $data = array( 'template' => 'emails.invite_new_seat_thnkv3', 'from' => 'QuestMirror@360Mirror.org', 'subject' => 'Create your Quest Mirror' );
                    }
                    return $data;
                case User::ENDEAVOR_SCAN:
                    if($inviteType == 'coach'){
                        $data = array( 'template' => 'emails.invite_new_coach_thnkv3_endeavor', 'from' => 'EndeavorScan@360Mirror.org', 'subject' => 'Coach your participants' );
                    }else{
                        $data = array( 'template' => 'emails.invite_new_seat_thnkv3_endeavor', 'from' => 'EndeavorScan@360Mirror.org', 'subject' => 'Create your Endeavor Scan' );
                    }
                    return $data;
                case User::RUNWAY_MIRROR:
                    if($inviteType == 'coach'){
                        $data = array( 'template' => 'emails.invite_new_coach_runway_mirror', 'from' => 'ScaleUpNation@360Mirror.org', 'subject' => 'Coach your participants' );
                    }else{
                        $data = array( 'template' => 'emails.invite_new_seat_runway_mirror', 'from' => 'ScaleUpNation@360Mirror.org', 'subject' => 'Create your ScaleUpNation Mirror' );
                    }
                    return $data;
                default:
                    if($inviteType == 'coach'){
                        if($thnkUser == 1){
                            $data = array( 'template' => 'emails.invite_new_coach_thnk', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coach your participants' );
                        }else{
                            $data = array( 'template' => 'emails.invite_new_coach', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Coach your participants' );
                        }
                    }else{
                        if($thnkUser == 1){
                            $data = array( 'template' => 'emails.invite_new_seat_thnk', 'from' => '360Mirror@360Mirror.org', 'subject' => 'Create your 360 Mirror' );
                        }else{
                            $data = array( 'template' => 'emails.invite_new_seat', 'from' => '360Mirror@360Mirror.org', 'subject' => trans('userModel.getMailInvite.default') );
                        }
                    }
                    return $data;
            }
        }
    }

    /**
     * Get Test intro path for registered users
     * @param  int    $user     client id
     * @return string           template path
     */
    public function getTestIntro($user)
    {
        switch ($user) {
            case User::IMPACT:
                $path = 'forms/test/intro_storypanda';
                return $path;
            case User::BAYER:
                $path = 'forms/test/intro_thnk';
                return $path;
            case User::OLX2018:
            case User::OLX20182:
            case User::OLX:
                $path = 'forms/test/intro_olx';
                return $path;
            case User::SIEMENS:
                $path = 'forms/test/intro_siemens';
                return $path;
            case User::LEADERSBRAZIL:
                $path = 'forms/test/intro_leadersinbrazil';
                return $path;
            case User::DSIL:            
            case User::NRC:
            case User::NRC2018:
            case User::NRC20182:
            case User::THNK_GUEST:
            case User::SIEMENSHEALTH:
            case User::QUEST_MIRROR:
                $path = 'forms/test/intro_thnk_quest';
                return $path;
            case User::ENDEAVOR_SCAN:
                $path = 'forms/test/intro_thnk_endeavor';
                return $path;
            case User::RUNWAY_MIRROR:
                $path = 'forms/test/intro_runway_mirror';
                return $path;
            case User::GGFLSCORP:
            $path = 'forms/test/intro_ggf-ls-corp-clients';
                return $path;
            default:
                $path = 'forms/test/intro';
                return $path;
        }
    }

    /**
     * Get Test intro path for respondents
     * @param  int    $user     client id
     * @return string           template path
     */
    public function getRespondentIntro($user)
    {
        switch ($user) {
            case User::ENDEAVOR_SCAN:
                $path = 'forms/test/intro_resp_endeavor';
                return $path;
            default:
                $path = 'false';
                return $path;
        }
    }

    /**
     * Get Test steps path
     * @param  int    $user     client id
     * @return string           template path
     */
    public function getTestSteps($user)
    {
        switch ($user) {
            case User::IMPACT:
                $path = 'forms/test/step_form_storypanda';
                return $path;
            default:
                $path = 'forms/test/step_form';
                return $path;
        }
    }

    /**
     * Get minimum required respondents
     * @param  int    $user     client id
     * @return int              number of respondents needed
     */
    public function getMinReq($user)
    {
        switch ($user) {
            case User::IMPACT:
                $minReq = 2;
                return $minReq;
            case User::NRC20182:
            case User::ENDEAVOR_SCAN:
                $minReq = 4;
                return $minReq;
            case User::LEADERSBRAZIL:
            case User::OLX2018:
            case User::OLX20182:
            case User::OLX:
                 $minReq = 5;
                return $minReq;
            case User::SIEMENS:
                 $minReq = 7;
                return $minReq;
            case User::SIEMENSHEALTH:
                 $minReq = 3;
                return $minReq;
            default:
                $minReq = 6;
                return $minReq;
        }
    }

    /**
     * Get respondents template path
     * @param  int    $thnkUser 1 indicates THNK programs
     * @param  int    $user     client id
     * @return string           template path
     */
    public function getRespTemplate($thnkUser, $user)
    {
        switch ($user) {
            case User::IMPACT:
                $path = 'forms/test/respondent_form_storypanda';
                return $path;
            case User::LEADERSBRAZIL:
            case User::OLX2018:
            case User::OLX20182:
            case User::OLX:
            case User::SIEMENS:
                $path = 'forms/test/respondent_form_thnk';
                return $path;
            default:
                if($thnkUser == 1){
                    $path = 'forms/test/respondent_form_thnk';
                }else{
                    $path = 'forms/test/respondent_form';
                }
                return $path;
        }
    }

    /**
     * Get respondents feedback url
     * @param  int    $thnkUser 1 indicates THNK programs
     * @param  int    $user     client id
     * @return string           final url
     */
    public function getRespFeedback($thnkUser, $user)
    {
        switch ($user) {
            case User::IMPACT:
                $path = '/feedback/thnk';
                return $path;
            default:
                if($thnkUser == 1){
                    $path = '/feedback/thnk';
                }else{
                    $path = '/feedback/thanks';
                }
                return $path;
        }
    }

    /**
     * Get logout url
     * @param  int    $thnkUser 1 indicates THNK programs
     * @param  int    $user     client id
     * @return string           final url
     */
    public function getLogOutURL($thnkUser, $user)
    {
        switch ($user) {
            case User::IMPACT:
                $path = '/impact';
                return $path;
            case User::ENDEAVOR_SCAN:
                $path = '/endeavorscan';
                return $path;
            case User::RUNWAY_MIRROR:
                $path = '/scaleupnation';
                return $path;
            default:
                if($thnkUser == 1){
                    $path = '/questmirror';
                }else{
                    $path = '/';
                }
                return $path;
        }
    }

    /**
     * Get introduction page on the PDF
     * @param  int $user        client id
     * @return boolean          true for intro
     */
    public function getIntroPDF($user)
    {
        switch ($user) {
            case User::IMPACT:
                $intro = false;
                return $intro;
            default:
                $intro = true;
                return $intro;
        }
    }

    /**
     * Get back page on the PDF (WHAT’S NEXT)
     * @param  int $user        client id
     * @return boolean          true for back page
     */
    public function getBackPDF($user)
    {
        switch ($user) {
            case User::IMPACT:
            case User::SIEMENS:
            case User::LEADERSBRAZIL:
                $back = false;
                return $back;
            default:
                $back = true;
                return $back;
        }
    }
    /**
     * Get last page on the PDF (YOUR REFLECTION)
     * @param  int $user        client id
     * @return boolean          true for last page
     */
    public function getLastPDF($user)
    {
        switch ($user) {
            case User::LEADERSBRAZIL:
                $last = false;
                return $last;
            default:
                $last = true;
                return $last;
        }
    }

    /**
     * Get back page on the PDF for the Endeavor Scan
     * @param  int $user        client id
     * @return boolean          true for back page
     */
    public function getBackPDFEndeavor($user)
    {
        switch ($user) {
            case User::ENDEAVOR_SCAN:
                $back = true;
                return $back;
            default:
                $back = false;
                return $back;
        }
    }

    /**
     * Get custom email text
     * @param  int    $thnkUser    1 indicates THNK programs
     * @param  int    $user        client id
     * @param  object $participant existing user
     * @return string              custom email text
     */
    public function getCustomText($thnkUser, $user, $participant)
    {
        switch ($user) {
            case User::IMPACT:
                $text = "<textarea class ='txt_o' name='custom_email[]'>Dear,\n\nAs someone who has some knowledge of my Impact Project, I’m hoping you can fill out a brief assessment. This will help me see where the strengths and weakness of my project are and where I need support and resources.\n\nPlease click the link below to send me your feedback. Answering these questions will take approximately 15 minutes, and the answers will be sent back to me anonymously. I appreciate your help!\n\nThank you!\n".$participant->firstname ."  ". $participant->lastname ."</textarea>";
                return $text;
            case User::ENDEAVOR_SCAN:
                $text = "<textarea style='height: 210px !important;' class ='txt_o' name='custom_email[]'>Dear,\n\nI will be participating in the THNK Executive Leadership Program (www.thnk.org).\nIn this program, I will work on my Endeavor project, a project I am passionate about to scale-up successfully in the real world.\nIf you are not familiarized yet with my Endeavor project, this is a brief description of the project:\n\n[PLEASE EXPLAIN YOUR ENDEAVOR PROJECT HERE]\n\nFor this purpose I would like to ask you to give feedback on my strong points and development areas.\nPlease click the link below to access the assessment tool.\nAnswering the questions will take approximately 30 minutes.\nThe answers will be sent back to me anonymously.\nI really appreciate your help.\n\nBest regards,\n".$participant->firstname ." ". $participant->lastname ."</textarea>";
                return $text;
            case User::RUNWAY_MIRROR:
                $text = "<textarea style='height: 210px !important;' class ='txt_o' name='custom_email[]'>Dear,\n\nI will be participating in the ScaleUpNation Program (www.scaleupnation.com).\nFor this purpose I would like to ask you to give feedback on my strengths and weaknesses.\nPlease click the link below to access the assessment tool.\nAnswering the questions will take approximately 30 minutes.\nThe answers will be sent back to me anonymously.\nI really appreciate your help.\n\nBest regards,\n".$participant->firstname ." ". $participant->lastname ."</textarea>";
                return $text;
            case User::SIEMENS:
                $text = "<textarea style='height: 210px !important;' class ='txt_o' name='custom_email[]'>Dear,\n\nI value your input and would like to ask you for help. I will be participating in the Siemens PS Impactivist Program. As part of the pre-work, I am reflecting on my leadership skills using an online tool called the MIRROR.\n\nThis includes 360º feedback from my manager, peers, people I work with, and have worked for. This feedback is important since it will help me identify my learning goals for the program. The MIRROR helps me to gain insight into my leadership skills, strengths, and weaknesses.\n\nPlease click the link below to send me your feedback. Answering these questions will take approximately 30 minutes, and the answers will be sent back to me anonymously. I appreciate your help!\n\nThank you,\n\n".$participant->firstname ." ". $participant->lastname ."</textarea>";
                return $text;
            case User::LEADERSBRAZIL:
                $text = "<textarea style='height: 210px !important;' class ='txt_o' name='custom_email[]'>Caro,\n\nComo você deve saber, eu estou participando do Programa Terceiro Setor Transforma, um programa destinado a ampliar minha capacidade de liderança frente à minha ONG.\n\nEu valorizo sua opinião e gostaria de pedi-lo ajuda. Como parte do programa, estou refletindo sobre minhas habilidades de liderança usando uma ferramenta online chamada 360 MIRROR.\n\nIsso inclui comentários 360º de colegas, minha equipe, pessoas com quem trabalho e para quem trabalhei. Seu feedback é importante, pois ajudará a identificar minhas habilidades, pontos fortes e pontos de liderança a melhorar.\n\nPor favor, clique no link abaixo para me enviar seu feedback. Responder a estas cinco perguntas levará aproximadamente 20 minutos e suas respostas serão enviadas de volta para mim anonimamente. Eu aprecio muito a sua ajuda!\n\nObrigado,\n\n".$participant->firstname ." ". $participant->lastname ."</textarea>";
                return $text;
            case User::BAYER:
            case User::DSIL:
            case User::OLX2018:
            case User::OLX20182:
            case User::OLX:
            case User::NRC:
            case User::NRC2018:
            case User::NRC20182:
            case User::SIEMENSHEALTH:
                $text = "<textarea class ='txt_o' name='custom_email[]'>". trans('userModel.getCustomText.defaultNoELPTextExample') .$participant->firstname ." ". $participant->lastname ."</textarea>";
                return $text;
            default:
                if($thnkUser == 1){
                    $text = "<textarea class ='txt_o' name='custom_email[]'>Dear,\n\nI will be participating in the THNK Executive Leadership Program (www.thnk.org).\nFor this purpose I would like to ask you to give feedback on my strengths and weaknesses.\nPlease click the link below to access the assessment tool.\nAnswering the questions will take approximately 30 minutes.\nThe answers will be sent back to me anonymously.\nI really appreciate your help.\n\nBest regards,\n".$participant->firstname ." ". $participant->lastname ."</textarea>";
                }else{
                    $text = "<textarea class ='txt_o' name='custom_email[]'>". trans('userModel.getCustomText.default') ."\n".$participant->firstname ." ". $participant->lastname ."</textarea>";
                }
                return $text;
        }
    }

    /**
     * Get box invite text
     * @param  int    $user        client id
     * @return string              box invite text
     */
    public function getBoxInvite($user)
    {
        switch ($user) {
            case User::IMPACT:
                $text = "
                    <p>
                        Congratulations you are entering the second part of your Impact Assessment. In this step, you should invite at least two people you work with or have worked with in the past to provide feedback on your impact venture project. This is a crucial part of your learning and development process where you will receive comprehensive and unfiltered input.
                    </p>

                    <h2>What to expect?</h2>
                    <p>
                        Your respondents will be asked to reflect on your entrepreneurial skills by answering the same questions you just answered before. Your respondents will also be given the opportunity to add additional comments. This will be included in your final report.
                    </p>

                    <p>Respondents should ideally include at least:</p>

                    <ul id='respondents_list'>
                        <li style='width:100%;'>1 co-worker or someone familiar with you and your project</li>
                        <li>1 mentor</li>
                    </ul>";
                return $text;
            case User::ENDEAVOR_SCAN:
                $text = "
                    <p>
                        Congratulations you are entering the second part of your Endeavor Scan. In this step, you can invite a few other people you work with to provide feedback on your Endeavor project. This will provide you with other perspectives to inform and help shape your project.
                    </p>

                    <h2>What to expect?</h2>
                    <p>
                        Your respondents will be asked to reflect on your project's characteristics by answering the same questions you just answered before. Your respondents will also be given the opportunity to add additional comments. This will be included in your final report.
                    </p>

                    <p>Respondents should ideally include at least:</p>

                    <ul id='respondents_list'>
                        <li>2 co-workers</li>
                        <li>2 peers who's opinion you value</li>
                    </ul>

                    <p>If they are not yet familiar with your Endevaor project, you can send them a brief description of your project in your email (or call them up and explain to them over the phone).</p>";
                return $text;
            case User::RUNWAY_MIRROR:
                $text = "
                    <p>
                        Congratulations you are entering the second part of your ScaleUpNation Mirror, known as 360º feedback. In this step, you should invite at least 6 people you work with or have worked with in the past to provide feedback on your leadership skills. This is a crucial part of your learning and development process where you will receive comprehensive and unfiltered input.
                    </p>

                    <h2>What to expect?</h2>
                    <p>
                        Your respondents will be asked to reflect on your leadership skills by answering the same questions you just answered before. Your respondents will also be given the opportunity to add additional comments. This will be included in your final report.
                    </p>

                    <p>Respondents should ideally include at least:</p>

                    <ul id='respondents_list'>
                        <li>2 superiors</li>
                        <li>2 peers</li>
                        <li>2 subordinates</li>
                    </ul>";
                return $text;
            case User::LEADERSBRAZIL:
                $text = "
                    <p>Parabéns, você está entrando na segunda parte do seu 360 MIRROR, conhecido como feedback 360º.
                    <br/><br/>
                    Nesta etapa, convide pelo menos cinco pessoas com quem você trabalha ou com quem trabalhou no passado para fornecer feedback sobre sua liderança. Essa é uma parte crucial de seu processo de aprendizado e desenvolvimento, no qual você receberá informações abrangentes e sem filtros.
                    <br/><br/>
                    Pedimos que coloque os nomes dos seus 5 entrevistados abaixo. Eles receberão um e-mail com um link para o 360 MIRROR. Você é livre para adicionar mais pessoas, pois isso ampliará as informações que você obterá com o feedback.
                    </p>

                    <h2>O QUE ESPERAR?</h2>
                    <p>Os entrevistados serão convidados a refletir sobre suas habilidades de liderança respondendo às mesmas perguntas que você respondeu anteriormente. Os feedbacks deles serão incluídos no seu relatório final.
                    <br/><br/>
                    Comece agora escolhendo o tipo de e-mail que você deseja enviar e adicionando os endereços de e-mail.
                    </p>";
                return $text;
            case User::OLX2018:
            case User::OLX20182:
            case User::OLX:
                $text = "
                    <p>".trans('userModel.getBoxInvite.intro5')."</p>

                    <h2>".trans('userModel.getBoxInvite.title')."</h2>
                    <p>".trans('userModel.getBoxInvite.instructions')."</p>

                    <p>".trans('userModel.getBoxInvite.minimumRespondents.title')."</p>

                    <ul id='respondents_list'>
                        <li>1 manager</li>
                        <li>".trans('userModel.getBoxInvite.minimumRespondents.peers')."</li>
                        <li>".trans('userModel.getBoxInvite.minimumRespondents.subordinates')."</li>
                    </ul>";
                return $text;
            case User::SIEMENS:
                $text = "
                    <p>".trans('userModel.getBoxInvite.intro7')."</p>

                    <h2>".trans('userModel.getBoxInvite.title')."</h2>
                    <p>".trans('userModel.getBoxInvite.instructions')."</p>

                    <p>".trans('userModel.getBoxInvite.minimumRespondents.title')."</p>

                    <ul id='respondents_list'>
                        <li>1 ".trans('userModel.getBoxInvite.minimumRespondents.one_manager')."</li>
                        <li>3 ".trans('userModel.getBoxInvite.minimumRespondents.num_peers')."</li>
                        <li>3 ".trans('userModel.getBoxInvite.minimumRespondents.num_teamMembers')."</li>
                    </ul>";
                return $text;
            case User::NRC:
            case User::NRC2018:
            case User::NRC20182:
                $text = "
                    <p>Congratulations you are entering the second part of your 360 Mirror, known as 360º feedback.<br/><br/>In this step, you should invite at least 4 people you work with or have worked with in the past to provide feedback on your leadership skills. This is a crucial part of your learning and development process where you will receive comprehensive and unfiltered input.</p>

                    <h2>".trans('userModel.getBoxInvite.title')."</h2>
                    <p>".trans('userModel.getBoxInvite.instructions')."</p>

                    <p>".trans('userModel.getBoxInvite.minimumRespondents.title')."</p>

                    <ul id='respondents_list'>
                        <li>4 respondents</li>
                    </ul>";
                return $text;
            default:
                $text = "
                    <p>".trans('userModel.getBoxInvite.intro')."</p>

                    <h2>".trans('userModel.getBoxInvite.title')."</h2>
                    <p>".trans('userModel.getBoxInvite.instructions')."</p>

                    <p>".trans('userModel.getBoxInvite.minimumRespondents.title')."</p>

                    <ul id='respondents_list'>
                        <li>".trans('userModel.getBoxInvite.minimumRespondents.superiors')."</li>
                        <li>".trans('userModel.getBoxInvite.minimumRespondents.peers')."</li>
                        <li>".trans('userModel.getBoxInvite.minimumRespondents.subordinates')."</li>
                    </ul>";
                return $text;
        }
    }

    /**
     * Get custom invite text template
     * @param  int    $thnkUser    1 indicates THNK programs
     * @param  int    $user     client id
     * @return string           template path
     */
    public function getInviteTextTemplate($thnkUser, $user)
    {
        switch ($user) {
            case User::IMPACT:
                $path = 'test/view_text_storypanda';
                return $path;
            case User::SIEMENS:
                $path = 'test/view_text_siemens';
                return $path;
            case User::OLX:
            case User::OLX2018:
            case User::OLX20182:
                $path = 'test/view_text_olx';
                return $path;
            case User::SIEMENSHEALTH:
                $path = 'test/view_text_thnk';
                return $path;
            case User::LEADERSBRAZIL:
                $path = 'test/view_text_leadersinbrazil';
                return $path;
            default:
                if($thnkUser == 1){
                    $path = 'test/view_text_thnk';
                }else{
                    $path = 'test/view_text';
                }

                return $path;
        }
    }

    /**
     * Get default invite email text
     * @param  int $user        client id
     * @return boolean          true for default invite email text
     */
    public function getDefEmailText($user)
    {
        switch ($user) {
            case User::ENDEAVOR_SCAN:
                $back = false;
                return $back;
            default:
                $back = true;
                return $back;
        }
    }

    /**
     * Get respondent invite boxes
     * @param  object $client           client user
     * @param  int    $thnkUser         1 indicates THNK programs
     * @param  int    $user             client id
     * @param  object $participant      existing user
     */
    public function getInviteBoxes($client, $thnkUser, $user, $participant)
    {
        $customText = $client->getCustomText($thnkUser ,$user, $participant);

        switch ($user) {
            case User::IMPACT:
                for ($i = 0; $i <2; $i++) {
                    if ($i == 0) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::COWORKER);
                    } else {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::MENTOR);
                    }
                    echo "
                        <li id='list" . $i . "'>
                            <h2>" . $respondentgroup->name . "*</h2>
                            <p>
                                <label>NAME</label>
                                <input type='text' value='' class='field " . $respondentgroup->name . "' name='name[]'/>
                            </p>
                            <p class='second'>
                                <label>Email Address</label>
                                <input type='email' value='' class='field email ".$respondentgroup->name."' name='email[]'/>
                                <input type='hidden' value='" . $respondentgroup->id . "' class='' name='respondentgroup[]'/>
                            </p>
                            <p class='second text_email_row'>
                                <label>Custom email text</label>
                                ".$customText."
                                <li class='last' id='listItem".$respondentgroup->id."'>
                            </p>
                        </li>
                        <li class='last' id='listItem".$respondentgroup->id."'>
                            <p>
                                <input type='submit' class='button extra_respondent' group_id = '". $respondentgroup->id ."' value='+ ". $respondentgroup->name ."' />
                            </p>
                        </li>";
                }
                break;
            case User::ENDEAVOR_SCAN:
                for ($i = 0; $i <4; $i++) {
                    // on default, we load 2,2 groups.
                    if ($i == 0 || $i == 1) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::COWORKER);
                    } else {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::PEER);
                    }
                    echo "
                        <li id='list" . $i . "'>
                            <h2>" . $respondentgroup->name . "</h2>
                            <p>
                                <label>NAME</label>
                                <input type='text' value='' class='field " . $respondentgroup->name . "' name='name[]'/>
                            </p>
                            <p class='second'>
                                <label>Email Address</label>
                                <input type='email' value='' class='field email ".$respondentgroup->name."' name='email[]'/>
                                <input type='hidden' value='" . $respondentgroup->id . "' class='' name='respondentgroup[]'/>
                            </p>
                            <p class='second text_email_row'>
                                <label>Custom email text</label>
                                ".$customText."                                
                            </p>
                        </li>";
                    if ($i % 2 == 1) {
                        echo "
                            <li class='last' id='listItem".$respondentgroup->id."'>
                                <p>
                                    <input type='submit' class='button extra_respondent' group_id = '".$respondentgroup->id."' value='+ ".$respondentgroup->name."' />
                                </p>
                            </li>";
                    }
                }
                break;
            case User::OLX2018:
            case User::OLX20182:
            case User::OLX:
                for ($i = 0; $i <7; $i++) {
                    // on default, we load 1,2,2 groups + others.
                    if ($i == 0 ) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::SUPERIOR);
                        if ($respondentgroup->id == 1){
                        }
                    } elseif ($i == 1 || $i == 2) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::PEER);
                    } elseif ($i == 3 || $i == 4) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::SUBORDINATE);
                    } else {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::OTHERS);
                    }
                    $respName = $respondentgroup->name;
                    if ($respName == "Superior"){
                        $respName = "Manager";
                    }
                    echo "
                        <li id='list" . $i . "'>
                            <h2>". trans('test.respondents.'. $respName) ."*</h2>
                            <p>
                                <label>".trans('userModel.getInviteBoxes.name')."</label>
                                <input type='text' value='' class='field " . $respondentgroup->name . "' name='name[]'/>
                            </p>
                            <p class='second'>
                                <label>".trans('userModel.getInviteBoxes.email')."</label>
                                <input type='email' value='' class='field email ".$respondentgroup->name."' name='email[]'/>
                                <input type='hidden' value='" . $respondentgroup->id . "' class='' name='respondentgroup[]'/>
                            </p>
                            <p class='second text_email_row'>
                                <label>".trans('userModel.getInviteBoxes.customText')."</label>
                                ".$customText."                                
                            </p>
                        </li>";
                    if ($i == 0) {
                        echo "
                            <li class='last' id='listItem".$respondentgroup->id."'>
                                <p>
                                    <input type='submit' class='button extra_respondent' group_id = '".$respondentgroup->id."' value='+ ". trans('test.respondents.'. $respName) ."' />
                                </p>
                            </li>";
                    } else {
                        if ($i % 2 != 1) {
                            echo "
                                <li class='last' id='listItem".$respondentgroup->id."'>
                                    <p>
                                        <input type='submit' class='button extra_respondent' group_id = '".$respondentgroup->id."' value='+ ". trans('test.respondents.'. $respName) ."' />
                                    </p>
                                </li>";
                        }
                    }
                }
                break;
            case User::LEADERSBRAZIL:
                for ($i = 0; $i <6; $i++) {
                    // on default, we load 0,2,2 groups + others.
                    if ($i == 0 || $i == 1) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::PEER);
                    } elseif ($i == 2 || $i == 3) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::SUBORDINATE);
                    } else {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::OTHERS);
                    }
                    $respName = $respondentgroup->name;
                    echo "
                        <li id='list" . $i . "'>
                            <h2>". trans('test.respondents.'. $respName) ."*</h2>
                            <p>
                                <label>".trans('userModel.getInviteBoxes.name')."</label>
                                <input type='text' value='' class='field " . $respondentgroup->name . "' name='name[]'/>
                            </p>
                            <p class='second'>
                                <label>".trans('userModel.getInviteBoxes.email')."</label>
                                <input type='email' value='' class='field email ".$respondentgroup->name."' name='email[]'/>
                                <input type='hidden' value='" . $respondentgroup->id . "' class='' name='respondentgroup[]'/>
                            </p>
                            <p class='second text_email_row'>
                                <label>".trans('userModel.getInviteBoxes.customText')."</label>
                                ".$customText."                                
                            </p>
                        </li>";
                    if ($i % 2 == 1) {
                        echo "
                            <li class='last' id='listItem".$respondentgroup->id."'>
                                <p>
                                    <input type='submit' class='button extra_respondent' group_id = '".$respondentgroup->id."' value='+ ". trans('test.respondents.'. $respName) ."' />
                                </p>
                            </li>";
                    }
                }
                break;
            case User::SIEMENS:
                for ($i = 0; $i <7; $i++) {
                    // on default, we load 1,2,2 groups + others.
                    if ($i == 0 ) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::SUPERIOR);
                        if ($respondentgroup->id == 1){
                        }
                    } elseif ($i == 1 || $i == 2) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::PEER);
                    } elseif ($i == 3 || $i == 4) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::SUBORDINATE);
                    } else {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::OTHERS);
                    }
                    $respName = $respondentgroup->name;
                    if ($respName == "Superior"){
                        $respName = "Manager";
                    }
                    if ($respName == "Subordinate"){
                        $respName = "Team member";
                    }
                    echo "
                        <li id='list" . $i . "'>
                            <h2>". trans('test.respondents.'. $respName) ."*</h2>
                            <p>
                                <label>".trans('userModel.getInviteBoxes.name')."</label>
                                <input type='text' value='' class='field " . $respondentgroup->name . "' name='name[]'/>
                            </p>
                            <p class='second'>
                                <label>".trans('userModel.getInviteBoxes.email')."</label>
                                <input type='email' value='' class='field email ".$respondentgroup->name."' name='email[]'/>
                                <input type='hidden' value='" . $respondentgroup->id . "' class='' name='respondentgroup[]'/>
                            </p>
                            <p class='second text_email_row'>
                                <label>".trans('userModel.getInviteBoxes.customText')."</label>
                                ".$customText."                                
                            </p>
                        </li>";
                    if ($i == 0) {
                        echo "
                            <li class='last' id='listItem".$respondentgroup->id."'>
                                <p>
                                    <input type='submit' class='button extra_respondent' group_id = '".$respondentgroup->id."' value='+ ". trans('test.respondents.'. $respName) ."' />
                                </p>
                            </li>";
                    } else {
                        if ($i % 2 != 1) {
                            echo "
                                <li class='last' id='listItem".$respondentgroup->id."'>
                                    <p>
                                        <input type='submit' class='button extra_respondent' group_id = '".$respondentgroup->id."' value='+ ". trans('test.respondents.'. $respName) ."' />
                                    </p>
                                </li>";
                        }
                    }
                }
                break;
            default:
                for ($i = 0; $i <8; $i++) {
                    // on default, we load 2,2,2 groups + others.
                    if ($i == 0 || $i == 1) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::SUPERIOR);
                    } elseif ($i == 2 || $i == 3) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::PEER);
                    } elseif ($i == 4 || $i == 5) {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::SUBORDINATE);
                    } else {
                        $respondentgroup = Respondentgroup::find(Respondentgroup::OTHERS);
                    }
                    $respName = $respondentgroup->name;
                    echo "
                        <li id='list" . $i . "'>
                            <h2>". trans('test.respondents.'. $respName) ."*</h2>
                            <p>
                                <label>".trans('userModel.getInviteBoxes.name')."</label>
                                <input type='text' value='' class='field " . $respondentgroup->name . "' name='name[]'/>
                            </p>
                            <p class='second'>
                                <label>".trans('userModel.getInviteBoxes.email')."</label>
                                <input type='email' value='' class='field email ".$respondentgroup->name."' name='email[]'/>
                                <input type='hidden' value='" . $respondentgroup->id . "' class='' name='respondentgroup[]'/>
                            </p>
                            <p class='second text_email_row'>
                                <label>".trans('userModel.getInviteBoxes.customText')."</label>
                                ".$customText."                                
                            </p>
                        </li>";
                    if ($i % 2 == 1) {
                        echo "
                            <li class='last' id='listItem".$respondentgroup->id."'>
                                <p>
                                    <input type='submit' class='button extra_respondent' group_id = '".$respondentgroup->id."' value='+ ". trans('test.respondents.'. $respName) ."' />
                                </p>
                            </li>";
                    }
                }
                break;
        }
    }

    /**
     * Get extra message text
     * @param  int    $user        client id
     * @param  object $client      client user
     * @return string              extra message text
     */
    public function getExtraMsgText($user, $client)
    {
        $minReq = $client->getMinReq($user);

        switch ($user) {
            case User::IMPACT:
                $text = "* If you lack co-workers or mentors, you can add more from other category. If you need, you can also include family and friends.<br><br>If it remains impossible to get ".$minReq." respondents, please notify your coach.";
                return $text;
            case User::LEADERSBRAZIL:
            case User::NRC:
            case User::NRC2018:
            case User::NRC20182:
            case User::OLX2018:
            case User::OLX20182:
            case User::OLX:
            case User::SIEMENS:
            case User::SIEMENSHEALTH:
            case User::ENDEAVOR_SCAN:
                $text = "";
                return $text;
            default:
                $text = trans('userModel.getExtraMsgText.alert.0') ." ".$minReq." ". trans('userModel.getExtraMsgText.alert.1');
                return $text;
        }
    }
}