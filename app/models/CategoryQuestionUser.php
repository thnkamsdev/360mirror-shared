<?php

class CategoryQuestionUser extends Eloquent {

    protected $table = 'category_question_user';
 
    
    public function question()
    {
        return $this->belongsTo('Question');
    }
}