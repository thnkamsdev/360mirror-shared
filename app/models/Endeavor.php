<?php

use THNK\Mirror\Localization\LocalizedModel;

class Endeavor extends Eloquent {

    use LocalizedModel;

    public function endeavorType() {
        return $this->belongsTo('EndeavorType');
    }

    public function endeavorStage() {
        return $this->belongsTo('EndeavorStage');
    }

    public static function getEndeavor($endeavor_id) {
        try {
            $endeavor = Endeavor::findOrFail($endeavor_id);
        } catch (Exception $e) {
            $endeavor = new Endeavor;
        }

        return $endeavor;
    }

    public static function getEndeavorByUser($user_id) {
        return self::where('user_id', '=', $user_id)->get();
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->getLocalized('name');
    }

    /**
     * @param string $value
     */
    public function setNameAttribute($value)
    {
        return $this->setLocalized('name', $value);
    }

    /**
     * @return string
     */
    public function getOtherTypeAttribute()
    {
        return $this->getLocalized('other_type');
    }

    /**
     * @param string $value
     */
    public function setOtherTypeAttribute($value)
    {
        return $this->setLocalized('other_type', $value);
    }

    /**
     * @return string
     */
    public function getDescriptionWhatAttribute()
    {
        return $this->getLocalized('descriptionWhat');
    }

    /**
     * @param string $value
     */
    public function setDescriptionWhatAttribute($value)
    {
        return $this->setLocalized('descriptionWhat', $value);
    }

    /**
     * @return string
     */
    public function getDescriptionHowAttribute()
    {
        return $this->getLocalized('descriptionHow');
    }

    /**
     * @param string $value
     */
    public function setDescriptionHowAttribute($value)
    {
        return $this->setLocalized('descriptionHow', $value);
    }

    /**
     * @return string
     */
    public function getDescriptionWhoAttribute()
    {
        return $this->getLocalized('descriptionWho');
    }

    /**
     * @param string $value
     */
    public function setDescriptionWhoAttribute($value)
    {
        return $this->setLocalized('descriptionWho', $value);
    }

    /**
     * @return string
     */
    public function getDescriptionWhyAttribute()
    {
        return $this->getLocalized('descriptionWhy');
    }

    /**
     * @param string $value
     */
    public function setDescriptionWhyAttribute($value)
    {
        return $this->setLocalized('descriptionWhy', $value);
    }

    /**
     * @return string
     */
    public function getDescriptionNextAttribute()
    {
        return $this->getLocalized('descriptionNext');
    }

    /**
     * @param string $value
     */
    public function setDescriptionNextAttribute($value)
    {
        return $this->setLocalized('descriptionNext', $value);
    }

}