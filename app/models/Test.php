<?php

class Test extends Eloquent {

    const TEST_BY_RESPONDER = 1;

    public function categoryQuestionUserTest()
    {
        return $this->hasMany('CategoryQuestionUserTest');
    }

    /**
     * @return bool
     */
    public function isSelfAssessment()
    {
        return $this->test_id == 0;
    }


    public function user() {
        return $this->belongsTo('User');
    }

    public function respondent() {
        return $this->belongsTo('Respondent');
    }

    public function teststatus()
    {
        return $this->belongsTo('Teststatus');
    }

    public function parent()
    {
        return $this->belongsTo('Test', 'test_id');
    }

    /**
     * @return \Test
     */
    public function getRootTest()
    {
        if ($this->isSelfAssessment())
        {
            return $this;
        }

        return $this->parent;
    }

     public static function getTest($test_id) {
        try {
            $test = Test::findOrFail($test_id);
        } catch (Exception $e) {
            $test = new Test;
        }

        return $test;
    }

     public static function getLatestTestByUser($user_id, $client_id) {

        $test = Test::where('user_id', '=' , $user_id)
                            ->where('client_id', '=', $client_id)
                            ->orderBy('created_at', 'desc')
                            ->first();

        if (!isset($test)) {
            $test = new Test;
        }

        return $test;

    }

    //TODO
     public static function getTestsByUser($user_id, $client_id) {

        $tests = Test::where('user_id', '=' , $user_id)
                            ->where('client_id', '=', $client_id)
                            ->orderBy('created_at', 'desc')
                            ->get();

        return $tests;

    }

     public static function getFirstTestsByRespondent($respondent_id) {

        $test = Test::where('respondent_id', '=' , $respondent_id)->first();


        return $test;

    }

    public function get_question_data($id){
        return Question::where('id', '=', $id)->get();
    }


    public function amount_completed() {
        $tests = Test::where('test_id', '=' , $this->id)
                            ->get();

        $amount_completed = 0;
        foreach ($tests as $test) {
            if ($test->teststatus_id == Teststatus::FINISHED_BY_USER_OR_RESPONDENT) {
                $amount_completed++;;
            }

        }
        return $amount_completed;
    }

    public static function getTestAreFinished($test_id) {

        $tests = Test::where('test_id', '=' , $test_id)
            ->get();
        $mainTest = Test::where('id', '=' , $test_id)
            ->get();

        $all_finished = false;
        $completed = 0;

        foreach ($tests as $test) {
            if ($test->teststatus_id == Teststatus::FINISHED_BY_USER_OR_RESPONDENT) {
                $completed ++;
            }
        }

        if ($completed >= $mainTest[0]->minimum_required){
            $all_finished = true;
        }

        return $all_finished;
    }

     public static function getTestAreNotFullyFinished() {

        $tests = Test::where('test_id', '=' , 0)->where('user_id', '<>' , 0)->where('teststatus_id', '<' , Teststatus::TEST_FULLY_FINISHED)->get();

        return $tests;

    }

    public function hex2rgb($hex) {
         $hex = str_replace("#", "", $hex);

         if(strlen($hex) == 3) {
            $r = hexdec(substr($hex,0,1).substr($hex,0,1));
            $g = hexdec(substr($hex,1,1).substr($hex,1,1));
            $b = hexdec(substr($hex,2,1).substr($hex,2,1));
         } else {
            $r = hexdec(substr($hex,0,2));
            $g = hexdec(substr($hex,2,2));
            $b = hexdec(substr($hex,4,2));
         }
         $rgb = array($r, $g, $b);
         //return implode(",", $rgb); // returns the rgb values separated by commas
         return $rgb; // returns an array with the rgb values
        }

    public function get_self_report_array($test_id, $user_id = null) {
        $pdf_array = array();

        $user = Auth::user();

        if (isset($user_id)) {
            $user = User::find($user_id); //finding the participant
        }

        $mainTest = Test::find($test_id);

        //CATEGORIES ON TEST
        $categories = $user->getCategories($mainTest->client_id);

        //ARRAY OF OBJECTS CONTAINING ALL ANSWERS TO QUESTIONS ON TEST (NUMERIC + TEXT)
        $test_question = $user->get_data($test_id);
        $test_question_length = count($test_question);

        $GLOBALS['respLegend'] = array(array('nameResp' => 'mainColor', 'colorBar' => $this->hex2rgb($categories[0]['relations']['pivot']['parent']['colorMain'])));

        //ITERATING CATEGORIES
        foreach ($categories as $category) {

            $id = $category['attributes']['id'];
            $id_questions = [];

            //ARRAY OF OBJECTS CONTAINING NUMERIC ANSWER TO QUESTIONS ON TEST FOR CURRENT CATEGORY
            $quest = $this->take_categories_questions($mainTest->client_id,$id);

            //ARRAY OF QUESTIONS IDs
            foreach($quest as $question_take){
                array_push($id_questions, $question_take->question_id);
            }

            $value = 0;
            $average = 0;
            $tot_quest = [];
               
            //ITERATING RESULTS
            for( $x = 0; $x < $test_question_length; $x++ ) {

                //NUMERIC ANSWER:
                    //MATCHING QUESTION BELONGING TO CURRENT CATEGORY
                    if($this->checkId($id_questions, $test_question[$x]->question_id)) {
                        
                        //NN COUNT AS 0
                        if($test_question[$x]->value == 9 || $test_question[$x]->value == 0) {
                            $test_question[$x]->value = 0;
                        } else {
                           $average++;
                        }

                        $value = $value + $test_question[$x]->value;

                        //GETTING QUESTION INFO (TITLE, DESC.)
                        $question = $this->get_question_data($test_question[$x]->question_id);

                        $cat_quest = array( "name"          =>  $question[0]->name,
                                        "explanation"       =>  $question[0]->description,
                                        "personal_score"    =>  $test_question[$x]->value,
                                        );

                        array_push($tot_quest,$cat_quest);
                    }

                //TEXT ANSWERS:
                if($test_question[$x]->question_id == 0){

                    //ANSWER BELONGS TO CURRENT CATEGORY
                    if($test_question[$x]->category_id == $id){
                            
                        $cat_comment = $test_question[$x]->commentfield;

                    }
                }
            }

            //FIX ERROR DIV BY 0
            if($average == 0) {
                $average = 1;
            }

            //ALERT: LANGUAGE HARDCODED AS enUS
            $comments_title = $category['attributes']['custom_open_question_enUS'];

            if(count($tot_quest) != 0) {
                $category_array = array (
                                "name"              =>  $category->name,
                                "explanation"       =>  $category->description,
                                "personal_score"    =>  $value/$average,
                                "questions"         =>  $tot_quest,
                                "comments"          =>  $cat_comment,
                                "categoryID"        =>  $id,
                                "comments_title"    =>  $comments_title, //OPEN QUESTION
                                );

                $pdf_array[] = $category_array;
            }

        }

        //TODO REORDER ARRAY: CATEGORIES SAME ORDER AS DEFAULT (USE $pdf_array[]['categoryID'])

        return $pdf_array;
    }


    public function get_responder_data($test_id, $no_split_resp){
        
        //ARRAY WITH ALL RESPONDERS WITH FINISHED TESTS
        $responders = [];
        
        //TAKING ALL FINISHED TESTS (ID == 7)
        $finishedTests = DB::table('tests')
            ->select('*')
            ->where('test_id', '=', $test_id)
            ->where('teststatus_id', '=', Teststatus::FINISHED_BY_USER_OR_RESPONDENT)
            ->get();

        foreach($finishedTests as $testVar){
            
            //TAKING EVERY RESPONDENT AS AUTHOR OF THE FINISHED TEST
            $respondent = DB::table('respondents')
                ->select('*')
                ->where('id', '=', $testVar->respondent_id)
                ->get();

            //SAVING ARRAY WITH GROUPS + INSIDE ARRAY PER RESPONDENT IN THE GROUP WITH idTest AND resp_id
            if(!isset($responders[$respondent[0]->respondentgroup_id])){
                $responders[$respondent[0]->respondentgroup_id] = [];
                
                //TAKING RESPONDENT GROUP NAME
                $groupName = DB::table('respondentgroups')
                    ->select('*')
                    ->where('id', '=', $respondent[0]->respondentgroup_id)
                    ->get();
                
                array_push($responders[$respondent[0]->respondentgroup_id], $groupName[0]->name);
                array_push($responders[$respondent[0]->respondentgroup_id], array('idTest' => $testVar->id, 'resp_id' => $testVar->respondent_id));
            }else{
                array_push($responders[$respondent[0]->respondentgroup_id], array('idTest' => $testVar->id, 'resp_id' => $testVar->respondent_id));
            }
        }

        //SKIPPING CLIENTS WITH 2 OR LESS MINIMUM REQUIRED RESPONDENTS
        $mainTest = Test::find($test_id);
        $client = User::find($mainTest->client_id);
        $minReq = $client->getMinReq($client->id);

        if ($minReq < 3){
            return $responders;
        }else{

            foreach ($responders as $key => $value) {
            
                //COPYING TO OTHERS GROUP LONELY RESPONDENTS AND DELETING THEM FROM THE MAIN ARRAY, SKIPPING OTHERS GROUP
                if(count($value)-1 < 2 && $key != 4){

                    //CREATING OTHERS GROUP IF DOES NOT EXIST IN THE ARRAY
                    if(!isset($responders[4]) || is_array($responders[4]) == false){
                        $responders[4] = [];
                        $responders[4][0] = 'Other';
                    }

                    array_push($responders[4], $value[1]);
                    unset($responders[$key]);
                }
            }

            if($no_split_resp){
                //no_split_resp == true, no split. THNK style
                
                //CHECKING ALL RESPONDENT GROUPS DIFFERENT FROM OTHER
                foreach ($responders as $key => $value) {

                    //CREATING OTHERS GROUP IF DOES NOT EXIST IN THE ARRAY
                    if(!isset($responders[4]) || is_array($responders[4]) == false){
                        $responders[4] = [];
                        $responders[4][0] = 'Other';
                    }

                    if($responders[$key][0] != 'Other'){
                        
                        //COPYING TO OTHERS GROUP RESPONDENTS FROM GROUPS DIFFERENT FROM OTHERS AND DELETING THEM FROM THE MAIN ARRAY
                        foreach ($value as $key2 => $value2) {

                            //SKIPPING THE GROUP NAME IN THE PUSHING
                            if(is_array($value2)){
                                array_push($responders[4], $value2);
                            }
                        }
                        unset($responders[$key]);
                    }
                }

                //RETURNING EMPTY ARRAY OF OTHERS IF LESS THAN 2 RESPONDERS IN OTHERS
                if(isset($responders[4]) && count($responders[4])-1 < 2){
                    $responders[4]=[];
                }
            }

            return $responders;
        }
    }

     public function stDev($responderArray,$average,$numQuestion){
            $question = [];
            $tot = 0;
            for($i=0;$i<$numQuestion;$i++){
                $tot += pow($responderArray[0]['numbers'][$numQuestion]-$average,2);
                $tot += pow($responderArray[1]['numbers'][$numQuestion]-$average,2);
                $tot += pow($responderArray[2]['numbers'][$numQuestion]-$average,2);
                return sqrt($tot/3);
            }
        }

        public function calcAverage($numbers ,$zero){

                $tot = 0;
                $div = 0;
                $nums = [];
                if($zero == false){
                    foreach ($numbers as $num) {
                        if($num != 0){
                            array_push($nums,$num);
                            $tot += $num;
                            $div++;
                        }
                    }
                }else{
                    foreach ($numbers as $num) {
                        array_push($nums,$num);
                        $tot += $num;
                        $div++;
                    }
                }
                if($div == 0){
                    $div = 1;
                }
                $average = $tot / $div;
                return $average;
        }

        public function calcStDeviation($numbers,$average){

            //DELETE CAN NOT RATE FROM ARRAY
            $del_val = 0;
            if(($key = array_search($del_val, $numbers)) !== false) {
                unset($numbers[$key]);
            }
                                
            if(is_array($numbers)){

                //AVOIDING DIVISION BY ZERO 
                if(count($numbers) < 1){
                    return 0;
                }else{
                    $countNumsNoZero = count($numbers);
                }

                $mean = array_sum($numbers) / $countNumsNoZero;
                foreach($numbers as $key => $num){
                    $devs[$key] = pow($num - $mean, 2);
                }
                
                //AVOIDING DIVISION BY ZERO 
                if(count($devs) - 1 < 1){
                    $countDevsLessOne = 1;
                }else{
                    $countDevsLessOne = count($devs) - 1;
                }

                return sqrt(array_sum($devs) / $countDevsLessOne);
            }else{
                return 0;
            }
        }

        public function take_question_data($test_id,$question_id){
            $value =     DB::table('category_question_user_test')
                                ->select('value')
                                ->where('test_id', '=', $test_id)
                                ->where('question_id', '=', $question_id)
                                ->get();

            if(isset($value[0])){
                if($value[0]->value == 9)
                    return 0;
                return $value[0]->value;
            }else{
                return 0;
            }
        }

        public function take_question_comment($test_id,$category_id){
            $value =     DB::table('category_question_user_test')
                                ->select('commentfield')
                                ->where('test_id', '=', $test_id)
                                ->where('category_id', '=', $category_id)
                                ->get();
            return $value[0]->commentfield;
        }

        public function checkId($Idarray,$checker){
            foreach ($Idarray as $id) {
                if($id == $checker)
                    return true;
            }
            return false;
        }

        public function take_categories_questions($user_id,$category_id){
            $questions =     DB::table('category_question_user')
                                ->select('question_id')
                                ->where('user_id', '=', $user_id)
                                ->where('category_id', '=', $category_id)
                                ->orderBy('order')
                                ->get();
            return $questions;
        }

    public function get_full_report_array($test_id,$user_id = null) {
        $pdf_array = array();

        $user = Auth::user();

        //FINDING PARTICIPANT
        if (isset($user_id)){
            $user = User::find($user_id);
        }

        $mainTest = Test::find($test_id);

        //ARRAY ACTIVE CATEGORIES IN CLIENT
        $categories = $user->getCategories($mainTest->client_id);

        if(!isset($categories[0]))
            return $pdf_array;

        //ARRAY QUESTIONS OWNED BY PARTICIPANT (ID, TEST_ID, CATEGORY_ID, VALUE, COMMENTFIELD, TIME)
        //EX.: 5CATEGORIES + 5-5-5-4-5QUESTIONS/CATEGORY = 29ELEMENTS
        $test_question = $user->get_data($test_id);

        //TOTAL NUMBER OF QUESTIONS ON CURRENT TEST INCLUDING COMMENTFIELDS
        //EX.: 5CATEGORIES + 5-5-4-5-5QUESTIONS/CATEGORY = 29ELEMENTS
        $totalNumberOfQuestions = count($test_question);

        //SEARCH FOR CLIENT
        if($mainTest->client_id != 0){
            $clientOfTest = User::find($mainTest->client_id);
        }

        if($clientOfTest->split_resp != 1){
            //ARRAY ALL RESPONDENTS ON TEST GROUPED BY RESPONDENT GROUP ([0]GROUP, [>0]IDTEST, RESP_ID)
            $test_question_responder = $this->get_responder_data($test_id, true); //no_split_resp == true, no split
        }else{
            $test_question_responder = $this->get_responder_data($test_id,false); //no_split_resp == false, split
        }

        if(empty($test_question_responder)){
            return $pdf_array;
        }

        $GLOBALS['respLegend'] = array(
                                        array(
                                                'nameResp' => 'mainColor', 
                                                'colorBar' => $this->hex2rgb($categories[0]['relations']['pivot']['parent']['colorMain'])
                                            ),
                                        array(
                                                'nameResp' => 'superior', 
                                                'colorBar' => $this->hex2rgb($categories[0]['relations']['pivot']['parent']['colorResponder1'])
                                            ),
                                        array(
                                                'nameResp' => 'peer', 
                                                'colorBar' => $this->hex2rgb($categories[0]['relations']['pivot']['parent']['colorResponder2'])
                                            ),
                                        array(
                                                'nameResp' => 'subordinate', 
                                                'colorBar' => $this->hex2rgb($categories[0]['relations']['pivot']['parent']['colorResponder3'])
                                            ),
                                        array(
                                                'nameResp' => 'other', 
                                                'colorBar' => $this->hex2rgb($categories[0]['relations']['pivot']['parent']['colorResponder4'])
                                            )
                                );

        foreach ($categories as $category) {
            
            //CATEGORY->ID
            $id = $category['attributes']['id'];
            
            $id_questions = [];
            
            //ARRAY OF QUESTIONS IDS IN THE CURRENT CATEGORY
            $quest = $this->take_categories_questions($mainTest->client_id,$id);
            
            foreach($quest as $question_take){
                array_push($id_questions,$question_take->question_id);
            }

            $value = 0;
            $aver = 0;
            $num_question = 0;
            $tot_quest = [];
            
            for($x = 0; $x < $totalNumberOfQuestions; $x++){

                //CATCHING A COMMENT AS ITEM OF ARRAY TEST_QUESTION
                if($test_question[$x]->question_id == 0){
                    
                    //CATCHING A COMMENT BELONGING TO CURRENT CATEGORY
                    if($test_question[$x]->category_id == $id){

                        $tempCatComment = [];

                        //KEY AS IDENTIFIER FOR DIFFERENT GROUPS OF RESPONDENTS
                        foreach ($test_question_responder as $key => $grup_resp) {
                            $temp1 = [];

                            //TAKING ARRAYS OF INDIVIDUAL RESPONDENTS (TESTID, RESP_ID), RANDOM ORDER??
                            foreach ($grup_resp as $resp) {
                                
                                //SKIPPING [0]GROUP OF THE ARRAY OF RESPONDENTS
                                if(is_array($resp)){

                                    //TAKING COMMENTFIELD FROM TABLE
                                    $tempComment = $this->take_question_comment($resp['idTest'],$id);
                                    
                                    array_push($temp1, $tempComment);
                                }
                            }
                            $tempCatComment[$key] = [];

                            //SAVING COMMENTS ON THEIR APPROPIATE GROUP [$KEY]
                            $tempCatComment[$key] = $temp1;
                        }
                    }

                //CATCHING ITEMS OF ARRAY TEST_QUESTION AS NON COMMENTS
                }else{
                    
                    //CHECK THAT EVERY QUESTION ID BELONGS TO CURRENT CATEGORY
                    foreach ($id_questions as $idQuest) {
                        if($idQuest == $test_question[$x]->question_id){
                            
                            //??
                            if($this->checkId($id_questions,$test_question[$x]->question_id)){
                            
                                //SKIPPING CAN NOT RATE OR EMPTY SCORES    
                                if($test_question[$x]->value == 9 || $test_question[$x]->value == 0){
                                    $test_question[$x]->value = 0;
                                //INCREMENT OF TOTAL VALID SCORES
                                }else{
                                   $aver++;
                                }

                                //INCREMENT OF TOTAL QUESTIONS
                                $num_question++;

                                $tempCat = [];

                                //KEY AS IDENTIFIER FOR DIFFERENT GROUPS OF RESPONDENTS
                                foreach ($test_question_responder as $key => $grup_resp) {

                                    $temp = [];

                                    //TAKING ARRAYS OF INDIVIDUAL RESPONDENTS (TESTID, RESP_ID), RANDOM ORDER??
                                    foreach ($grup_resp as $resp) {

                                        //SKIPPING [0]GROUP OF THE ARRAY OF RESPONDENTS
                                        if(is_array($resp)){

                                            //TAKING SCORE FROM TABLE
                                            $tempValue = $this->take_question_data($resp['idTest'],$test_question[$x]->question_id);

                                            array_push($temp, $tempValue);
                                        }
                                    }

                                    $AvQuestion = $this->calcAverage($temp,false);
                                    $stDevQuestion = $this->calcStDeviation($temp,$AvQuestion);

                                    $tempCat[$grup_resp[0]] = [];
                                    $tempCat[$grup_resp[0]]['average'] = $AvQuestion;
                                    $tempCat[$grup_resp[0]]['stDev'] = $stDevQuestion;
                                    $tempCat[$grup_resp[0]]['numbers'] = $temp;                                
                                }

                                //SUM OF QUESTION SCORES
                                $value = $value + $test_question[$x]->value;

                                //ARRAY ACTIVE QUESTION IN CLIENT, SELECTED BY ID
                                $question = $this->get_question_data($test_question[$x]->question_id);

                                //PREPARING COMPLETE DATA FOR EACH QUESTION
                                $cat_quest = array( "id"                => $question[0]->id,
                                                    "name"              => $question[0]->name,
                                                    "explanation"       => $question[0]->description,
                                                    "personal_score"    => $test_question[$x]->value,
                                                    "respondents_score" => $tempCat);
                                
                                //ARRAY OF QUESTIONS WITH FULL DATA
                                array_push($tot_quest,$cat_quest);
                            }
                        }
                    }
                }
            }

            $groups = [];
            foreach ($tot_quest as $quest) {
                foreach ($quest['respondents_score'] as $key => $valueF) {
                    
                    //BUILD ARRAY GROUPS USING KEY [RESPONDENT GROUP NAME]
                    if(!isset($groups[$key])){
                        $groups[$key] = [];
                    }

                    //GROUP ALL RESPONDENTS SCORES FROM EACH QUESTION FILTERED BY KEY [RESPONDENT GROUP NAME]
                    //EX.: 5QUESTIONS/CATEGORY, 9RESPONDENTS, 5*9=45 SCORES IN ARRAY
                    foreach ($valueF['numbers'] as $val) {
                        array_push($groups[$key] , $val);
                    }
                }
            }

            $respondersCatScore = [];
            //AVERAGE & STANDARD DEVIATION PER RESPONDENT GROUP ON CURRENT CATEGORY
            foreach ($groups as $key => $valueT) {
                $Avgroup = $this->calcAverage($valueT,false);
                $stDevGroup = $this->calcStDeviation($valueT,$Avgroup);
                $respondersCatScore[$key] = array('average' => $Avgroup,'stDev' => $stDevGroup);
            }

            if($aver == 0)
                $aver = 1;

            if(count($tot_quest) != 0){
                
                $tempCom = [];
                
                //SAVING COMMENTS FOR CURRENT CATEGORY ON FUTURE & FINAL PDF ARRAY
                for($x = 0; $x < $totalNumberOfQuestions; $x++){
                    if($test_question[$x]->category_id == $id){
                        $tempCom['personal'] = $test_question[$x]->commentfield;
                    }
                }
				
                if(isset($tempCatComment))
                	$tempCom['responders'] = $tempCatComment;

                //ALERT: LANGUAGE HARDCODED AS enUS
                $comments_title = $category['attributes']['custom_open_question_enUS'];

                //SAVING FULL DATA FROM CURRENT CATEGORY ON FUTURE & FINAL PDF ARRAY
                $category_array = array (
                                            "id"                => $category->id,
                                            "name"              => $category->name,
                                            "explanation"       => $category->description,
                                            "personal_score"    => $value/$aver, //SUM TOTAL QUESTION SCORES / TOTAL NUMBER ANSWERED SCORES (AVERAGE)
                                            "respondents_score" => $respondersCatScore,
                                            "questions"         => $tot_quest,
                                            "comments"          => $tempCom,
                                            "comments_title"    => $comments_title, //OPEN QUESTION
                                        );
                array_push($pdf_array, $category_array);
            }
        }

        //TODO RANDOM ORDER ITEMS IN $pdf_array["comments"][GROUP OF RESPONDENTS]

        return $pdf_array;
    }
}