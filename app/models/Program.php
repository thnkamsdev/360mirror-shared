<?php

class Program extends Eloquent {

	const NOT_ACTIVE       	                   = 1;

    const MARCH_2012_AMS                       = 2;
    const SEPTEMBER_2012_AMS                   = 3;
    const MARCH_2013_AMS                       = 4;

    const NOT_A_PARTICIPANT                    = 5;

    const SEPTEMBER_2013_AMS                   = 6;
    const MARCH_2014_AMS                       = 7;
    const SEPTEMBER_2014_AMS                   = 8;
    const MARCH_2015_AMS                       = 9;
    const SEPTEMBER_2015_AMS                   = 10;
    const JANUARY_2015_VAN                     = 11;
    const JUNE_2015_VAN	                       = 12;
    const FEBRUARY_2015_LIS                    = 13;

}