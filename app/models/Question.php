<?php

use THNK\Mirror\Localization\LocalizedModel;

class Question extends Eloquent
{
    use LocalizedModel;

    public function category()
    {
        return $this->belongsTo('Category');
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->getLocalized('name');
    }

    /**
     * @param string $value
     */
    public function setNameAttribute($value)
    {
        $this->setLocalized('name', $value);
    }

    /**
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return $this->getLocalized('description');
    }

    /**
     * @param string $value
     */
    public function setDescriptionAttribute($value)
    {
        $this->setLocalized('description', $value);
    }
}