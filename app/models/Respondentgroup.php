<?php

class Respondentgroup extends Eloquent {
    const SUPERIOR = 1;
    const PEER  = 2;
    const SUBORDINATE = 3;
    const OTHERS = 4;
    
    //USER::IMPACT
    const COWORKER = 5;
    const MENTOR = 6;

    //USER::ENDEAVOR
    // 5
    // 2
    
    //USER::OLX
    //const MANAGER = 7;
    
    
    public function responders()
    {
        return $this->hasMany('Responder');
    }    
    
}
