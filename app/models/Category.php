<?php

use THNK\Mirror\Localization\LocalizedModel;

class Category extends Eloquent
{
    use LocalizedModel;

    public function users()
    {
        return $this->belongsToMany('User');
    }

    public function questions()
    {
        return $this->hasMany('Question');
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->getLocalized('name');
    }

    /**
     * @param string $value
     */
    public function setNameAttribute($value)
    {
        $this->setLocalized('name', $value);
    }

    /**
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return $this->getLocalized('description');
    }

    /**
     * @param string $value
     */
    public function setDescriptionAttribute($value)
    {
        $this->setLocalized('description', $value);
    }

    /**
     * @return string
     */
    public function getShortAttribute()
    {
        return $this->getLocalized('short');
    }

    /**
     * @param string $value
     */
    public function setShortAttribute($value)
    {
        $this->setLocalized('short', $value);
    }
}