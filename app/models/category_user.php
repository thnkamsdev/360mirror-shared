<?php

class CategorySander extends Eloquent {

    public function users()
    {
        return $this->belongsToMany('User');
    }

    
    public function questions()
    {
        return $this->hasMany('Question');
    }    
}