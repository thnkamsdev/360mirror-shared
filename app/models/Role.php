<?php
 
class Role extends Eloquent
{

    const USER_THNK_ADMIN    = 1;
    const USER_CLIENT        = 2;
    const USER_CLIENT_ADMIN  = 3;
    const USER_CLIENT_COACH  = 4;
    const USER_PARTICIPANT   = 5;
    
    /**
     * Set timestamps off
     */
    public $timestamps = false;
 
    /**
     * Get users with a certain role
     */
    public function users()
    {
        return $this->belongsToMany('User', 'users_roles');
    }
}