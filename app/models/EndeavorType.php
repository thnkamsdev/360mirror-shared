<?php

use THNK\Mirror\Localization\LocalizedModel;

class EndeavorType extends Eloquent {

    use LocalizedModel;

	const A_SOCIAL_ENTERPRISE                               = 1;
    const A_COMMERCIAL_VENTURE                              = 2;
    const A_NEW_VENTURE_OR_INITIATIVE_IN_CORPORATE_SETTING  = 3;
    const OTHER                                             = 4;

    public function endeavors()
    {
        return $this->hasMany('Endeavor');
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->getLocalized('name');
    }

    /**
     * @param string $value
     */
    public function setNameAttribute($value)
    {
        $this->setLocalized('name', $value);
    }
}