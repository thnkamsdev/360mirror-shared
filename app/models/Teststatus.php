<?php

use THNK\Mirror\Localization\LocalizedModel;

class Teststatus extends Eloquent {

    use LocalizedModel;

    const NOT_STARTED                          = 0;
    const JUST_STARTED                         = 1;
    
    const FINISHED_BY_USER_OR_RESPONDENT       = 7;
    const TEST_FULLY_FINISHED                  = 8;
    const ALL_FINISHED_MIN_REQ_NOT_REACHED	   = 9;
    
    public function tests()
    {
        return $this->hasMany('Test');
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->getLocalized('name');
    }

    /**
     * @param string $value
     */
    public function setNameAttribute($value)
    {
        $this->setLocalized('name', $value);
    }
}
