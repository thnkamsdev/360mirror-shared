<?php

class ProfileController extends WebsiteController {

    public function getProfile($client_id, $id) {

        if (Auth::user()->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, Auth::user()->id) && !Auth::user()->hasRoleInClient(Role::USER_CLIENT_COACH, $client_id, Auth::user()->id)){
        	//PARTICIPANT ACCESSING TO HIS PROFILE
            $participant = Auth::user();

            $user = User::find($client_id); //finding client ($user variable used also in template main)

            $country_options = array('' => 'Your country') + lists(Country::all(), 'short_name');
            $program_options = array('' => 'Your program') + lists(Program::orderBy('name')->get(), 'name');

            return View::make('forms/profile')
                ->with('country_options',$country_options)
                ->with('program_options',$program_options)
                ->with('participant_id', $id)
                ->with('user', $user)
                ->with('participant', $participant);

        }else{

        	//COACH OR THNK ADMIN ACCESSING TO PARTICIPANTS PROFILES
            $participant = User::find($id); //finding participant
            $loggedUser = Auth::user();

			if ( $loggedUser->hasRoleInClient(Role::USER_CLIENT_COACH, $client_id, $loggedUser->id) || $loggedUser->hasRole('USER_THNK_ADMIN') ) {
				if ($participant->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, $participant->id)){
					$country_options = array('' => trans('profileClient.countryPlaceholder')) + lists(Country::all(), 'short_name');
		            $program_options = array('' => trans('profileClient.programPlaceholder')) + Program::orderBy('name')->lists('name', 'id');
		            $tests = DB::table('tests')
		                        ->select('*')
		                        ->where('user_id', '=', (int)$id)
		                        ->where('client_id', '=', (int)$client_id)
		                        ->orderBy('id', 'DESC')
		                        ->get();

                    $endeavors = DB::table('endeavors')
		                        ->select('*')
		                        ->where('user_id', '=', (int)$id)
		                        ->where('client_id', '=', (int)$client_id)
		                        ->orderBy('id', 'DESC')
		                        ->get();

		            $user = User::find($client_id); //finding client ($user variable used also in template main)          

		            return View::make('forms/profile')
		                ->with('country_options',$country_options)
		                ->with('program_options',$program_options)
		                ->with('tests',$tests)
		                ->with('endeavors',$endeavors)
		                ->with('participant_id', $id)
		                ->with('participant',$participant)
		                ->with('user', $user);
				}else{
					$message = 'Security error #1HM2836m';
                	return Redirect::to(translate_url('/'))->with(['message' => $message]);
				}
				
	        }else{
	        	$message = 'Security error #n7941F9D';
            	return Redirect::to(translate_url('/'))->with(['message' => $message]);
	        }
        }
    }

    public function postProfile($client_id, $id) {
    	
    	$loggedUser = Auth::user();
    	$saveUserData = false;

    	$user = User::find($client_id); //finding client
    	$participant = User::find($id); //finding participant

    	//COACH OR THNK ADMIN SAVING PARTICIPANTS PROFILES
    	if ( $loggedUser->hasRoleInClient(Role::USER_CLIENT_COACH, $client_id, $loggedUser->id) || $loggedUser->hasRole('USER_THNK_ADMIN') ) {
			if ($participant->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, $participant->id)){
				//SAVE USER DATA
				$saveUserData = true;
			}else{
				return Redirect::to(translate_url('/clients/getAdmin/' . $client_id))->withErrors('Security error.');
			}
			
		//PARTICIPANT SAVING HIS PROFILE	
        }elseif ($loggedUser->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, $loggedUser->id)){
        	//SAVE USER DATA
			$saveUserData = true;

        }else{
        	$message = 'Security error #78aW588c';
        	return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        if ($saveUserData == true){

			$typeSubmit = Input::get('knop');
	        if($typeSubmit == trans('profile.buttons.save')){
				$reset = Input::get('reset');

				if ( null !== Input::get('password') ) {
				    $rules = array(
				      'email'             =>  'required|email|unique:users,email,'.$id,
				      'password'          =>  'required|min:8',
				      'password_confirm'  =>  'required|same:password',
				      'firstname'         =>  'required|alpha_spaces',
				      'lastname'          =>  'required|alpha_spaces',
				      'gender'            =>  'required',
				      'date_of_birth'     =>  'required|date|date_format:"Y-m-d"',
				      'country'           =>  'required',
				      'company'           =>  'required',
				    );
				} else {
					//COACH OR THNK ADMIN SAVING PARTICIPANT DATA ($reset == 'true')
				    $rules = array(
				      'email'             =>  'required|email|unique:users,email,'.$id,
				      'firstname'         =>  'required|alpha_spaces',
				      'lastname'          =>  'required|alpha_spaces',
				      'gender'            =>  'required',
				      'date_of_birth'     =>  'required|date|date_format:"Y-m-d"',
				      'country'           =>  'required',
				      'company'           =>  'required',
				    );
				}

				//ALERT ONLY 1 PROGRAM OPTION PER PARTICIPANT, OVERWRITE PROGRAM OPTION FROM OLDER CLIENT 
	            if($user->THNKParticipant == 1){
	                $rules['program_options'] = 'required';
	            }

				// Validation
				$validator = Validator::make(Input::all(), $rules);

				if ($validator->fails()){
					//RESET FALSE FOR PARTICIPANT
					if ($reset == 'false' ){
				    	return Redirect::to(translate_url('/test/myprofile/'.$client_id .'/'. $id))->withErrors($validator)->withInput(Input::except('password', 'password_confirm'));
					}
					//RESET TRUE FOR COACH OR THNK ADMIN
				    else{
				    	return Redirect::to(translate_url('/test/myprofile/'. $client_id .'/'. $id))->withErrors($validator)->withInput();
				    }

				} else {

					if (Input::get('resetPassword') == 1){
					    $pwd = substr(md5(rand()), 0, 15);
					    if (isset($pwd) && $pwd != ''){
					        $participant->password = Hash::make($pwd);
					    }
					    $data = array();
					    $data['firstname'] = $participant->firstname;
					    $data['email'] = $_POST['email'];
					    $data['pwd'] = $pwd;

					    $typeInvite = 'newSeat';
                        $dataMail = $user->getMailInvite($typeInvite, $user->THNKParticipant, $user->id, $participant);
                        Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
			                $message->to($_POST['email'], $_POST['firstname'])->from($dataMail['from'], $dataMail['firstname'])->subject($dataMail['subject']);
			                $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
			            });
					}

					$participant->email = rtrim(Input::get('email'));

					if (null !== Input::get('password') && Input::get('password') != '') {
					    $participant->password = Hash::make(Input::get('password'));
					}

					$participant->firstname = rtrim(Input::get('firstname'));
					$participant->lastname = rtrim(Input::get('lastname'));
					$participant->gender = Input::get('gender');
					$participant->date_of_birth = Input::get('date_of_birth');
					$participant->country = Input::get('country');
					$participant->company = rtrim(Input::get('company'));
	                if(Input::get('program_options') !== null){
	                    $participant->THNKprogram = Input::get('program_options');
	                }

					$participant->update();
				}
			
				if ( $loggedUser->hasRole('USER_PARTICIPANT') ) {
					if ($reset == 'false'){
						//LOGGED USER IS ONLY PARTICIPANT IN THIS CLIENT
				    	return Redirect::to(translate_url('/dashboard/'.$client_id))->with('success', 'Your profile has been updated');
					}else{
						//LOGGED USER IS PARTICIPANT PLUS COACH IN THIS CLIENT
				    	return Redirect::to(translate_url('/clients/getAdmin/'.$client_id))->with('success_save', 'Participant profile has been updated');
				    }
				
				}else{
					//LOGGED COACH OR THNK ADMIN, NOT PARTICIPANTS OF THIS CLIENT
				    return Redirect::to(translate_url('/clients/getAdmin/'.$client_id))->with('success_save', 'Participant profile has been updated');
				}

			} else{
				$message = 'Security error #zA1w0ZON';
                return Redirect::to(translate_url('/'))->with(['message' => $message]);
			}

        }else{
        	$message = 'Security error #3732Y7Ad';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }
    }


}
