<?php

class ClientsController extends WebsiteController {

    public function getClients ($user_list = NULL )
    {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #R1em43No';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $data       = array();
        $list_data  = array();


        $list_data['kolommen'] = array(
            'firstname'     => trans('table.headers.fname'),
            'lastname'      => trans('table.headers.lname'),
            'company'       => trans('table.headers.company'),
            'edit_client'   => trans('table.headers.client'),
        );

        if (NULL !== (Session::get('user_list')))
                $user_list = Session::get('user_list');
        else {
            $user_list = User::where('users.id', '>', 0) //dummy condition
            ->join('users_roles', 'users.id', '=', 'users_roles.user_id')
            ->where('users_roles.role_id', '=', Role::USER_CLIENT)
            ->get();
        }

        // Titel van de pagina
        $list_data['list']          = $user_list;
        $list_data['component']     = "test";
        $list_data['action']        = 0;
        $list_data['controller']    = 'mytest';
        $data['titel']              = '';

        $data['list'] = $list_data;

        //return
        return View::make('/forms/admin_dashboard', $data);

    }

    public function getEdit($id = NULL) {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #yJD0DdjV';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        if (isset($id)){
            $user = User::find($id);
            $newClient = false;
        }else{
            $user = new User;
            $newClient = true;
        }

        $country_options = array('' => trans('profileClient.countryPlaceholder')) + lists(Country::all(), 'short_name');

        if($user->logo_name != NULL){
            $logo = $user->logo_name;
        } else{
            $logo = NULL;
        }

        return View::make('forms/client_form')
            ->with('logo', $logo)
            ->with('country_options',$country_options)
            ->with('user', $user)
            ->with('newClient', $newClient);
    }

    public function postEdit($id = NULL) {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #PT7rC3Gp';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        if (isset($id)) {
            $user = User::find($id);
        } else {
            $user = new User;
        }

        $submit = Input::get('knop');

        if ($submit == trans('profileClient.admin.questions')) {
            return Redirect::to(translate_url('/clients/getQuestions/' . $user->id));

        } else if ($submit == trans('profileClient.admin.coaches')) {
            return Redirect::to(translate_url('/clients/getCoaches/' . $user->id));

        } else if ($submit == trans('profileClient.admin.toDashboard')) {
            return Redirect::to(translate_url('/clients/getAdmin/' . $user->id));

        } else if ($submit == trans('profileClient.buttons.save')) {

            $logo = Input::get('logo');
            if ($logo == 0){
                $rules = array(
                    'company'               =>  'required',
                    'country'               =>  'required',
                    'firstname'             =>  'required|alpha_spaces',
                    'lastname'              =>  'required|alpha_spaces',
                    'email'                 =>  'required|email|unique:users,email,'.$id,
                    'test_per_user'         =>  'required',
                    'total_seats'           =>  'required|numeric',
                    'image'                 =>  'required|image|image_size:1-1234,38-626',
                    'THNKParticipant'       =>  'required',
                    'split_resp'            =>  'required',
                    'multilanguage'         =>  'required',
                    'type_of_report'        =>  'required',
                    'buy_more_seats'        =>  'required|email',
                    'custom_open_question'  =>  'required',
                    'THNKGGF_footer'        =>  'required'
                );

            } else {
                $rules = array(
                    'company'               =>  'required',
                    'country'               =>  'required',
                    'firstname'             =>  'required|alpha_spaces',
                    'lastname'              =>  'required|alpha_spaces',
                    'email'                 =>  'required|email|unique:users,email,'.$id,
                    'test_per_user'         =>  'required',
                    'total_seats'           =>  'required|numeric',
                    'image'                 =>  'image|image_size:1-1234,38-626',
                    'THNKParticipant'       =>  'required',
                    'split_resp'            =>  'required',
                    'multilanguage'         =>  'required',
                    'type_of_report'        =>  'required',
                    'buy_more_seats'        =>  'required|email',
                    'custom_open_question'  =>  'required',
                    'THNKGGF_footer'        =>  'required'
                );
            }

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::to(translate_url('/clients/edit/' .$id))
                    ->withErrors($validator)
                    ->withInput(Input::except('password','image'));
            } else {

                $user->buy_more_seats = Input::get('buy_more_seats');
                $user->company = rtrim(Input::get('company'));
                $user->country = Input::get('country');
                $user->email = rtrim(Input::get('email'));
                $user->bcc_for_tests = rtrim(Input::get('bcc_for_tests'));

                if (Input::get('resetPassword') == 1){
                    $pwd = substr(md5(rand()), 0, 15);
                    if (isset($pwd) && $pwd != ''){
                        $user->password = Hash::make($pwd);
                    }
                }

                if(Input::file('image') != ''){
                    $file = Input::file('image');
                    $data = getimagesize($file);//index 0 width index 1 height

                    if($data[0]>50 && $data[1]>25 && $data[0]<900 && $data[1]<650){
                        $destinationPath = 'media/images/clientLogo/';
                        $fileOriginal = $file->getClientOriginalName();
                        $fileExtension = substr($fileOriginal, -4);
                        $filename = "logo".$fileExtension;
                        //Input::file('image')->move($destinationPath, $filename);
                        $ToF = true;
                    } else {
                        $messageImage = 'The image size not valid';
                        return Redirect::back()
                            ->with('messageImage',true)
                            ->withInput(Input::except('password','image'));
                    }
                } else {
                    $ToF = false;
                }

                $user->firstname = rtrim(Input::get('firstname'));
                $user->lastname = rtrim(Input::get('lastname'));
                $user->type_of_report = Input::get('type_of_report');
                $user->buy_more_seats = Input::get('buy_more_seats');
                $user->colorMain = Input::get('colorMain');
                $user->colorResponder1 = Input::get('colorResponder1');
                $user->colorResponder2 = Input::get('colorResponder2');
                $user->colorResponder3 = Input::get('colorResponder3');
                $user->colorResponder4 = Input::get('colorResponder4');
                $user->test_per_user = Input::get('test_per_user');
                $user->multilanguage = Input::get('multilanguage');
                $user->THNKParticipant = Input::get('THNKParticipant');
                $user->split_resp = Input::get('split_resp');
                $user->custom_open_question = Input::get('custom_open_question');
                $user->THNKGGF_footer = Input::get('THNKGGF_footer');
                //$user->userfunction_id = 1;
                if($ToF == true){
                    $user->logo_name = $filename;
                }

                $nr_of_seats = Input::get('total_seats');
                if (isset($nr_of_seats)){
                    $user->total_seats = $nr_of_seats;
                }

                $user->save();
                if (Input::file('image') != ''){
                    $user->logo_name = $user->id.$filename;
                    Input::file('image')->move($destinationPath, $user->id.$filename);
                }

                if (Input::get('resetPassword') == 1){
                    $pwd = substr(md5(rand()), 0, 15);
                    if (isset($pwd) && $pwd != ''){
                        $user->password = Hash::make($pwd);
                    }
                    $data = array();
                    $data['firstname'] = $user->firstname;
                    $data['email'] = $_POST['email'];
                    $data['pwd'] = $pwd;

                    //THNK Clients (including programs)
                    if (Input::get('THNKParticipant') == 1){
                        //email for reset the password of a client
                        Mail::send('emails.invite_new_seat_thnk', $data, function($message)
                        {
                          $message->to($_POST['email'], $_POST['firstname'])->from("360Mirror@360mirror.org")->subject("360 Mirror Client Credentials");
                          $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                        });
                    //NON THNK Clients (non including programs)
                    }else{
                        //email for reset the password of a client
                        Mail::send('emails.invite_new_seat', $data, function($message)
                        {
                          $message->to($_POST['email'], $_POST['firstname'])->from("360Mirror@360mirror.org")->subject("360 Mirror Client Credentials");
                          $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                        });
                    }
                }
                $user->save();

                $result = DB::table('users_roles')
                    ->select('*')
                    ->where('user_id', $user->id)
                    ->where('client_id', $user->id)
                    ->where('role_id', Role::USER_CLIENT)
                    ->get();
                $num_rows = count($result);

                if ($num_rows > 0) {

                    // redirect
                    Session::flash('messageUp', 'Successfully updated a client!');
                    return Redirect::to(translate_url('/clients'));
                }else{

                    DB::table('users_roles')->insert(
                        array('user_id' => $user->id, 'role_id' => Role::USER_CLIENT, 'client_id' => $user->id)
                    );

                    // redirect
                    Session::flash('message', 'Successfully created a client!');
                    return Redirect::to(translate_url('/clients'));
                }
            }
        }
    }

    public function getQuestions($id = NULL) {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #VPb6TKtT';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        if (isset($id)) {
            $user = User::find($id);
        }

        $user_categories = $user->categories;
        $arr_user_categories = array();

        foreach ($user_categories as $uc) {
            $arr_user_categories[] = $uc->id;
        }

        $category_question_users = $user->categoryQuestionUser;

        $arr_user_questions = array();

        foreach ($category_question_users as $cqu) {
            $arr_user_questions[] = $cqu->question_id;
        }

        $categories = Category::where('is_default', '=' , 1)->get();

        $custom_categories = Category::join('category_user', 'category_user.category_id', '=', 'categories.id')
            ->select('categories.*')
            ->where('user_id', '=', $id)
            ->where('is_default', '=', 0)
            ->get();

        if(Session::get('jsCat') != null){
            return View::make('forms/manage_questions')
            ->with('categories', $categories)
            ->with('custom_categories', $custom_categories)
            ->with('arr_user_categories', $arr_user_categories)
            ->with('arr_user_questions', $arr_user_questions)
            ->with('user', $user)
            ->with('jsCat',Session::get('jsCat'));
        }else if(Session::get('jsCheck') == true){
            return View::make('forms/manage_questions')
            ->with('categories', $categories)
            ->with('custom_categories', $custom_categories)
            ->with('arr_user_categories', $arr_user_categories)
            ->with('arr_user_questions', $arr_user_questions)
            ->with('user', $user)
            ->with('jsCheck',Session::get('jsCheck'));
        }else{
            return View::make('forms/manage_questions')
            ->with('categories', $categories)
            ->with('custom_categories', $custom_categories)
            ->with('arr_user_categories', $arr_user_categories)
            ->with('arr_user_questions', $arr_user_questions)
            ->with('user', $user);
        }
    }

    public function postQuestions($user_id = NULL)
    {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #51B7ADjG';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $submit = Input::get('knop');
        if (isset($user_id)) {
            $user = User::find($user_id);
        }

        if ($submit == trans('questions.buttons.category')) {
            return Redirect::to(translate_url('/clients/getCategory/'  . $user_id));
        } elseif ($submit == trans('questions.buttons.question')) {
            return Redirect::to(translate_url('/clients/getQuestion/'  . $user_id));
        } elseif ($submit == trans('questions.buttons.save')) {

            $categories = Input::get('category');

            // first delete all rows.
            CategoryUser::where('user_id', '=', $user_id)->delete();

            if (isset($categories) && is_array($categories)) {

                // add all checked categories
                foreach ($categories as $key => $val) {
                    $cat_user = new CategoryUser;
                    $cat_user->user_id = $user_id;
                    $cat_user->category_id = $key;
                    $cat_user->save();
                }
            }

            $questions = Input::get('questions');

            // first delete all rows.
            CategoryQuestionUser::where('user_id', '=', $user_id)->delete();

            if (isset($questions) && is_array($questions)) {
                // add all checked categories
                foreach ($questions as $key => $val) {

                    foreach ($val as $k => $v) {
                        $category_question_user = new CategoryQuestionUser;
                        $category_question_user->user_id     = $user_id;
                        $category_question_user->category_id = $key;
                        $category_question_user->question_id = $k;
                        $category_question_user->save();
                    }
                }
            }

            return Redirect::to(translate_url('/clients/edit/'  . $user_id));
        } else {
            return Redirect::to(translate_url('/clients'));
        }

    }

    public function getCategory($user_id = NULL, $id = NULL) {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #58jIQE7z';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        if (isset($id)) {
            $category = Category::find($id);
        } else {
            $category = new Category;
        }

        return View::make('forms/category_form')
            ->with('category', $category)
            ->with('user_id', $user_id);
    }

    public function postCategory($user_id = NULL, $category_id = NULL)
    {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #x58Vlc5N';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $user = User::find($user_id);
        if ($user->custom_open_question == 1){

            if ($user->type_of_report == 1){
                // validate
                $rules = array(
                    'name'       => 'required',
                    'description'       => 'required',
                    'custom_open_question'       => 'required',
                    'behavior_custom_open_question'       => 'required',
                );
            }else{
                // validate
                $rules = array(
                    'name'       => 'required',
                    'description'       => 'required',
                    'custom_open_question'       => 'required',
                );
            }
        }else{
            // validate
            $rules = array(
                'name'       => 'required',
                'description'       => 'required',
            );
        }        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to(translate_url('/clients/getCategory'))
                ->withErrors($validator);

        } else {
            $existing_category_id = Input::get('existing_category_id');
            if (isset($existing_category_id)) {
                $category = Category::find($existing_category_id);
            } else {
                $category = new Category;
            }
            $category->name_enUS          = Input::get('name');
            $category->name_esES          = Input::get('name');
            $category->description_enUS   = Input::get('description');
            $category->description_esES   = Input::get('description');
            if ($user->custom_open_question == 1){
                $category->custom_open_question_enUS    = Input::get('custom_open_question');
                $category->custom_open_question_esES    = Input::get('custom_open_question');
                if ($user->type_of_report == 1){
                    $category->behavior_custom_open_question_enUS    = Input::get('behavior_custom_open_question');
                    $category->behavior_custom_open_question_esES    = Input::get('behavior_custom_open_question');
                }
            }
            $category->save();


            if (isset($existing_category_id)) {
                //NOT CREATE RECORD ON CATEGORY_USER TABLE FOR EXISTING CATEGORIES
            } else {
                $category_user = new CategoryUser;
                $category_user->category_id = $category->id;
                $category_user->user_id = $user_id;
                $category_user->save();
            }            

            // redirect
            Session::flash('message', 'Successfully.');

            $submit = Input::get('knop');
            return Redirect::to(translate_url('/clients/getQuestions/' . $user_id))
                                ->with('jsCheck',true);
        }
    }

    public function getQuestion($category_id = NULL, $user_id = NULL, $id = NULL) {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #0Zfx2Ejz';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        if (isset($id)) {
            $question = Question::find($id);
        } else {
            $question = new Question;
        }

        $user_id = Input::get('user_id');
        $user = User::find($user_id);

        return View::make('forms/single_question_form')
            ->with('question', $question)
            ->with('category_id', $category_id)
            ->with('user_id', $user_id)
            ->with('user', $user)
            ->with('jsTabs',Input::get('tab'));
    }

    public function postQuestion($category_id = NULL, $question_id = NULL)
    {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #we8A865a';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $user_id = Input::get('user_id');
        $user = User::find($user_id);
        if ($user->type_of_report == 1){
            // validate
            $rules = array(
                'name'       => 'required',
                'description'       => 'required',
                'behaviorDesc'       => 'required',
            );
        }else{
            // validate
            $rules = array(
                'name'       => 'required',
                'description'       => 'required',
            );
        }        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to(translate_url('/clients/getCategory'))->withErrors($validator);

        } else {
            $question_id = Input::get('question_id');

            if (isset($question_id) && $question_id !=0) {
                //RETRIEVING EXISTING ENTRY ON QUESTION TABLE
                $question = Question::find($question_id);
                $existing_question = true;
                $question->name_enUS                 = Input::get('name');
                $question->name_esES                 = Input::get('name');
                $question->description_enUS          = Input::get('description');
                $question->description_esES          = Input::get('description');
                if ($user->type_of_report == 1){
                    $question->behaviorDesc_enUS    = Input::get('behaviorDesc');
                    $question->behaviorDesc_esES    = Input::get('behaviorDesc');
                }                
                $question->save();
            } else {
                //CREATING NEW ENTRY FOR QUESTION TABLE
                $question = new Question;
                $existing_question = false;
                $question->name_enUS                 = Input::get('name');
                $question->name_esES                 = Input::get('name');
                $question->description_enUS          = Input::get('description');
                $question->description_esES          = Input::get('description');
                if ($user->type_of_report == 1){
                    $question->behaviorDesc_enUS    = Input::get('behaviorDesc');
                    $question->behaviorDesc_esES    = Input::get('behaviorDesc');
                }
                $question->category_id          = Input::get('category_id');
                $question->save();
            }

            if ( $existing_question ) {
                //EXISTING ENTRY, DO NOT CREATE ENTRY ON CategoryQuestionUser TABLE
            } else {
                //NEW ENTRY
                $category_question_user = new CategoryQuestionUser;
                $category_question_user->question_id = $question->id;
                $category_question_user->category_id = Input::get('category_id');
                $category_question_user->user_id = $user_id;
                $category_question_user->save();
            }

            // redirect
            Session::flash('message', 'Successfully.');

            $submit = Input::get('knop');
            if(Input::get('tab') != null){
                return Redirect::to(translate_url('/clients/getQuestions/' . $user_id))->with('jsCat',Input::get('tab'));
            }else{
                return Redirect::to(translate_url('/clients/getQuestions/' . $user_id));
            }
        }
    }

    public function postOrderQuestions() {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #wS9Erh1D';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $this->auto_render=false;

        $items = $_POST['item'];

        $order = 0;
        foreach ($items as $key => $val) {

            // Find question on the customer level.
            $parse_string = explode("#", $val);
            $user_id = $parse_string[0];
            $question_id = $parse_string[1];

            $category_question_user  = CategoryQuestionUser::where('question_id', '=' , $question_id)
                ->where('user_id', '=' , $user_id)
                ->first();

            if (isset($category_question_user)) {
                $category_question_user->order = $key;
                $category_question_user->save();
            }

            $order++;
        }
    }

    public function getCoaches($id = NULL) {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #762y8n1M';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        //ONLY USER_THNK_ADMIN CAN ACCESS

        if (isset($id)) {
            $user = User::find($id);
        }

        $data       = array();
        $list_data  = array();

        // Which colums would you like to have in the table.

        // key => value
        // column-name-in-database => Header in table
        $data['kolommen'] = array(
            'firstname'         => 'First name',
            'lastname'          => 'Last name',
            'email'             => 'E-mail',
        );

        $data['kolommen']['resend_coach_credentials']      = 'Action';

        if (NULL !== (Session::get('user_list')))
                $user_list = Session::get('user_list');
        else {

            $user_list = DB::table('users_roles')
                        ->select('*')
                        ->where('client_id', $user->id)
                        ->where('role_id', Role::USER_CLIENT_COACH)
                        ->join('users', 'users_roles.user_id', '=', 'users.id')
                        ->get();
        }

        // Titel van de pagina
        $data['list']          = $user_list;
        $data['component']     = "";
        $data['action']        = 0;
        $data['controller']    = "";
        $data['titel']         = (int)$id;

        //return
        $list_view =  View::make('generic.lijstscherm', $data);

        return View::make('forms/client_coach_form')
            ->with('user', $user)
            ->with('list_view', $list_view);
    }

    public function postCoaches($id = NULL)
    {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #Wv1D7RIT';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $rules = array(
            'firstname' => 'required|alpha_spaces',
            'lastname'  => 'required|alpha_spaces',
            'email'     => 'required|email',
         );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to(translate_url('/clients/getCoaches/' . $id))
                ->withErrors($validator)
                ->withInput();

        } else {

            $result1 = DB::table('users')
                        ->select('*')
                        ->where('email', '=', rtrim($_POST['email']))
                        ->get();
            $num_rows1 = count($result1);

            if ($num_rows1 > 0) {

                $result2 = DB::table('users_roles')
                            ->select('*')
                            ->where('user_id', '=', $result1[0]->id)
                            ->where('role_id', '=', Role::USER_CLIENT_COACH)
                            ->where('client_id', '=', $id)
                            ->get();
                $num_rows2 = count($result2);

                if ($num_rows2 > 0) {
                    //USER ALREADY EXISTS WITH THE SELECTED ROLE IN THE SELECTED CLIENT
                    Session::flash('csvEmailExists', rtrim($_POST['email']).' is already in the database.');
                    return Redirect::to(translate_url('/clients/getAdmin/' . $id));
                }else{

                    //USER ALREADY EXISTS, DOES NOT HAVE THE SELECTED ROLE IN THE SELECTED CLIENT
                                
                    $existingUser = User::find($result1[0]->id);
                                        
                    //EMAIL WITHOUT PASSWORD
                    $data = array();
                    $data['firstname'] = $existingUser->firstname;
                    $data['email'] = $existingUser->email;
                    $data['pwd'] = 'NOPASSWORD';

                    $client = User::find($id);
                    $typeInvite = 'coach';
                    $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $existingUser);
                    Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
                        $message->to($dataMail['email'], $dataMail['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
                        $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                    });

                    DB::table('users_roles')->insert(
                        array('user_id' => $result1[0]->id, 'role_id' => Role::USER_CLIENT_COACH, 'client_id' => $id)
                    );
                }

            }else{
                $user = new User;

                $user->firstname    = rtrim($_POST['firstname']);
                $user->lastname     = rtrim($_POST['lastname']);
                $user->email        = rtrim($_POST['email']);

                $pwd = substr(md5(rand()), 0, 15);
                if (isset($pwd) && $pwd != '') $user->password = Hash::make($pwd);

                $user->save();

                $data = array();
                $data['firstname'] = $_POST['firstname'];
                $data['email'] = $_POST['email'];
                $data['pwd'] = $pwd;

                $existingUser = new stdClass();
                $client = User::find($id);
                $typeInvite = 'coach';
                $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $existingUser);
                Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
                    $message->to($_POST['email'], $_POST['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
                    $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                });

                DB::table('users_roles')->insert(
                    array('user_id' => $user->id, 'role_id' => Role::USER_CLIENT_COACH, 'client_id' => $id)
                );
            }
            Session::flash('message', 'Successfully created a coach!');
            return Redirect::to(translate_url('/clients/getCoaches/' . $id));
        }
    }

    public function getAdmin($id = null, $user_list = null) {

        $loggedInUser = Auth::user();
        $program_options = array('' => 'Select program') + Program::orderBy('name')->lists('name', 'id');
        $program_invite = array('' => 'User in program') + Program::orderBy('name')->lists('name', 'id');


        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }elseif( $loggedInUser->hasRoleInClient(Role::USER_CLIENT, $id, $loggedInUser->id) ){
            $roleCheck = true;
        }elseif( $loggedInUser->hasRoleInClient(Role::USER_CLIENT_ADMIN, $id, $loggedInUser->id) ){
            $roleCheck = true;
        }elseif( $loggedInUser->hasRoleInClient(Role::USER_CLIENT_COACH, $id, $loggedInUser->id) ){
            $roleCheck = true;
        }else{
            $message = 'Security error #czFbS21f';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        //GETTING CLIENT AS $user
        if (isset($id)) {
            if ( $loggedInUser->hasRole('USER_THNK_ADMIN') ) {
                $user = User::find($id);

            } else {
                if ( $loggedInUser->hasRoleInClient(Role::USER_CLIENT, $id, $loggedInUser->id) && !$loggedInUser->hasRoleInClient(Role::USER_CLIENT_ADMIN, $id, $loggedInUser->id) && !$loggedInUser->hasRoleInClient(Role::USER_PARTICIPANT, $id, $loggedInUser->id) && !$loggedInUser->hasRoleInClient(Role::USER_CLIENT_COACH, $id, $loggedInUser->id) ) {
                    $user = Auth::user();
                } else {
                    $user = User::find($id);
                }
            }

        //NOT LANDING HERE
        } else {
            $message = 'Security error #f06WaH0A';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $data       = array();
        $list_data  = array();

        // Which colums would you like to have in the table.

        // key => value
        // column-name-in-database => Header in table

        $data['kolommen'] = array(
                                    'firstname'         => 'First name',
                                    'lastname'          => 'Last name',
                                    'email'             => 'E-mail',
                                );

        // Based on the role, you get different columns / actions
        if ( $loggedInUser->hasRole('USER_THNK_ADMIN') || $loggedInUser->hasRoleInClient(Role::USER_CLIENT_COACH, $id, $loggedInUser->id) ) {
            $data['kolommen']['test_status'] = trans('table.headers.status');
            $data['kolommen']['edit_profile'] = trans('table.headers.user');
        }

        if ( $loggedInUser->hasRole('USER_THNK_ADMIN') || $loggedInUser->hasRoleInClient(Role::USER_CLIENT, $id, $loggedInUser->id) || $loggedInUser->hasRoleInClient(Role::USER_CLIENT_ADMIN, $id, $loggedInUser->id) || $loggedInUser->hasRoleInClient(Role::USER_CLIENT_COACH, $id, $loggedInUser->id) ) {
            $data['kolommen']['resend_participant_credentials']      = trans('table.headers.pwd');
        }

        // ROLE BOXES ON TABLE (ONLY SUPERADMIN & MANOLO)
        if ( $loggedInUser->hasRole('USER_THNK_ADMIN') || $loggedInUser->id == 1494 ) {
            $data['kolommen']['participantRole'] = trans('table.headers.roles.participant');
            $data['kolommen']['coachRole'] = trans('table.headers.roles.coach');
            $data['kolommen']['clientRole'] = trans('table.headers.roles.admin');
        }

        if (NULL !== (Session::get('user_list'))){
            $user_list = Session::get('user_list');
            $idChecker = true;

        }else {
            $idChecker = true;

            if ( $loggedInUser->hasRoleInClient(Role::USER_CLIENT, $id, $loggedInUser->id) || $loggedInUser->hasRoleInClient(Role::USER_CLIENT_ADMIN, $id, $loggedInUser->id) && !$loggedInUser->hasRoleInClient(Role::USER_CLIENT_COACH, $id, $loggedInUser->id) ) {

                if( $loggedInUser->hasRoleInClient(Role::USER_CLIENT, $id, $loggedInUser->id) || $loggedInUser->hasRoleInClient(Role::USER_CLIENT_ADMIN, $id, $loggedInUser->id) ){
                    if((int)$id == (int)$user->id){
                        $user_list = DB::table('users_roles')
                        ->select('*')
                        ->where('client_id', $user->id)
                        ->join('users', 'users_roles.user_id', '=', 'users.id')
                        ->where('users_roles.role_id', '!=', Role::USER_CLIENT)
                        ->groupBy('users.id')
                        ->get();
                    }else{
                        $idChecker = false;

                    }
                }

            //COACH ROLE EXISTS
            }elseif ( $loggedInUser->hasRoleInClient(Role::USER_CLIENT_COACH, $id, $loggedInUser->id) ) {
                if ($loggedInUser->hasRoleInClient(Role::USER_CLIENT_ADMIN, $id, $loggedInUser->id)){

                    if((int)$id == (int)$user->id){
                        $user_list = DB::table('users_roles')
                        ->select('*')
                        ->where('client_id', $user->id)
                        ->join('users', 'users_roles.user_id', '=', 'users.id')
                        ->where('users_roles.role_id', '!=', Role::USER_CLIENT)
                        ->groupBy('users.id')
                        ->get();
                    }else{
                        $idChecker = false;

                    }

                }else{

                    if($id == $user->id){
                        $user_list = DB::table('users_roles')
                        ->select('*')
                        ->where('client_id', $user->id)
                        ->join('users', 'users_roles.user_id', '=', 'users.id')
                        ->where('users_roles.role_id', '=', Role::USER_PARTICIPANT)
                        ->groupBy('users.id')
                        ->get();
                    }else{
                        $idChecker = false;
                    }

                }

            //THNK ADMIN ONLY
            }elseif ( $loggedInUser->hasRole('USER_THNK_ADMIN') ) {
                $user_list = DB::table('users_roles')
                        ->select('*')
                        ->where('client_id', $user->id)
                        ->join('users', 'users_roles.user_id', '=', 'users.id')
                        ->where('users_roles.role_id', '!=', Role::USER_CLIENT)
                        ->groupBy('users.id')
                        ->get();

            //NONE OF MENTIONED ROLES
            }else{
                $idChecker = false;
            }
        }

        // Titel van de pagina
        $data['list']          = $user_list;
        $data['component']     = "test";
        $data['action']        = 0;
        $data['controller']    =  'mytest';
        $data['titel']         = (int)$id; //sending client id

        $program_selected = Input::get('program_options');
        $program_invite_selected = Input::get('program_invite');

        //return
        $list_view =  View::make('generic.lijstscherm', $data);

        if($idChecker == false){
            $message = 'Security error #gW2M8kLx';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }else{
            return View::make('forms/client_dashboard')
                ->with('user', $user)
                ->with('list_view', $list_view)
                ->with('export_data_url', action('ClientsController@getDataExport', ['id' => $id]))
                ->with('program_options',$program_options)
                ->with('program_invite',$program_invite)
                ->with('program_invite_selected',$program_invite_selected)
                ->with('program_selected',Input::get('program_options'));
        }
    }

    public function postAdmin($id = NULL, $totals = NULL) {

        if (NULL !==(Input::get('id'))){
            $id = (int)Input::get('id');
        }

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }elseif( $loggedInUser->hasRoleInClient(Role::USER_CLIENT, $id, $loggedInUser->id) ){
            $roleCheck = true;
        }elseif( $loggedInUser->hasRoleInClient(Role::USER_CLIENT_ADMIN, $id, $loggedInUser->id) ){
            $roleCheck = true;
        }elseif( $loggedInUser->hasRoleInClient(Role::USER_CLIENT_COACH, $id, $loggedInUser->id) ){
            $roleCheck = true;
        }else{
            $message = 'Security error #2i5vjlvF';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        // check on file.
        if(Input::file('csv_file') != ''){
            $csvErrors = false;
            $emailCSVErrorList = [];
            $stringCSVError = "The following emails are already in the database:<br/>";
            $file = Input::file('csv_file');

            $row = 1;
            if (($handle = fopen($file, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    $num = count($data);
                    $array = explode(',',$data[0]);

                    $array[0] = str_replace('"', "", $array[0]);
                    $array[1] = str_replace('"', "", $array[1]);
                    $array[2] = str_replace('"', "", $array[2]);                    
                    $array[0] = rtrim($array[0]);
                    $array[1] = rtrim($array[1]);
                    $array[2] = rtrim($array[2]);
                    
                    //IF PROGRAM IS SPECIFIED
                    if (isset($array[3])) {
                        $array[3] = str_replace('"', "", $array[3]);
                        $array[3] = rtrim($array[3]);
                    }

                    //Validate firstname
                    if(rtrim ($array[0]) == '' || !preg_match("/[a-zA-Z]{2,60}$/", rtrim($array[0]))) {
                        Session::flash('csvTxtFname', 'Only letters and spaces allowed for the firstname: '.$array[0]);
                        return Redirect::to(translate_url('/clients/getAdmin/' . $id));
                    }
                    //Validate lastname
                    if(rtrim ($array[1]) == '' || !preg_match("/[a-zA-Z]{2,60}$/", rtrim($array[1]))) {
                        Session::flash('csvTxtLname', 'Only letters and spaces allowed for the lastname: '.$array[1]);
                        return Redirect::to(translate_url('/clients/getAdmin/' . $id));
                    }
                    //Validate email
                    if(rtrim ($array[2]) == '' || !filter_var(rtrim($array[2]), FILTER_VALIDATE_EMAIL)) {
                        Session::flash('csvTxtEmail', 'Invalid email format: '.$array[2]);
                        return Redirect::to(translate_url('/clients/getAdmin/' . $id));
                    }

                    if (Input::get('program') == 1){
                        //Validate program
                        if(rtrim ($array[3]) == '' || !preg_match('/^\d+$/', $array[3]) ) {
                            Session::flash('csvTxtProgram', 'Invalid program id: '.$array[3]);
                            return Redirect::to(translate_url('/clients/getAdmin/' . $id));
                        }
                    }
                }
                fclose($handle);
            }

            $row = 1;
            if (($handle = fopen($file, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    $num = count($data);
                    $array = explode(',',$data[0]);

                    $array[0] = str_replace('"', "", $array[0]);
                    $array[1] = str_replace('"', "", $array[1]);
                    $array[2] = str_replace('"', "", $array[2]);                    
                    $array[0] = rtrim($array[0]);
                    $array[1] = rtrim($array[1]);
                    $array[2] = rtrim($array[2]);
                    
                    //IF PROGRAM IS SPECIFIED
                    if (isset($array[3])) {
                        $array[3] = str_replace('"', "", $array[3]);
                        $array[3] = rtrim($array[3]);
                    }


                    if(rtrim($array[0]) != '' && rtrim($array[1]) != '' && rtrim($array[2]) != ''){

                        $result1 = DB::table('users')
                                ->select('*')
                                ->where('email', '=', rtrim($array[2]))
                                ->get();

                        $num_rows1 = count($result1);

                        if ($num_rows1 > 0) {

                            $result2 = DB::table('users_roles')
                                    ->select('*')
                                    ->where('user_id', '=', $result1[0]->id)
                                    ->where('role_id', '=', Role::USER_PARTICIPANT)
                                    ->where('client_id', '=', $id)
                                    ->get();
                            $num_rows2 = count($result2);

                            if ($num_rows2 > 0) {
                                //USER ALREADY EXISTS WITH THE SELECTED ROLE IN THE SELECTED CLIENT
                                array_push($emailCSVErrorList, $result1[0]->email);
                                $csvErrors = true;
                            }else{
                                //USER ALREADY EXISTS, DOES NOT HAVE THE SELECTED ROLE IN THE SELECTED CLIENT
                                
                                $existingUser = User::find($result1[0]->id);
                                
                                //EMAIL WITHOUT PASSWORD
                                $data = array();
                                $data['firstname'] = $existingUser->firstname;
                                $data['email'] = $existingUser->email;
                                $data['pwd'] = 'NOPASSWORD';

                                $client = User::find($id);
                                $typeInvite = 'newSeat';
                                $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $existingUser);
                                Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
                                    $message->to($dataMail['email'], $dataMail['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
                                    $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                                });

                                DB::table('users_roles')->insert(
                                    array('user_id' => $result1[0]->id, 'role_id' => Role::USER_PARTICIPANT, 'client_id' => $id)
                                );
                            }
                        }else{
                            //Only adding new users when the email is new
                            $row++;

                            $user = new User;

                            $pwd = substr(md5(rand()), 0, 15);
                            if (isset($pwd) && $pwd != ''){
                                $user->password = Hash::make($pwd);
                            }

                            $user->firstname = rtrim($array[0]);
                            $user->lastname = rtrim($array[1]);
                            $user->email = rtrim($array[2]);
                            
                            if (Input::get('program') == 1){
                                $user->THNKProgram = $array[3];
                            }

                            $data = array();
                            $data['firstname'] = rtrim($array[0]);
                            $data['pwd'] = $pwd;
                            $data['email'] = rtrim($array[2]);

                            $existingUser = new stdClass();
                            $client = User::find($id);
                            $typeInvite = 'newSeat';
                            $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $existingUser);
                            Mail::send($dataMail['template'], $data, function($message) use ($dataMail, $data) {
                                $message->to($data['email'], $data['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
                                $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                            });

                            $user->save();
                            DB::table('users_roles')->insert(
                                array('user_id' => $user->id, 'role_id' => Role::USER_PARTICIPANT, 'client_id' => $id)
                            );

                        }
                   }
                }
                fclose($handle);
           }
           if($csvErrors){

                foreach ($emailCSVErrorList as $key){
                    $stringCSVError .= $key.'<br/>';
                }

                Session::flash('csvEmailExists', $stringCSVError);
                return Redirect::to(translate_url('/clients/getAdmin/' . $id));
           }else{
                Session::flash('success_save', 'New users successfully added to the database.');
                return Redirect::to(translate_url('/clients/getAdmin/' . $id));
           }

        } else {

            $typeSubmit = Input::get('knop');

            if($typeSubmit == trans('dashboardClient.enroll.csv.button')){

                if (Input::get('program') == 1){
                    //logged as admin id not null
                    $rules = array(
                        'firstname'       => 'required|alpha_spaces',
                        'lastname'        => 'required|alpha_spaces',
                        'email'           => 'required|email',
                        'program_invite'  => 'required|integer'
                     );
                }else{
                    //logged as admin id not null
                    $rules = array(
                        'firstname' => 'required|alpha_spaces',
                        'lastname'  => 'required|alpha_spaces',
                        'email'     => 'required|email'
                     );
                }                

                $validator = Validator::make(Input::all(), $rules);

                if ($validator->fails()) {
                    return Redirect::to(translate_url('/clients/getAdmin/' . $id))
                        ->withErrors($validator)
                        ->withInput();

                } else {

                    $result1 = DB::table('users')
                                ->select('*')
                                ->where('email', '=', rtrim($_POST['email']))
                                ->get();
                    $num_rows1 = count($result1);

                    if ($num_rows1 > 0) {

                        $result2 = DB::table('users_roles')
                                    ->select('*')
                                    ->where('user_id', '=', $result1[0]->id)
                                    ->where('role_id', '=', Role::USER_PARTICIPANT)
                                    ->where('client_id', '=', $id)
                                    ->get();
                        $num_rows2 = count($result2);

                        if ($num_rows2 > 0) {
                            //USER ALREADY EXISTS WITH THE SELECTED ROLE IN THE SELECTED CLIENT
                            Session::flash('csvEmailExists', rtrim($_POST['email']).' is already in the database.');
                            return Redirect::to(translate_url('/clients/getAdmin/' . $id));
                        }else{

                            //USER ALREADY EXISTS, DOES NOT HAVE THE SELECTED ROLE IN THE SELECTED CLIENT
                                
                            $existingUser = User::find($result1[0]->id);
                            
                            //EMAIL WITHOUT PASSWORD
                            $data = array();
                            $data['firstname'] = $existingUser->firstname;
                            $data['email'] = $existingUser->email;
                            $data['pwd'] = 'NOPASSWORD';

                            $client = User::find($id);
                            $typeInvite = 'newSeat';
                            $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $existingUser);
                            Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
                                $message->to($dataMail['email'], $dataMail['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
                                $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                            });

                            DB::table('users_roles')->insert(
                                array('user_id' => $result1[0]->id, 'role_id' => Role::USER_PARTICIPANT, 'client_id' => $id)
                            );
                            if (Input::get('program') == 1){
                                DB::table('users')
                                ->where('id', $result1[0]->id)
                                ->update(array('THNKProgram' => rtrim($_POST['program_invite'])));
                            }
                        }

                    }else{

                        $user = new User;

                        $user->firstname        = rtrim($_POST['firstname']);
                        $user->lastname        = rtrim($_POST['lastname']);
                        $user->email            = rtrim($_POST['email']);
                        if (Input::get('program') == 1){
                            $user->THNKProgram = rtrim($_POST['program_invite']);
                        }

                        $pwd = substr(md5(rand()), 0, 15);
                        if (isset($pwd) && $pwd != '') $user->password = Hash::make($pwd);

                        $user->save();

                        DB::table('users_roles')->insert(
                            array('user_id' => $user->id, 'role_id' => Role::USER_PARTICIPANT, 'client_id' => $id)
                        );

                        $data = array();
                        $data['firstname'] = $_POST['firstname'];
                        $data['email'] = $_POST['email'];
                        $data['pwd'] = $pwd;

                        $existingUser = new stdClass();
                        $client = User::find($id);
                        $typeInvite = 'newSeat';
                        $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $existingUser);
                        Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
                            $message->to($_POST['email'], $_POST['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
                            $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                        });
                    }
                    Session::flash('success_save', 'New users successfully added to the database.');
                    return Redirect::to(translate_url('/clients/getAdmin/' . $id));
                }
                
            } elseif ($typeSubmit == trans('dashboardClient.hasPrograms.button')) {
                $program_selected = Input::get('program_options');

                if($program_selected == ''){
                    return Redirect::to(translate_url('/clients/getAdmin/' . $id));
                }else{
                    $user_list = User::where('users.id', '>', 0)
                        ->join('users_roles', 'users.id', '=', 'users_roles.user_id')
                        ->where('users_roles.role_id', '=', Role::USER_PARTICIPANT)
                        ->where('users.THNKProgram', '=', $program_selected)
                        ->where('users_roles.client_id', '=', $id)
                        ->groupBy('users_roles.user_id')
                        ->get();
                }

                if(isset($id) && $id != ''){
                    return Redirect::to(translate_url('/clients/getAdmin/'.$id))
                            ->with('user_list', $user_list)
                            ->with('program_selected', Input::get('program_options'));
                }else{
                    $message = 'Security error #K6938SiH';
                    return Redirect::to(translate_url('/'))->with(['message' => $message]);
                }

            } elseif ($typeSubmit == trans('dashboardClient.participants.search.button')) {

                $clientId = $id;

                if (isset($_POST['name']) && $_POST['name'] != '') {
                    $name = $_POST['name'];
                    $user_list = DB::table('users')
                        ->where(function($query) use ($name){
                            $query->where('users.firstname', 'LIKE', '%'.$name.'%');
                            $query->orWhere('users.lastname', 'LIKE', '%'.$name.'%');
                            $query->orWhere('users.email', 'LIKE', '%'.$name.'%');
                        })
                        ->join('users_roles', 'users.id', '=', 'users_roles.user_id')
                        ->where('users_roles.role_id', '!=', Role::USER_CLIENT)
                        ->where('users_roles.role_id', '!=', Role::USER_THNK_ADMIN)
                        ->where('users_roles.client_id', '=', $id)
                        ->groupBy('users_roles.user_id')
                        ->get();
                    return Redirect::to(translate_url('/clients/getAdmin/' . $id))->with('user_list', $user_list);
                }
                return Redirect::to(translate_url('/clients/getAdmin/' . $id));

            //COACH ONLY
            } elseif ($typeSubmit == trans('dashboardClient.participants.search.button')) {

                $clientId = $id;

                if (isset($_POST['name']) && $_POST['name'] != '') {
                    $name = $_POST['name'];
                    $user_list = DB::table('users')
                        ->where(function($query) use ($name){
                            $query->where('users.firstname', 'LIKE', '%'.$name.'%');
                            $query->orWhere('users.lastname', 'LIKE', '%'.$name.'%');
                            $query->orWhere('users.email', 'LIKE', '%'.$name.'%');
                        })
                        ->join('users_roles', 'users.id', '=', 'users_roles.user_id')
                        ->where('users_roles.role_id', '=', Role::USER_PARTICIPANT)
                        ->where('users_roles.client_id', '=', $id)
                        ->groupBy('users_roles.user_id')
                        ->get();
                    return Redirect::to(translate_url('/clients/getAdmin/' . $id))->with('user_list', $user_list);
                }
                return Redirect::to(translate_url('/clients/getAdmin/' . $id));

            //THNK ADMIN ONLY
            } elseif ($typeSubmit == trans('dashboardClient.participants.search.button')) {

                $clientId = $id;

                if (isset($_POST['name']) && $_POST['name'] != '') {
                    $name = $_POST['name'];
                    $user_list = DB::table('users')
                        ->where(function($query) use ($name){
                            $query->where('users.firstname', 'LIKE', '%'.$name.'%');
                            $query->orWhere('users.lastname', 'LIKE', '%'.$name.'%');
                            $query->orWhere('users.email', 'LIKE', '%'.$name.'%');
                        })
                        ->join('users_roles', 'users.id', '=', 'users_roles.user_id')
                        ->where('users_roles.role_id', '=', Role::USER_CLIENT_COACH)
                        ->where('users_roles.client_id', '=', $id)
                        ->get();
                    return Redirect::to(translate_url('/clients/getCoaches/' . $id))->with('user_list', $user_list);
                }
                return Redirect::to(translate_url('/clients/getCoaches/' . $id));

            //THNK ADMIN SEARCH
            } elseif ($typeSubmit == trans('adminDashboard.searchTable.button')) {
                if (isset($_POST['name']) && $_POST['name'] != '') {
                    $name = $_POST['name'];
                    $user_list = DB::table('users')
                        ->where(function($query) use ($name){
                            $query->where('users.company', 'LIKE', '%'.$name.'%');
                            $query->orWhere('users.firstname', 'LIKE', '%'.$name.'%');
                            $query->orWhere('users.lastname', 'LIKE', '%'.$name.'%');
                            $query->orWhere('users.email', 'LIKE', '%'.$name.'%');
                        })
                        ->join('users_roles', 'users.id', '=', 'users_roles.user_id')
                        ->where('users_roles.role_id', '=', Role::USER_CLIENT)
                        ->get();
                    return Redirect::to(translate_url('/clients/'))->with('user_list', $user_list);
                }
                return Redirect::to(translate_url('/clients/'));

            }
        }
    }

    public function postClientTestUser(){

        $submit = Input::get('knop');
        $checkIf = Input::get('checkIf');

        if ($submit == trans('profile.buttons.back')){

            $user = Auth::user();

            if ( $user->hasRole('USER_THNK_ADMIN') ) {
                return Redirect::route('clients_index');
            }else{
                return Redirect::route('clients_admin');
            }
        }

        if(isset($_POST['testList'])){
            $id = Input::get('testList');
        }
        if(isset($_POST['id'])){
            $id = Input::get('id');
        }
        if(isset($_POST['client_id'])){
            $clientId = Input::get('client_id');
        }

        $test_id = (int)$id;

        $test = Test::find($test_id);

        $user = Test::find($test_id)->user;

        $loggedUser = Auth::user();

        if( $loggedUser->hasRole('USER_THNK_ADMIN') ) {
            echo '';
        }elseif ( $loggedUser->hasRole('USER_CLIENT_COACH') ) {
            if( !$loggedUser->hasRoleInClient(Role::USER_CLIENT_COACH, $clientId, $loggedUser->id) ){
              return Redirect::to(translate_url('/clients/getAdmin/'.$id))
                  ->withErrors('Security error #R4C9zqRf');
            }
        }else{
            return Redirect::to(translate_url('/clients/getAdmin/'.$id))
                ->withErrors('Security error #rN1F3M6P');
        }

        if(isset($_POST['minimum_required'])){
            $minimum_required = Input::get('minimum_required');
            if ($minimum_required > 1) {
                $test->minimum_required = $minimum_required;
                $test->save();
            }
            //IF NEW MIN_REQ >= AMOUNT COMPLETED, TEST STATUS FULLY FINISHED
            if ($test->amount_completed() >= $test->minimum_required){
                $test->teststatus_id = Teststatus::TEST_FULLY_FINISHED;
                $test->save();
            }
            //IF NEW MIN_REQ < AMOUNT COMPLETED, TEST STATUS FINISHED_BY_USER_OR_RESPONDENT
            if ($test->amount_completed() < $test->minimum_required){
                $test->teststatus_id = Teststatus::FINISHED_BY_USER_OR_RESPONDENT;
                $test->save();
            }
        }

        if($checkIf != null){
            return Redirect::back();
        }

        return View::make('forms/test_view')
            ->with('user_id', $user->id)
            ->with('test_id', $test_id);
    }

    /*
     * Parse all xml files in directory, and add them as participants.
     */
    public function parseOrders() {

        $files = glob("../xml/*xml");

        if (isset($files) && count($files) > 0 && is_array($files)) {
            foreach($files as $filename) {

                $path_parts = pathinfo($filename);
                $xml_file = file_get_contents($filename, FILE_TEXT);

                $xml = new SimpleXMLElement($xml_file);

                $user = new User;

                $user->company       = $xml->participant->company;
                $user->firstname          = $xml->participant->firstname;
                $user->lastname          = $xml->participant->lastname;
                $user->email         = $xml->participant->email;
                $pwd = substr(md5(rand()), 0, 8);
                if (isset($pwd) && $pwd != ''){
                    $user->password      = Hash::make($pwd);
                }

                $user->type             = User::USER_PARTICIPANT;
                $user->user_id          = $xml->participant->user_id;
                //$user->userfunction_id  = 1;

                $user->save();
                DB::table('users_roles')->insert(
                    array('user_id' => $user->id, 'role_id' => Role::USER_PARTICIPANT)
                );

                echo $pwd;

                rename($filename, "../xml/succeeded/" . $path_parts['filename'] . ".xml");
            }
        }
    }

    public function getResendParticipantCredentials ($client_id, $user_id) {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }elseif( $loggedInUser->hasRoleInClient(Role::USER_CLIENT, $client_id, $loggedInUser->id) ){
            $roleCheck = true;
        }elseif( $loggedInUser->hasRoleInClient(Role::USER_CLIENT_ADMIN, $client_id, $loggedInUser->id) ){
            $roleCheck = true;
        }elseif( $loggedInUser->hasRoleInClient(Role::USER_CLIENT_COACH, $client_id, $loggedInUser->id) ){
            $roleCheck = true;
        }else{
            $message = 'Security error #pW9WAblY';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $user = User::find($user_id);

        $pwd = substr(md5(rand()), 0, 15);
        if (isset($pwd) && $pwd != ''){
            $user->password = Hash::make($pwd);
        }

        $user->save();

        $data = array();

        $data['firstname'] = $user->firstname;
        $data['pwd'] = $pwd;
        $data['email'] = $user->email;

        $client = User::find($client_id);
        $typeInvite = 'newSeat';
        $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $user);
        Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
            $message->to($dataMail['email'], $dataMail['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
            $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
        });

        // Redirect back.
        return Redirect::to(translate_url('/clients/getAdmin/' . $client_id))
            ->with('success_credentials', 'New credentials have been sent.');

    }

    public function getResendCoachCredentials ($client_id, $user_id) {

        $loggedInUser = Auth::user();

        //SECURITY CHECK
        $roleCheck = false;
        if($loggedInUser->hasRole('USER_THNK_ADMIN')){
            $roleCheck = true;
        }else{
            $message = 'Security error #AIh1hsph';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $user = User::find($user_id);

        $pwd = substr(md5(rand()), 0, 15);
        if (isset($pwd) && $pwd != ''){
            $user->password = Hash::make($pwd);
        }
        $user->save();

        $data = array();

        $data['firstname'] = $user->firstname;
        $data['email'] = $user->email;
        $data['pwd'] = $pwd;
        
        $client = User::find($client_id);
        $typeInvite = 'coach';
        $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $user);
        Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
            $message->to($dataMail['email'], $dataMail['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
            $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
        });

        // Redirect back.
        return Redirect::to(translate_url('/clients/getCoaches/' . $client_id))
            ->with('success_credentials', 'New credentials have been sent.');

    }

    public function getDataExport($clientId)
    {
        $loggedInUser = Auth::user();

        // Verify the logged in user has access
        if( ! ($loggedInUser->hasRole('USER_THNK_ADMIN') ||
            $loggedInUser->hasRoleInClient(Role::USER_CLIENT, $clientId, $loggedInUser->id) ||
            $loggedInUser->hasRoleInClient(Role::USER_CLIENT_ADMIN, $clientId, $loggedInUser->id) ||
            $loggedInUser->hasRoleInClient(Role::USER_CLIENT_COACH, $clientId, $loggedInUser->id)
        )) {
            return Redirect::to(translate_url('/'))->with(['message' => 'Security error #czFkS45g']);
        }

        return View::make('forms.export_data')
            ->with('formActionURL', action('DataExportController@downloadAsExcel'))
            ->with('client', User::find($clientId))
            ->with('users', User::allForClient($clientId))
            ->with('program_options',['' => 'Select program'] + Program::orderBy('name')->lists('name', 'id'));
    }
}
