<?php
class PdfController extends WebsiteController {

    //TODO: Save path as global variable by client id

    public function preview() {
      $loggedInUser = Auth::user();

      //SECURITY CHECK
      $roleCheck = false;
      if($loggedInUser->hasRole('USER_THNK_ADMIN')){
          $roleCheck = true;
      }else{
          $message = 'Security error #D3qD0bll';
          return Redirect::to(translate_url('/'))->with(['message' => $message]);
      }

      $pathToFile = '../app/helpers/preview.pdf';
      return Response::download($pathToFile, 'template-preview.pdf', array('content-type' => 'application/pdf'));

    }

    public function selfReport() {

      $loggedUser = Auth::user();
      $testSelfId = Input::get('generate_own_report_id');
      $client_id = Input::get('client_id');
      $testSelf = Test::find($testSelfId);

      if ($loggedUser->hasRoleInClient(Role::USER_CLIENT_COACH, $client_id, $loggedUser->id) && !$loggedUser->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, $loggedUser->id) ){
            echo '';
      }elseif($loggedUser->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, $loggedUser->id)){
            echo '';
      }elseif($loggedUser->hasRole('USER_THNK_ADMIN')){
            echo '';
      }else{
        $message = 'Security error #v83916Tu';
        return Redirect::to(translate_url('/'))->with(['message' => $message]);
      }

      $tests = DB::table('tests')
        ->select('*')
        ->where('id', '=', $testSelfId)
        ->get();

      $month = explode('-',$tests[0]->created_at);
      $path = explode(' ',$tests[0]->created_at);

      $user = DB::table('users')
        ->select('*')
        ->where('id', '=', $tests[0]->user_id)
        ->get();

      if($client_id != 0){
        $client = User::find($client_id);
        $logo =$client->logo_name;
      }

      $GLOBALS['logoName'] = $logo;
      $user_id = Input::get('user_id');

      $id = $user[0]->id;
      $pdfArray = App::make('TestController')->postSelfReport(Input::get('generate_own_report_id'));

      if(!empty($pdfArray)){
        $user = User::find($id);
        $cred = [];
        $cred[0] = $user->firstname;
        $cred[1] = $user->lastname;
        require_once('../app/helpers/mypdf.php');

        $pdf = new PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        include('../app/helpers/selfPdfGenerator.php');
        setting($pdf);

        $pathLayout = $client->getUserPDF('full', $client->THNKParticipant, $client->id);
        
        firstPages($pdf,$cred,$month[1],$pathLayout,$client->id,$user->id);

        $pathLayoutOv = $client->getUserPDF('self', $client->THNKParticipant, $client->id);
        
        overallPage($pdf,$pdfArray,$cred,$pathLayoutOv);
        
        foreach ($pdfArray as $x) {
          createPage($pdf,$pathLayout,$x['name'],$x['explanation'],$x['questions'],$x['comments'],$x['personal_score'],$cred,$x['comments_title']);
        }
        lastPages($pdf,$cred,$pathLayout,$client->id,$user->id);
        closePDF($pdf,$cred,$path[0]);
      }else{
            return View::make('generic/closeTab');
      }
    }

 public function fullReport($generate_report_id = null, $user_id = null, $save_to_disk = false){

    if(!$save_to_disk){
      $loggedUser = Auth::user();
      $testSelfId = Input::get('generate_report_id');
      $client_id = Input::get('client_id');
      $testSelf = Test::find($testSelfId);

      if ($loggedUser->hasRoleInClient(Role::USER_CLIENT_COACH, $client_id, $loggedUser->id) && !$loggedUser->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, $loggedUser->id) ){
            echo '';
      }elseif($loggedUser->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, $loggedUser->id)){
            echo '';
      }elseif($loggedUser->hasRole('USER_THNK_ADMIN')){
            echo '';
      }else{
        $message = 'Security error #v83916Tu';
        return Redirect::to(translate_url('/'))->with(['message' => $message]);
      }

    }

     if (isset($generate_report_id) && is_numeric($generate_report_id)) {
         $tests =      DB::table('tests')
              ->select('*')
              ->where('id', '=', $generate_report_id)
              ->get();
     } else {
        $tests =      DB::table('tests')
              ->select('*')
              ->where('id', '=', $testSelfId)
              ->get();

        $generate_report_id = $testSelfId;
     }

     if (!isset($user_id) || !is_numeric($user_id)) {
         $user_id = Input::get('user_id');
     }

      $month = explode('-',$tests[0]->created_at);
      $path = explode(' ',$tests[0]->created_at);

      $user =      DB::table('users')
              ->select('*')
              ->where('id', '=', $tests[0]->user_id)
              ->get();

      //SEARCH FOR CLIENT
      $mainTest = Test::find($generate_report_id);
      if($mainTest->client_id != 0){

          $clientOfTest = User::find($mainTest->client_id);

          $logo =$clientOfTest->logo_name;

      }

      $GLOBALS['logoName'] = $logo;
      $id = $user[0]->id;
      $pdfArray = App::make('TestController')->postFullReport($generate_report_id, $user_id);

      if(!empty($pdfArray)){

        $user = User::find($id);
        $cred = [];
        $cred[0] = $user->firstname;
        $cred[1] = $user->lastname;
        
        $no_split_resp = false; //so, split_resp == 1
        if( $clientOfTest->split_resp !=1 ){
          $no_split_resp = true; //so, split_resp == 0
        }
        
        require_once('../app/helpers/mypdf.php');
        $pdf = new PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        include('../app/helpers/fullPdfGenerator.php');
        setting($pdf);

        $pathLayout = $clientOfTest->getUserPDF('full', $clientOfTest->THNKParticipant, $clientOfTest->id);

        firstPages($pdf,$cred,$month[1],$pathLayout,$clientOfTest->id,$user->id);

        $pathLayoutOv = $pathLayout;

        overallPage($pdf,$pdfArray,$cred,$no_split_resp,$pathLayoutOv);
        
        foreach ($pdfArray as $x) {
          createPage($pdf,$pathLayout,$x['name'],$x['explanation'],$x['questions'],$x['comments'],$x['personal_score'],$x['respondents_score'],$cred,$no_split_resp,$x['comments_title']);
        }
        lastPages($pdf,$cred,$pathLayout,$clientOfTest->id,$user->id);

        if ($save_to_disk) {
            return $pdf;
        } else {
            closePDF($pdf,$cred,$path[0]);
        }
      }else{
            return View::make('generic/closeTab');
      }
    }
}
