<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use THNK\Mirror\DataAnalysis\DataAnalysis;
use THNK\Mirror\DataAnalysis\QueryArguments;
use THNK\Mirror\DataAnalysis\Writers\ExcelWriter;

class DataExportController extends BaseController
{
	/**
	 * @var \THNK\Mirror\DataAnalysis\Writers\ExcelWriter
	 */
	private $excelWriter;

	/**
	 * @param \THNK\Mirror\DataAnalysis\Writers\ExcelWriter $excelWriter
	 */
	public function __construct(ExcelWriter $excelWriter)
	{
		$this->excelWriter = $excelWriter;
	}

	/**
	 * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
	 * @throws \THNK\Mirror\DataAnalysis\InvalidQueryArgumentsValue
	 * @throws \PHPExcel_Reader_Exception
	 * @throws \InvalidArgumentException
	 */
	public function downloadAsExcel()
	{
		// Validate input
		if ( ! $this->isValidInput(Input::all()))
		{
			return Redirect::back()->withErrors($this->getInputValidator(Input::all()));
		}

		// Validate access
		if ( ! $this->userHasAccess(Input::all()))
		{
			return Redirect::back()->withErrors([
				'auth' => 'You are not authorized to access this data.',
			]);
		}

		// Get query
		$arguments = $this->getQueryArgumentsForInput();

		// Return the excel
		return $this->downloadQueryArgumentsAsExcel($arguments);
	}

	/**
	 * @param array $input
	 *
	 * @return bool
	 */
	private function isValidInput(array $input)
	{
		return $this->getInputValidator($input)->passes();
	}

	/**
	 * @param array $input
	 *
	 * @return \Illuminate\Validation\Validator
	 */
	private function getInputValidator(array $input)
	{
		return Validator::make($input, [
			'type'       => 'required|in:users,program',
			'client_id'  => 'required|exists:users,id',
			'program_id' => 'required_if:type,program|exists:programs,id',
			'user_ids'   => 'required_if:type,users|',
			'filters'    => '',
		]);
	}

	/**
	 * @param array $input
	 *
	 * @return bool
	 * @throws \InvalidArgumentException
	 */
	private function userHasAccess(array $input)
	{
		$exportType = $input['type'];

		switch ($exportType)
		{
			case 'users':				

				$loggedInUser = Auth::user();

				// Validate the user is authed as the superadmin
				if($loggedInUser->hasRole('USER_THNK_ADMIN')){
					
					return true;
				} else {
					// Validate the user is authed as the admin of the current client
					$clientId = $this->getCurrentUserClient()->getId();

					return $this->userIsAdminOfClient($clientId);
				}
				

			case 'program':
				return $this->userHasAccessToClient($input['client_id']);
		}

		throw new InvalidArgumentException('Invalid request type');
	}

	/**
	 * @param \THNK\Mirror\DataAnalysis\QueryArguments $arguments
	 *
	 * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
	 * @throws \PHPExcel_Reader_Exception
	 */
	private function downloadQueryArgumentsAsExcel(QueryArguments $arguments)
	{
		$analysis = DataAnalysis::createForQuery($arguments);
		$file = $this->excelWriter->writeToDisk($analysis, $arguments);

		return Response::download($file->getRealPath(), $file->getFilename());
	}

	/**
	 * @return \THNK\Mirror\DataAnalysis\QueryArguments
	 * @throws \THNK\Mirror\DataAnalysis\InvalidQueryArgumentsValue
	 */
	private function getQueryArgumentsForInput()
	{
		$type = Input::get('type');
		$clientId = Input::get('client_id');
		$programId = Input::get('program_id', 0);
		$filters = Input::get('filters');

		$arguments = new QueryArguments((int) $clientId);
		if ($type === 'program' && $programId)
		{
			$arguments->setProgramId((int) $programId);
		}

		if ($type === 'users' && Input::get('user_ids'))
		{
			$arguments->setUserIds(explode(',', Input::get('user_ids')));
		}

		// Split the filters by the , sign and check each of them
		foreach (explode(',', $filters) as $filter)
		{
			switch ($filter)
			{
				case 'completed':
					$arguments->onlyCompleted(true);
					break;

				case 'totals':
					$arguments->onlyTotals(true);
					break;
			}
		}

		return $arguments;
	}

	/**
	 * @param int $clientId
	 *
	 * @return bool
	 */
	private function userIsAdminOfClient($clientId)
	{
		/** @var \User $user */
		$user = Auth::user();

		return $user->hasRole('USER_THNK_ADMIN') || $user->hasRoleInClient(Role::USER_THNK_ADMIN, $clientId) || $user->hasRoleInClient(Role::USER_CLIENT_ADMIN, $clientId);
	}

	/**
	 * @param int $clientId
	 *
	 * @return bool
	 */
	private function userHasAccessToClient($clientId)
	{
		/** @var \User $user */
		$user = Auth::user();

		return $user->hasRole('USER_THNK_ADMIN') || $user->hasRoleInClient(Role::USER_CLIENT, $clientId) || $user->hasRoleInClient(Role::USER_CLIENT_ADMIN, $clientId);
	}

	/**
	 * @return \THNK\Mirror\Client\Client
	 */
	private function getCurrentUserClient()
	{
		/** @var \User $user */
		$user = Auth::user();

		return $user->getClient();
	}
}