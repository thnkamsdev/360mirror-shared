<?php

class DashboardController extends WebsiteController {

    public function getIndex ( $client_id = null, $id = null)
    {

        $loggedUser = Auth::user();
        $user = User::find($client_id); //finding client

        if ( $loggedUser->hasRoleInClient(Role::USER_PARTICIPANT, $user->id, $loggedUser->id ) ) {

            $selected_test = array();
            $respondents = array();

            if($id != null){
              $selected_test = Test::find($id);
              $respondents = Respondent::getRespondentsByMainTest($id);
            }else{
              $selected_test = Test::getLatestTestByUser($loggedUser->id, $client_id);
              $respondents = Respondent::getRespondentsByMainTest($selected_test->id);
            }
            
            $data   = array();

            $endeavor = Endeavor::getEndeavorByUser($loggedUser->id);

            return View::make('forms/dashboard', $data)
                    ->with('user', $user)
                    ->with('participant', $loggedUser)
                    ->with('respondents', $respondents)
                    ->with('selected_test', $selected_test)
                    ->with('endeavor', $endeavor);

        }else{
            $message = 'Security error #Nh8Zv54e';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }
    }


    public function postIndex ($client_id = null, $id = null)
    {
      if (Auth::user()->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, Auth::user()->id)){
          $user = Auth::user();
      }elseif (Auth::user()->hasRole('USER_THNK_ADMIN')){
          $user_id = $_POST["user_id"];
          $user = User::find($user_id);
          if($client_id == '{client_id}'){
            if(isset($_POST['test_id_reminder'])){
              $respondent_test = Test::find($_POST['test_id_reminder']);
            }else{
              $respondent_test = Test::find($_POST['test_id_delete']);
            }
            $client_id = $respondent_test->client_id;
            if($client_id == 0){
              $participant_test = Test::find($respondent_test->test_id);
              $client_id = $participant_test->client_id;
            }
          }
      }else{
          $message = 'Security error #0TD2YNOg';
          return Redirect::to(translate_url('/'))->with(['message' => $message]);
      }

      //REMOVE USER
      if (isset($_POST['test_id_delete'])){
        $test_reminder = Test::find($_POST['test_id_delete']);
        $respondent = $test_reminder->respondent;
        DB::table('respondents')
          ->where('id', $respondent->id)
          ->delete();
        DB::table('tests')
          ->where('respondent_id', $respondent->id)
          ->delete();
      
      } else {
        //SEND REMINDER

        $test_reminder = Test::find($_POST['test_id_reminder']);
        $respondent = $test_reminder->respondent;

        /*
        * Send invite
        */
        $data = array();
        $data['md5_email'] = md5($respondent->email);
        $data['respondent'] = $respondent;
        $data['user'] = $user;
        $data['test'] = $test_reminder;
        $data['client_id'] = $client_id;

        if (Auth::user()->hasRole('USER_PARTICIPANT') ){
          $user = Auth::user();
        }else{
          $user = User::find($_POST['user_id']);
        }
        $client = User::find($client_id);
        $typeInvite = 'general';
        $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $user);
        Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
          $test_reminder = Test::find($_POST['test_id_reminder']);
          $respondent = $test_reminder->respondent;
          $message->to($respondent->email, $respondent->name)->from($dataMail['from'], $dataMail['firstname'])->replyTo($dataMail['email'], $dataMail['firstname'])->subject($dataMail['subject']);
          $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
        });

        $test_reminder->last_reminder = date("Y-m-d H:i:s");
        $test_reminder->save();
      }

      if (Auth::user()->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, Auth::user()->id)){
          return Redirect::to(translate_url('/dashboard/index/' . $client_id .'/'. $test_reminder->test_id));
      }else{
          return Redirect::to(translate_url('/test/myprofile/' . $client_id .'/'. $_POST['user_id']));
      }
        
    }
}
