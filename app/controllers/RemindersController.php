<?php

class RemindersController extends Controller {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind() {

		$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		if (false !== strpos($url,'/thnk')) {
		    return View::make('forms/password_remind_thnk');
		} elseif (false !== strpos($url,'/impact')) {
		    return View::make('forms/password_remind_storypanda');
		} elseif (false !== strpos($url,'/questmirror')) {
		    return View::make('forms/password_remind_thnkv3');
		} elseif (false !== strpos($url,'/endeavorscan')) {
		    return View::make('forms/password_remind_thnkv3_endeavor');
		}else{
			return View::make('forms/password_remind');
		}
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		//$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		$rules = array(
            'email' => 'required|email'
            );

		// Validation
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()){

			return Redirect::to(translate_url('/password/reset'))
                ->withErrors($validator)
                ->withInput();
        
        } else {

        	Password::remind(Input::only('email'), function($message)
			{
			    $message->subject('360 Mirror Password Reset');
			});

			return Redirect::to(translate_url('/password/reset'))->with('success','1');
        }
	}
	public function postRemindTHNK()
	{
		//$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		$rules = array(
            'email' => 'required|email'
            );

		// Validation
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()){

			return Redirect::to(translate_url('/password/reset/thnk'))
                ->withErrors($validator)
                ->withInput();
        
        } else {

        	Password::remind(Input::only('email'), function($message)
			{
			    $message->subject('360 Mirror Password Reset');
			});

			return Redirect::to(translate_url('/password/thnk'))->with('success','1');
        }
	}
	public function postRemindImpact()
	{
		//$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		$rules = array(
            'email' => 'required|email'
            );

		// Validation
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()){

			return Redirect::to(translate_url('/password/reset/impact'))
                ->withErrors($validator)
                ->withInput();
        
        } else {

        	Password::remind(Input::only('email'), function($message)
			{
			    $message->subject('360 Mirror Password Reset');
			});

			return Redirect::to(translate_url('/password/impact'))->with('success','1');
        }
	}
	public function postRemindQM()
	{
		$rules = array(
            'email' => 'required|email'
            );

		// Validation
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()){
			return Redirect::to(translate_url('/password/reset/questmirror'))
                ->withErrors($validator)
                ->withInput();        
        } else {
        	Password::remind(Input::only('email'), function($message)
			{
			    $message->subject('Quest Mirror Password Reset');
			});
			return Redirect::to(translate_url('/password/questmirror'))->with('success','1');
        }
	}
	public function postRemindES()
	{
		$rules = array(
            'email' => 'required|email'
            );

		// Validation
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()){
			return Redirect::to(translate_url('/password/reset/endeavorscan'))
                ->withErrors($validator)
                ->withInput();        
        } else {
        	Password::remind(Input::only('email'), function($message)
			{
			    $message->subject('Endeavor Scan Password Reset');
			});
			return Redirect::to(translate_url('/password/endeavorscan'))->with('success','1');
        }
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);

		$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		if (false !== strpos($url,'/thnk')) {
		    return View::make('forms/password_reset_thnk')->with('token', $token);
		} elseif (false !== strpos($url,'/impact')) {
		    return View::make('forms/password_reset_storypanda')->with('token', $token);
		} elseif (false !== strpos($url,'/questmirror')) {
		    return View::make('forms/password_request_thnkv3')->with('token', $token);
		} elseif (false !== strpos($url,'/endeavorscan')) {
		    return View::make('forms/password_request_thnkv3_endeavor')->with('token', $token);
		}else{
			return View::make('forms/password_reset')->with('token', $token);
		}
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{

		$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		$rules = array(
            'email'             	 =>  'required|email', //required|email|unique:users for new users
            'password'         		 =>  'required|min:8',
            'password_confirmation'  =>  'required|same:password'
            );

		// Validation
        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()){

        	return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        
        } else {            
            
           	$credentials = Input::only(
				'email', 'password', 'password_confirmation', 'token'
			);
                
			$response = Password::reset($credentials, function($user, $password)
			{
				$user->password = Hash::make($password);

				$user->save();
			});
                

			switch ($response)
			{
				case Password::INVALID_PASSWORD:
				case Password::INVALID_TOKEN:
				case Password::INVALID_USER:
					return Redirect::back()->with('error', Lang::get($response));

				case Password::PASSWORD_RESET:

					$message = 'Your password has been reset.';

					if (false !== strpos($url,'/thnk')) {
					    return Redirect::to(translate_url('/thnk'))->with(['message' => $message]);
					} elseif (false !== strpos($url,'/impact')) {
					    return Redirect::to(translate_url('/impact'))->with(['message' => $message]);
					} elseif (false !== strpos($url,'/questmirror')) {
					    return Redirect::to(translate_url('/questmirror'))->with(['message' => $message]);
					} elseif (false !== strpos($url,'/endeavorscan')) {
					    return Redirect::to(translate_url('/endeavorscan'))->with(['message' => $message]);
					}else{
						return Redirect::to(translate_url('/'))->with(['message' => $message]);
					}
			}
                        
        }
	}

}
