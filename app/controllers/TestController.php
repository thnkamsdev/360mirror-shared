<?php

class TestController extends WebsiteController {


    public function getTest($client_id, $test_id = null) {


        $loggedInUser = Auth::user();

        if($test_id != NULL){
            $selected_test = Test::find($test_id);
            $userOwner = User::find($selected_test->user_id);

            if($loggedInUser->id != $userOwner->id){
                $message = 'Security error #42A184aF';
                return Redirect::to(translate_url('/'))->with(['message' => $message]);
            }

            if($selected_test->teststatus_id > Teststatus::JUST_STARTED){
                return Redirect::to(translate_url('/dashboard'))
                    ->withErrors('Your test is already finished.');
            }
        }

        $user =  User::find($client_id); //finding the client

        // Check if the user is allowed to create a test
        if ($user->test_per_user == 1 // unlimited tests per user
                || ($user->test_per_user == 2
                        && count(Test::getTestsByUser($loggedInUser->id, $client_id)) == 0)
                  || ($user->test_per_user == 2
                        && count(Test::getTestsByUser($loggedInUser->id, $client_id)) == 1
                        && $test_id > 0)) {

            $test = Test::getTest($test_id);

            //Check if test exists
            if(isset($test->created_at)){
                $intro = false;
            }else{
                $intro = true;
            }

            if ($test->category_id > 0) {
                $category = Category::find($test->category_id);
            } else {
                $category = $loggedInUser->getFirstCategory($user->id);
            }

            $firstCategory = $loggedInUser->getFirstCategory($user->id);
            $lastCategory = $loggedInUser->getLastCategory($user->id);
            $progress = $loggedInUser->getProgress($category->id, $user->id);
            $total_numbers_of_categories = $loggedInUser->getNumberOfCategories($user->id);
            $category_question_users = $loggedInUser->get_category_questions($category->id, $user->id);
            $endeavor = Endeavor::getEndeavorByUser($loggedInUser->id);           
            $introTemplatePath = $user->getTestIntro($user->id);
            $templatePath = $user->getTestSteps($user->id);
            $final_endeavor = false;

            if($intro == true){
                return View::make($introTemplatePath)
                ->with('user', $user) //client
                ->with('participant', $loggedInUser) //participant
                ->with('test', $test)
                ->with('test_id', $test->id)
                ->with('category', $category)
                ->with('progress', $progress)
                ->with('total_numbers_of_categories', $total_numbers_of_categories)
                ->with('firstCategory', $firstCategory)
                ->with('lastCategory', $lastCategory)
                ->with('color',$user->colorMain)
                ->with('category_question_users', $category_question_users)
                ->with('test', $test)
                ->with('intro', $intro)
                ->with('endeavor', $endeavor);
            }else{
                return View::make($templatePath)
                ->with('user', $user) //client
                ->with('participant', $loggedInUser) //participant
                ->with('test', $test)
                ->with('test_id', $test->id)
                ->with('category', $category)
                ->with('progress', $progress)
                ->with('total_numbers_of_categories', $total_numbers_of_categories)
                ->with('firstCategory', $firstCategory)
                ->with('lastCategory', $lastCategory)
                ->with('color',$user->colorMain)
                ->with('category_question_users', $category_question_users)
                ->with('test', $test)
                ->with('intro', $intro)
                ->with('endeavor', $endeavor);
            }

         } else {

            return View::make('forms/test/not_allowed');
        }

    }

    public function postTest($client_id)
    {

        $participant = Auth::user();

        //ENDEAVOR DECISSION TREE HACKED
        // Check submit button
        $button = Input::get('knopEndeavor');
        if ($button == trans('test.buttons.newEndeavor') || $button == trans('test.buttons.oldEndeavor')) {
            return Redirect::to(translate_url('/endeavor/' . $client_id . '/' . $participant->id));
        }

        $test_id = Input::get('test_id');

        if (Input::has('test_id') && $test_id > 0) {
            $test = Test::find($test_id);
        } else {
            // test not created yet. do so:
            $test = new Test();
            $test->teststatus_id = Teststatus::JUST_STARTED;
            $test->user_id = $participant->id;
            $test->client_id = $client_id;
        }

        $test->save();
        $test_id = $test->id;

        // Save all questions.
        $questions = Input::get('question');
        if (is_array($questions)) {
            foreach ($questions as $key => $val) {

                $categoryQuestionUserTest = CategoryQuestionUserTest::getCategoryQuestionUserTest($key, $test_id);
                $categoryQuestionUserTest->value = $val;
                $categoryQuestionUserTest->test_id = $test_id;
                $categoryQuestionUserTest->question_id = $key;
                $categoryQuestionUserTest->save();
            }
        }

        // Optional Comment field
        $category_id = Input::get('category_id');
        if (isset($category_id) && $category_id > 0) {
            $commentfield = Input::get('commentfield');
            if (isset($category_id)) {
                $categoryQuestionUserTest = CategoryQuestionUserTest::getCategoryQuestionUserTestOpenQuestion($category_id, $test_id);
                $categoryQuestionUserTest->category_id  = $category_id;
                $categoryQuestionUserTest->test_id      = $test_id;
                //Escaping string
                $escapedCommentField = str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('', '', '', '', '', '', ''), $commentfield);
                $categoryQuestionUserTest->commentfield = $escapedCommentField;
                $categoryQuestionUserTest->save();
            }
        }

        // Save the current state / current category.
        $test->category_id = $category_id;
        $test->save();

        // Check submit button
        $button = Input::get('knop');
        if ($button == trans('test.buttons.next') || $button == trans('test.buttons.finish')) {
            // store the next category

            // Check if this is the last category.
            if ($participant->getLastCategory($client_id)->id == $category_id) {

                $test->teststatus_id = Teststatus::FINISHED_BY_USER_OR_RESPONDENT;
                
                $client =  User::find($client_id);
                $minReq = $client->getMinReq($client->id);
                $test->minimum_required = $minReq;

                $test->save();

                return Redirect::to(translate_url('/test/invite_respondents/' .$client_id. '/' . $test->id));
            }

            if (isset($client_id) && $client_id > 0) {
                $client =  User::find($client_id);
                $categories = $client->categories;
            } else {
                $categories = $participant->categories;
            }

            $hit = false;
            foreach ($categories as $category) {
                if ($hit) {
                    $test->category_id = $category->id;
                    $test->save();
                    break;
                }
                if ($category->id == $category_id) {
                    $hit = true;
                }
            }
        }
        if ($button == trans('test.buttons.back')) {
            // store the next category
            if (isset($client_id) && $client_id > 0) {
                $client =  User::find($client_id);
                $categories = $client->categories;
            } else {
                $categories = $participant->categories;
            }

            $hit = false;
            $previous_category_id = 0;
            foreach ($categories as $category) {

                if ($category->id == $category_id)
                    $hit = true;

                if ($hit) {
                    $test->category_id = $previous_category_id;
                    $test->save();
                    break;
                }

                $previous_category_id = $category->id;
            }
        }

        return Redirect::to(translate_url('/test/edit/' . $client_id . '/' . $test->id));

    }

    public function getInviteRespondents($client_id, $test_id) {

        $loggedInUser = Auth::user();
        $user = User::find($client_id);

        if ( !$loggedInUser->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, $loggedInUser->id) ){
            $message = 'Security error #N8b8F3bO';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        $test = Test::getTest($test_id);

        if ($test->category_id > 0) {

            $category = Category::find($test->category_id);

        } else {

            $category = $user->getFirstCategory($client_id);

        }

        $firstCategory = $loggedInUser->getFirstCategory($client_id);
        $progress = $loggedInUser->getProgress($category->id, $client_id);
        $total_numbers_of_categories = $user->getNumberOfCategories($client_id);

        $category_question_users = CategoryQuestionUser::join('questions', 'category_question_user.question_id', '=', 'questions.id')
            ->select('category_question_user.*')
            ->where('category_question_user.category_id', '=', $category->id)
            ->where('user_id', '=', $client_id)
            ->get();

        $respondents = Respondent::getRespondentsByMainTest($test_id);

        return View::make('forms/test/invite_form')
            ->with('user', $user) //client
            ->with('participant', $loggedInUser) //participant
            ->with('test', $test)
            ->with('test_id', $test->id)
            ->with('category', $category)
            ->with('progress', $progress)
            ->with('total_numbers_of_categories', $total_numbers_of_categories)
            ->with('firstCategory', $firstCategory)
            ->with('category_question_users', $category_question_users)
            ->with('respondents',$respondents)
            ->with('minimum_required',$test['attributes']['minimum_required'])
            ->with('test', $test);
    }

    public function postInviteRespondents($client_id, $test_id) {

        $email = Input::get('email');
        $name = Input::get('name');
        $respondentgroup = Input::get('respondentgroup');
        $use_mail = Input::get('use_email');
        $custom_mail = Input::get('custom_email');

        // Validation
        $rules = array();

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()){
            return Redirect::to(translate_url('/test/invite_respondents/' .$client_id. '/' . $test_id))
                ->withErrors($validator)
                ->withInput();

        } else {

            if (isset($respondentgroup) && is_array($respondentgroup)) {

                foreach ($respondentgroup as $key => $val) {

                    if (isset($name[$key]) && isset($email[$key]) && $name[$key] != '' && $email[$key] != '') {
                        $respondent = New Respondent();
                        $respondent->name = $name[$key];
                        $respondent->email = $email[$key];
                        $respondent->respondentgroup_id = $val;
                        $respondent->save();
                        if($use_mail == 1)
                            $respondent->create_test($client_id, $test_id, 0);
                        else
                            $respondent->create_test($client_id, $test_id, 0,nl2br($custom_mail[$key]));
                    }

                }
            }

        }
        return Redirect::to(translate_url('/dashboard/index/' .$client_id. '/' . $test_id));
    }

    public function getTestRespondent($test_id = null, $md5 = '') {

        $test = Test::getTest($test_id);

        if (isset($test_id) && $md5 == md5($test->respondent->email)) {

            $test = Test::getTest($test_id);
            $mainTest = Test::getTest($test->test_id);

            if ($test->teststatus_id == Teststatus::JUST_STARTED
                    || $test->teststatus_id == Teststatus::NOT_STARTED) {

                $user = $test->getTest($test->test_id)->user; //finding participant owner of test

                if ($test->category_id > 0) {
                    $category = Category::find($test->category_id);
                } else {
                    $category = $user->getFirstCategory($mainTest->client_id);
                }

                $firstCategory = $user->getFirstCategory($mainTest->client_id);
                $lastCategory = $user->getLastCategory($mainTest->client_id);
                $progress = $user->getProgress($category->id, $mainTest->client_id);
                $total_numbers_of_categories = $user->getNumberOfCategories($mainTest->client_id);

                $category_question_users = $user->get_category_questions($category->id, $mainTest->client_id);

                $client = User::find($mainTest->client_id);
                $respPath = $client->getRespTemplate($client->THNKParticipant, $client->id);

                //LOADING ENDEAVOR PROJECT FOR ENDEAVOR CLIENT ON RESPONDENT TEST VIEW
                if (\Route::current()->getName() == 'feedback_intro_get') {
                    $endeavorProject = Endeavor::getEndeavorByUser($user->id);
                    $introRespTemplatePath = $user->getRespondentIntro($client->id);
                    $type_options = array('' => 'Type of project') + lists(EndeavorType::all(), 'name');
                    $stage_options = array('' => 'Stage of development') + lists(EndeavorStage::all(), 'name');
                    if ($introRespTemplatePath != 'false') {

                        return View::make($introRespTemplatePath)
                            ->with('user', $user) //participant
                            ->with('test_id', $test->id)
                            ->with('category', $category)
                            ->with('progress', $progress)
                            ->with('total_numbers_of_categories', $total_numbers_of_categories)
                            ->with('firstCategory', $firstCategory)
                            ->with('lastCategory', $lastCategory)
                            ->with('category_question_users', $category_question_users)
                            ->with('test', $test)
                            ->with('md5', $md5)
                            ->with('client', $client) //client
                            ->with('type_options',$type_options)
                            ->with('stage_options',$stage_options)
                            ->with('endeavor', $endeavorProject);

                    } else {
                        $message = 'Security error #WLV0d96J';
                        return Redirect::to(translate_url('/'))->with(['message' => $message]);
                    }
                } else {

                    return View::make($respPath)
                        ->with('user', $user) //participant
                        ->with('test_id', $test->id)
                        ->with('category', $category)
                        ->with('progress', $progress)
                        ->with('total_numbers_of_categories', $total_numbers_of_categories)
                        ->with('firstCategory', $firstCategory)
                        ->with('lastCategory', $lastCategory)
                        ->with('category_question_users', $category_question_users)
                        ->with('test', $test);

                }

            }elseif ($test->teststatus_id == Teststatus::FINISHED_BY_USER_OR_RESPONDENT){
                
                $client = User::find($mainTest->client_id); //finding client
                $feedPath = $client->getRespFeedback($client->THNKParticipant, $client->id);
                return Redirect::to(translate_url($feedPath));

            } else {
                $message = 'Security error #65h9Ed2O';
                return Redirect::to(translate_url('/'))->with(['message' => $message]);
            }
        } else {
            $message = 'Security error #N49Ebu7E';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }
    }

    public function postTestRespondent($test_id = null, $md5 = '') {

        //REDIRECTING FROM ENDEAVOR INTRO TO ENDEAVOR FEEDBACK
        if(isset($_POST['md5']) && isset($_POST['test_id']) & isset($_POST['intro']) ) {
            return Redirect::to(translate_url('/feedback/' . $test_id . "/" . $md5));
        }

        $test = Test::find($test_id);
        $mainTest = Test::getTest($test->test_id);

        if (isset($test_id) && $md5 == md5($test->respondent->email)) {

            $user = $test->getTest($test->test_id)->user; //finding participant owner of test

            $test_id = Input::get('test_id');

            if (Input::has('test_id') && $test_id > 0) {
                $test = Test::find($test_id);
            } else {
                // test not created yet. do so:
                $test = new Test();
                $test->teststatus_id = Teststatus::JUST_STARTED;
                $test->user_id = $user->id;
            }

            // check if test is not started
            if ($test->teststatus_id == Teststatus::NOT_STARTED) {
                $test->teststatus_id = Teststatus::JUST_STARTED;
            }

            $test->client_id = $mainTest->client_id;
            $test->save();
            
            //In case test was not created yet
            $test_id = $test->id;
            $currentTest = Test::find($test_id);
            $mainTest = Test::getTest($currentTest->test_id);
            $test->client_id = $mainTest->client_id;
            $test->save();

            // Save all questions.
            $questions = Input::get('question');

            if (is_array($questions)) {
                foreach ($questions as $key => $val) {

                    $categoryQuestionUserTest = CategoryQuestionUserTest::getCategoryQuestionUserTest($key, $test_id);
                    $categoryQuestionUserTest->value = $val;
                    $categoryQuestionUserTest->test_id = $test_id;
                    $categoryQuestionUserTest->question_id = $key;
                    $categoryQuestionUserTest->save();
                }
            }

            // Optional Comment field
            $category_id = Input::get('category_id');
            if (isset($category_id) && $category_id > 0) {
                $commentfield = Input::get('commentfield');
                if (isset($category_id)) {
                    $categoryQuestionUserTest = CategoryQuestionUserTest::getCategoryQuestionUserTestOpenQuestion($category_id, $test_id);
                    $categoryQuestionUserTest->category_id  = $category_id;
                    $categoryQuestionUserTest->test_id      = $test_id;
                    $categoryQuestionUserTest->commentfield = $commentfield;
                    $categoryQuestionUserTest->save();
                }
            }

            // Save the current state / current category.
            $test->category_id = $category_id;
            $test->save();

            // Check submit button
            $button = Input::get('knop');
            if ($button == trans('test.buttons.next') || $button == trans('test.buttons.finish')) {
                // store the next category

                // Check if this is the last category.
                if ($user->getLastCategory($mainTest->client_id)->id == $category_id) {
                    $test->teststatus_id = Teststatus::FINISHED_BY_USER_OR_RESPONDENT;
                    $test->save();

                    // E-mail to participant to inform somebody finished their test.
                    $data = array();
                    $data['user'] = $user;

                    $main_client_user = User::find($mainTest->client_id);
                    $typeInvite = 'respondentDone';
                    $dataMail = $main_client_user->getMailInvite($typeInvite, $main_client_user->THNKParticipant, $main_client_user->id, $user);
                    Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
                        $message->to($dataMail['email'], $dataMail['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
                        $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                    });
                    
                    //Check if all respondents are done with the test.
                    if (Test::getTestAreFinished($test->test_id)) {

                        // IF MINIMUM_REQUIRED IS NOT REACHED
                        // ex. if user invites 8 respondents, deletes 7 and the 8 finishes the test
                        if ($mainTest->amount_completed() < $mainTest->minimum_required){
                            $mainTest->teststatus_id = Teststatus::ALL_FINISHED_MIN_REQ_NOT_REACHED;
                            $mainTest->save();

                        } else {

                            // Generate the PDF on the fly.
                            $pdf = App::make('PdfController')->fullReport($test->test_id, $user->id, true);

                            $pdf_path = 'media/pdf/'.$user->firstname.$user->lastname.'_fullReport_' . date("Y-m-d") . ".pdf";

                            $pdf->Output(__DIR__ . '/../../public_html/' . $pdf_path, 'F');

                            $data = array();
                            $data['user'] = $user;

                            // set status of test.
                            $mainTest->teststatus_id = Teststatus::TEST_FULLY_FINISHED;
                            $mainTest->save();

                            // mail participant with the pdf.
                            $main_client_user = User::find($mainTest->client_id);
                            $typeInvite = 'allDone';
                            $dataMail = $main_client_user->getMailInvite($typeInvite, $main_client_user->THNKParticipant, $main_client_user->id, $user);
                            Mail::send($dataMail['template'], $data, function($message) use ($dataMail, $pdf_path) {
                                $message->to($dataMail['email'], $dataMail['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
                                $message->attach($pdf_path);
                                $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                            });
                            // mail other users that want the pdf.
                            if ($main_client_user->bcc_for_tests != '') {
                                Mail::send($dataMail['template'], $data, function($message) use ($dataMail, $main_client_user, $user, $pdf_path) {
                                    $all_addresses = explode(",",$main_client_user->bcc_for_tests);
                                    $subjectReceiver = $user->firstname . " " . $user->lastname ."'s report is completed";
                                    foreach ($all_addresses as $address) {
                                        $message->to($address)->from($dataMail['from'])->subject($subjectReceiver);
                                    }
                                    $message->attach($pdf_path);
                                    $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                                });
                            }

                            unlink($pdf_path);
                            // Attach the PDF
                        }
                    }

                    $main_client_user = User::find($mainTest->client_id); 
                    if($main_client_user->THNKParticipant == 1){
                        return Redirect::to(translate_url('/feedback/thnk'));
                    }else{
                        return Redirect::to(translate_url('/feedback/thanks'));
                    }
                    
                }

                if (isset($mainTest) && $mainTest->client_id > 0) {
                    $client =  User::find($mainTest->client_id);
                    $categories = $client->categories;
                } else {
                    $categories = $user->categories;
                }

                $hit = false;
                foreach ($categories as $category) {
                    if ($hit) {
                        $test->category_id = $category->id;
                        $test->save();
                        break;
                    }
                    if ($category->id == $category_id) {
                        $hit = true;
                    }
                }
            }

            if ($button == trans('test.buttons.back')) {
                // store the next category
                if (isset($mainTest) && $mainTest->client_id > 0) {
                    $client =  User::find($mainTest->client_id);
                    $categories = $client->categories;
                } else {
                    $categories = $user->categories;
                }

                $hit = false;
                $previous_category_id = 0;
                foreach ($categories as $category) {

                    if ($category->id == $category_id)
                        $hit = true;

                    if ($hit) {
                        $test->category_id = $previous_category_id;
                        $test->save();
                        break;
                    }

                    $previous_category_id = $category->id;
                }
            }
            return Redirect::to(translate_url('/feedback/' . $test->id . "/" . $md5));
        
        } else {
            $message = 'Security error #8csL10UV';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }
    }

    public function getThanksRespondent() {
        $data = array();
        if (isset($message)) {
            $data['message'] = $message;
        }

        return View::make('forms/test/thanks_form', $data);
    }
    public function getThanksTHNK() {
        $data = array();
        if (isset($message)) {
            $data['message'] = $message;
        }

        return View::make('forms/test/thanks_form_thnk', $data);
    }

    public function getViewText() {

        return View::make('forms/test/view_text');
    }

    public function getViewTextTHNK() {

        return View::make('forms/test/view_text_thnk');
    }

    public function getViewTextSP() {

        return View::make('forms/test/view_text_storypanda');
    }

    public function getViewTextSiemens() {

        return View::make('forms/test/view_text_siemens');
    }

    public function getViewTextOLX() {

        return View::make('forms/test/view_text_olx');
    }

    //TODO
    public function postSelfReport() {

        $test_id = Input::get('generate_own_report_id');
        $user_id = Input::get('user_id',null);

        $test = Test::getTest($test_id);

        return ($test->get_self_report_array($test_id, $user_id));

    }

    //TODO
    public function postFullReport($generate_report_id = null, $user_id = null) {

        if (!isset($generate_report_id))
            $test_id = Input::get('generate_report_id');
        else
            $test_id = $generate_report_id;

        if (!isset($user_id))
            $user_id = Input::get('user_id',null);

        $test = Test::getTest($test_id);

        return ($test->get_full_report_array($test_id, $user_id));

    }

    //TODO
    public function getRemindParticipantsNonActivity() {

        $tests = Test::getTestAreNotFullyFinished();

        echo "<ul>";
        if (isset($tests)) {
            foreach ($tests as $test) {

                $client = User::find($test->client_id);

                $user = User::find($test->user_id);

                echo "<li>" . $test->created_at . "</li>";

                $test_of_respondents = Test::where('test_id', '=' , $test->id)
                    ->where('teststatus_id', '<>' , Teststatus::TEST_FULLY_FINISHED)
                    ->where('teststatus_id', '<>' , Teststatus::FINISHED_BY_USER_OR_RESPONDENT)
                    ->get();

                $nr_of_resp = 0;
                $within_2_weeks = false;
                if (isset($test_of_respondents)) {
                    $remind = false;
                    echo "<ul>";
                    foreach ($test_of_respondents as $test_of_respondent) {
                        $nr_of_resp++;
                        echo "<li>" . $test_of_respondent->updated_at;

                        if (strtotime($test_of_respondent->updated_at) > strtotime('-14 day')) {
                            $within_2_weeks = true;
                            echo "*";
                        }
                        echo  "</li>";
                    }
                    echo "</ul>";

                }

                $data = array();

                if (isset($user) && $user->exists) {
                    $data['firstname'] = $user->firstname;

                    if ($nr_of_resp > 0 && !$within_2_weeks) {
                        $typeInvite = 'reminder';
                        $dataMail = $client->getMailInvite($typeInvite, $client->THNKParticipant, $client->id, $user);
                        Mail::send($dataMail['template'], $data, function($message) use ($dataMail) {
                            $message->to($dataMail['email'], $dataMail['firstname'])->from($dataMail['from'])->subject($dataMail['subject']);
                            $message->getHeaders()->addTextHeader('X-MC-Subaccount', '360 Mirror');
                            echo "<b>Mailed.</b>";
                        });
                    }
                }

            }
        }
        echo "</ul>";

    }

    /*
    public function test_email() {

        $test = Test::find(164);
        $user = User::find($test->user_id);

        // Generate the PDF on the fly.
        $pdf = App::make('PdfController')->fullReport($test->id, true);

        $pdf_path = 'media/pdf/pdf_' . date("Y_m_d_H_i_s") . ".pdf";

        $data = array();
        $data['user'] = $user;
        $pdf->Output($pdf_path, 'F');

        Mail::send('emails.respondent_finished', $data, function($message) use($pdf_path)
        {
            $message->to("sander@softwarevooru.nl", "Sander")->from("360Mirror@360mirror.org")->subject("Respondent finished the test");

            $message->attach($pdf_path);

        });

        unlink($pdf_path);

        echo "E-mail done";

        exit();
            // Attach the PDF
    }
    */

}
