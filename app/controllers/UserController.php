<?php

class UserController extends BaseController {

    public function getIndex ()
    {
        $data   = array(); 
        if (isset($message)) {
            $data['message'] = $message;
        }

        return View::make('forms/login_form', $data);
        
    }

    public function getIndexSP ()
    {
        $data   = array(); 
        if (isset($message)) {
            $data['message'] = $message;
        }

        return View::make('forms/login_form_storypanda', $data);
        
    }

    public function getIndexTHNK ()
    {
        $data   = array(); 
        if (isset($message)) {
            $data['message'] = $message;
        }

        return View::make('forms/login_form_thnk', $data);
        
    }

    public function getIndexQM ()
    {
        $data   = array(); 
        if (isset($message)) {
            $data['message'] = $message;
        }

        return View::make('forms/login_form_thnkv3', $data);
        
    }

    public function getIndexES ()
    {
        $data   = array(); 
        if (isset($message)) {
            $data['message'] = $message;
        }

        return View::make('forms/login_form_thnkv3_endeavor', $data);
        
    }

    public function getIndexRM ()
    {
        $data   = array(); 
        if (isset($message)) {
            $data['message'] = $message;
        }

        return View::make('forms/login_form_runway_mirror', $data);
        
    }
  
    public function postIndex ()
    {

        // Check if the form was submitted
        if (isset($_POST['email'])) {

            if (Auth::attempt(array('email' => $_POST['email'], 'password' => $_POST['password']))) {

                // Redirect to the index page if the user was logged in successfully
                if (Auth::check()) {
                    
                    $user = Auth::user();
                    $loginURL = URL::previous();

                    // Check to see if the passwords needs to be re-hashed (when the hash technique is different than when originally saved)
                    if (Hash::needsRehash($user->password)) {
                        $user->password = Hash::make(Input::get('password'));
                        $user->save();
                    }
                    
                    if ( $user->hasRole('USER_THNK_ADMIN') ){

                        //LOGIN MULTILANGUAGE ADMIN DASHBOARD
                        if (strpos($loginURL, '/en') !== false) {
                            return Redirect::intended('/clients/');
                        } else {
                            return Redirect::intended('es/clients/');
                        }
                        
                        

                    //USER CLIENT MUST ONLY BE USER_CLIENT
                    //SUGGESTED -> EMAIL HOSTED BY THNK    
                    }elseif ( $user->hasRole('USER_CLIENT') ){

                        //LOGIN MULTILANGUAGE CLIENT DASHBOARD
                        if ($user->multilanguage == 1) {
                            if (strpos($loginURL, '/en') !== false) {
                                return Redirect::intended('/clients/getAdmin/'.$user->id);
                            } else {
                                return Redirect::intended('es/clients/getAdmin/'.$user->id);
                            }  
                        } else {
                            return Redirect::intended('/clients/getAdmin/'.$user->id);   
                        }                       
                    
                    }else{

                        //CHECK IF USER INVITED TO >1 SURVEY
                        $surveys = DB::table('users_roles')
                                    ->select('users_roles.client_id')
                                    ->select('users.*')
                                    ->where('users_roles.user_id', $user->id)
                                    ->groupBy('users_roles.client_id')
                                    ->join('users', 'users_roles.client_id', '=', 'users.id')
                                    ->orderBy('users.company', 'asc')
                                    ->get();

                        $num_rows = count($surveys);

                        if ($num_rows > 0) {
                            if ($num_rows > 1){

                                //LOGIN MULTILANGUAGE MULTIPLE SURVEYS
                                if (strpos($loginURL, '/en') !== false) {
                                    //SELECT YOUR CLIENT VIEW
                                    return Redirect::intended('/surveys/'.$user->id);
                                } else {
                                    //SELECT YOUR CLIENT VIEW
                                    return Redirect::intended('es/surveys/'.$user->id);
                                }

                            }else{

                                $firstClientId = DB::table('users_roles')
                                    ->select('users_roles.client_id')
                                    ->where('users_roles.user_id', $user->id)
                                    ->groupBy('users_roles.client_id')
                                    ->join('users', 'users_roles.client_id', '=', 'users.id')
                                    ->orderBy('users.company', 'asc')
                                    ->take(1)
                                    ->get();

                                $client = User::find($firstClientId[0]->client_id);

                                if ( $user->hasRoleInClient(Role::USER_CLIENT_ADMIN, $firstClientId[0]->client_id, $user->id) || $user->hasRoleInClient(Role::USER_CLIENT_COACH, $firstClientId[0]->client_id, $user->id) ){

                                    //LOGIN MULTILANGUAGE CLIENT DASHBOARD
                                    if ($client->multilanguage == 1) {
                                        if (strpos($loginURL, '/en') !== false) {
                                            return Redirect::intended('/clients/getAdmin/'.$client->id);
                                        } else {
                                            return Redirect::intended('es/clients/getAdmin/'.$client->id);
                                        }  
                                    } else {
                                        return Redirect::intended('/clients/getAdmin/'.$client->id);   
                                    }
                                
                                }elseif ( $user->hasRoleInClient(Role::USER_PARTICIPANT, $firstClientId[0]->client_id, $user->id) ){

                                    //LOGIN MULTILANGUAGE PARTICIPANT DASHBOARD
                                    if ($client->multilanguage == 1) {
                                        if (strpos($loginURL, '/en') !== false) {
                                            return Redirect::intended('/dashboard/'.$client->id);
                                        } else {
                                            return Redirect::intended('es/dashboard/'.$client->id);
                                        }  
                                    } else {
                                        return Redirect::intended('/dashboard/'.$client->id);   
                                    }
                                
                                }else{
                                    
                                    $message = 'Incorrect role type.';
                                    return Redirect::to(translate_url('/'))->with(['message' => $message]);
                                }
                            }
                        }else{
                            //NO RESULTS
                            $message = 'Your account is not invited to any survey.';
                            return Redirect::to(translate_url('/'))->with(['message' => $message]);
                        }

                    }

                    $message = 'Success';

                } else { 
                    $message = "Credentials not valid.";
                }
            } else {
                $message = "Credentials not valid.";
            }

        } else {
            $message = "Credentials not valid.";
        }
        
        return Redirect::to(translate_url('/'))->with(['message' => $message]);     
    }    
  
    public function getSurveys ($user_id) {

        $loggedInUser = Auth::user();

        if($user_id == $loggedInUser->id){

            $data   = array();

            $surveys = DB::table('users_roles')
                        ->select('users_roles.client_id')
                        ->select('users.*')
                        ->where('users_roles.user_id', $loggedInUser->id)
                        ->groupBy('users_roles.client_id')
                        ->join('users', 'users_roles.client_id', '=', 'users.id')
                        ->orderBy('users.company', 'asc')
                        ->get();

            $survey_options = array('' => 'Select your survey');

            foreach ($surveys as $key => $value) {
                $survey_options[$value->id] = $value->company;
            }

            return View::make('forms/surveys', $data)
                    ->with('survey_options' , $survey_options)
                    ->with('participant', $loggedInUser);

        }else{
            $message = "Security error lR5Vko8e";
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }
        
    }

    public function postSurveys ($user_id) {

        $user = Auth::user();
        $loginURL = URL::previous();

        if($user_id == $user->id){

            $rules = array(
                      'survey_options' =>  'required',
                    );

            // Validation
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()){

                //LOGIN MULTILANGUAGE MULTIPLE SURVEYS
                if (strpos($loginURL, '/en') !== false) {
                    return Redirect::to(translate_url('/surveys/'.$user_id))->withErrors($validator)->withInput();
                } else {
                    return Redirect::to(translate_url('es/surveys/'.$user_id))->withErrors($validator)->withInput();
                }

            } else {
                $survey_selected = Input::get('survey_options');
                $client = User::find($survey_selected);

                if ( $user->hasRoleInClient(Role::USER_CLIENT_ADMIN, $survey_selected, $user->id) || $user->hasRoleInClient(Role::USER_CLIENT_COACH, $survey_selected, $user->id) ){

                    //LOGIN MULTILANGUAGE CLIENT DASHBOARD
                    if ($client->multilanguage == 1) {
                        if (strpos($loginURL, '/en') !== false) {
                            return Redirect::intended('/clients/getAdmin/'.$client->id);
                        } else {
                            return Redirect::intended('es/clients/getAdmin/'.$client->id);
                        }  
                    } else {
                        return Redirect::intended('/clients/getAdmin/'.$client->id);   
                    }
                
                }elseif ( $user->hasRoleInClient(Role::USER_PARTICIPANT, $survey_selected, $user->id) ){

                    //LOGIN MULTILANGUAGE CLIENT DASHBOARD
                    if ($client->multilanguage == 1) {
                        if (strpos($loginURL, '/en') !== false) {
                            return Redirect::intended('/dashboard/'.$client->id);
                        } else {
                            return Redirect::intended('es/dashboard/'.$client->id);
                        }  
                    } else {
                        return Redirect::intended('/dashboard/'.$client->id);   
                    }
                
                }else{
                    
                    $message = 'Incorrect role type.';
                    return Redirect::to(translate_url('/'))->with(['message' => $message]);
                
                }
            }
        }else{
            $message = "Security error G9sDTlLw";
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }
        
    }    

    public function logout ($id)
    {
        //Auth::logout();
        $client = User::find($id);

        if($id == 0){
            return Redirect::to(translate_url('/'));
        }else{
            $logouturl = $client->getLogOutURL($client->THNKParticipant, $client->id);
            return Redirect::to(translate_url($logouturl));
        }     
    }
}