<?php

class EndeavorController extends WebsiteController {

    public function getIndex($client_id, $id) {

        

        if ( Auth::user()->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, Auth::user()->id) || Auth::user()->hasRoleInClient(Role::USER_CLIENT_COACH, $client_id,  Auth::user()->id) || Auth::user()->hasRole('USER_THNK_ADMIN') ){
        	
            $participant = User::find($id); //finding participant (non-dependent of logged user)

            $user = User::find($client_id); //finding client ($user variable used also in template main)

            $type_options = array('' => 'Type of project') + lists(EndeavorType::all(), 'name');
            $stage_options = array('' => 'Stage of development') + lists(EndeavorStage::all(), 'name');
	        
            $endeavor0 = Endeavor::getEndeavorByUser($participant->id);
            if( $endeavor0->count() ){
                $endeavor = $endeavor0;
            }else{
                $endeavor = new Endeavor;
            }

            return View::make('forms/endeavor')
                ->with('type_options',$type_options)
                ->with('stage_options',$stage_options)
                ->with('participant_id', $id)
                ->with('user', $user)
                ->with('participant', $participant)
                ->with('endeavor', $endeavor);

        }else{
        	$message = 'Security error #555Y22Cv';
        	return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }
    }

    public function postIndex($client_id, $id) {
    	
    	$loggedUser = Auth::user();
    	$saveUserData = false;

    	$user = User::find($client_id); //finding client
    	$participant = User::find($id); //finding participant
    	
        $oldEndeavorCheck = 0;
        $endeavor0 = Endeavor::getEndeavorByUser($participant->id);
        if( $endeavor0->count() ){
            $endeavor = $endeavor0[0];
            $oldEndeavorCheck = 1;
        }else{
            $endeavor = new Endeavor;
        }

    	//PARTICIPANTS SAVING THEIR OWN PROFILES + ADMIN
    	if ( $loggedUser->hasRoleInClient(Role::USER_PARTICIPANT, $client_id, $loggedUser->id) || Auth::user()->hasRole('USER_THNK_ADMIN') ){
        	//SAVE USER DATA
			$saveUserData = true;

        }else{
        	$message = 'Security error #FmX5xTlv';
        	return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }

        if ($saveUserData == true){

			$typeSubmit = Input::get('knop');
			$hiddenSubmit = Input::get('other_type_options_hidden');
	        if($typeSubmit == trans('endeavor.buttons.save')){
			    $rules = array(
			      'name'             =>  'required',
			      'type_options'	 =>  'required|numeric|min:1',
			      'total_people'	 =>  'required|numeric',
			      'stage_options'    =>  'required|numeric|min:1',
                  'descriptionWhat' =>  'required',
                  'descriptionHow'  =>  'required',
                  'descriptionWho'  =>  'required',
                  'descriptionWhy'  =>  'required',
                  'descriptionNext' =>  'required',
			    );

			    if($hiddenSubmit > 0){
			    	//$rules['otherTypeTxt'] = 'required'; NOT WORKING
			    }

				// Validation
				$validator = Validator::make(Input::all(), $rules);

				if ($validator->fails()){
			    	return Redirect::to(translate_url('/endeavor/'.$client_id .'/'. $id))->withErrors($validator)->withInput();
				} else {
                    $endeavor->user_id = $id;
                    $endeavor->client_id = $client_id;
					$endeavor->name = rtrim(Input::get('name'));
					$endeavor->type_id = rtrim(Input::get('type_options'));
					$endeavor->people = rtrim(Input::get('total_people'));
					$endeavor->stage_id = rtrim(Input::get('stage_options'));
                    $endeavor->descriptionWhat = rtrim(Input::get('descriptionWhat'));
                    $endeavor->descriptionHow = rtrim(Input::get('descriptionHow'));
                    $endeavor->descriptionWho = rtrim(Input::get('descriptionWho'));
                    $endeavor->descriptionWhy = rtrim(Input::get('descriptionWhy'));
                    $endeavor->descriptionNext = rtrim(Input::get('descriptionNext'));
					if($hiddenSubmit > 0){
				    	$endeavor->other_type = rtrim(Input::get('otherTypeTxt'));
				    }

                    if( $oldEndeavorCheck == 0 ) {
                        $endeavor->save();
                        return Redirect::to(translate_url('/dashboard/'.$client_id));
                    }else{
                        $endeavor->update();
                        if ( Auth::user()->hasRole('USER_THNK_ADMIN') ) {
                            return Redirect::to(translate_url('/test/myprofile/'.$client_id .'/'. $id));
                        } else {
                            return Redirect::to(translate_url('/dashboard/'.$client_id)/*->with('success', 'Your project has been updated')*/);
                        }
                    }					
				}

			} else{
				$message = 'Security error #QGbuer8W';
                return Redirect::to(translate_url('/'))->with(['message' => $message]);
			}

        }else{
        	$message = 'Security error #1vn8V83O';
            return Redirect::to(translate_url('/'))->with(['message' => $message]);
        }
    }


}
