<?php

class WebsiteController extends BaseController {
    
    public function __construct()
    {

        $value = Session::pull('redirect_route', '/test/test/index"');

        // Check if the user is logged in.
        if (!Auth::check()) {

            $my_string = Request::url();
            Session::put('redirect_route', $my_string);
            Redirect::to(translate_url('/'));     
        } 
 

    }
    
    
    
}
