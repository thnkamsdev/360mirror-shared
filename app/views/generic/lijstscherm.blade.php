<?php
  if (!isset($extra_titel))
        $extra_titel = '';

  if ($extra_titel != '') {
         echo "<div id = 'title'>";
         echo $extra_titel;
         echo "</div><br/>";
  }
?>

<div>

    <?php
        if (!isset($sort_column))
            $sort_column = 0;

        if (!isset($action))
            $action = 2;

        if (!isset($custom_method_delete))
            $custom_method_delete = "delete";

        if (!isset($custom_method_edit))
            $custom_method_edit = "edit";

        if (!isset($extraParameter))
            $extraParameter = "";

        if (!isset($extraHtml))
            $extraHtml = "";

        if (!isset($extraHtml2))
            $extraHtml2 = "";

        if (!isset($extraIcon))
            $extraIcon = "";

        if (!isset($extraMethod))
            $extraMethod = "";    

        if (!isset($extraController))
            $extraController = "";

        if (!isset($extraAttribute))
            $extraAttribute = ""; 

        if (!isset($extraTarget))
            $extraTarget = "";

        if (!isset($search))
            $search = false;

        if ($search == true) {
            echo "<div id='searchBox'>";
            echo $search_view;
            echo "</div><br/>";
        }
    ?>  <p>
            <font size="2">
                <strong><?= ''//$titel ?></strong>
            </font>
        </p>
        <br/>
    <?php
        echo table::lijstscherm($titel, $kolommen, $list, $component, $controller, $sort_column, 
                                $action, $custom_method_delete, $custom_method_edit, $extraParameter,
                                $extraHtml, $extraHtml2, $extraIcon, $extraMethod, $extraController, $extraAttribute, $extraTarget);
    ?>

</div>