@extends('emails.email_template_thnk')
@section('content_email')
Dear <?= $firstname ?>,
<br/><br/>
<?php $url = App::make('url')->to('/');?>
Here's how you login to take your Impact Assessment survey. Visit <a href="<?= $url . "/impact" ?>"><?= $url . "/impact" ?></a> to start this process.
<br/><br/>
<?php
	if ($pwd != 'NOPASSWORD'){
?>
	Login with your e-mail and <b><?=$pwd?></b> as your password. 
	<br/><br/>
<?php
	}
?>
When you're done you'll be able to invite two co-workers or mentors to also complete the survey.
<br/><br/>
Thank you for taking the time to complete this. It helps us match you with a THNK mentor based on your stage of impact growth.
@stop