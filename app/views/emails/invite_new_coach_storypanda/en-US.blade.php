@extends('emails.email_template_thnk')
@section('content_email')
Dear <?= $firstname ?>,
<br/><br/>
<?php $url = App::make('url')->to('/');?>
Here's how you login to coach your Impact Assessment surveys. Visit <a href="<?= $url . "/impact" ?>"><?= $url . "/impact" ?></a> to start this process.
<br/><br/>
<?php
	if ($pwd != 'NOPASSWORD'){
?>
	Login with your e-mail and <b><?=$pwd?></b> as your password. 
	<br/><br/>
<?php
	}
?>
Thank you for taking the time to complete this.
@stop