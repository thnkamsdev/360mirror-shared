@extends('emails.email_template_olx')
@section('content_email')
Dear <?= $user->firstname ?>,
<br/><br/>
We would like to inform you that one of your respondents just finished providing feedback on your OLX Leadership Skills via the 360 MIRROR.
<br/><br/>
Thank you
<br/>
@stop