@extends('emails.email_template_olx')
@section('content_email')
Dear <?= $firstname ?>,
<br/><br/>
<?php $url = App::make('url')->to('/');?>

As a participant in the upcoming OLX Leadership Accelerator Program (LAP), you are invited to partake in a 360 leadership assessment. 
<br/><br/>
You will do a self-assessment as well as invite respondents to provide you with feedback.
<br/><br/>
The leadership skills of OLX leaders are developed along five distinctive dimensions: 
<br/><br/>
Embraces and drives change<br/>
Collaborates in a global context<br/>
Thrives within ambiguity<br/>
Leads without formal authority<br/>
Leads self (personal mastery)<br/>
<br/><br/>
The 360 assessment will help you gain insight into your leadership strengths and learning edges and will support you in determining your leadership development goals.
<br/><br/>
When five respondents (the minimum number) have provided feedback, you will receive your report. You will receive a new report after each additional respondent has filled out their feedback.
<br/><br/>
Please visit <a href="<?= $url ?>"><?= $url ?></a> to start this process.
<br/><br/>
<?php
	if ($pwd == 'NOPASSWORD'){
		echo 'Good luck!';
	}else{
?>
		Login with <b><?= $email ?></b> as your e-mail and <b><?= $pwd ?></b> as your password. 
		<br/><br/>
		Good luck!
<?php
	}
?>
@stop