@extends('emails.email_template_siemens')
@section('content_email')
Dear <?= $user->firstname ?>,
<br/><br/>
We would like to inform you about that a respondent just finished providing feedback on your MIRROR.
<br/>
@stop