@extends('emails.email_template_siemens')
@section('content_email')
Dear <?= $firstname ?>,
<br/><br/>
<?php $url = App::make('url')->to('/');?>
In preparation for the Siemens PS Impactivism Program, you are invited to partake in a 360 assessment. This assessment addresses the leadership mindsets and behaviors that we will explore in the program.
<br/><br/>
The deadline for completion (including the assessment by 7 respondents) is Sunday 28 January and so we invite you to start your survey as soon as possible.
<br/><br/>
Please visit <a href="<?= $url . "/thnk" ?>"><?= $url . "/thnk" ?></a> to start this process.
<br/><br/>
<?php
	if ($pwd == 'NOPASSWORD'){
		echo 'Regards';
	}else{
?>
		Login with your e-mail and <b><?=$pwd?></b> as your password. 
		<br/><br/>
		Regards
<?php
	}
?>
@stop