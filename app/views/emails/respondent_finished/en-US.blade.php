@extends('emails.email_template')
@section('content_email')
@lang('emails.respondentDone.dear') <?= $user->firstname ?>,
<br/><br/>
@lang('emails.respondentDone.message')
<br/>
@stop