@extends('emails.email_template')
@section('content_email')
Dear <?= $firstname ?>,
<br/><br/>
<?php 
	$url = App::make('url')->to('/es');
	$spaURL = str_replace("/en/", "/es/", $url);
?>
@lang('emails.newCoach.message.0') <a href="<?= $spaURL ?>"><?= $spaURL ?></a> @lang('emails.newCoach.message.1')
<br/><br/>
<?php
	if ($pwd == 'NOPASSWORD'){
		echo trans('emails.newCoach.thanks.default');
	}else{
?>
		@lang('emails.newCoach.thanks.extra.0') <b><?=$pwd?></b> @lang('emails.newCoach.thanks.extra.1') 
		<br/><br/>
		@lang('emails.newCoach.thanks.default')
<?php
	}
?>
@stop