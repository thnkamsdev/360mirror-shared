@extends('emails.email_template')
@section('content_email')
@lang('emails.inviteGeneral.dear') <?= $respondent->name ?>,
<br/><br/>
@lang('emails.inviteGeneral.intro')
<br/><br/>
@lang('emails.inviteGeneral.action')
<br/><br/>
@lang('emails.inviteGeneral.thanks')
<br/><br/>
<?= ucfirst($user->firstname); ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/feedback/" . $test->id . "/" . $md5_email ?>">@lang('emails.inviteGeneral.message')</a>
<br/><br/>
<br/>
@stop