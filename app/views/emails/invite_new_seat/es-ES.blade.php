@extends('emails.email_template')
@section('content_email')
@lang('emails.newSeat.dear') <?= $firstname ?>,
<br/><br/>
<?php 
	$url = App::make('url')->to('/es');
	$spaURL = str_replace("/en/", "/es/", $url);
?>
@lang('emails.newSeat.message.0') <a href="<?= $spaURL ?>"><?= $spaURL ?></a> @lang('emails.newSeat.message.1')
<br/><br/>
<?php
	if ($pwd == 'NOPASSWORD'){
		echo trans('emails.newSeat.thanks.default');
	}else{
?>
		@lang('emails.newSeat.thanks.extra.0') <b><?=$pwd?></b> @lang('emails.newSeat.thanks.extra.1') 
		<br/><br/>
		@lang('emails.newSeat.thanks.default')
<?php
	}
?>
@stop