@extends('emails.email_template_thnk')
@section('content_email')
Dear <?= $user->firstname ?>,
<br/><br/>
We would like to inform you about that a respondent just finished providing feedback on your Creative Leadership skills via 360 MIRROR.
<br/>
@stop