@extends('emails.email_template_thnkv3')
@section('content_email')
Dear <?= $user->firstname ?>,
<br/><br/>
We would like to inform you about that a respondent just finished providing feedback on your assessment via QUEST MIRROR.
<br/>
@stop