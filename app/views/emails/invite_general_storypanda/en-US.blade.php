@extends('emails.email_template_thnk')
@section('content_email')
Dear <?= $respondent->name ?>,
<br/><br/>
As someone who has some knowledge of my Impact Project, I'm hoping you can fill out a brief assessment. This will help me see where the strengths and weakness of my project are and where I need support and resources.
<br/><br/>
Please click the link below to send me your feedback. Answering these questions will take approximately 15 minutes, and the answers will be sent back to me anonymously. I appreciate your help!
<br/><br/>
Thank you!
<br/>
<?= ucfirst($user->firstname); ?> <?= ucfirst($user->firstname); ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/feedback/" . $test->id . "/" . $md5_email ?>">Visit my Impact Assessment and provide feedback</a>
<br/><br/>
<br/>
@stop