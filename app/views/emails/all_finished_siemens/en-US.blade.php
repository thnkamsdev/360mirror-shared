@extends('emails.email_template_siemens')
@section('content_email')
@lang('emails.allFinished.dear') <?= $user->firstname ?>,
<br/><br/>
@lang('emails.allFinished.message') 
<br/>
@lang('emails.allFinished.report')
@stop