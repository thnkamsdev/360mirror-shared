<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>@lang('emails.default.appTitle')</title>
	<style type='text/css'>
		body, table {
			font-family: 'Georgia', 'Times New Roman', serif; 
			font-size: 14px; 
			line-height: 150%;
			color: #000000;
			background-color: #ffffff;
		}
		.content {
			margin: 40px 0px 10px 0px;
		}
		#email {
			margin: 0 auto;
			padding: 0px;
		}
	</style>
</head>
<body>
	<div id="email">
		<div class="content">
			@yield('content_email')
		</div>
		<div id="footer">
			<p>@lang('emails.default.footer.0') <a href="http://www.thnk.org/">@lang('emails.default.footer.1')</a> @lang('emails.default.footer.2') <a href="http://globalgoodfund.org/">@lang('emails.default.footer.3')</a>.</p>
		</div>
	</div>
</body>
</html>
