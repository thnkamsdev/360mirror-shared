@extends('emails.email_template_thnk')
@section('content_email')
Dear <?= $respondent->name ?>,
<br/><br/>
I value your input and would like to ask you for help. I am reflecting on my Leadership skills using an online tool called 360 MIRROR. This includes 360º feedback from my peers, people I work with, and have worked for. This feedback is specifically on my leadership skills, strengths, and weaknesses. 
<br/><br/>
Please click the link below to send me your feedback. Answering these questions will take approximately 30 minutes, and the answers will be sent back to me anonymously. I appreciate your help! 
<br/><br/>
Thank you!
<br/><br/>
<?= ucfirst($user->firstname); ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/feedback/" . $test->id . "/" . $md5_email ?>">Visit my 360 MIRROR and provide feedback</a>
<br/><br/>
<br/>
@stop