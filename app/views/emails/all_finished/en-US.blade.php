@extends('emails.email_template')
@section('content_email')
@lang('emails.allFinished.dear') <?= $user->firstname ?>,
<br/><br/>
@lang('emails.allFinished.message') 
<br/>
@lang('emails.allFinished.report')
@stop