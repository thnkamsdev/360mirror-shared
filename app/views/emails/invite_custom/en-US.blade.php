@extends('emails.email_template')
@section('content_email')
<?= $custom_text ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/feedback/" . $test->id . "/" . $md5_email ?>">@lang('emails.inviteCustom.message')</a>
<br/><br/>
<br/>
@stop