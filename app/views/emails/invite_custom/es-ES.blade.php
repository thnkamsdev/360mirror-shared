@extends('emails.email_template')
@section('content_email')
<?= $custom_text ?>
<br/><br/>
<?php 
	$url = App::make('url')->to('/es');
	$urlFullPath = $url . "/feedback/" . $test->id . "/" . $md5_email;
	$spaURL = str_replace("/en/", "/es/", $urlFullPath);
?>
<a href="<?= $spaURL ?>">@lang('emails.inviteCustom.message')</a>
<br/><br/>
<br/>
@stop