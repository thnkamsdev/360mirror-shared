@extends('emails.email_template_thnkv3')
@section('content_email')
Caro <?= $firstname ?>,
<br/><br/>
<?php $url = App::make('url')->to('/');?>
Como participante do Programa Terceiro Setor Transforma, você está convidado a participar de uma Pesquisa de Liderança 360.
<br/><br/>
Você fará uma autoavaliação e convidará os entrevistados a fornecer feedback.
<br/><br/>
A pesquisa o ajudará a obter insights sobre seus pontos fortes de liderança e aprendizado e o apoiará na determinação de suas metas de desenvolvimento de liderança.
<br/><br/>
A avaliação não deve demorar mais de 20 minutos para ser concluída. Nós convidamos você a fazer sua autoavaliação o quanto antes. Após a conclusão, você será solicitado a nomear um mínimo de 5 respondentes (membros da equipe, colegas, pessoas com quem você está trabalhando ou com quem já trabalhou).
<br/><br/>
O prazo para conclusão (incluindo a avaliação dos entrevistados) é domingo, 21 de outubro.
<br/><br/>
Quando cinco entrevistados (o número mínimo) fornecerem feedback, você receberá seu relatório. Você receberá um novo relatório depois que cada participante adicional tiver preenchido o feedback.
<br/><br/>
Por favor, visite <a href="<?= $url ?>"><?= $url ?></a> para iniciar este processo.
<br/><br/>
<?php
	if ($pwd == 'NOPASSWORD'){
		echo 'Boa sorte!';
	}else{
?>
		Login com seu email e <b><?=$pwd?></b> como sua senha. 
		<br/><br/>
		Boa sorte!
<?php
	}
?>
@stop