@extends('emails.email_template_thnkv3_endeavor')
@section('content_email')
Dear <?= $firstname ?>,
<br/><br/>
<?php $url = App::make('url')->to('/');?>
You are invited as a coach to an assessment. Please visit <a href="<?= $url . "/endeavorscan" ?>"><?= $url . "/endeavorscan" ?></a> to start this process.
<br/><br/>
<?php
	if ($pwd == 'NOPASSWORD'){
		echo 'Regards';
	}else{
?>
		Login with your e-mail and <b><?=$pwd?></b> as your password. 
		<br/><br/>
		Regards
<?php
	}
?>
@stop