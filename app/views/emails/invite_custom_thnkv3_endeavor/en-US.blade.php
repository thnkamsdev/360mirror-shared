@extends('emails.email_template_thnkv3_endeavor')
@section('content_email')
<?= $custom_text ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/intro/" . $test->id . "/" . $md5_email ?>">Visit my assessment and provide feedback</a>
<br/><br/>
<br/>
@stop