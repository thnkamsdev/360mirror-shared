@extends('emails.email_template')
@section('content_email')
@lang('emails.appReminder.dear') <?= $firstname ?>,
<br/><br/>
@lang('emails.appReminder.message')
<br/><br/>
@lang('emails.appReminder.thanks')
<br/><br/>
<br/>
@stop
