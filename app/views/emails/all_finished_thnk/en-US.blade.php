@extends('emails.email_template_thnk')
@section('content_email')
Dear <?= $user->firstname ?>,
<br/><br/>
We would like to inform you that all your respondents have finished giving feedback. 
<br/>
Please see the attached PDF file for your full report.
@stop