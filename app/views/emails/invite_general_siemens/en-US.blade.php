@extends('emails.email_template_siemens')
@section('content_email')
Dear <?= $respondent->name ?>,
<br/><br/>
I value your input and would like to ask you for help. I will be participating in the Siemens PS Impactivist Program. As part of the pre-work, I am reflecting on my leadership skills using an online tool called the MIRROR.
<br/><br/>
This includes 360º feedback from my manager, peers, people I work with, and have worked for. This feedback is important since it will help me identify my learning goals for the program. The MIRROR helps me to gain insight into my leadership skills, strengths, and weaknesses.
<br/><br/>
Please click the link below to send me your feedback. Answering these questions will take approximately 30 minutes, and the answers will be sent back to me anonymously. I appreciate your help!
<br/><br/>
Thank you!
<br/><br/>
<?= ucfirst($user->firstname); ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/feedback/" . $test->id . "/" . $md5_email ?>">Visit my 360 MIRROR and provide feedback</a>
<br/><br/>
<br/>
@stop