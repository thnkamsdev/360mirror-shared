@extends('emails.email_template_olx')
@section('content_email')
Dear <?= $respondent->name ?>,
<br/><br/>
I value your input and would like to ask you for help. I will be participating in the OLX Leadership Accelerator Program (LAP). As part of the pre-work, I am reflecting on my leadership skills using an online tool called <b>360 MIRROR</b>.<br/>
This includes 360o feedback from my peers, people I work with, and have worked for. This feedback is important since it will help me identify my leadership strengths and weaknesses.<br/>
The leadership skills of OLX leaders are developed along five distinctive dimensions: 
<br/><br/>
Embraces and drives change<br/>
Collaborates in a global context<br/>
Thrives within ambiguity<br/>
Leads without formal authority<br/>
Leads self (personal mastery)<br/>
<br/><br/>
Please click the link below to send me your feedback. Answering these questions will take approximately 20 minutes, and the answers will be sent back to me anonymously. The deadline for this is <b>Monday 24 September</b>. Thank you for taking the time to do this - I appreciate your help!
<br/><br/>
Thank you,<br/>
<?= ucfirst($user->firstname); ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/feedback/" . $test->id . "/" . $md5_email ?>">Visit my 360 MIRROR and provide feedback</a>
<br/><br/>
<br/>
@stop