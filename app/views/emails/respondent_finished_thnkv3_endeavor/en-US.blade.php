@extends('emails.email_template_thnkv3_endeavor')
@section('content_email')
Dear <?= $user->firstname ?>,
<br/><br/>
We would like to inform you about that a respondent just finished providing feedback on your assessment via ENDEAVOR SCAN.
<br/>
@stop