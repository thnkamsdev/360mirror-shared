@extends('emails.email_template_siemens')
@section('content_email')
Dear <?= $firstname ?>,
<br/><br/>
We are mailing because you have an open assessment, but one or more respondents didn't have any activity during the last two weeks. You may want to send them a reminder.
<br/><br/>
Thank you!
<br/><br/>
<br/>
@stop
