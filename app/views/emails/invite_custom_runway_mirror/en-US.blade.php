@extends('emails.email_template_runway_mirror')
@section('content_email')
<?= $custom_text ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/feedback/" . $test->id . "/" . $md5_email ?>">Visit my assessment and provide feedback</a>
<br/><br/>
<br/>
@stop