@extends('emails.email_template_runway_mirror')
@section('content_email')
Dear <?= $firstname ?>,
<br/><br/>
<?php $url = App::make('url')->to('/');?>
You are invited as a coach in a ScaleUpNation leadership assessment. Please visit <a href="<?= $url . "/scaleupnation" ?>"><?= $url . "/scaleupnation" ?></a> to start this process.
<br/><br/>
<?php
	if ($pwd == 'NOPASSWORD'){
		echo 'Regards';
	}else{
?>
		Login with your e-mail and <b><?=$pwd?></b> as your password. 
		<br/><br/>
		Regards
<?php
	}
?>
@stop