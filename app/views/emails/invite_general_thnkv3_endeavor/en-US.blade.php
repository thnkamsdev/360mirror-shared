@extends('emails.email_template_thnkv3_endeavor')
@section('content_email')
Dear <?= $respondent->name ?>,
<br/><br/>
I will be participating in the THNK Executive Leadership Program (www.thnk.org).
<br/>
In this program, I will work on my Endeavor project, a project I am passionate about to scale-up successfully in the real world.
<br/>
For this purpose I would like to ask you to give feedback on my strong points and development areas.
<br/>
Please click the link below to access the assessment tool.
<br/>
Answering the questions will take approximately 30 minutes.
<br/>
The answers will be sent back to me anonymously.
<br/><br/>
Best regards,
<br/>
<?= ucfirst($user->firstname); ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/intro/" . $test->id . "/" . $md5_email ?>">Visit my assessment and provide feedback</a>
<br/><br/>
<br/>
@stop