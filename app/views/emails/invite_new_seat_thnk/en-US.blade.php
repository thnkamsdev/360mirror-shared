@extends('emails.email_template_thnk')
@section('content_email')
Dear <?= $firstname ?>,
<br/><br/>
<?php $url = App::make('url')->to('/');?>
You are invited to partake in a 360 leadership assessment. Please visit <a href="<?= $url . "/thnk" ?>"><?= $url . "/thnk" ?></a> to start this process.
<br/><br/>
<?php
	if ($pwd == 'NOPASSWORD'){
		echo 'Regards';
	}else{
?>
		Login with your e-mail and <b><?=$pwd?></b> as your password. 
		<br/><br/>
		Regards
<?php
	}
?>
@stop