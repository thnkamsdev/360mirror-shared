@extends('emails.email_template_thnkv3')
@section('content_email')
Caro <?= $respondent->name ?>,
<br/><br/>
Como você deve saber, eu estou participando do Programa Terceiro Setor Transforma, um programa destinado a ampliar minha capacidade de liderança frente à minha ONG. 
<br/><br/>
Eu valorizo sua opinião e gostaria de pedi-lo ajuda. Como parte do programa, estou refletindo sobre minhas habilidades de liderança usando uma ferramenta online chamada 360 MIRROR.
<br/><br/>
Isso inclui comentários 360º de colegas, minha equipe, pessoas com quem trabalho e para quem trabalhei. Seu feedback é importante, pois ajudará a identificar minhas habilidades, pontos fortes e pontos de liderança a melhorar.
<br/><br/>
Por favor, clique no link abaixo para me enviar seu feedback. Responder a estas cinco perguntas levará aproximadamente 20 minutos e suas respostas serão enviadas de volta para mim anonimamente. Eu aprecio muito a sua ajuda!
<br/><br/>
Obrigado,
<br/><br/>
<?= ucfirst($user->firstname); ?>
<br/><br/>
<?php $url = App::make('url')->to('/');?>
<a href="<?= $url . "/feedback/" . $test->id . "/" . $md5_email ?>"><?= $url . "/feedback/" . $test->id . "/" . $md5_email ?></a>
<br/><br/>
<br/>
@stop