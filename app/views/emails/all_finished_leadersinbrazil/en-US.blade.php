@extends('emails.email_template_thnkv3')
@section('content_email')
Caro <?= $user->firstname ?>,
<br/><br/>
Gostaríamos de informar que todos os entrevistados terminaram de dar feedback. 
<br/>
Por favor, veja o arquivo PDF anexado para o seu relatório completo.
@stop