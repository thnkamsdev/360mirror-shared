<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>360 MIRROR</title>

        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="pragma" content="no-cache" />

        <meta name="keywords" content="THNK, Mirror, assessment tool, 360, assessment, feedback, self assessment, report, creative leadership, school of creative leadership" />
        <meta name="description" content="360 Mirror is a comprehensive leadership development tool developed by THNK School of Creative Leadership, to provide insights into an individual’s creative leadership skill for social entrepreneurs. In only a few steps you’ll learn about your strengths and learning edges, resulting in the ultimate chance to enhance your effectiveness as a leader in your enterprise. 360 Mirror is comprehensive, provided in two parts: Self-assessment Evaluation and 360° Peer Feedback." />
        <meta name="robots" content="index, follow" />

        <meta property="og:title" content="360 Mirror THNK Tool" /> 
        <meta property="og:description" content="360 Mirror is a comprehensive leadership development tool developed by THNK School of Creative Leadership, to provide insights into an individual’s creative leadership skill for social entrepreneurs. In only a few steps you’ll learn about your strengths and learning edges, resulting in the ultimate chance to enhance your effectiveness as a leader in your enterprise. 360 Mirror is comprehensive, provided in two parts: Self-assessment Evaluation and 360° Peer Feedback." />  
        <meta property="og:image" content="{{asset('media/images/thnkbat.jpg')}}" /> 

        <link rel="shortcut icon" href="{{asset('media/icons/favicon.ico')}}" type="image/x-icon">
        <link rel="icon" href="{{asset('media/icons/favicon.ico')}}" type="image/x-icon">

        <link href="{{asset('media/css/thnk.org/application.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('media/css/thnk.org/fonts/thnk-grotesk-office/stylesheet.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('media/css/thnk.org/fonts/arnopro-display/stylesheet.css')}}" rel="stylesheet" type="text/css" />

        <script type='text/javascript' src='//code.jquery.com/jquery-1.12.4.min.js'></script>
        <script type='text/javascript' src="{{asset('media/css/thnk.org/application.js')}}"></script>

        <script>
            if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-35807799-1']);
                _gaq.push(['_trackPageview']);
                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            }
            $(document).ready(function () {
                $.cookieCuttr({
                    cookieAnalytics: false,
                    cookiePolicyPage: true,
                    cookieDeclineButton: true,
                    cookiePolicyPageMessage: 'We uses cookies to tailor your experience and understand how you and other visitors use our website.<br><br>You can <a href="https://www.thnk.org/privacy-policy/#cookies" target= "_blank" title="read about our cookies">read about cookies here</a>.<br><br>'
                });    
            });
        </script>
    </head>

    <body>

        <!-- HEADER -->
        <header class="header header--home header--scrolled">
            <a href="#" class="header__logo"></a>
            <div class="header__menu"></div>
            <nav>
                <ul class="header__nav">


                    <li class="header__nav__item header__nav__item--dropdown">


                        <div class="header__nav__item__title">
                            Programs

                        </div>

                        <div class="nav__dropdown-container">

                            <ul class="nav__dropdown">

                                <img class="nav__close" src="http://ideajackpot.thnk.org/content/themes/thnk/assets/images/menu-close-white.svg">


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/programs/executive-leadership-program/">
                                        <div class="dropdown__item__title">Executive Leadership Program</div>
                                        <div class="dropdown__item__sub-title">For experienced leaders and innovators with big ideas and big hearts</div>
                                    </a>
                                </li>


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/programs/custom-programs/">
                                        <div class="dropdown__item__title">Custom programs</div>
                                        <div class="dropdown__item__sub-title">For senior leadership teams and purpose-driven organizations</div>
                                    </a>
                                </li>


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/programs/pop-up-partner-programs/">
                                        <div class="dropdown__item__title">Pop-Up + Partner Programs</div>
                                        <div class="dropdown__item__sub-title">Short format programs for leaders seeking inspiration</div>
                                    </a>
                                </li>


                            </ul>
                        </div>


                    </li>


                    <li class="header__nav__item header__nav__item--dropdown">


                        <div class="header__nav__item__title">
                            People

                        </div>

                        <div class="nav__dropdown-container">

                            <ul class="nav__dropdown">

                                <img class="nav__close" src="http://ideajackpot.thnk.org/content/themes/thnk/assets/images/menu-close-white.svg">


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/community/">
                                        <div class="dropdown__item__title">Community</div>
                                        <div class="dropdown__item__sub-title">Building a better future through collaboration around around the globe</div>
                                    </a>
                                </li>


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/community/faculty/">
                                        <div class="dropdown__item__title">Faculty</div>
                                        <div class="dropdown__item__sub-title">Designing and facilitating learning experiences</div>
                                    </a>
                                </li>


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/community/founders-and-partners/">
                                        <div class="dropdown__item__title">Founders + Partners</div>
                                        <div class="dropdown__item__sub-title">Guiding our expansion and future</div>
                                    </a>
                                </li>


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/community/global-advisory-board/">
                                        <div class="dropdown__item__title">Global Advisory Board</div>
                                        <div class="dropdown__item__sub-title">Shaping our vision and curriculum</div>
                                    </a>
                                </li>


                            </ul>
                        </div>


                    </li>


                    <li class="header__nav__item header__nav__item--dropdown">


                        <div class="header__nav__item__title">
                            About

                        </div>

                        <div class="nav__dropdown-container">

                            <ul class="nav__dropdown">

                                <img class="nav__close" src="http://ideajackpot.thnk.org/content/themes/thnk/assets/images/menu-close-white.svg">


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/about-us/">
                                        <div class="dropdown__item__title">About THNK</div>
                                        <div class="dropdown__item__sub-title">Why THNK – our vision, mission, values, and history</div>
                                    </a>
                                </li>


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/locations/">
                                        <div class="dropdown__item__title">Locations</div>
                                        <div class="dropdown__item__sub-title">Homes and hubs in cities around the world</div>
                                    </a>
                                </li>


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/resources/">
                                        <div class="dropdown__item__title">Resources</div>
                                        <div class="dropdown__item__sub-title">Creative Leadership books and open source innovation tools</div>
                                    </a>
                                </li>


                                <li class="nav__dropdown__item">
                                    <a href="https://www.thnk.org/insights/">
                                        <div class="dropdown__item__title">Insights</div>
                                        <div class="dropdown__item__sub-title">Research and Methodology</div>
                                    </a>
                                </li>


                            </ul>
                        </div>


                    </li>


                    <li class="header__nav__item">


                        <a href="https://www.thnk.org/blog/">Blog</a>


                    </li>


                </ul>
            </nav>
        </header>
        <!-- END HEADER -->

        <!-- MAIN -->
            @yield('content')
        <!-- END MAIN -->

        <!-- FOOTER -->
        <footer class="footer">
            <div class="footer__newsletter">

                <div class="newsletter form form--inline">
                    <h2 class="form__title newsletter__title">
                        Get leadership tips and innovation insights delivered straight to your inbox.<br>Sign up for our biweekly newsletter
                    </h2>

                    <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_7">
                        <form method="get" enctype="multipart/form-data" target="gform_ajax_frame_7" id="gform_7" action="https://www.thnk.org/#footer__newsletter">
                            <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_7" class="gform_button button" value="Sign me up" "window.open('https://www.thnk.org/?#footer__newsletter')">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="footer__box">

                <a href="#" class="footer__logo"><img src="http://ideajackpot.thnk.org/content/themes/thnk/assets/images/THNK_logo_white.png" alt="THNK"></a>

                <ul class="footer__menu">

                    <li class="footer__menu__column">

                        <a class="footer__menu__title" href="https://www.thnk.org/locations/amsterdam/">Amsterdam</a>
                        <p class="footer__menu__description">
                            Keizersgracht 264<br> 1016 EV Amsterdam<br> The Netherlands<br>
                            <a href="mailto:amsterdam@thnk.org" target="_top" class="gtrackexternal">amsterdam@thnk.org</a><br> Telephone: +31 20 684 25 06<br>
                            <a href="https://www.google.nl/maps/place/Keizersgracht+264,+1016+EV+Amsterdam/@52.3715626,4.8841627,17z/data=!3m1!4b1!4m2!3m1!1s0x47c609c36936ba15:0x914b91668418128b" target="_blank" class="gtrackexternal">Show on Google maps</a>
                        </p>
                    </li>

                    <li class="footer__menu__column">

                        <a class="footer__menu__title" href="https://www.thnk.org/locations/lisbon/">Lisbon</a>
                        <p class="footer__menu__description">
                            Oceanário de Lisboa S.A. Auditório<br> Mar da Palha<br> s/nº Esplanada Dom Carlos I<br> 1990-005 Lisbon, Portugal<br>
                            <a href="mailto:lisbon@thnk.org" target="_top" class="gtrackexternal">lisbon@thnk.org</a><br>
                            <a href="https://www.google.nl/maps/place/Lisbon+Oceanarium/@38.763544,-9.093742,17z/data=!3m1!4b1!4m2!3m1!1s0xd193183750e5809:0x983f2e673a62e130" target="_blank" class="gtrackexternal">Show on Google Maps</a>
                        </p>
                    </li>

                    <li class="footer__menu__column">

                        <a class="footer__menu__title" href="https://www.thnk.org/locations/vancouver/">Vancouver</a>
                        <p class="footer__menu__description">
                            <a href="mailto:vancouver@thnk.org" target="_top" class="gtrackexternal">vancouver@thnk.org</a><br>
                        </p>
                    </li>

                    <li class="footer__menu__column">
                        <h3 class="footer__menu__title">Follow</h3>
                        <a href="http://www.facebook.com/THNKschool" target="_blank" class="footer__menu__link gtrackexternal">Facebook</a>
                        <a href="http://twitter.com/THNKschool" target="_blank" class="footer__menu__link gtrackexternal">Twitter</a>
                        <a href="http://www.linkedin.com/company/969087" target="_blank" class="footer__menu__link gtrackexternal">Linkedin</a>
                        <a href="http://www.youtube.com/user/THNKAmsterdam?sub_confirmation=1" target="_blank" class="footer__menu__link gtrackexternal">Youtube</a>
                        <a href="https://medium.com/@THNK" target="_blank" class="footer__menu__link gtrackexternal">Medium</a>
                    </li>
                </ul>

            </div>

        </footer>
        <!-- END FOOTER -->
    </body>
</html>