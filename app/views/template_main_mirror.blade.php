<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>

	<title>@lang('mainTemplate.title')</title>

	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta name="keywords" content="@lang('mainTemplate.head.keywords')" />
    <meta name="description" content="@lang('mainTemplate.head.description')" />
    <meta name="robots" content="index, follow" />
    <meta property="og:url" content="http://www.360mirror.org" />
    <meta property="og:title" content="@lang('mainTemplate.title')" />
    <meta property="og:description" content="@lang('mainTemplate.head.description')" />
    <meta property="og:image" content="{{asset('media/images/ipad_report.png')}}" />

        <link rel="shortcut icon" href="{{asset('media/icons/favicon.ico')}}" type="image/x-icon">
        <link rel="icon" href="{{asset('media/icons/favicon.ico')}}" type="image/x-icon">

        <link href="{{asset('media/fonts/NeuBauGrotesk/stylesheet.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('media/css/stylesheet.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('media/css/global.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('media/javascript/themes/base/jquery.ui.all.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('media/javascript/fancybox/jquery.fancybox-1.3.1.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{asset('media/javascript/cookies/assets/cookiecuttr.css')}}" rel="stylesheet">
        <script src="{{asset('media/javascript/cookies/assets/old/jquery-1.7.1.js')}}"></script>
        <script src="{{asset('media/javascript/cookies/assets/old/jquery.cookie.js')}}"></script>
        <script src="{{asset('media/javascript/cookies/jquery.cookiecuttr.js')}}"></script>

        <script src="{{asset('media/javascript/custom-form-elements.js')}}"></script>
        
        <script src="{{asset('media/javascript/jquery.validate.js')}}"></script>
        <script src="{{asset('media/javascript/custom-sugarfree.js')}}"></script>
        <script src="{{asset('media/javascript/fancybox/jquery.fancybox-1.3.1.pack.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.core.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.widget.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.tooltip.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.mouse.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.button.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.draggable.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.position.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.sortable.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.dialog.js')}}"></script>
        <script src="{{asset('media/javascript/ui/jquery.ui.tabs.js')}}"></script>
        <script src="{{asset('media/javascript/tablesort.js')}}"></script>
        <script src="{{asset('media/javascript/jquery.tablesorter.min.js')}}"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $(".inviteForm1").validate();
                $(".inviteForm2").validate();
                $(".inviteForm3").validate();
                $(".inviteForm4").validate();
                $(".inviteForm5").validate();
                $(".inviteForm6").validate();

                 $("#commentForm").validate();

            $( ".sortable" ).sortable({
            revert: true,
            stop         : function(event,ui){

                $.ajax
                ({
                    type: "POST",
                    url: "/clients/postOrderQuestions",
                    data: $(this).sortable("serialize"),
                    success: function(data)
                    {
                    $('.flash').show();
                    $('.flash').html("Output opgeslagen")
                    }
                    });

                    setTimeout(function()
                    {
                    $(".flash").slideUp("slow", function () {
                    $(".flash").hide();
                }); }, 3000);

            }});

            $( ".sortable" ).disableSelection();


            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {

                $(function() {
                    $( ".tabs" ).tabs();
                 });

                $(".slide").fancybox({
                    'onComplete': function () { $("body").css("overflow", "hidden"); },
                    'onClosed': function () { $("body").css("overflow", "scroll"); },
                    'autoScale'         : true,
                    'transitionIn'      : 'none',
                    'transitionOut'     : 'none',
                    'type'              : 'iframe'
                });

                $(".delete").click(function() {
                  var myS=$(this);
                  var myForm = myS.parent();
                  var d = $("<div/>",{text:"Are you sure you want to remove the selected respondent?"})
                    .dialog({
                        modal:true,
                        close:function() {
                            $(this).dialog("destroy").remove();
                        },
                        buttons: {
                            'Remove': function() {
                                myForm.submit();
                                return true;
                            },
                            'Cancel': function() {
                                $(this).dialog('close');
                            }
                        }
                    });
                    return false;
                });

            });
        </script>
        
        <script>
            if (jQuery.cookie('cc_cookie_decline') == "cc_cookie_decline") {
            } else {
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-35807799-1']);
                _gaq.push(['_trackPageview']);
                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            }
        </script>

        <style type="text/css">
            #commentForm {  }
            #commentForm { }
            #commentForm label { }
            #commentForm input.error { border: 1px solid #ff0000 }
            #commentForm label.error, #commentForm input.submit { margin-left: 10px; }

            #signupForm {  }
            #signupForm { }
            #signupForm label.error { color: #ff0000; }
            #signupForm input.error { border: 1px solid #ff0000 }
            #signupForm label.error, #signupForm input.submit { margin-left: 10px; }

            .validateForm {  }
            .validateForm { }
            .validateForm label { }
            .validateForm input.error { border: 1px solid #ff0000 }
            .validateForm label.error, .validateForm input.submit { margin-left: 10px; }
        </style>

</head>

<body>
    <header id="header">
        <div class="container">
            <div class="branding">
                <div class="site-name">
                    @if (!Auth::user()->hasRole('USER_THNK_ADMIN') )
                        <?php
                        if(isset($user)){
                            if( Auth::user()->hasRoleInClient(Role::USER_PARTICIPANT, $user->id, Auth::user()->id) ){ ?>
                                <a title="Home" rel="home" href="{{ translate_url('/dashboard/' . $user->id) }}" class="logo">

                            <?php }else{ ?>
                                <a title="Home" rel="home" href="{{ translate_url('/clients/getAdmin/' . $user->id) }}" class="logo">
                            <?php }

                            $currentURL = Request::url();
                            if (strpos( $currentURL, '192.168.10.150') !== false ) {
                                //MAC FIX
                                $tempFix = "http://192.168.10.150/";
                                $imageSize = getimagesize($tempFix."media/images/clientLogo/".$user->logo_name."");
                            } elseif ( (strpos($currentURL, '192.168.33.10') !== false) || (strpos($currentURL, '.vagrant') !== false) ) {
                                //MAC FIX
                                $tempFix = "http://192.168.33.10/";
                                $imageSize = getimagesize($tempFix."media/images/clientLogo/".$user->logo_name."");
                            } elseif ( (strpos($currentURL, '192.168.1.249') !== false) || (strpos($currentURL, '.vagrant') !== false) ) {
                                //MAC FIX
                                $tempFix = "http://192.168.1.249/";
                                $imageSize = getimagesize($tempFix."media/images/clientLogo/".$user->logo_name."");
                            } elseif (strpos($currentURL, '.local') !== false) {
                                //LOCAL FIX
                                $imageSize = getimagesize(public_path("media/images/clientLogo/".$user->logo_name.""));
                            } elseif (strpos($currentURL, '360mirrorvagrant') !== false) {
                                //LOCAL FIX
                                $image_filename = public_path("media/images/clientLogo/".$user->logo_name."");
                                if (!file_exists($image_filename)){
                                    $imageSize = [200,60];
                                }else {
                                    $imageSize = getimagesize($image_filename);
                                }
                            } elseif (strpos($currentURL, '360mirror.thnk.org') !== false) {
                                //DEV FIX
                                $tempFix = "/home/thnk/domains/360mirror.thnk.org/360mirror-dev/public_html/";
                                $imageSize = getimagesize($tempFix."media/images/clientLogo/".$user->logo_name."");
                            } else {
                                //PROD FIX
                                $tempFix = "/home/360mirror/dev360/public_html/";
                                $imageSize = getimagesize($tempFix."media/images/clientLogo/".$user->logo_name."");
                            }
                            
                            $maxWidth = 200;
                            $maxHeight = 60;
                            $newHeight = ($maxWidth*$imageSize[1])/$imageSize[0];
                            if($newHeight>$maxHeight){
                                $newWidth = ($imageSize[0]*$maxHeight)/$imageSize[1];
                                echo '<img src="'.asset("media/images/clientLogo/".$user->logo_name."").'" style="height: '.$maxHeight.'px!important; width: '.$newWidth.'px!important; " alt="360 Mirror Logo">';
                            }else{
                                echo '<img src="'.asset("media/images/clientLogo/".$user->logo_name."").'" style="height: '.$newHeight.'px!important; width: '.$maxWidth.'px!important; " alt="360 Mirror Logo">';
                            }
                            
                            echo '</a>';
                        }else{
                            $user = array();
                            $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            $path = parse_url($url, PHP_URL_PATH);
                            $parts = explode("/", $path);

                            foreach($parts as $val){
                                if( is_numeric($val) ){
                                    $clientId = DB::table('users')
                                        ->select('id')
                                        ->join('users_roles', 'users.id', '=', 'users_roles.user_id')
                                        ->where('users_roles.user_id', '=', (int)$val )
                                        ->where('users_roles.role_id', '=', Role::USER_CLIENT)
                                        ->get();
                                }
                            }

                            if(count($clientId)<1){
                                ?>
                                <script>
                                    window.location.href = '/';
                                </script>
                                <?php
                            }else{
                                $user = User::find($clientId[0]->id);
                                if( Auth::user()->hasRoleInClient(Role::USER_PARTICIPANT, $user->id, Auth::user()->id) ){ ?>
                                    <a title="Home" rel="home" href="{{ translate_url('/dashboard/' . $user->id) }}" class="logo">
                                <?php }else{ ?>
                                    <a title="Home" rel="home" href="{{ translate_url('/clients/getAdmin/' . $user->id) }}" class="logo">
                                <?php }

                                //PROD FIX
                                $tempFix = "/home/360mirror/dev360/public_html/";
                                $imageSize = getimagesize($tempFix."media/images/clientLogo/".$user->logo_name."");
                                
                                //LOCAL FIX
                                //$imageSize = getimagesize(public_path("media/images/clientLogo/".$user->logo_name.""));
                                

                                $newWidth = ($imageSize[0]*38)/$imageSize[1];
                                echo '<img src="'.asset("media/images/clientLogo/".$user->logo_name."").'" style="height: 38px !important; width: '.$newWidth.'px!important; " alt="360 Mirror Logo">';
                                echo '</a>';
                            }
                        }
                        ?>
                    @else
                        <?php
                        //THNK ADMIN ONLY ?>
                        <a title="Home" rel="home" href="{{ translate_url('/clients') }}" class="logo">
                        <?php echo '<img src="'.asset("media/images/clientLogo/THNK_logo-BIG_RGB.png").'" style="height: 38px !important; " alt="360 Mirror Logo">';
                        echo '</a>';
                        ?>
                    @endif
                </div>
            </div>
            <nav class="block-menu">
                <ul class="menu">
                    
                    @if (Auth::user()->hasRole('USER_THNK_ADMIN'))
                    <li class="mainNav">
                        <a href="{{ translate_url('/clients') }}" title="" class="@if  (Request::is('clients')) active @endif">
                            <span class="vert">
                                <span>
                                    @lang('mainTemplate.navigation.admin')
                                </span>
                            </span>
                        </a>
                    </li>
                    @else

                        @if (Auth::user()->hasRole('USER_PARTICIPANT'))

                        <?php

                            //CHECK IF USER IS PARTICIPANT FOR THE CURRENT CLIENT
                            $userParticipantInCurrentClient = DB::table('users_roles')
                                ->select('*')
                                ->where('users_roles.user_id', '=', Auth::user()->id)
                                ->where('users_roles.role_id', '=', Role::USER_PARTICIPANT)
                                ->where('client_id', '=', $user->id)
                                ->get();

                            $num_rows = count($userParticipantInCurrentClient);

                            if ($num_rows > 0) {
                        ?>                        
                                <li class="main partNav" id="outLi">
                                    <a href="#" class="@if  (Request::is('dashboard')) active @elseif (Request::is('dashboard/*')) active @endif">
                                        <span class="vert">
                                            <span id="down">
                                                @lang('mainTemplate.navigation.participant')
                                            </span>
                                        </span>
                                    </a>
                                    <ul class="partUl">
                                        <?php
                                         $clients = DB::table('users_roles')
                                                  ->select('*')
                                                  ->where('user_id', '=', Auth::user()->id)
                                                  ->where('role_id', '=', Role::USER_PARTICIPANT)                                                  
                                                  ->get();

                                            for($y=0;$y<count($clients);$y++){
                                               $client = User::find($clients[$y]->client_id);
                                               $clientUrl = '/dashboard/'.$client->id;
                                        ?>
                                                <li>
                                                    <a href="{{ translate_url( $clientUrl ) }}" title="" class="@if (Request::is(@clientUrl)) active @endif">
                                                        <span class="vert">
                                                            <span>
                                                                <?php echo $client->company;?> @lang('mainTemplate.navigation.dashboard')
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                        <?php
                                            }
                                        ?>
                                        <li>
                                            <?php
                                                $url = '/test/myprofile/'.$user->id.'/'.Auth::user()->id;
                                            ?>
                                            <a href="{{ translate_url( $url ) }}" title="" class="@if (Request::is(@url)) active @endif">
                                                <span class="vert">
                                                    <span>
                                                        @lang('mainTemplate.navigation.profile')
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                        <?php
                            }
                        ?>
                        @endif
                        @if ( Auth::user()->hasRole('USER_CLIENT') || Auth::user()->hasRole('USER_CLIENT_ADMIN') || Auth::user()->hasRole('USER_CLIENT_COACH') )
                        <li class="main adminNav" id="outLi">
                            <a href="#" class="@if  (Request::is('clients/getAdmin/*')) active @elseif (Request::is('clients/getAdmin')) active @endif">
                                <span class="vert">
                                    <span id="down">
                                            @lang('mainTemplate.navigation.client')
                                    </span>
                                </span>
                            </a>
                            <ul class="adminUl">
                                @if ( Auth::user()->hasRole('USER_CLIENT') || Auth::user()->hasRole('USER_CLIENT_ADMIN'))
                                    
                                    @if (Auth::user()->hasRoleInClient(Role::USER_CLIENT, $user->id, Auth::user()->id))

                                        <?php
                                            $clientUrl = '/clients/getAdmin/'.Auth::user()->id;
                                        ?>

                                       <li>
                                            <a href="{{ translate_url( $clientUrl ) }}" title="" class="@if (Request::is(@clientUrl)) active @endif">
                                                <span class="vert">
                                                    <span>
                                                        <?php echo Auth::user()->company;?> @lang('mainTemplate.navigation.dashboard')
                                                    </span>
                                                </span>
                                            </a>
                                        </li>

                                    @else

                                        <?php
                                         $clients = DB::table('users_roles')
                                                  ->select('*')
                                                  ->where('user_id', '=', Auth::user()->id)
                                                  ->where('role_id', '=', Role::USER_CLIENT_ADMIN)                                                  
                                                  ->get();
                                            $checkClient = [];
                                            for($y=0;$y<count($clients);$y++){
                                                
                                               $client = User::find($clients[$y]->client_id);
                                               array_push($checkClient, $client->id);
                                               $clientUrl = '/clients/getAdmin/'.$client->id;
                                        ?>
                                                <li>
                                                    <a href="{{ translate_url( $clientUrl ) }}" title="" class="@if (Request::is(@clientUrl)) active @endif">
                                                        <span class="vert">
                                                            <span>
                                                                <?php echo $client->company;?> @lang('mainTemplate.navigation.dashboard')
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                        <?php
                                            }
                                        ?>
                                    @endif
                                @endif
                                @if ( Auth::user()->hasRole('USER_CLIENT_COACH') )
                                    <?php

                                         $clientsCoaches = DB::table('users_roles')
                                                  ->select('*')
                                                  ->where('user_id', '=', Auth::user()->id)
                                                  ->where('role_id', '=', Role::USER_CLIENT_COACH)
                                                  ->get();

                                        for($y=0;$y<count($clientsCoaches);$y++){
                                            
                                           $client = User::find($clientsCoaches[$y]->client_id);

                                           if(isset($checkClient) && in_array($client->id, $checkClient))
                                                continue;
                                           $clientUrl = '/clients/getAdmin/'.$client->id;
                                    ?>
                                            <li>
                                                <a href="{{ translate_url( $clientUrl ) }}" title="" class="@if (Request::is(@clientUrl)) active @endif">
                                                    <span class="vert">
                                                        <span>
                                                            <?php echo $client->company;?> @lang('mainTemplate.navigation.dashboard')
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                    <?php
                                        }
                                    ?>
                                @endif
                            </ul>
                        </li>
                        @endif

                    @endif

                    @if (Auth::user()->hasRole('USER_THNK_ADMIN'))
                        @if ( Auth::user()->multilanguage == 1 )
                        <li class="locale-picker">
                            <a href="#">
                                <span class="vert">
                                    <span id="down">
                                        {{ $locale->getLocalLabel() }}
                                    </span>
                                </span>
                            </a>
                            <ul class="locale-picker__options">
                                @foreach($availableLocales as $otherLocale)
                                    <li class="locale-picker__option @if($otherLocale->equals($locale)) locale-picker__option--current @endif">
                                        <a href="{{ $urlTranslator->translate(Request::url(), $otherLocale) }}">
                                            <span class="vert"><span>{{ $otherLocale->getLocalLabel() }}</span></span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        @endif
                    @else
                        @if ( $user->multilanguage == 1 )
                        <li class="locale-picker">
                            <a href="#">
                                <span class="vert">
                                    <span id="down">
                                        {{ $locale->getLocalLabel() }}
                                    </span>
                                </span>
                            </a>
                            <ul class="locale-picker__options">
                                @foreach($availableLocales as $otherLocale)
                                    <li class="locale-picker__option @if($otherLocale->equals($locale)) locale-picker__option--current @endif">
                                        <a href="{{ $urlTranslator->translate(Request::url(), $otherLocale) }}">
                                            <span class="vert"><span>{{ $otherLocale->getLocalLabel() }}</span></span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        @endif
                    @endif
                    
                    <li class="logout outNav" id="outLi">
                        <a href="#">
                            <span class="vert">
                                <span id="down">
                                    @lang('mainTemplate.navigation.welcome') <?php echo Auth::user()->firstname ?>
                                </span>
                            </span>
                        </a>
                        <ul class="outUl">
                            <li>
                                @if (Auth::user()->hasRole('USER_THNK_ADMIN'))
                                <a href="{{ translate_url( '/clients/logout/0' ) }}">
                                @else
                                <a href="{{ translate_url( '/clients/logout/' . $user->id) }}">
                                @endif
                                    <span class="vert">
                                        <span>
                                            @lang('mainTemplate.navigation.logout')
                                        </span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </nav>
        </div>
    </header>

    <div class="pusher" id="wrapper">
        <div class="content-top" id="mainCntr">
            <div class="container">

        		<!--  / content container \ -->
        		<div id="contentCntr">

                            @yield('content')

        		</div>
        		<!--  \ content container / -->
                <div class="thnk_32"></div>

            </div>
        </div>
    </div>

    <?php
        if (isset($user) && $user->THNKGGF_footer != 1){
    ?>
    <footer id="footer">
        <div class="container">
            <div class="f3c">
                <h3>@lang('mainTemplate.footer.by')</h3>
            </div>
            <div class="f3c">
                <a href="http://www.thnk.org" target="_blank">
                    <img src="{{asset('media/images/thnk_footer.png')}}">
                </a>
            </div>
            <div class="f3c">
                <div id="f3c_mail">
                    <a href="mailto:feedback@thnk.org" target="_blank">
                        <img id="f3c_img_mail"src="{{asset('media/images/email_icon_32x32.png')}}">
                        <span id="f3c_txt_mail">@lang('mainTemplate.footer.feedback')</span>
                    </a>
                </div>
            </div>
        </div>
    </footer>
    <?php
        }else{
    ?>
    <footer id="footer">
        <div class="container">
            <div class="f4c">
                <h3>@lang('mainTemplate.footer.by')</h3>
            </div>
            <div class="f4c">
                <a href="http://www.thnk.org" target="_blank">
                    <img src="{{asset('media/images/thnk_footer.png')}}">
                </a>
            </div>
            <div class="f4c">
                <a href="http://www.globalgoodfund.org" target="_blank">
                    <img src="{{asset('media/images/ggf_footer.png')}}">
                </a>
            </div>
            <div class="f4c">
                <div id="f4c_mail">
                    <a href="mailto:feedback@thnk.org" target="_blank">
                        <img id="f4c_img_mail"src="{{asset('media/images/email_icon_32x32.png')}}">
                        <span id="f4c_txt_mail">@lang('mainTemplate.footer.feedback')</span>
                    </a>
                </div>
            </div>
        </div>
    </footer>
    <?php
        }
    ?>

    <script type="text/javascript">
        $(".outUl").hide();
        $(".partUl").hide();
        $(".adminUl").hide();
        $(".locale-picker__options").hide();

        $(".locale-picker").hover(
            function() {
                $( ".locale-picker__options" ).show();
                $(".locale-picker").addClass("logSelColor");
            }, function() {
                $( ".locale-picker__options" ).hide();
                $(".locale-picker") .removeClass("logSelColor");
            }
        );

        $(".outNav").hover(
            function() {
                $( ".outUl" ).show();
                $(".outNav") .addClass("logSelColor");
            }, function() {
                $( ".outUl" ).hide();
                $(".outNav") .removeClass("logSelColor");
            }
        );

        $(".partNav") .hover(
            function() {
                $( ".partUl" ).show();
                $(".partNav") .addClass("logSelColor");
            }, function() {
                $( ".partUl" ).hide();
                $(".partNav") .removeClass("logSelColor");
            }
        );

        $(".adminNav") .hover(
            function() {
                $( ".adminUl" ).show();
                $(".adminNav") .addClass("logSelColor");
            }, function() {
                $( ".adminUl" ).hide();
                $(".adminNav") .removeClass("logSelColor");
            }
        );
    </script>

</body>

</html>
