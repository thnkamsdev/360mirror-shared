<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>

	<title>@lang('mainTemplate.title')</title>

	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta name="keywords" content="@lang('mainTemplate.head.keywords')" />
	<meta name="description" content="@lang('mainTemplate.head.description')" />
	<meta name="robots" content="noindex, nofollow" />

	<link rel="shortcut icon" href="{{asset('media/icons/favicon.ico')}}" type="image/x-icon">
	<link rel="icon" href="{{asset('media/icons/favicon.ico')}}" type="image/x-icon">

	<link href="{{asset('media/fonts/NeuBauGrotesk/stylesheet.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('media/css/stylesheet.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('media/css/global.css')}}" rel="stylesheet" type="text/css" />

	<script src="{{asset('media/javascript/custom-form-elements.js')}}"></script>
	<script src="{{asset('media/javascript/jquery-1.4.2.min.js')}}"></script>
	<script src="{{asset('media/javascript/jquery.validate.js')}}"></script>
	<script src="{{asset('media/javascript/custom-sugarfree.js')}}"></script>
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35807799-1']);
		_gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
</head>

<body>
	<div class="popup">
		@yield('content')
	</div>
</body>
</html>
