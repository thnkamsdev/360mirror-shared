@extends('template_main_mirror')
@section('content')

<!--  / left container \ -->
<div id="leftCntr">
    <!--  / test box \ -->
    <div class="testBox">

        <?php
            $tests = array();

            if ( $participant->hasRoleInClient(Role::USER_PARTICIPANT, $user->id, $participant->id) ) {

                if( $user->id == User::ENDEAVOR_SCAN ) {
                    echo '
                    <h2>'.trans('dashboard.sidebar.title').'</h2>
                    <ul>
                        <div class="new_ref_button">';
                        if($endeavor != null){ ?>
                            <a href="{{ translate_url('/endeavor/' . $user->id . '/' . $participant->id) }}" id="new_ref_button3" class="button">@lang('dashboard.sidebar.buttons.update')</a>
                        <?php }else{ ?>
                            <a href="{{ translate_url('/endeavor/' . $user->id . '/' . $participant->id) }}" id="new_ref_button2" class="button red">@lang('dashboard.sidebar.buttons.new')</a>
                        <?php }
                    echo '
                        </div>
                    </ul>
                    <div class="spacer8" id="space6"></div>
                    ';
                }

                echo "<h2>" . $participant->firstname . " ". trans('dashboard.sidebar.tests.title') ."</h2>";
                $tests = Test::getTestsByUser($participant->id, $user->id);                        
            }
        ?>

        <ul>
            <?php

            if ($tests != null)
                $number_of_tests = $tests->count();
            else
                $number_of_tests = 0;

            if ($number_of_tests == 0 ) {
            ?>

                <script>
                    window.location.href = "{{ translate_url('/test/edit/' . $user->id) }}";
                </script>

            <?php
            } else {                        
                $i = 0;
                foreach ($tests as $test) {
                    $a = $number_of_tests - $i;
                    $i++;

                    if ($test->id == $selected_test->id) {
                        $c = "selected";
                    } else {
                        $c = "second";
                    }
                ?>
                	<li class="{{ $c }}">
                        <?php
                        //FINISH YOUR TEST
                        if ($test->teststatus_id == Teststatus::JUST_STARTED
                            || $test->teststatus_id == Teststatus::NOT_STARTED
                        ) {
                        ?>
                        <a href="{{ translate_url('/test/edit/' .$user->id. '/' . $test->id ) }}">
                        <?php
                        } else {
                        //TEST FINISHED BY PARTICIPANT
                        ?>
                        <a href="{{ translate_url('/dashboard/index/' .$user->id. '/' . $test->id ) }}">
                        <?php
                        }
                        ?>
                            <span class="open">@lang('dashboard.sidebar.tests.open')</span>

                            <span class="one">
                                <?= $a ?>
                            </span>

                            <span class="left">
                                <span class="time">
                                    <?= date(trans('dashboard.sidebar.tests.date'), strtotime($test->created_at)); ?>
                                </span>
                                <span class="text">
                                    <?php
                                       echo ($test->teststatus->name);
                                    ?>
                                </span>
                            </span>
                        </a>
                    </li>
                <?php } ?>
                <div class="new_ref_button">
                    <a href="{{ translate_url('/test/edit/' . $user->id) }}" id="new_ref_button" class="button">@lang('dashboard.sidebar.buttons.create')</a>
                </div>
            <?php } ?>
        </ul>

	</div>
	<!--  \ test box / -->
</div>
<!--  \ left container / -->

<!--  / right container \ -->
<div id="rightCntr">
    <?php

        if ( $number_of_tests > 0 || $user->hasRole('USER_THNK_ADMIN') ) {
    ?>
		<!--  / dash box \ -->
    <div class="dashBox">

        {{ Form::open(array('id' => 'minimumForm', 'enctype' => 'multipart/form-data')) }}
        	<fieldset>

                <ul>
                    <li>
                        <?php
                        if (isset($feedback_reminder) && $feedback_reminder) {
                            echo trans('dashboard.main.sent');
                        }

                         if (isset($feedback_delete) && $feedback_delete) {
                            echo trans('dashboard.main.deleted');
                        }

                        ?>
                        
                        <h2><?php $client = User::find($user->id); echo $client->company; ?> @lang('dashboard.dashboard'): <span><?= date(trans('dashboard.sidebar.tests.date'), strtotime($selected_test->created_at)); ?> </span></h2>

                        @if($errors->has())
                            <div class="inviteBox dashErr">
                                <div class="errorsTop">
                                    <h3>@lang('dashboard.main.errors.title')</h3>
                                    <ul id="#respondents_list"> 
                                    @foreach ($errors->all() as $error)
                                        <li id="secError">{{ $error }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <p id="congrats">
                        <?php
                        if ($selected_test->teststatus_id >= Teststatus::FINISHED_BY_USER_OR_RESPONDENT) {
                            ?>
                                @lang('dashboard.main.tests.completed')<br />
                            <?php
                        }

                      if ($selected_test->teststatus_id == Teststatus::JUST_STARTED
                                  || $selected_test->teststatus_id == Teststatus::NOT_STARTED) {

                          echo "<div class=\"completeBox2\">"; ?><a href="{{ translate_url('/test/edit/' .$user->id. '/' . $selected_test->id ) }}" class='button finishTest'><?php echo trans('dashboard.main.tests.pending') . "</a></div>";
                        } else {
                            echo trans('dashboard.main.welcome');
                        }
                        ?>
                        </p>
                    </li>

                    <li class="next">
                        <h2>@lang('dashboard.main.progressTitle')</h2>

                    </li>
                </ul>

            </fieldset>
        </form>

    </div>
    <!--  \ dash box / -->

     <?php

     $number_of_respondents = 0;
     $ammount_completed = 0;


    foreach ($respondents as $respondent) {

        $number_of_respondents++;
        $e = '';
            if ($number_of_respondents==1) {
                $e = 'first';
                ?>

            <!--  / response box \ -->
            <div class="responseBox">

                <div class="top">

                    <h2 id="respondentsH2">@lang('dashboard.main.respondents')</h2>

                </div>

                <ul>

                <?php
            }
            ?>

            <li class="<?= $e ?>">

                <div class="one">

                    <?php
                        $respondentgroup = Respondentgroup::find($respondent->respondentgroup_id);
                        $respName = $respondentgroup->name;
                        echo "<h3>". trans('test.respondents.'. $respName) ."</h3>";
                    ?>

                </div>
                <div class="two">

                    <h4><?= $respondent->name ?></h4>



                    <?php

                    $respondent_test = Test::getFirstTestsByRespondent($respondent->id);

                    if ($respondent_test->teststatus_id == Teststatus::FINISHED_BY_USER_OR_RESPONDENT || $respondent_test->teststatus_id == Teststatus::TEST_FULLY_FINISHED) {
                        echo "<span>". trans('dashboard.main.tests.status.completed') ."</span>";
                        $ammount_completed++;
                    } elseif ($respondent_test->teststatus_id == 0) {
                        echo "<span class='red'>". trans('dashboard.main.tests.status.pending') ."<br/><br/>". trans('dashboard.main.tests.reminder') ."<br/>";
                        if ($respondent_test->last_reminder != "0000-00-00 00:00:00")
                            echo date(trans('dashboard.sidebar.tests.date'), strtotime($respondent_test->last_reminder));
                        else
                            echo "-";

                        echo "<br>" . $respondent->email;
                        echo "</span>";
                    } else {
                        echo "<span class='yellow'>". trans('dashboard.main.tests.status.inProgress') ."<br/><br/>". trans('dashboard.main.tests.reminder') ."<br/>";
                        if ($respondent_test->last_reminder != "0000-00-00 00:00:00")
                            echo date(trans('dashboard.sidebar.tests.date'), strtotime($respondent_test->last_reminder));
                        else
                            echo "-";

                        echo "<br>" . $respondent->email;
                        echo "</span>";
                    }
                    ?>


                </div>
                <div class="last">

                    <?php if ($respondent_test->teststatus_id != Teststatus::FINISHED_BY_USER_OR_RESPONDENT) { ?>
                            
                <form method="POST" action="{{ translate_url('/dashboard/' . $user->id) }}" id="remindForm">
                        <input type='submit' value="@lang('dashboard.main.buttons.reminder')" name="knop_reminder" class="btn aanmeld">
                        <input type ="hidden" name="test_id_reminder" value="<?= $respondent_test->id ?>"/>
                        <input type ="hidden" name="client_id" value="<?= $user->id ?>"/>
                        <input type ="hidden" name="user_id" value="<?= Auth::user()->id ?>"/>
                </form>
                <form method="POST" action="{{ translate_url('/dashboard/' . $user->id) }}" id="remindForm">
                        <input type='submit' value="@lang('dashboard.main.buttons.removeUser')" name="knop_reminder" class="btn aanmeld differ delete grey">
                        <input type ="hidden" name="test_id_delete" value="<?= $respondent_test->id ?>"/>
                        <input type ="hidden" name="client_id" value="<?= $user->id ?>"/>
                        <input type ="hidden" name="user_id" value="<?= Auth::user()->id ?>"/>
                </form>
                    <?php }?>

                </div>

            </li>

                <?php
            }

    if ($number_of_respondents > 0 ) {
    ?>
                </ul></div>
    <?php }


    ?>


    <!--  \ response box / -->
    <?php
        if ($selected_test->teststatus_id == Teststatus::FINISHED_BY_USER_OR_RESPONDENT)  {
        // only visible when there are respondents that still need to finish it

            if (!Test::getTestAreFinished($selected_test->id)) {
    ?>
                <br/>
                <div class="completeBox2">
                    <a href="{{ translate_url('/test/invite_respondents/' . $user->id . '/' . $selected_test->id) }}" class="button">@lang('dashboard.main.addRespondents')</a>
                </div>
    <?php
            }
        }
        // or if Teststatus::ALL_FINISHED_MIN_REQ_NOT_REACHED
        if ($selected_test->teststatus_id == Teststatus::ALL_FINISHED_MIN_REQ_NOT_REACHED) {
    ?>
            <br/>
            <div class="completeBox2">
                <a href="{{ translate_url('/test/invite_respondents/' . $user->id . '/' . $selected_test->id) }}" class="button">@lang('dashboard.main.addRespondents')</a>
            </div>
    <?php
        }
    ?>


    <!--  / view box \ -->
    <div class="viewBox">

        <h2>@lang('dashboard.main.reports.title')</h2>

        <ul>
            <?php
            if ($selected_test->teststatus_id >= Teststatus::FINISHED_BY_USER_OR_RESPONDENT) {
            ?>
        	<li>
                <div class="space3">
                    <p class="ready">@lang('dashboard.main.reports.status.ready')</p>
                </div>
                <div class="space3">
                    {{ Form::open(array('id' => 'commentForm', 'action' => "generate_own_report" ,'target' => '_blank', 'enctype' => 'multipart/form-data')) }}
                            <input type='hidden' value='<?= $selected_test->id ?>' name="generate_own_report_id" class=""/>
                            <input type ="hidden" name="client_id" value="<?= $user->id ?>"/>
                            <?php echo"<input type='submit' value='". trans('dashboard.main.buttons.self') ."' name='knop_report' class='button '/>";?>
                    <?= form::close();?>
                </div>
                <div class="space1">
                    <?php echo '<img src="'.asset("media/images").'/info.png"  alt="info" class="help-dashboard" id="manage-users-info" title="'. trans("dashboard.main.help.self") .'">';?>
                </div>
            </li>
            <?php } else {
            ?>
            <li>
                <div class="space3">
                    <p class="not-ready">@lang('dashboard.main.reports.status.notReady')</p>
                </div>
                <div class="space3">
                    <a href="#" class="button disabled" rel="viewselfassessment">@lang('dashboard.main.buttons.360')</a>
                </div>
                <div class="space1">
                    <?php echo '<img src="'.asset("media/images").'/info.png"  alt="info" class="help-dashboard" id="manage-users-info" title="'. trans("dashboard.main.help.360") .'">';?>
                </div>
            </li>
            <?php
            }
            ?>
        	<li class="second">

                <?php

                if ($selected_test->teststatus_id < Teststatus::FINISHED_BY_USER_OR_RESPONDENT){
                ?>
                    <div class="space3">
                        <p>There are not enough completed assessments yet</p>
                    </div>
                    <div class="space3">
                        <a href="#" class="disabled button" rel="viewfullreport_disabled">@lang('dashboard.main.buttons.360')</a>
                    </div>
                    <div class="space1">
                        <?php echo '<img src="'.asset("media/images").'/info.png"  alt="info" class="help-dashboard" id="manage-users-info" title="'. trans("dashboard.main.help.360") .'">';?>
                    </div>
                <?php
                }else{
                    if ($selected_test->teststatus_id == Teststatus::TEST_FULLY_FINISHED ||
                    $selected_test->amount_completed() >= $selected_test->minimum_required
                    ) {
                        echo '<div class="space3">';
                        if($user->split_resp == 1){
                            echo "<p class='groen'>". trans('dashboard.main.reports.status.full') ."*</p>";
                        }else{
                            echo "<p class='groen'>". trans('dashboard.main.reports.status.full') ."</p>";
                        }
                        echo '</div>';
                        ?>
                        <div class="space3">                                    
                            {{ Form::open(array('id' => 'commentForm', 'target' => '_blank','action' => "generate_full_report", 'enctype' => 'multipart/form-data')) }}
                                <input type='hidden' value='<?= $selected_test->id ?>' name="generate_report_id" class=""/>
                                <input type ="hidden" name="client_id" value="<?= $user->id ?>"/>
                                <?php echo "<input type='submit' value='". trans('dashboard.main.buttons.360') ."' name='knop_report' class='button '/>";?>
                            <?= form::close();?>
                        </div>
                        <div class="space1">
                            <?php echo '<img src="'.asset("media/images").'/info.png"  alt="info" class="help-dashboard" id="manage-users-info" title="'. trans("dashboard.main.help.360") .'">';?>
                        </div>
                        <?php
                    } else {
                        echo '<div class="space3">';
                        if($user->split_resp == 1){
                            echo "<p>". trans('dashboard.main.reports.status.missingRespondents') ."*</p>";
                        }else{
                            echo "<p>". trans('dashboard.main.reports.status.missingRespondents') ."</p>";
                        }
                        echo '</div>';
                        ?>
                        <div class="space3">
                            <a href="#" class="disabled button" rel="viewfullreport_disabled">@lang('dashboard.main.buttons.360')</a>
                        </div>
                        <div class="space1">
                            <?php echo '<img src="'.asset("media/images").'/info.png"  alt="info" class="help-dashboard" id="manage-users-info" title="'. trans("dashboard.main.help.360") .'">';?>
                        </div>
                    <?php
                    }
                }
                ?>

            </li>
        </ul>
        <?php
            if($user->split_resp == 1){
                echo '<p id="numResp">'. trans("dashboard.main.warning") .'</p>';
            }
        ?>
	</div>
	<!--  \ view box / -->

<?php } ?>
</div>
<!--  \ right container / -->

<div class="clear"></div>

<script type="text/javascript">
    $(document).ready(function() {
        // Tooltip only Text
        $('.help-dashboard').hover(function(){
                // Hover over code
                var title = $(this).attr('title');
                $(this).data('tipText', title).removeAttr('title');
                $('<p class="tooltip"></p>')
                .text(title)
                .appendTo('body')
                .fadeIn('slow');
        }, function() {
                // Hover out code
                $(this).attr('title', $(this).data('tipText'));
                $('.tooltip').remove();
        }).mousemove(function(e) {
                var mousex = e.pageX + 20; //Get X coordinates
                var mousey = e.pageY + 10; //Get Y coordinates
                $('.tooltip')
                .css({ top: mousey, left: mousex });
        });
    });
</script>
@stop
