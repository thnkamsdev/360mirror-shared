@extends('template_main_mirror')
@section('content')


{{HTML::script('media/javascript/custom-form-elements.js')}}

<!--  / profile box \ -->
<div class="profileBox second">

    <div class="left">

        <!-- FORM STARTS HERE -->
        <form method="POST" action="{{ translate_url('/test/myprofile/' . $user->id . '/' . $participant->id) }}" id= "commentForm">

            <h2>@lang('profile.title')</h2>

        @if($errors->has())
            <div class="inviteBox">
                <div class="errorsTop">
                    <h3>@lang('profile.errors.title')</h3>
                    <ul id="#respondents_list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            </div>
        @endif

            <p>
                {{ Form::label(trans('profile.email')) }}
                {{ Form::email(
                   'email',
                    $participant->email,
                    array(
                        'class' => 'field required',
                        'id' => 'email',
                        'placeholder' => trans('profile.emailPlaceholder')
                    )
                 ) }}
            </p>

            <?php
                if ( !Auth::user()->hasRoleInClient(Role::USER_PARTICIPANT, $user->id, Auth::user()->id) || Auth::user()->id != $participant_id ) {
            ?>
            <p class="gap13">

                <a href="{{ translate_url('/clients/getResendParticipantCredentials/' . $user->id . '/' . $participant->id) }}" class='button' id='buttonReset'> @lang('profile.pwdReset') </a>
                <br/><br/>
             </p>
            <?php } else { ?>
            <p>
                {{ Form::label(trans('profile.pwd')) }}
                <input type="password" id="password" class="field" name="password">
            </p>

            <p>
                {{ Form::label(trans('profile.pwdConfirm')) }}
                <input type="password" id="password_confirm" class="field" name="password_confirm">
            </p>
             <?php } ?>
            <p>
                {{ Form::label(trans('profile.fname')) }}
                {{ Form::text(
                   'firstname',
                    $participant->firstname,
                    array(
                        'class' => 'field required',
                        'id' => 'firstname',
                        'placeholder' => trans('profile.fnamePlaceholder')
                    )
                 ) }}
            </p>

            <p>
                {{ Form::label(trans('profile.lname')) }}
                {{ Form::text(
                   'lastname',
                    $participant->lastname,
                    array(
                        'class' => 'field required',
                        'id' => 'lastname',
                        'placeholder' => trans('profile.lnamePlaceholder')
                    )
                 ) }}
            </p>

            <p>
                {{ Form::label(trans('profile.gender')) }}
                @if ($participant->gender == 0)
                    {{ Form::radio(
                       'gender',
                        0,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'genderMale'
                        )
                     ) }}
                @elseif ($participant->gender != 0)
                    {{ Form::radio(
                       'gender',
                        0,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'genderMale'
                        )
                     ) }}
                @endif
                 {{ Form::label(trans('profile.genderMale'), '', array('class' => 'same')) }}
                @if ($participant->gender == 1)
                    {{ Form::radio(
                       'gender',
                        1,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'genderFemale'
                        )
                     ) }}
                @elseif ($participant->gender != 1)
                    {{ Form::radio(
                       'gender',
                        1,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'genderFemale'
                        )
                     ) }}
                @endif
                 {{ Form::label(trans('profile.genderFemale'), '', array('class' => 'same')) }}
            </p>

            <p>
                {{ Form::label(trans('profile.dob')) }}
                <input type="date" id="date_of_birth" class="field required" name="date_of_birth" placeholder="@lang('profile.dobFormat')" value="<?= $participant->date_of_birth ?>">
            </p>

            <p>
                {{ Form::label(trans('profile.country')) }}
                {{ Form::select(
                   'country',
                    $country_options,
                    $participant->country,
                    array(
                        'class' => 'styled',
                        'id' => 'country'
                    )
                 ) }}
            </p>

            <p>
                {{ Form::label(trans('profile.company')) }}
                {{ Form::text(
                   'company',
                    $participant->company,
                    array(
                        'class' => 'field required',
                        'id' => 'company',
                        'placeholder' => trans('profile.companyPlaceholder')
                    )
                 ) }}
            </p>
            @if (User::find($user->id)->THNKParticipant == 1)
            <p id = "ddownProfile">
                {{ Form::label(trans('profile.groups')) }}
                {{ Form::select(
                   'program_options',
                    $program_options,
                    $participant->THNKProgram,
                    array(
                        'class' => 'styled',
                        'id' => 'program_options'
                    )
                 ) }}
            </p>
            @endif
            
            <?php
                if ( !Auth::user()->hasRoleInClient(Role::USER_PARTICIPANT, $user->id, Auth::user()->id) || isset($tests) ) {
            ?>
                {{ Form::hidden('reset', 'true') }}
            <?php
                }else{
            ?>
                {{ Form::hidden('reset', 'false') }}
            <?php
                }
            ?>

            <p>
                {{ Form::submit(trans('profile.buttons.save'),array('name' => 'knop')) }}
                <a href="{{ URL::previous() }}" class="button-link-admin" id="back">@lang('profile.buttons.back')</a>
                <br /><br /><br />
            </p>


        {{ Form::close() }}
    </div>
</div>

 <?php
    $checkRole = 0;
    if ( Auth::user()->hasRoleInClient(Role::USER_CLIENT_COACH, $user->id, Auth::user()->id) || Auth::user()->hasRole('USER_THNK_ADMIN') ){
        $checkRole = 1;
    }

    if ( $checkRole > 0 ) {
?>
        <script type="text/javascript">
            $('.profileBox').css({'float':'left'});
        </script>

        <?php //ENABLE COACHES AND THNK ADMIN TO VIEW THE ENDEAVOR PROJECT ?>
        @if( $user->id == User::ENDEAVOR_SCAN )
            @if( isset($endeavors) )
                <div id="testsView" class="profileBox second">
                    <h2>@lang('profile.reports.titleEndeavors')</h2>
                    <?php
                        $totalEndeavors = count($endeavors);

                        if ( $totalEndeavors < 1 ) {
                    ?>
                            <p>{{ Form::label(trans('profile.reports.noEndeavors')) }}</p>
                    <?php
                        }else{
                    ?>
                            <ul>
                                <div class="new_ref_button">
                                    <a href="{{ translate_url('/endeavor/' . $user->id . '/' . $participant->id) }}" id="new_ref_button3" class="button">@lang('profile.reports.endeavors')</a>
                                </div>
                            </ul>
                            <div class="spacer8" id="space6"></div>
                    <?php
                        }
                    ?>
                </div>
            @endif
        @endif

        @if( isset($tests) )
            <div id="testsView" class="profileBox second">
                <h2>@lang('profile.reports.title')</h2>
                <?php
                    $totalTests = count($tests);

                    if($totalTests < 1){
                    
                        $respondents = [];

                    ?>
                        <p>
                        {{ Form::label(trans('profile.reports.noTests')) }}
                        </p>
                    <?php
                    }else{

                        $table =    '<table id="testTable">';

                        foreach ($tests as $key => $value) {

                            $testNumber = $totalTests--;

                            $selected_test = Test::find($value->id);

                            $respTests = DB::table('tests')
                                ->select('*')
                                ->where('test_id', '=', $value->id)
                                ->where('teststatus_id', '=', 7)
                                ->get();

                            $respondents = DB::table('respondents')
                                ->join('tests', 'tests.respondent_id', '=', 'respondents.id')
                                ->select('respondents.*')
                                ->where('tests.test_id', '=', $value->id)
                                ->orderby('respondentgroup_id')
                                ->get();


                            if($value->teststatus_id >= Teststatus::FINISHED_BY_USER_OR_RESPONDENT){
                                        $self = Form::open(array('id' => 'commentForm2', 'action' => "generate_own_report" ,'target' => '_blank', 'enctype' => 'multipart/form-data')).
                                                '<input type=\'hidden\' value="'.$value->id.'" name="generate_own_report_id" class=""/>
                                                <input type ="hidden" name="client_id" value="'.$user->id.'"/>
                                                <input type="submit" value="'.trans("profile.reports.buttons.self").'" name="knop_report" class="button "/>'
                                                .Form::close().
                                            '</td>
                                        </tr>';
                                }else{
                                        $self=  '<a href="javascript:void(0);" class="button disabled" rel="viewselfassessment">'.trans("profile.reports.buttons.self").'</a>
                                            </td>
                                        </tr>';
                            }

                            if($value->teststatus_id == Teststatus::TEST_FULLY_FINISHED || $selected_test->amount_completed() >= $value->minimum_required){
                                        $full = Form::open(array('id' => 'commentForm2', 'action' => "generate_full_report" ,'target' => '_blank', 'enctype' => 'multipart/form-data')).
                                                '<input type=\'hidden\' value="'.$value->id.'" name="generate_report_id" class=""/>
                                                <input type ="hidden" name="client_id" value="'.$user->id.'"/>
                                                <input type="submit" value="'.trans("profile.reports.buttons.full").'" name="knop_report" class="button "/>'
                                                .Form::close().
                                            '</td>
                                        </tr>';
                            }else{
                                        $full = '<a href="javascript:void(0);" class="disabled button" rel="viewfull360report_disabled">'.trans("profile.reports.buttons.full").'</a>
                                            </td>
                                        </tr>';
                            }

                            $subTable = '<table align="right">
                                            <tr>
                                                <td>'
                                                    .$self.
                                            '<tr>
                                                <td>'
                                                    .$full.
                                            '<tr>
                                                <td>'
                                                    .Form::open(array('id' => 'commentForm2', 'action' => "ClientsController@postClientTestUser")).
                                                    '<div class="text" id="minReq">'
                                                        .Form::hidden('checkIf', 'true').Form::hidden('id', $value->id).Form::hidden('client_id', $user->id)
                                                        .Form::submit(trans("profile.reports.buttons.update"),array('name' => 'update_minimum', 'class' => 'button two')).'
                                                        <div class="minReqBox">
                                                            <input type="text" value="'.$value->minimum_required.'" maxlength="3" name="minimum_required" class="field" />
                                                        </div>
                                                        <img src="'.asset("media/images").'/info.png"  alt="info" class="info-help help-icon" id="min-req-info" title="'.trans("profile.reports.minimumRespondentsDescription").'">
                                                    </div>'
                                                    .Form::close().
                                                '</td>
                                            </tr>
                                        </table>';
                            $table .=
                                            '<tr>
                                                <td valign="top" id="pBoxLab">
                                                    '.trans("profile.reports.test").' '.($testNumber).' <br/>'.$value->created_at.
                                                '</td>
                                                <td class="numbersTest">
                                                    '.count($respTests).' / '.count($respondents).' '.trans("profile.reports.respondents").'
                                                </td>
                                                <td>
                                                    '.$subTable.'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr id="minReqHr" />
                                                </td>
                                            </tr>';
                        }
                        $table .='</table>';
                        echo $table;
                    }


                ?>
            </div>

            <?php            
                //ENABLE THNK ADMIN TO RESEND REMINDER TO/DELETE RESPONDENTS FROM LAST TEST
                if (Auth::user()->hasRole('USER_THNK_ADMIN')){

                    $totalTests = count($tests);
                    $number_of_respondents = 0;
                    $ammount_completed = 0;

                    foreach ($tests as $key => $value) {

                        $testNumber = $totalTests--;

                        $selected_test = Test::find($value->id);

                        $respTests = DB::table('tests')
                            ->select('*')
                            ->where('test_id', '=', $value->id)
                            ->where('teststatus_id', '=', 7)
                            ->get();

                        $respondents =      DB::table('respondents')
                                ->join('tests', 'tests.respondent_id', '=', 'respondents.id')
                                ->select('respondents.*')
                                ->where('tests.test_id', '=', $value->id)
                                ->orderby('respondentgroup_id')
                                ->get();

                        foreach ($respondents as $respondent) {

                            $number_of_respondents++;
                            $e = '';
                            if ($number_of_respondents==1) {
                                $e = 'first';
            ?>
                                <!--  / response box \ -->
                                <div class="responseBox">
                                    <div class="top">
                                        <h2 id="respondentsH2">
                                            @lang('profile.adminFix.title')<?php echo $value->id; ?>
                                        </h2>
                                    </div>
                                    <ul>
            <?php
                            }
            ?>
                                        <li class="<?= $e ?>">
                                            <div class="one">
                                                <h3><?php $respondentgroup = Respondentgroup::find($respondent->respondentgroup_id); echo $respondentgroup->name?></h3>
                                            </div>                                            
                                            <div class="two">
                                                <h4><?= $respondent->name ?></h4>
            <?php
                                                $respondent_test = Test::getFirstTestsByRespondent($respondent->id);

                                                if ($respondent_test->teststatus_id == Teststatus::FINISHED_BY_USER_OR_RESPONDENT || $respondent_test->teststatus_id == Teststatus::TEST_FULLY_FINISHED) {
                                                    echo "<span>".trans('profile.adminFix.tests.completed')."</span>";
                                                    $ammount_completed++;
                                                } elseif ($respondent_test->teststatus_id == 0) {
                                                    echo "<span class='red'>".trans('profile.adminFix.tests.notStarted');
                                                    if ($respondent_test->last_reminder != "0000-00-00 00:00:00")
                                                        echo date("Y-m-d H:i", strtotime($respondent_test->last_reminder));
                                                    else
                                                        echo "-";
                                                    echo "<br>" . $respondent->email;
                                                    echo "</span>";
                                                } else {
                                                    echo "<span class='yellow'>".trans('profile.adminFix.tests.inProgress');
                                                    if ($respondent_test->last_reminder != "0000-00-00 00:00:00")
                                                        echo date("Y-m-d H:i", strtotime($respondent_test->last_reminder));
                                                    else
                                                        echo "-";
                                                    echo "<br>" . $respondent->email;
                                                    echo "</span>";
                                                }
            ?>
                                            </div>
                                            <div class="last">
                                                <?php if ($respondent_test->teststatus_id != Teststatus::FINISHED_BY_USER_OR_RESPONDENT) { ?>
                                                    {{ Form::open(array('action' => array('DashboardController@postIndex'), 'id'=>'remindForm')) }}
                                                        <input type='submit' value="@lang('profile.adminFix.buttons.reminder')" name="knop_reminder" class="btn aanmeld">
                                                        <input type ="hidden" name="test_id_reminder" value="<?= $respondent_test->id ?>"/>
                                                        <input type ="hidden" name="user_id" value="<?= $participant->id ?>"/>
                                                    </form>
                                                    {{ Form::open(array('action' => array('DashboardController@postIndex'), 'id'=>'replaceForm')) }}
                                                        <input type='submit' value="@lang('profile.adminFix.buttons.delete')" name="knop_reminder" class="btn aanmeld differ delete grey">
                                                        <input type ="hidden" name="test_id_delete" value="<?= $respondent_test->id ?>"/>
                                                        <input type ="hidden" name="user_id" value="<?= $participant->id ?>"/>
                                                    </form>
                                                <?php }?>
                                            </div>
                                        </li>
            <?php
                        }
                        if ($number_of_respondents > 0 ) {
            ?>
                                    </ul>
                                </div>
                                <br/><br/>
                                <div class="clear"></div>
            <?php
                            $number_of_respondents = 0;
                        }
                    }
                }
            ?> 
        @endif

        <div class="clear"></div>
<?php
    }
?>
<!--  \ profile box / -->

<script type="text/javascript">
    $(document).ready(function() {
        // Tooltip only Text
        $('.help-icon').hover(function(){
                // Hover over code
                var title = $(this).attr('title');
                $(this).data('tipText', title).removeAttr('title');
                $('<p class="tooltip"></p>')
                .text(title)
                .appendTo('body')
                .fadeIn('slow');
        }, function() {
                // Hover out code
                $(this).attr('title', $(this).data('tipText'));
                $('.tooltip').remove();
        }).mousemove(function(e) {
                var mousex = e.pageX + 20; //Get X coordinates
                var mousey = e.pageY + 10; //Get Y coordinates
                $('.tooltip')
                .css({ top: mousey, left: mousex })
        });
    });
</script>

@stop
