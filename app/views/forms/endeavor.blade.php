@extends('template_main_mirror')
@section('content')

<?php 
    if( $endeavor->count() ){
        if (is_null($endeavor[0])){
            $endeavor = new Endeavor;
        }else{
            $endeavor = $endeavor[0];
        }
    }else{
        $endeavor = new Endeavor;
    }
?>

{{HTML::script('media/javascript/custom-form-elements.js')}}

<!--  / profile box \ -->
<div class="profileBox second full">
    <div class="left">
        <!-- FORM STARTS HERE -->
        <form method="POST" action="{{ translate_url('/endeavor/' . $user->id . '/' . $participant->id) }}" id= "commentForm">

            <h2>@lang('endeavor.title')</h2>

            @if($errors->has())
                <div class="inviteBox">
                    <div class="errorsTop">
                        <h3>@lang('endeavor.errors.title')</h3>
                        <ul id="#respondents_list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            <p>
                {{ Form::label(trans('endeavor.name')) }}
                {{ Form::text(
                   'name',
                    $endeavor->name,
                    array(
                        'class' => 'field required',
                        'id' => 'name',
                        'placeholder' => trans('endeavor.namePlaceholder')
                    )
                 ) }}
            </p>

            <div id="type-project">
                {{ Form::label(trans('endeavor.type')) }}
                
                <div class="spacer16"></div>
                
                <div>
                 @if ($endeavor->type_id == 1)
                    {{ Form::radio(
                       'type_options',
                        1,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'type_option_1'
                        )
                     ) }}
                @elseif ($endeavor->type_id != 1)
                    {{ Form::radio(
                       'type_options',
                        1,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'type_option_1'
                        )
                     ) }}
                @endif
                 {{ Form::label($type_options[1], '', array('class' => 'same')) }}
                </div>
                
                <div class="clear spacer16"></div>
                
                <div>
                @if ($endeavor->type_id == 2)
                    {{ Form::radio(
                       'type_options',
                        2,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'type_option_2'
                        )
                     ) }}
                @elseif ($endeavor->type_id != 2)
                    {{ Form::radio(
                       'type_options',
                        2,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'type_option_2'
                        )
                     ) }}
                @endif
                 {{ Form::label($type_options[2], '', array('class' => 'same')) }}
                 </div>
                 <div class="clear spacer16"></div>
                 <div>
                 @if ($endeavor->type_id == 3)
                    {{ Form::radio(
                       'type_options',
                        3,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'type_option_3'
                        )
                     ) }}
                @elseif ($endeavor->type_id != 3)
                    {{ Form::radio(
                       'type_options',
                        3,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'type_option_3'
                        )
                     ) }}
                @endif
                 {{ Form::label($type_options[3], '', array('class' => 'same')) }}
                 </div>
                 <div class="clear spacer16"></div>
                 <div>
                 @if ($endeavor->type_id == 4)
                    {{ Form::radio(
                       'type_options',
                        4,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'type_option_4'
                        )
                     ) }}
                @elseif ($endeavor->type_id != 4)
                    {{ Form::radio(
                       'type_options',
                        4,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'type_option_4'
                        )
                     ) }}
                @endif
                 {{ Form::label($type_options[4], '', array('class' => 'same')) }}
                 </div>

                 <div id="other-type">
                    <div class="clear spacer16"></div>
                    {{ Form::label(trans('endeavor.otherType')) }}
                    {{ Form::text(
                       'otherTypeTxt',
                        $endeavor->other_type,
                        array(
                            'class' => 'field',
                            'id' => 'otherTypeTxt',
                            'placeholder' => trans('endeavor.otherTypePlaceholder')
                        )
                     ) }}
                     <input name="other_type_options_hidden" id="otherType_options" type="hidden" value="0">
                 </div>

                 <input name="type_options_hidden" id="type_options" type="hidden" value="0">

                <div class="clear spacer32"></div>
                <div class="clear spacer16"></div>
            </div>

            <p>
                {{ Form::label(trans('endeavor.size')) }}
                <input type="number" name="total_people" min="1" class="field required" value="<?= $endeavor->people ?>">
            </p>
            
                {{ Form::label(trans('endeavor.stage')) }}
            <div class="spacer16"></div>
            <div>
                 @if ($endeavor->stage_id == 1)
                    {{ Form::radio(
                       'stage_options',
                        1,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'stage_options'
                        )
                     ) }}
                @elseif ($endeavor->stage_id != 1)
                    {{ Form::radio(
                       'stage_options',
                        1,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'stage_options'
                        )
                     ) }}
                @endif
                 {{ Form::label($stage_options[1], '', array('class' => 'same')) }}
            </div>
            <div class="clear spacer16"></div>
            <div>
                @if ($endeavor->stage_id == 2)
                    {{ Form::radio(
                       'stage_options',
                        2,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'stage_options'
                        )
                     ) }}
                @elseif ($endeavor->stage_id != 2)
                    {{ Form::radio(
                       'stage_options',
                        2,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'stage_options'
                        )
                     ) }}
                @endif
                 {{ Form::label($stage_options[2], '', array('class' => 'same')) }}
            </div>
            <div class="clear spacer16"></div>
            <div>
                 @if ($endeavor->stage_id == 3)
                    {{ Form::radio(
                       'stage_options',
                        3,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'stage_options'
                        )
                     ) }}
                @elseif ($endeavor->stage_id != 3)
                    {{ Form::radio(
                       'stage_options',
                        3,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'stage_options'
                        )
                     ) }}
                @endif
                 {{ Form::label($stage_options[3], '', array('class' => 'same')) }}
            </div>
            <div class="clear spacer16"></div>
            <div>
                 @if ($endeavor->stage_id == 4)
                    {{ Form::radio(
                       'stage_options',
                        4,
                        true,
                        array(
                            'class' => 'styled',
                            'id' => 'stage_options'
                        )
                     ) }}
                @elseif ($endeavor->stage_id != 4)
                    {{ Form::radio(
                       'stage_options',
                        4,
                        null,
                        array(
                            'class' => 'styled',
                            'id' => 'stage_options'
                        )
                     ) }}
                @endif
                 {{ Form::label($stage_options[4], '', array('class' => 'same')) }}
            </div>
            <div class="clear spacer16"></div>
            <div class="clear spacer16"></div>
            <div class="endeavorTitle">
                {{ Form::label(trans('endeavor.descriptionWhatTitle')) }}
                <div class="endeavorDesc">@lang('endeavor.descriptionWhat')</div>
                {{ Form::textarea(
                   'descriptionWhat',
                    $endeavor->descriptionWhat,
                    array(
                        'class' => 'txt_area required',
                        'id' => 'descriptionWhat',
                        'placeholder' => trans('endeavor.descriptionPlaceholderWhat')
                    )
                 ) }}
            </div>
            <div class="clear spacer16"></div>
            <div class="endeavorTitle">
                {{ Form::label(trans('endeavor.descriptionHowTitle')) }}
                <div class="endeavorDesc">@lang('endeavor.descriptionHow')</div>
                {{ Form::textarea(
                   'descriptionHow',
                    $endeavor->descriptionHow,
                    array(
                        'class' => 'txt_area required',
                        'id' => 'descriptionHow',
                        'placeholder' => trans('endeavor.descriptionPlaceholderHow')
                    )
                 ) }}
            </div>
            <div class="clear spacer16"></div>
            <div class="endeavorTitle">
                {{ Form::label(trans('endeavor.descriptionWhoTitle')) }}
                <div class="endeavorDesc">@lang('endeavor.descriptionWho')</div>
                {{ Form::textarea(
                   'descriptionWho',
                    $endeavor->descriptionWho,
                    array(
                        'class' => 'txt_area required',
                        'id' => 'descriptionWho',
                        'placeholder' => trans('endeavor.descriptionPlaceholderWho')
                    )
                 ) }}
            </div>
            <div class="clear spacer16"></div>
            <div class="endeavorTitle">
                {{ Form::label(trans('endeavor.descriptionWhyTitle')) }}
                <div class="endeavorDesc">@lang('endeavor.descriptionWhy')</div>
                {{ Form::textarea(
                   'descriptionWhy',
                    $endeavor->descriptionWhy,
                    array(
                        'class' => 'txt_area required',
                        'id' => 'descriptionWhy',
                        'placeholder' => trans('endeavor.descriptionPlaceholderWhy')
                    )
                 ) }}
            </div>
            <div class="clear spacer16"></div>
            <div class="endeavorTitle">
                {{ Form::label(trans('endeavor.descriptionNextTitle')) }}
                {{ Form::textarea(
                   'descriptionNext',
                    $endeavor->descriptionNext,
                    array(
                        'class' => 'txt_area required',
                        'id' => 'descriptionNext',
                        'placeholder' => trans('endeavor.descriptionPlaceholderNext')
                    )
                 ) }}
            </div>
            
            
            <p>
                <?php //ONLY OWNER OR CREATOR CAN SAVE + ADMIN ?>
                @if ( $endeavor->user_id == Auth::user()->id || is_null($endeavor->user_id) || Auth::user()->hasRole('USER_THNK_ADMIN') )
                {{ Form::submit(trans('endeavor.buttons.save'),array('name' => 'knop')) }}
                @endif
                <a href="{{ URL::previous() }}" class="button-link-admin" id="back">@lang('endeavor.buttons.back')</a>
                <br /><br /><br />
            </p>
        {{ Form::close() }}
    </div>
    <div class="clear"></div>
</div>
<!--  \ profile box / -->

<script type="text/javascript">
    if($('#type_option_4').is(':checked')) { 
        $('#other-type').show();
        $('#otherTypeTxt').addClass('required');
        $('input[name=type_options_hidden]').val('4');
        $('input[name=other_type_options_hidden]').val('1');
    }else{
        $('#other-type').hide();
        $('#otherTypeTxt').removeClass('required');
    }
    $('#type-project').click(function() {
        if($('#type_option_1').is(':checked')) { 
            $('input[name=type_options_hidden]').val('1');
            $('input[name=other_type_options_hidden]').val('0');
            $('#other-type').hide();
            $('#otherTypeTxt').removeClass('required');
        }else if($('#type_option_2').is(':checked')) { 
            $('input[name=type_options_hidden]').val('2');
            $('input[name=other_type_options_hidden]').val('0');
            $('#other-type').hide();
            $('#otherTypeTxt').removeClass('required');
        }else if($('#type_option_3').is(':checked')) { 
            $('input[name=type_options_hidden]').val('3');
            $('input[name=other_type_options_hidden]').val('0');
            $('#other-type').hide();
            $('#otherTypeTxt').removeClass('required');
        }else if($('#type_option_4').is(':checked')) {
            $('input[name=type_options_hidden]').val('4');
            $('input[name=other_type_options_hidden]').val('1');
            $('#other-type').show();
            $('#otherTypeTxt').addClass('required')
        }
    });
</script>

@stop