@extends('template_main_thnkwebsite')
@section('content')

<main class="main">
    <div class="block heading">
        <h1 class="heading__title">Thank you!</h1>
    </div>

    <div class="block text text--white sub-title">
        <h2 class="sub-title__title"></h2>
        <div class="text__text columns--1">
            <p>You have finished the assessment. Thank you for sharing your feedback!</p>
        </div>
    </div>

    <div class="block text text--grey sub-title">
        <h2 class="sub-title__title"></h2>
        <div class="text__text columns--1">
            <p>At THNK School of Creative Leadership, we design and facilitate transformational in-person learning experiences.</p>
            <p>We train and support global leaders to develop the mindsets, skill sets, and toolsets needed to scale their impact on the world’s biggest challenges.</p>
            <p>Find out how THNK can help you take your own leadership to the next level with our <a href="https://www.thnk.org/programs/executive-leadership-program/">Executive Program</a>, or how to elevate your entire organisation with a bespoke <a href="https://www.thnk.org/programs/custom-programs/">Custom Programs</a>.</p>
        </div>
    </div>

    <?php /*
    <div class="block block--links">
        <div class="block program">
            <div class="program__holder">
                <div class="program__image">
                    <img src="https://www.thnk.org/content/uploads/2017/02/executive-leadership-program-2-700x455-c-default.jpg" alt="Executive Leadership Program" title="Executive Leadership Program">
                </div>
                <div class="program__info">
                    <h2 class="progam__heading">Executive Leadership Program</h2>
                    <div class="program__text">
                        <p>You are experienced, change-ready, and keen to try out creative solutions in an uncertain world. Your ambition is to accomplish good work and good impact: you are ready to act on a vision of positivity and to invigorate your team or organization for a state of constant experimentation, focus, and flow.</p>
                        <a href="https://www.thnk.org/programs/executive-leadership-program/" class="button">
                            <span class="button__content">explore</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="block block--links">
	    <div class="block program">
	        <div class="program__holder">
	            <div class="program__image">
	                <img src="https://www.thnk.org/content/uploads/2017/02/custom-programs-3-700x455-c-default.jpg" alt="Custom programs 1" title="Custom programs 1">
	            </div>
	            <div class="program__info">
	                <h2 class="progam__heading">Custom programs</h2>
	                <div class="program__text">
	                    <p>You want to redefine your industry or sector through your business lines or product. You understand that your organization and the way your teams work together can be a catalyst for greater change. You are looking for a Partner to co-design and deliver long-term solutions.</p>
	                    <a href="https://www.thnk.org/programs/custom-programs/" class="button">
	                    	<span class="button__content">explore</span>
	                    </a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    */ ?>

</main>

@stop