@extends('template_main_thnk')
@section('content')
    <div class="inviteBox intro">            
        <h2>THE MIRROR</h2>
        <p>You are looking into the Mirror. Looking in a mirror means looking at yourself. What do you see? And how is this similar or different to how others see you? The Mirror addresses the leadership mindsets and behaviors that we have been exploring in the Impactivist Program this year.
        <br><br>
        The Mirror will show you how the leadership mindsets and behaviors that we have been working on during the program are currently perceived by yourself and by others. It will show your strengths and development opportunities, what we call Gifts and Learning Edges. It will help you to consciously identify those aspects of your leadership that you might like to focus on going forward.
        </p>

        <h3>FIVE KEY LEARNING GOALS</h3>
        <p>The Impactivist Program addresses some of the mindsets and behaviors that make for a successful leader within Siemens PG. Among other things, a successful leader:</p>
        <ul>
            <li>1.  Has a Strong Inner Axis</li>
            <li>2.  Envisions a Better Future</li>
            <li>3.  Embraces an Entrepreneur’s Mindset</li>
            <li>4.  Is an Inspiring People Leader</li>
            <li>5.  Drives Organizational Change</li>
        </ul>
        <p>This survey is designed to measure where you are on these five leadership dimensions and to guide you as you define your leadership development goals both for the remainder of the program and beyond.</p>

        <h3>CONFIDENTIALITY & TIMELINE</h3>
        <p>Your report is confidential and will not be shared with anyone else without your approval.
        <br><br>
        The assessment should take no more than 30 minutes to complete. We invite you to do your self-assessment as soon as possible. Upon completion, you will be asked to nominate a minimum of 7 respondents (your manager, team members and peers).
        <br><br>
        The deadline for completion (including the assessment by your respondents) is Friday 24 January.
        </p>

        <div class="center">
            {{ Form::open(array('id' => 'commentTestForm')) }}
                <input type='submit' value='Start your assessment' name="knop" class="button1 name2">
            {{ Form::close() }}
            <div class="clear"></div>
        </div>
    </div>
@stop