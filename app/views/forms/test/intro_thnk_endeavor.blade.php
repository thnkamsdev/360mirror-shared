@extends('template_main_thnk')
@section('content')
    <?php 
        if ($endeavor->count()) { ?>
        
            <div class="inviteBox intro">
                <h2>Self-assessment</h2>
                <p>The next step will be an assessment on how you currently rate yourself on each of the four characteristics:</p>
                <div class="left">
                    <ul>
                        <li>Delighted Customers</li>
                        <li>Scalable Platforms</li>
                        <li>Empowered Networks</li>
                        <li>Dynamic Focus</li>
                    </ul>
                    <p>And the underpinning fifth element:</p>
                    <ul>
                        <li>Good Governance</li>
                    </ul>
                    <p>Don’t worry if you can’t fully answer every questions at the moment. Especially if you are still in the concepting phase, there might be elements, which you find difficult to score at the current stage of your project.
                    <br/><br/>
                    After finishing the assessment, you will able to invite others to take the assessment and view a generated report.</p>
                </div>
                <div class="right">
                    <img src="/media/images/thnk-endeavor.png">
                </div>
                <div class="clear"></div>

                <h3>How read the report</h3>
                <p>Start with the big picture: your score for each of the four major characteristics. Then zoom in on each of the three sub-characteristics. At the end of the document, you will find an aggregated table that helps you reflect on the scan.
                <br/><br/>
                Look for where your scores are significantly lacking or substantially strong. We invite you to list the results that please, displease, or surprise you. This will help make your insights actionable and provide focus for your Endeavor coaching sessions.</p>
                
                <div class="center">
                    {{ Form::open(array('id' => 'commentTestForm')) }}
                        <input type='submit' value='Start your assessment' name="knop" class="button1 name2">
                    {{ Form::close() }}
                    <div class="clear"></div>
                </div>
            </div>
            
    <?php }else{ ?>
            
            <div class="inviteBox intro">
                <h2>Your Endeavor</h2>
                <p>Your Endeavor is a personal undertaking, defined by you, aimed at making an impact on a large challenge (with societal impact), and might involve starting a new enterprise, achieving a major business transformation within your corporation, or realizing a crucial transformation in the public or social realms. During the Executive Leadership Program, your Endeavor is the vehicle to which you will apply new insights and learnings from the THNK Innovative Enterprise model, with an eye to further enhancing and accelerating your Endeavor.
                <br/><br/>
                From our experience, your Endeavor has the most chance of succeeding if you can already give a convincing answer on these reflections:</p>

                <ul>
                    <li>Are you able to give your Endeavor the attention which it is due, and are you leveraging relevant experience or competences?</li>
                    <li>Does it give you joy, will it sustain your energy, even when the going gets tough?</li>
                    <li>Can it be financially self-sustaining? Are there existing or foreseeable revenues or funding streams to pay yourself a salary, hire talent, maintain independence and invest in further growth?</li>
                    <li>Will it improve the planet and its people? Does it have an intended impact beyond financial success?</li>
                </ul>

                <h3>Endeavor Scan</h3>

                <div class="left">
                    <p>THNK has conducted extensive research on what makes projects and ventures grow their impact exponentially, soon after their early start-up phase. We identified four major characteristics of a successful Scale-Up Enterprise, that are pivotal in this accelerated growth trajectory, and should to be present in the venture design and execution in the earliest stage:</p>
                    <ul>
                        <li>Delighted Customers</li>
                        <li>Scalable Platforms</li>
                        <li>Empowered Networks</li>
                        <li>Dynamic Focus</li>
                    </ul>
                    <p>The above mentioned characteristics, are further underpinned by a fifth element:</p>
                    <ul>
                        <li>Good Governance</li>
                    </ul>
                </div>
                <div class="right">
                    <img src="/media/images/thnk-endeavor.png">
                </div>
                <div class="clear"></div>

                <p>This Endeavor scan tells you how closely the design and execution of your Endeavor is aligned with these characteristics, and will highlight the strong points and latent development areas of your Endeavor, which you can choose to focus on during the next 6 months of the Executive Leadership Program and associated Endeavor coaching sessions.
                <br/><br/>
                As a first step, we ask you to fill in a description of your Endeavor and its current stage.</p>
                
                <div class="center">
                    {{ Form::open(array('id' => 'commentTestForm')) }}
                        <input type='submit' value='Fill in your endeavor' name="knopEndeavor" class="button1 name2">
                    {{ Form::close() }}
                    <div class="clear"></div>
                </div>
            </div>

    <?php } ?>
@stop