@extends('template_main_storypanda')@section('content'){{HTML::script('media/javascript/custom-form-elements2.js')}}{{HTML::script('media/javascript/progressBar.js')}}{{ Form::open(array('id' => 'commentTestForm')) }}            <!--  / center container \ -->            <div id="centerCntr" class="ended">                <input type="hidden" name="color" value="#ec1c24" />                <!--  / progress box \ -->                <div class="progressBox">                    <ul>                        <?php                            $i = 0;                            for ($i = 0; $i < $total_numbers_of_categories; $i++) {                                $extra = '';                                if ($progress >= $i) {                                    $extra = "completed";                                }                                if ($i == 0)                                    $extra .= " first";                                echo "<li class='$extra'>";                                if ($i == 0)                                    echo "Progress";                                else                                    echo "&nbsp;";                                echo "</li>";                            }                            if ($i == 1)                                ?> <script type="text/javascript"> $('li.first').css('width','127px'); </script> <?php                        ?>                    </ul>                </div>                <!--  \ progress box / -->                <div class="clear"></div>                <!--  / demon box \ -->                <div class="demonBox">                        <fieldset>                            <ul>                                @include('forms.test.header_test')                                <?php                                $i =0;                                foreach ($category_question_users as $user_question) {                                    $question = $user_question->question;                                    if ($i == 0)                                        $c = '';                                    else                                        $c = 'gap';                                ?>                                <li class="<?=$c?>">                                    <div class="one">                                        <h3><?= $question->name ?></h3>                                        <p><?= $question->description ?></p>                                        <label for=question[<?=$question->id?>] class="error" style="display:none;color:#ff0000"><nobr>Please choose one.</nobr></label>                                    </div>                                    <?php                                    $s = return_s($question->id, $test_id);                                    $divs = return_divs($question->id, $s);                                    echo $divs;                                    ?>                                </li>                            <?php                                $i++;                            }                            ?>                            </ul>                        </fieldset>                        <fieldset class="new">                            <span>Feel free to provide additional examples, supporting points or narrative <label for="commentfield" class="error" style="display:none;color:#ff0000"><nobr class="errorTxt_o">Required field.</nobr></label></span>                            <p>                                <textarea class ="txt_o" name="commentfield"><?php echo CategoryQuestionUserTest::getCategoryQuestionUserTestOpenQuestion($category->id, $test_id)->commentfield ?></textarea>                            </p>                            <p>                                <input type="hidden" name="category_id" value="<?=$category->id?>"/>                                <input type="hidden" name="test_id" value="<?=$test->id?>"/>                                <?php if ($category->id != $firstCategory->id) { ?>                                <input type='submit' value='Back' name="knop" class="button1 cancel grey">                                <?php } ?>                                <input type='submit' value='Save' name="knop" class="button1 cancel name">                                <?php if ($category->id != $lastCategory->id) { ?>                                <input type='submit' value='Save and next step' name="knop" class="button1 name2">                                <?php } else { ?>                                <input type='submit' value='Finish' name="knop" class="button1 name2">                                <?php } ?>                            </p>                        </fieldset>                </div>                <!--  \ demon box / -->            </div>            <!--  \ center container / -->            <div class="clear"></div></form><script type="text/javascript">    $(document).ready(function() {        $("#commentTestForm").validate({            rules: {                <?php                foreach ($category_question_users as $user_question) {                    $question = $user_question->question;                    echo "'question[" . $question->id . "]': {required:true},";                }                ?>                'commentfield': {required:true}            }        });    });</script><?php    function return_s ($question_id, $test_id) {        if (isset($test_id)) {            // Get already given answer to question.            $category_question_user_tests = CategoryQuestionUserTest::join('questions', 'questions.id', '=', 'category_question_user_test.question_id')                                    ->where('questions.id', '=', $question_id)                                    ->where('test_id', '=', $test_id)                                    ->get();            foreach ($category_question_user_tests as $category_question_user_test) {                break;            }            if (isset($category_question_user_test) && $category_question_user_test->value > 0)                $value = $category_question_user_test->value;            else                $value = 0;        } else {            $value = 0;        }        $s = array();        $s[0] = false; $s[1] = false; $s[2] = false; $s[3] = false; $s[4] = false; $s[5] = false;        if ($value == 1) {            $s[0] = true;        } elseif ($value == 2) {            $s[1] = true;        } elseif ($value == 3) {            $s[2] = true;        } elseif ($value == 4) {            $s[3] = true;        } elseif ($value == 5) {            $s[4] = true;        } elseif ($value == 9) {            $s[5] = true;        }        return $s;    }    function return_divs($question_id, $s) {        $var = "        <div class=\"two\">            <p>                    <input type=\"radio\" class=\"styled2\" item='required_field' value=\"1\" name=\"question[" . $question_id . "]\" " . (($s[0]) ? "checked" : "") . "/>            </p>        </div>        <div class=\"three\">            <p>                    <input type=\"radio\" class=\"styled2\" value=\"2\" name=\"question[" . $question_id . "]\" " . (($s[1]) ? "checked" : "") . "/>            </p>        </div>        <div class=\"four\">            <p>                    <input type=\"radio\" class=\"styled2\" value=\"3\" name=\"question[" . $question_id . "]\" " . (($s[2]) ? "checked" : "") . "/>            </p>        </div>        <div class=\"five\">            <p>                    <input type=\"radio\" class=\"styled2\" value=\"4\" name=\"question[" . $question_id . "]\" " . (($s[3]) ? "checked" : "") . "/>            </p>        </div>        <div class=\"six\">            <p>                    <input type=\"radio\" class=\"styled2\" value=\"5\" name=\"question[" . $question_id . "]\" " . (($s[4]) ? "checked" : "") . "/>            </p>        </div>        <div class=\"seven\">            <p>                    <input type=\"radio\" class=\"styled2\" value=\"9\" name=\"question[" . $question_id . "]\" " . (($s[5]) ? "checked" : "") . "/>            </p>        </div>        <div class=\"bar\">        </div>        <progress max=\"100\" value=\"0\"></progress>";        return $var;    }    ?>@stop