<script type="text/javascript">
    $(document).ready(function() {

        //TOTAL RESPONDENTS
        var numResp = $('input[name="numResp"]').val();        

        function setup_form() {
            //CUSTOM EMAIL BOX
            var customEmailBox = $("input[name='use_email']:checked").val();

            if (customEmailBox == 1) {
                $(".text_email_row").hide();
            } else {
                $(".text_email_row").show();
            }
        }

        $('#commentForm').submit(function(e) {
            $('#jsError').text('');
            var coworker = 0;
            var mentor = 0;
            var check5 = null;
            var check6 = null;
            var miss = $('input[name="minimum_required"]').val();
            var temp = 0;
            var reached = numResp;

            //CO-WORKERS GROUP
            var values = {};
            var $coworkerT = $('input.Co-worker:text');
            $coworkerT.each(function(d) {
                if($(this).val() !== '' || $(this).parent().parent().find('p').eq(1).find('input').val() != ''){
                    values[d] = {};
                    values[d]['name'] = $(this).val();
                    values[d]['email'] = $(this).parent().parent().find('p').eq(1).find('input').val();
                }
            });

            jQuery.each(values, function(key,val) {
                if(val.name != '' && val.email != '')
                    reached ++;
                    coworker ++;
                if(val.name === '' || val.email === '')
                    check5 = true;
                if(val.name === '' && val.email === '')
                    check5 = null;
            });
            
            //MENTORS GROUP
            var values = {};
            var $mentorT = $('input.Mentor:text');
            $mentorT.each(function(d) {
                if($(this).val() !== '' || $(this).parent().parent().find('p').eq(1).find('input').val() != ''){
                    values[d] = {};
                    values[d]['name'] = $(this).val();
                    values[d]['email'] = $(this).parent().parent().find('p').eq(1).find('input').val();
                }
            });

            temp = reached;

            jQuery.each(values, function(key,val) {
                if(val.name != '' && val.email != '')
                    reached ++;
                    mentor ++;
                if(val.name === '' || val.email === '')
                    check6 = true;
                if(val.name === '' && val.email === '')
                    check6 = null;
            });
            
            //VALIDATION ERROR
            if(reached < miss){
                $('#jsError').append('<label>You must invite at least <?php echo $minReq ?> respondents</label>');
                e.preventDefault();
                
                if(check5 === true){
                    $('#jsError').append('<label>In the co-worker group there is one or more name or email missing</label>');
                    e.preventDefault();
                }
                if(check6 === true){
                    $('#jsError').append('<label>In the mentor group there is one or more name or email missing</label>');
                    e.preventDefault();
                }
            }
        });
        setup_form(); 

        $(".extra_respondent").live('click', function(e) {
            e.preventDefault();
            var group;
            group = $(this).attr('group_id');
            var groupname;
            if (group == 1)
                groupname = 'Superior';
            if (group == 2)
                groupname = 'Peer';
            if (group == 3)
                groupname = 'Subordinate';
            if (group == 4)
                groupname = 'Other';
            if (group == 5)
                groupname = 'Co-worker';
            if (group == 6)
                groupname = 'Mentor';
            var html;

            html = "<li>"
                    +"<h2>"
                        + groupname
                    + "*</h2>"
                    +"<p>"
                        +"<label>NAME</label>"
                        +'<input type="text" value="" class="field extra '+groupname+'" name="name[]"/>'
                    +"</p>"
                    +"<p class='second'>"
                        +"<label>Email Address</label>"
                        +'<input type="email" value="" class="field email extra '+groupname+'" name="email[]"/>'
                    +'</p>'
                    +'<p class="second text_email_row">'
                        +'<label>Custom email text</label>'
                        +'<input type="hidden" value="'+ group +'" class="" name="respondentgroup[]"/>'
                        +"<?php echo $customTextJS ?>"
                    +'</p>'
                +'</li>';

            $('#respondentList li#listItem' + group).before(html);
            setup_form(); 
        });     
    });
</script>