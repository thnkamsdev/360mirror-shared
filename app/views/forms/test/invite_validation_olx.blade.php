<script type="text/javascript">
    $(document).ready(function() {

        //TOTAL RESPONDENTS
        var numResp = $('input[name="numResp"]').val();   

        function setup_form() {
            //CUSTOM EMAIL BOX
            var customEmailBox = $("input[name='use_email']:checked").val();

            if (customEmailBox == 1) {
                $(".text_email_row").hide();
            } else {
                $(".text_email_row").show();
            }
        }

        $('#commentForm').submit(function(e) {
            var superior = null;
            var peer = null;
            var subordinate = null;
            var others = null;
            var check1 = null;
            var check2 = null;
            var check3 = null;
            var check4 = null;
            var miss = $('input[name="minimum_required"]').val();
            var reached = numResp;
            var temp = 0;
            var numSuperior = 0;  

            $('#jsError').text('');

            //SUPERIORS GROUP
            var $superiorT = $('input.Superior:text');
            var values = {};
            $superiorT.each(function(d) {
                if($(this).val() !== '' || $(this).parent().parent().find('p').eq(1).find('input').val() != ''){
                    values[d] = {};
                    values[d]['name'] = $(this).val();
                    values[d]['email'] = $(this).parent().parent().find('p').eq(1).find('input').val();
                }
            });
            jQuery.each(values, function(key,val) {
                if(val.name != '' && val.email != '')
                    reached ++;
                    numSuperior ++;
                if(val.name === '' || val.email === '')
                    check1 = true;
                if(val.name === '' && val.email === '')
                    check1 = null;
            });
            if(numResp == 0){
                if(numSuperior < 1){
                    check1 = true;
                }
            }

            //PEERS GROUP
            var values = {};
            var $peerT = $('input.Peer:text');
            $peerT.each(function(d) {
                if($(this).val() !== '' || $(this).parent().parent().find('p').eq(1).find('input').val() != ''){
                    values[d] = {};
                    values[d]['name'] = $(this).val();
                    values[d]['email'] = $(this).parent().parent().find('p').eq(1).find('input').val();
                }
            });
            temp = reached;
            jQuery.each(values, function(key,val) {
                if(val.name != '' && val.email != '')
                    reached ++;
                if(val.name === '' || val.email === '')
                    check2 = true;
                if(val.name === '' && val.email === '')
                    check2 = null;
            });
            if(numResp == 0){
                if((reached - temp) == 1 && reached < miss)
                    $('#jsError').append('<label>@lang("invite.errors.peer")</label>');
            }

            //SUBORDINATES GROUP
            var values = {};
            var $subordinateT = $('input.Subordinate:text');
            $subordinateT.each(function(d) {
                if($(this).val() !== '' || $(this).parent().parent().find('p').eq(1).find('input').val() != ''){
                    values[d] = {};
                    values[d]['name'] = $(this).val();
                    values[d]['email'] = $(this).parent().parent().find('p').eq(1).find('input').val();
                }
            });
            temp = reached;
            jQuery.each(values, function(key,val) {
                if(val.name != '' && val.email != '')
                    reached ++;
                if(val.name === '' || val.email === '')
                    check3 = true;
                if(val.name === '' && val.email === '')
                    check3 = null;
            });
            if(numResp == 0){
                if((reached - temp) == 1 && reached < miss){
                    $('#jsError').append('<label>@lang("invite.errors.subordinate")</label>');
                }
            }

            //OTHERS GROUP
            var values = {};
            var $othersT = $('input.Other:text');
            $othersT.each(function(d) {
                if($(this).val() !== '' || $(this).parent().parent().find('p').eq(1).find('input').val() != ''){
                    values[d] = {};
                    values[d]['name'] = $(this).val();
                    values[d]['email'] = $(this).parent().parent().find('p').eq(1).find('input').val();
                }
            });
            temp = reached;
            jQuery.each(values, function(key,val) {
                if(val.name != '' && val.email != '')
                    reached ++;
                if(val.name === '' || val.email === '')
                    check4 = true;
                if(val.name === '' && val.email === '')
                    check4 = null;
            });
            if(numResp == 0){
                if((reached - temp) == 1 && reached < miss)
                    $('#jsError').append('<label>@lang("invite.errors.other")</label>');
            }

            //MISSING NAMES OR EMAILS
            if(superior === null || peer === null || subordinate === null || check1 === true || check2 === true || check3 === true || check4 === true){
                if(reached < miss){
                    var pending = miss-reached;
                    $('#jsError').append('<label>@lang("invite.errors.totals.0") '+ reached +' @lang("invite.errors.totals.1") <?php echo $minimum_required ?> @lang("invite.errors.totals.2") '+ pending +' @lang("invite.errors.totals.3") <?php echo $minimum_required ?> @lang("invite.errors.totals.4")');
                    e.preventDefault();
                }
                if(check1 === true){
                    $('#jsError').append('<label>@lang("invite.errors.missing.superior")</label>');
                    e.preventDefault();
                }
                if(check2 === true){
                    $('#jsError').append('<label>@lang("invite.errors.missing.peer")</label>');
                    e.preventDefault();
                }
                if(check3 === true){
                    $('#jsError').append('<label>@lang("invite.errors.missing.subordinate")</label>');
                    e.preventDefault();
                }
                if(check4 === true){
                    $('#jsError').append('<label>@lang("invite.errors.missing.other")</label>');
                    e.preventDefault();
                }
            }
        });
        setup_form(); 

        $(".extra_respondent").live('click', function(e) {
            e.preventDefault();
            var group;
            group = $(this).attr('group_id');
            var html;

            if (window.location.href.indexOf("/es/") > -1) {
                var tit_name = 'Nombre';
                var tit_mail = 'Correo electrónico';
                
                var groupname;
                if (group == 1)
                    groupname = 'Superior';
                if (group == 2)
                    groupname = 'Par';
                if (group == 3)
                    groupname = 'Subordinado';
                if (group == 4)
                    groupname = 'Otro';
                if (group == 5)
                    groupname = 'Colega';
                if (group == 6)
                    groupname = 'Mentor';
            }else{
                var tit_name = 'Name';
                var tit_mail = 'Email';

                var groupname;
                if (group == 1)
                    groupname = 'Superior';
                if (group == 2)
                    groupname = 'Peer';
                if (group == 3)
                    groupname = 'Subordinate';
                if (group == 4)
                    groupname = 'Other';
                if (group == 5)
                    groupname = 'Co-worker';
                if (group == 6)
                    groupname = 'Mentor';
            }

            var groupnameToShow;
            if (groupname == 'Superior') {
                groupnameToShow = 'Manager';
            } else {
                groupnameToShow = groupname;
            }

            html = "<li>"
                    +"<h2>"+groupnameToShow+"*</h2>"
                    +"<p>"
                        +"<label>"+tit_name+"</label>"
                        +'<input type="text" value="" class="field extra '+groupname+'" name="name[]"/>'
                    +"</p>"
                    +"<p class='second'>"
                        +"<label>"+tit_mail+"</label>"
                        +'<input type="email" value="" class="field email extra '+groupname+'" name="email[]"/>'
                    +'</p>'
                    +'<p class="second text_email_row">'
                        +'<label><?php echo trans("invite.errors.invites.customText") ?></label>'
                        +'<input type="hidden" value="'+ group +'" class="" name="respondentgroup[]"/>'
                        +"<?php echo $customTextJS ?>"
                    +'</p>'
                +'</li>';

            $('#respondentList li#listItem' + group).before(html);
            setup_form(); 
        });     
    });
</script>