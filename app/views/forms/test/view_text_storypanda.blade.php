<link href="http://www.360mirror.org/media/css/global.css" rel="stylesheet" type="text/css">

<div id ="view_text">
Dear {NAME},
<br/><br/>
As someone who has some knowledge of my Impact Project, I’m hoping you can fill out a brief assessment. This will help me see where the strengths and weakness of my project are and where I need support and resources.
<br/><br/>
Please click the link below to send me your feedback. Answering these questions will take approximately 15 minutes, and the answers will be sent back to me anonymously. I appreciate your help!
<br/><br/>
Thank you!
<br/><br/>
{YOUR_NAME}
<br/><br/>
</div>