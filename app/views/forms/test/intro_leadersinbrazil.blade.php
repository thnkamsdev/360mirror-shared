@extends('template_main_thnk')
@section('content')
    <div class="inviteBox intro">            
        <h2>O 360 MIRROR</h2>
        <p>Você está olhando para o MIRROR. É como se olhar no espelho. Olhar no espelho significa olhar para si mesmo. O que você vê? E como isso é semelhante ou diferente de como os outros o veem?
        <br/><br/>
        O MIRROR apoiará você em sua jornada de desenvolvimento durante o Programa Terceiro Setor Transforma. Ele o ajudará a entender como você é percebido por você e pelos outros. Ele mostrará seus pontos fortes e oportunidades de desenvolvimento. Isso ajudará você a identificar conscientemente os aspectos de sua liderança nos quais gostaria de trabalhar.</p>

        <h3>CONFIDENCIALIDADE E ESTIMATIVA DE TEMPO</h3>
        <p>Seu relatório é confidencial e não será compartilhado com mais ninguém sem sua aprovação.
        <br/><br/>
        A avaliação não deve demorar mais de 20 minutos para ser concluída. Nós convidamos você a fazer sua autoavaliação o quanto antes. Após a conclusão, você será solicitado a nomear um mínimo de 5 respondentes (membros da equipe, colegas, pessoas com quem você está trabalhando ou com quem já trabalhou).
        <br/><br/>
        O prazo para conclusão (incluindo a avaliação dos entrevistados) é domingo, 21 de outubro.</p>

        <h3>TRÊS PRINCIPAIS TEMAS DA LIDERANÇA</h3>
        <p>O Programa Terceiro Setor Transforma aborda três temas de liderança de ONGs que são particularmente importantes para liderar no mundo de hoje:</p>
        <ul>
            <li><b>Fortalecer:</b> Ser uma liderança autêntica, resiliente e com uma mentalidade orientada para o crescimento.</li>
            <li><b>Ampliar:</b> Ter um alcance cada vez maior a partir da construção de iniciativas estruturadas para ir cada vez mais longe ao lado de times engajados e criativos.</li>
            <li><b>Liderar:</b> Desenvolver um impacto sustentável com uma visão de longo prazo; liderar iniciativas inovadoras.</li>
        </ul>
        <p>Essa avaliação é projetada para avaliar seu nível atual de competência e orientá-lo ao definir suas metas de desenvolvimento de liderança.</p>

        <h3>COMO LER O 360 MIRROR</h3>
        <p>A ideia do MIRROR é ter uma noção de onde estão suas forças naturais e áreas de melhoria. Cada pessoa tem um conjunto único de pontos fortes e oportunidades de desenvolvimento. Essa avaliação ajudará você a descobrir o seu.
        <br/><br/>
        Comece com o quadro geral: sua pontuação nas principais dimensões. Em seguida, amplie em cada dimensão. A seção " In Addition" apresenta comentários ou exemplos concretos de como você exibe as habilidades de liderança fornecidas pelos entrevistados. No final deste documento, você encontrará uma tabela que ajuda a refletir sobre o MIRROR. Convidamos você a listar os resultados que agradam e desagradam ou surpreendem você, o que ajudará a tornar suas percepções acionáveis.
        <br/><br/>
        O que você deveria procurar? Tome nota de onde você ganha nota alta ou baixa. Procure onde a pontuação de sua autoavaliação difere significativamente de como os outros a classificaram e, em seguida, considere por que essas classificações são diferentes. Tome nota especial das áreas onde muitos entrevistados parecem concordar fortemente. Dê uma olhada em alguns dos feedbacks qualitativos - muitas vezes eles contêm preciosidades de insights.</p>

        <div class="center">
            {{ Form::open(array('id' => 'commentTestForm')) }}
                <input type='submit' value='INICIE SUA AVALIAÇÃO' name="knop" class="button1 name2">
            {{ Form::close() }}
            <div class="clear"></div>
        </div>
    </div>
@stop