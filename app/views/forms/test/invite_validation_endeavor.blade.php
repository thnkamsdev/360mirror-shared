<script type="text/javascript">
    $(document).ready(function() {

        //TOTAL RESPONDENTS
        var numResp = $('input[name="numResp"]').val();        

        function setup_form() {
            //CUSTOM EMAIL BOX
            var customEmailBox = $("input[name='use_email']:checked").val();

            if (customEmailBox == 1) {
                $(".text_email_row").hide();
            } else {
                $(".text_email_row").show();
            }
        }

        $('#commentForm').submit(function(e) {
            $('#jsError').text('');
            var coworker = null;
            var peer = null;
            var check7 = null;
            var check8 = null;
            var miss = $('input[name="minimum_required"]').val();
            var reached = numResp;
            var temp = 0;

            //CO-WORKERS GROUP
            var $coworkerT = $('input.Co-worker:text');
            var values = {};
            $coworkerT.each(function(d) {
                if($(this).val() !== '' || $(this).parent().parent().find('p').eq(1).find('input').val() != ''){
                    values[d] = {};
                    values[d]['name'] = $(this).val();
                    values[d]['email'] = $(this).parent().parent().find('p').eq(1).find('input').val();
                }
            });
            jQuery.each(values, function(key,val) {
                if(val.name != '' && val.email != '')
                    reached ++;
                if(val.name === '' || val.email === '')
                    check7 = true;
                if(val.name === '' && val.email === '')
                    check7 = null;
            });
            if(numResp == 0){
                if(reached == 1 && reached < miss)
                    $('#jsError').append('<label>The co-worker group contains only 1 respondent, ideally every group should contain at least 2 respondents</label>');
            }

            //PEERS GROUP
            var values = {};
            var $peerT = $('input.Peer:text');
            $peerT.each(function(d) {
                if($(this).val() !== '' || $(this).parent().parent().find('p').eq(1).find('input').val() != ''){
                    values[d] = {};
                    values[d]['name'] = $(this).val();
                    values[d]['email'] = $(this).parent().parent().find('p').eq(1).find('input').val();
                }
            });
            temp = reached;
            jQuery.each(values, function(key,val) {
                if(val.name != '' && val.email != '')
                    reached ++;
                if(val.name === '' || val.email === '')
                    check8 = true;
                if(val.name === '' && val.email === '')
                    check8 = null;
            });
            if(numResp == 0){
                if((reached - temp) == 1 && reached < miss)
                    $('#jsError').append('<label>The peer group contains only 1 respondent, ideally every group should contain at least 2 respondents</label>');
            }

            //MISSING NAMES OR EMAILS
            if(coworker === null || peer === null || check7 === true || check8 === true){
                if(reached < miss){
                    var pending = miss-reached;
                    $('#jsError').append('<label>You have invited '+ reached +' respondents,<br/>the minimum required is <?php echo $minimum_required ?> respondents.<br/>You still need to invite '+ pending +' more resondents<br/><br/>If it remains impossible to get <?php echo $minimum_required ?> respondents,<br/>please notify your coach.</label>');
                    e.preventDefault();
                }
                if(check7 === true){
                    $('#jsError').append('<label>In the co-worker group there is one or more name or email missing</label>');
                    e.preventDefault();
                }
                if(check8 === true){
                    $('#jsError').append('<label>In the peer group there is one or more name or email missing</label>');
                    e.preventDefault();
                }
            }
        });
        setup_form(); 

        $(".extra_respondent").live('click', function(e) {
            e.preventDefault();
            var group;
            group = $(this).attr('group_id');
            var groupname;
            if (group == 1)
                groupname = 'Superior';
            if (group == 2)
                groupname = 'Peer';
            if (group == 3)
                groupname = 'Subordinate';
            if (group == 4)
                groupname = 'Other';
            if (group == 5)
                groupname = 'Co-worker';
            if (group == 6)
                groupname = 'Mentor';
            var html;

            html = "<li>"
                    +"<h2>"
                        + groupname
                    + "*</h2>"
                    +"<p>"
                        +"<label>NAME</label>"
                        +'<input type="text" value="" class="field extra '+groupname+'" name="name[]"/>'
                    +"</p>"
                    +"<p class='second'>"
                        +"<label>Email Address</label>"
                        +'<input type="email" value="" class="field email extra '+groupname+'" name="email[]"/>'
                    +'</p>'
                    +'<p class="second text_email_row">'
                        +'<label>Custom email text</label>'
                        +'<input type="hidden" value="'+ group +'" class="" name="respondentgroup[]"/>'
                        +"<?php echo $customTextJS ?>"
                    +'</p>'
                +'</li>';

            $('#respondentList li#listItem' + group).before(html);
            setup_form(); 
        });     
    });
</script>