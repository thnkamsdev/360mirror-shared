@extends('template_main_thnk')
@section('content')

<?php //var_dump($endeavor);exit; ?>
    
<div class="inviteBox intro">
    <h2><?= $user->firstname; ?> <?= $user->lastname; ?> Endeavor Project:</h2>

    <div class="profileBox second full">
        <div class="endeavorTitle">
            <div class="endeavorDesc">@lang('endeavor.name')</div>
            <p><?= $endeavor[0]->name_enUS; ?></p>
        </div>

        <div class="endeavorTitle">
            <div class="endeavorDesc">@lang('endeavor.type')</div>
            <p><?= $type_options[$endeavor[0]->type_id] ?></p>
        </div>

        <div class="endeavorTitle">
            <div class="endeavorDesc">@lang('endeavor.size')</div>
            <p><?= $endeavor[0]->people; ?></p>
        </div>
        
        <div class="endeavorTitle">
            <div class="endeavorDesc">@lang('endeavor.stage')</div>
            <p><?= $stage_options[$endeavor[0]->stage_id] ?></p>
        </div>

        <div class="endeavorTitle">
            <div class="endeavorDesc">@lang('endeavor.descriptionWhat')</div>
            <p><?= $endeavor[0]->descriptionWhat_enUS; ?></p>
        </div>

        <div class="endeavorTitle">
            <div class="endeavorDesc">@lang('endeavor.descriptionHow')</div>
            <p><?= $endeavor[0]->descriptionHow_enUS; ?></p>
        </div>

        <div class="endeavorTitle">
            <div class="endeavorDesc">@lang('endeavor.descriptionWho')</div>
            <p><?= $endeavor[0]->descriptionWho_enUS; ?></p>
        </div>

        <div class="endeavorTitle">
            <div class="endeavorDesc">@lang('endeavor.descriptionWhy')</div>
            <p><?= $endeavor[0]->descriptionWhy_enUS; ?></p>
        </div>

        <div class="endeavorTitle">
            <div class="endeavorDesc">@lang('endeavor.descriptionNextTitle')</div>
            <p><?= $endeavor[0]->descriptionNext_enUS; ?></p>
        </div>
    </div>

    <div class="clear"></div>

    <div class="center">
        {{ Form::open(array('id' => 'commentTestForm')) }}
            <input type='submit' value='@lang("endeavor.buttons.feedback")' name="knop" class="button1 name2">
            <input type="hidden" name="md5" value="<?php echo '$md5'; ?>">
            <input type="hidden" name="test_id" value="<?php echo '$test_id'; ?>">
            <input type="hidden" name="intro" value="<?php echo 'intro'; ?>">
        </form>
        <div class="clear"></div>
    </div>
</div>

@stop