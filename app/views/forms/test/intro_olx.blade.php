@extends('template_main_thnk')
@section('content')
    <div class="inviteBox intro">            
        <h2>THE 360 MIRROR</h2>
        <p>You are looking into the 360 Mirror. Looking in a mirror means looking at yourself. What do you see? And how do others see you? This assessment tells you about your leadership skills, perceived by yourself and by others. It shows your strengths and development potential, or gifts and learning edges. This assessment will help you to consciously identify and develop the aspects of your leadership you’d like to focus on as a leader with OLX Group.
        <br/><br/>
        Leadership development is an ongoing journey throughout your career - we invite you to take full opportunity of the insights from the 360 Mirror to define the next steps in your development as a leader.</p>

        <h3>FIVE KEY LEADERSHIP DIMENSIONS AT OLX GROUP</h3>
        <p>This survey has been designed specifically by and for OLX Group. It gauges your current level of competency on each of the five dimensions that have been identified as being critical for leaders at OLX Group. These five dimensions are:</p>
        <ul>
            <li>Embraces and drives change</li>
            <li>Collaborates in a global context</li>
            <li>Thrives within ambiguity</li>
            <li>Leads without formal authority</li>
            <li>Leads self</li>
        </ul>

        <h3>HOW TO READ THE MIRROR</h3>
        <p>The idea of the 360 Mirror is to get a sense of where your natural strengths and areas of improvement lie. We call these your Leadership Gifts and your Learning Edges. Every person has a unique set of gifts and edges. This assessment allows you to discover yours.
        <br/><br/>
        What should you look for? Look for where your self-scoring significantly differs from how others rated you and then consider why these ratings are different. Take special note of areas where many respondents seem to agree strongly. Also take notice of where you score notably high or low.
        <br/><br/>
        Start with the big picture: your score over the major dimensions. Then zoom in on each dimension. The “In Addition” section features comments or concrete examples of how you display leadership dimensions as provided by your raters. At the end of this document, you will find a table that helps you reflect on the 360 Mirror. We invite you to list the results that please you and displease or surprise you, which will help make your insights actionable.</p>

        <div class="center">
            {{ Form::open(array('id' => 'commentTestForm')) }}
                <input type='submit' value='Start your assessment' name="knop" class="button1 name2">
            {{ Form::close() }}
            <div class="clear"></div>
        </div>
    </div>
@stop