<?php
    //SEARCHING FOR CLIENT ON RESPONDENTS VIEW
    if ( isset($user) ) {
        if ( empty($user->slug) ) {
            if( $test->teststatus_id != 1 ) {
                $test_parent = Test::find($test->test_id);
                $user = User::find($test_parent->client_id);
            }
            
        }        
    }

    //BEHAVIOR SURVEY TYPE
    if ($user->type_of_report == 1) {
?>
        <li class="header">
            <div class="one">
                <h2 style="font-size: 20px;"><?php echo $category->name; ?></h2>                       
            </div>
        <?php if ($user->id == 2560) { ?>
            <div class="two non">
                <p class="name indent">precisa de melhora significativa<br /></p>
            </div>

            <div class="three">
                <p class="indent">precisa de alguma melhoria<br /><br /></p>
            </div>

            <div class="two">
                <p class="indent">competente<br /><br /><br /></p>
            </div>

            <div class="three add">
                <p class="indent">força<br /><br /><br /><br /></p>
            </div>

            <div class="two next">
                <p class="indent">força excepcional<br /><br /></p>
            </div>

            <div class="seven">
                <p class="indent">não pode avaliar<br /></p>
            </div>
        <?php }else{ ?>
            <div class="two non">
                <p class="name indent">@lang('test.behavior_scores.1')<br /></p>
            </div>

            <div class="three">
                <p class="indent">@lang('test.behavior_scores.2')<br /><br /></p>
            </div>

            <div class="two">
                <p class="indent">@lang('test.behavior_scores.3')<br /><br /><br /></p>
            </div>

            <div class="three add">
                <p class="indent">@lang('test.behavior_scores.4')<br /><br /></p>
            </div>

            <div class="two next">
                <p class="indent">@lang('test.behavior_scores.5')<br /></p>
            </div>

            <div class="seven">
                <p class="indent">@lang('test.behavior_scores.canNotRate')<br /></p>
            </div>
        <?php } ?>
        </li>
<?php
    } else {
        //COMPETENCY SURVEY TYPE
?>
        <li class="header">
            <div class="one">
                <h2 style="font-size: 20px;"><?php echo $category->name; ?></h2>
            </div>

            <div class="two non">
                <p class="name indent">@lang('test.competency_scores.1')<br /></p>
            </div>

            <div class="three">
                <p class="indent">@lang('test.competency_scores.2')<br /><br /></p>
            </div>

            <div class="two">
                <p class="indent">@lang('test.competency_scores.3')<br /><br /><br /></p>
            </div>

            <div class="three add">
                <p class="indent">@lang('test.competency_scores.4')<br /><br /></p>
            </div>

            <div class="two next">
                <p class="indent">@lang('test.competency_scores.5')<br /></p>
            </div>

            <div class="seven">
                <p class="indent">@lang('test.competency_scores.canNotRate')<br /></p>
            </div>
        </li>
<?php
    }

 ?>