@extends('template_main_mirror')
@section('content')
    <div class="inviteBox intro">            
        <h2>@lang('test.intro.title1')</h2>
        <p>@lang('test.intro.description1')</p>

        <h3>@lang('test.intro.title2')</h3>
        <div class="left">
            <p>@lang('test.intro.description2')</p>
        </div>
        <div class="right">
            <img src="/media/images/ggf-4boxes.png">
        </div>
        <div class="clear"></div>

        <h3>@lang('test.intro.title3')</h3>
        <p>@lang('test.intro.description3')</p>

        <div class="center">
            {{ Form::open(array('id' => 'commentTestForm')) }}
                <input type='submit' value='@lang("test.intro.button")' name="knop" class="button1 name2">
            {{ Form::close() }}
            <div class="clear"></div>
        </div>
    </div>
@stop