@extends('template_main_mirror')
@section('content')

{{HTML::script('media/javascript/custom-form-elements.js')}}

<?php
    $customText = $user->getCustomText($user->THNKParticipant, $user->id, $participant);
    $customTextJS = str_replace("\n", "\\n", $customText);
    $getBoxInvite = $user->getBoxInvite($user->id);
    $getInviteTextTemplate = $user->getInviteTextTemplate($user->THNKParticipant, $user->id);
    $getDefEmailText = $user->getDefEmailText($user->id);;
    $extraMsgText = $user->getExtraMsgText($user->id, $user);
?>

<?php
//LOAD CLIENT VALIDATION JS
switch ($user->id) {
    case '1394':
        ?>@include('forms.test.invite_validation_storypanda')<?php
        break;
    case '1675':
        ?>@include('forms.test.invite_validation_endeavor')<?php
        break;
    case '2129':
    case '2344':
        ?>@include('forms.test.invite_validation_olx')<?php
        break;
    case '2283':
        ?>@include('forms.test.invite_validation_siemens')<?php
        break;
    case '2588':
        ?>@include('forms.test.invite_validation_nrc')<?php
        break;
    default:
        ?>@include('forms.test.invite_validation')<?php
        break;
}
?>

<?php
    //PRINTING LAYOUT FOR FRESH INVITES
    $reachedResps = count($respondents);
    if($reachedResps<1){
?> 
        <!--  / center container \ -->
        <div class="maxWidth">

            @if($errors->has())
                <div class="inviteBox">
                    <div class="errors">
                        <h3>@lang('invite.errors.title')</h3>
                        <ul id="#respondents_list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            <!--  / invite box \ -->
            <div class="inviteBox">
                <?php
                    echo'<input type="hidden" name="numResp" value="'.count($respondents).'" /><input type="hidden" name="minimum_required" value="'.$minimum_required.'" />';
                ?>
                
                <h2>@lang('invite.title')</h2>

                <div class="txt">

                    <h3>@lang('invite.userIntro') <?= $participant->firstname; ?></h3>

                    <?php echo $getBoxInvite; ?>

                    <div class="end">
                        <p>@lang('invite.description')</p>
                    </div>

                </div>
            </div>
            <!--  \ invite box / -->

            {{ Form::open(array('id' => 'commentForm')) }}

                <div class="inviteBox">
                    <p>@lang('invite.emailType.title')</p>

                    <div class="radio1">

                        <?php if($getDefEmailText){ ?>
                            <p>
                                <input type="radio" class="styled" name="use_email" value="1" checked/>
                                @lang('invite.emailType.default.0') <a href="{{ URL::to($getInviteTextTemplate) }}" class="slide">@lang('invite.emailType.default.1')</a>
                            </p>
                            <p>
                                <input type="radio" class="styled" name="use_email" value="2" />
                                @lang('invite.emailType.custom')
                           </p>
                        <?php }else{?>
                            <p>
                                <input type="radio" class="styled" name="use_email" value="2" checked/>
                                @lang('invite.emailType.custom')
                           </p>
                        <?php } ?>

                    </div>
                </div>

                <!--  / extra box \ -->
                <div class="extraBox" id='profileBoxInvite'>

                    <fieldset>
                        <div class="profileBox" id='profileBoxInvite'>

                            <ul id="respondentList">
                                
                                <?php $user->getInviteBoxes($user, $user->THNKParticipant ,$user->id, $participant); ?>

                                <li class="last">
                                    <p>
                                        <input type="submit" class="button" value="@lang('invite.buttons.invite')" />
                                    </p>
                                    <div id="jsError"></div>
                                </li>
                            </ul>
                        </div>
                    </fieldset>

                    <p class='extra_message'>
                        <?php echo $extraMsgText ?>
                    </p>

                </div>
                <!--  \ extra box / -->
            {{Form::close()}}

        </div>
        <!--  \ center container / -->

        <div class="clear"></div>
<?php
    }else{
        //PRINTING LAYOUT FOR WHEN SOME RESPONDENTS ALREADY INVITED
?>
        <!--  / center container \ -->
        <div class="maxWidth">

            @if($errors->has())
                <div class="inviteBox">
                    <div class="errors">
                        <h3>@lang('invite.errors.title')</h3>
                        <ul id="#respondents_list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <input type="hidden" name="numResp" value="<?php echo count($respondents); ?>" /><input type="hidden" name="minimum_required" value="<?php echo $minimum_required; ?>" />
            <?php $invitedResps = count($respondents); ?>
            
            <div class="inviteBox">                
                <h2>@lang('invite.alreadyInvited.title')</h2>

                <?php if ($minimum_required-$invitedResps >= 0) { ?>
                <div class="txt">
                    <h3>@lang('invite.alreadyInvited.title') <?= $participant->firstname; ?></h3>                    
                    <p>@lang('invite.errors.totals.0') <?php echo $invitedResps; ?> @lang('invite.errors.totals.1') <?php echo $minimum_required ?> @lang('invite.errors.totals.2') <?php echo $minimum_required-$invitedResps; ?> @lang('invite.errors.totals.3') <?php echo $minimum_required ?> @lang('invite.errors.totals.4')</p>
                </div>
                <?php } ?>
                
            </div>

            {{ Form::open(array('id' => 'commentForm')) }}
                <div class="inviteBox">
                    <p>@lang('invite.emailType.title')</p>

                    <div class="radio1">

                        <?php if($getDefEmailText){ ?>
                            <p>
                                <input type="radio" class="styled" name="use_email" value="1" checked/>
                                @lang('invite.emailType.default.0') <a href="{{ URL::to($getInviteTextTemplate) }}" class="slide">@lang('invite.emailType.default.1')</a>
                            </p>
                            <p>
                                <input type="radio" class="styled" name="use_email" value="2" />
                                @lang('invite.emailType.custom')
                           </p>
                        <?php }else{?>
                            <p>
                                <input type="radio" class="styled" name="use_email" value="2" checked/>
                                @lang('invite.emailType.custom')
                           </p>
                        <?php } ?>

                    </div>
                </div>

                <!--  / extra box \ -->
                <div class="extraBox" id='profileBoxInvite'>

                    <fieldset>
                        <div class="profileBox" id='profileBoxInvite'>

                            <ul id="respondentList">

                                <?php $user->getInviteBoxes($user, $user->THNKParticipant ,$user->id, $participant); ?>

                                <li class="last">
                                    <p>
                                        <input type="submit" class="button" value="@lang('invite.buttons.invite')" />
                                    </p>
                                    <div id="jsError"></div>
                                </li>
                            </ul>
                        </div>
                    </fieldset>

                    <p class='extra_message'>
                        <?php echo $extraMsgText ?>
                    </p>

                </div>
                <!--  \ extra box / -->
            {{Form::close()}}

        </div>
        <!--  \ center container / -->

        <div class="clear"></div>
<?php
}
?>
@stop