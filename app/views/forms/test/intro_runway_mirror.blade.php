@extends('template_main_runway_mirror')
@section('content')
    <div class="inviteBox intro">            
        <h2>SCALEUPNATION MIRROR</h2>
        <p>You are looking into ScaleUpNation Mirror. Looking in a mirror means looking at yourself. What do you see?  And how do others see you? This assesment tells you about your leadership skills, perceived by yourself and by others. It shows your strengths and development potential, or gifts and learning edges. This assessment will help you to consciously identify and develop the aspects of your leadership you’d like to focus on as a creative leader. Leadership development is an ongoing journey throughout your career and we hope the ScaleUpNation Mirror becomes a routine part of your personal journey.</p>

        <h3>CREATIVE LEADERSHIP</h3>
        <div class="left">
            <p>The leadership skills of today’s and tomorrow’s leaders are developed along four distinctive areas of competency, as indicated in the model below, as well as on personal mastery. This assessment is meant to gauge your current level of competency and guide you as you define your leadership development goals.
            <br/><br/>
           Each of the four competencies (Exploring, Architecting, Conducting, and Directing) and Personal Mastery will be explained individually at the beginning of each section that follows.</p>
        </div>
        <div class="right">
            <img src="/media/images/ipad_report_runway_mirror.png">
        </div>
        <div class="clear"></div>

        <h3>HOW TO READ</h3>
        <p>The idea of the ScaleUpNation Mirror is to get a sense of where your natural strengths and areas of improvement lie. We call these your Leadership Gifts and your Learning Edges. Every person has a unique set of gifts and edges.  This assessment allows you to discover yours. What should you look for? Look for where your self-scoring significantly differs from how others rated you and then consider why these ratings are different. Take special note of areas where many respondents seem to agree strongly. Also take notice of where you score notably high or low.
        <br/><br/>
        Start with the big picture: your score over the major competencies. Then zoom in on each competency. The “In Addition” section features comments or concrete examples of how you display leadership competencies as provided by your raters. At the end of this document, you will find a table that helps you reflect on the 360 Mirror. We invite you to list the results that please you and displease or surprise you, which will help make your insights actionable.</p>

        <div class="center">
            {{ Form::open(array('id' => 'commentTestForm')) }}
                <input type='submit' value='Start your assessment' name="knop" class="button1 name2">
            {{ Form::close() }}
            <div class="clear"></div>
        </div>
    </div>
@stop