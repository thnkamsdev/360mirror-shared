@extends('template_main')
@section('content')
			<!--  / center container \ -->
			<div id="centerCntr">

				<!--  / complete box \ -->
                <div class="completeBox leftThxBox">

                    <h2>@lang('test.thanks.title')</h2>

                    <ul>
                    	<li>@lang('test.thanks.description')<br /> </li>
                    </ul>

                    <a href="mailto:manolo.paez@globalgoodfund.org?subject=Acquire 360 Mirror seat&body=Hi, I am interested in acquire a seat on your 360 Mirror tool. Could you please advice on pricing? Thanks!" class="button">@lang('test.thanks.button')</a>

				</div>
				<!--  \ complete box / -->

			</div>
			<!--  \ center container / -->

            <div class="clear"></div>

<script type="text/javascript">
	$('#example1, #example3').accordion();
	$('#example2').accordion({
		canToggle: true
	});
	$('#example4').accordion({
		canToggle: true,
		canOpenMultiple: true
	});
	$(".loading").removeClass("loading");
</script>

@stop