<link href="http://www.360mirror.org/media/css/global.css" rel="stylesheet" type="text/css">

<div id ="view_text">
Dear {NAME},
<br/><br/>
I will be participating in a <a href="https://www.thnk.org/">THNK School of Creative Leadership</a> assessment.
<br/>
For this purpose I would like to ask you to give feedback on my strengths and weaknesses.
<br/>
Please click the link below to access the assessment tool.
<br/>
Answering the questions will take approximately 30 minutes.
<br/>
The answers will be sent back to me anonymously.
<br/>
I really appreciate your help.
<br/><br/>
Best regards,
<br/>
{YOUR_NAME}
<br/><br/>
</div>