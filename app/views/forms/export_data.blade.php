@extends('layouts.popup')

@section('content')
	@lang('dashboardClient.export.info')

	<div class="export-data profileBox">

		<ul class="export-data__tabs">
			<li class="export-data__tab"><a class="export-data__tab-link" href="#program">@lang('dashboardClient.export.program')</a></li>
			<li class="export-data__tab"><a class="export-data__tab-link" href="#users">@lang('dashboardClient.export.users')</a></li>
		</ul>

		<div class="export-data__content-wrapper">

			{{ Form::open(['url' => $formActionURL]) }}
			{{ Form::hidden('type', 'program', ['id' => 'field_type']) }}
			{{ Form::hidden('client_id', $client->id) }}
			{{ Form::hidden('user_ids', '', ['id' => 'field_user_ids']) }}
			{{ Form::hidden('filters', '', ['id' => 'field_filters']) }}

			<div class="export-data__content export-data__content--program" id="tab-program">
				<label>@lang('dashboardClient.hasPrograms.title')</label>

				{{ Form::select( 'program_id', $program_options, '', [ 'class' => 'styled', 'id' => 'program_options_data_select' ] ) }}
			</div>

			<div class="export-data__content export-data__content--users" id="tab-users">

				<label>@lang('dashboardClient.export.usersHowTo')</label>

				<div class="export-data__user-select">

					<div class="export-data__users-wrapper">
						<h4>@lang('dashboardClient.export.usersAll')</h4>
						<ul class="export-data__users export-data__users--source">
							@foreach ($users as $user)
								<li class="export-data__user" data-id="{{ $user->id }}">
									<div class="export-data__user__name">{{ $user->firstname }} {{ $user->lastname }}</div>
									<div class="export-data__user__email">{{ $user->email }}</div>
								</li>
							@endforeach
						</ul>
					</div>

					<div class="export-data__users-wrapper">
						<h4>@lang('dashboardClient.export.usersHowTo') (<b class="export-data__selected-users">0</b>)</h4>
						<ul class="export-data__users export-data__users--target">
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="export-data__filter">
			{{ Form::label(trans('dashboardClient.export.totals'), '', array('class' => 'net')) }}

			{{ Form::radio('filter_totals', 'totals', true, [ 'class' => 'styled radio export-date__filter' ] ) }}
			{{ Form::label(trans('dashboardClient.export.yes'), '', array('class' => 'same')) }}

			{{ Form::radio('filter_totals', '', null, [ 'class' => 'styled radio export-date__filter' ] ) }}
			{{ Form::label(trans('dashboardClient.export.no'), '', array('class' => 'same')) }}
		</div>
		<div class="export-data__filter">
			{{ Form::label(trans('dashboardClient.export.completed'), '', array('class' => 'net')) }}
			{{ Form::radio('filter_completed', 'completed', true, [ 'class' => 'styled radio export-date__filter' ] ) }}
			{{ Form::label(trans('dashboardClient.export.yes'), '', array('class' => 'same')) }}

			{{ Form::radio('filter_completed', '', null, [ 'class' => 'styled radio export-date__filter' ] ) }}
			{{ Form::label(trans('dashboardClient.export.no'), '', array('class' => 'same')) }}
		</div>

		@if($errors->has())
			<ul class="export-data__errors">
				@foreach ($errors->all() as $error)
					<li class="export-data__error">{{ $error }}</li>
				@endforeach
			</ul>
		@endif

		{{ Form::submit(trans('dashboardClient.export.buttonDefault'), ['name' => 'knop', 'class' => 'button grey cancel export-data__submit']) }}
		{{ Form::close() }}
	</div>

	<!-- CDNJS :: Sortable (https://cdnjs.com/) -->
	{{--<script src="//cdnjs.cloudflare.com/ajax/libs/Sortable/1.5.0-rc1/Sortable.min.js"></script>--}}
	<script src="/media/javascript/sortable-1.5.0-rc1.js"></script>

	<script>
		(function() {

			/**
			 * Keep track of the selected user ids
			 * @type {Array}
			 */
			var $selectedUsersCount = $('.export-data__selected-users');
			var $selectedTypeField = $('#field_type');
			var $selectedUsersField = $('#field_user_ids');
			var $selectedFiltersField = $('#field_filters');
			var $allFilters = $('.export-date__filter');
			var selectedUsers = [];

			function updateUserIdsField() {
				$selectedUsersField.val(selectedUsers.join(','));
				$selectedUsersCount.html(selectedUsers.length);
			}

			function addUser(id) {
				selectedUsers.push(id);
				updateUserIdsField();
			}

			function removeUser(id) {
				var position = selectedUsers.indexOf(id);

				if (position >= 0) {
					selectedUsers.splice(position, 1);
				}

				updateUserIdsField(selectedUsers);
			}

			/**
			 * Make the user lists draggable
			 */
			var config = {
				group: 'users',
				ghostClass: 'export-data__user--ghost',
				chosenClass: 'export-data__user--chosen',
				dragClass: 'export-data__user--dragging',
				sort: false,
				animation: 150,
			};
			var source = new Sortable($('.export-data__users--source').get(0), config);
			var target = new Sortable($('.export-data__users--target').get(0), $.extend(config, {
				onAdd: function(event) {
					var id = parseInt($(event.item).attr('data-id'));
					addUser(id);
				},

				onRemove: function(event) {
					var id = parseInt($(event.item).attr('data-id'));
					removeUser(id);
				}
			}));

			/**
			 * Make tabs select the correct content
			 */
			$('.export-data__tab-link').click(function(event) {
				var uri = event.currentTarget.href;
				var uriParts = uri.split('#');
				var tabName = uriParts[1];

				// Disable old
				$('.export-data__content--active').removeClass('export-data__content--active');
				$('.export-data__tab--active').removeClass('export-data__tab--active');

				// Enable new
				$('#tab-' + tabName).addClass('export-data__content--active');
				$(this).parent().addClass('export-data__tab--active');

				// Re-use the tab name as the export type
				$selectedTypeField.val(tabName);
			});
			$('.export-data__tab-link:first').click();

			/**
			 * Update filters field
			 */
			function updateFilters() {
				// Collect all checked filters
				var filters = [];
				$allFilters.filter(':checked').each(function(index, filter) {
					filters.push($(this).val());
				});

				// Update the value of the filters field
				var filterString = filters.filter(function(filter) {
					return !!filter;
				}).join(',');
				$selectedFiltersField.val(filterString);
			}

			$('.export-date__filter').change(updateFilters);
			updateFilters();

		})();
	</script>
@stop