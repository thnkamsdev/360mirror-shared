<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!--Recommended Meta Tags-->
        <meta charset="utf-8">
        <meta name="language" content="english"> 
        <meta http-equiv="content-type" content="text/html">
        <meta name="author" content="THNK">
        <meta name="designer" content="THNK">
        <meta name="publisher" content="THNK">
        <meta name="no-email-collection" content="http://www.unspam.com/noemailcollection/">

        <!--Search Engine Optimization Meta Tags-->
        <title>@lang('mainTemplate.title')</title>
        <meta name="description" content="@lang('mainTemplate.head.description')">
        <meta name="keywords" content="@lang('mainTemplate.head.keywords')">
        <meta name="robots" content="index,follow">
        <meta name="revisit-after" content="7 days">
        <meta name="distribution" content="web">
        <meta http-equiv="refresh" content="3600">
        <meta name="robots" content="noodp">
        
        <!--Optional Meta Tags-->
        <meta name="distribution" content="web">
        <meta name="web_author" content="THNK">
        <meta name="rating" content="general">
        <meta name="title" content="360 Mirror">
        <meta name="reply-to" content="info@thnk.org">
        <meta name="abstract" content="@lang('mainTemplate.head.description')">
        <meta name="city" content="Amsterdam">
        <meta name="country" content="Netherlands">
        <meta name="distribution" content="global">
        <meta name="classification" content="@lang('mainTemplate.head.keywords')">
      
        <!--Meta Tags for HTML pages on Mobile-->
        <meta name="format-detection" content="telephone=yes"/>
        <meta name="HandheldFriendly" content="true"/> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        
        <!--http-equiv Tags-->
        <meta http-equiv="Content-Style-Type" content="text/css">
        <meta http-equiv="Content-Script-Type" content="text/javascript">

        <?php /* NEW --> TO BE IMPLEMENTED
        <!--App::Assets-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="shortcut icon" href="{{asset('media/icons/favicon.ico')}}">
        <link rel="icon" href="{{asset('media/icons/favicon.ico')}}">        
        <link href="{{asset('media/fonts/NeuBauGrotesk/stylesheet.css')}}" rel="stylesheet"> */ ?>
        
        <?php /* OLD --> TO BE REMOVED */ ?>
        <link rel="shortcut icon" href="{{asset('media/icons/favicon.ico')}}">
        <link rel="icon" href="{{asset('media/icons/favicon.ico')}}">        
        <link href="{{asset('media/fonts/NeuBauGrotesk/stylesheet.css')}}" rel="stylesheet">
        <link href="{{asset('media/css/stylesheet.css')}}" rel="stylesheet">
        <link href="{{asset('media/css/global.css')}}" rel="stylesheet">        
        <link href="{{asset('media/javascript/themes/base/jquery.ui.all.css')}}" rel="stylesheet">        
        <link href="{{asset('media/javascript/fancybox/jquery.fancybox-1.3.1.css')}}" rel="stylesheet">

        <link href="{{asset('media/javascript/cookies/assets/cookiecuttr.css')}}" rel="stylesheet">
        <script src="{{asset('media/javascript/cookies/assets/old/jquery-1.7.1.js')}}"></script>
        <script src="{{asset('media/javascript/cookies/assets/old/jquery.cookie.js')}}"></script>
        <script src="{{asset('media/javascript/cookies/jquery.cookiecuttr.js')}}"></script> 
                   
        <script src="{{asset('media/javascript/custom-form-elements.js')}}"></script>       
        <script src="{{asset('media/javascript/jquery.validate.js')}}"></script>
        <script src="{{asset('media/javascript/custom-sugarfree.js')}}"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {
                $("#commentForm").validate();
            });
        </script>

        <script>
            if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-35807799-1']);
                _gaq.push(['_trackPageview']);
                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            }
            $(document).ready(function () {
                $.cookieCuttr({
                    cookieAnalytics: false,
                    cookiePolicyPage: true,
                    cookieDeclineButton: true,
                    cookiePolicyPageMessage: 'We uses cookies to tailor your experience and understand how you and other visitors use our website.<br><br>You can <a href="https://www.thnk.org/privacy-policy/#cookies" target= "_blank" title="read about our cookies">read about cookies here</a>.<br><br>'
                });    
            });
        </script>

        <style type="text/css">
            #commentForm {  }
            #commentForm { }
            #commentForm label { }
            #commentForm input.error { border: 1px solid #ff0000 }
            #commentForm label.error, #commentForm input.submit { margin-left: 10px; }
            .validateForm {  }
            .validateForm { }
            .validateForm label { }
            .validateForm input.error { border: 1px solid #ff0000 }
            .validateForm label.error, .validateForm input.submit { margin-left: 10px; }
        </style>     
    </head>

    <body>

        <header id="header">
            <div class="container">
                <hgroup class="branding_main">
                    <a id="logo" href="{{ translate_url('/') }}">
                        <h2>@lang('mainTemplate.title')</h2>
                    </a>
                </hgroup>
            </div>
        </header>

        <div class="pusher" id="wrapper">
            <div class="content-top" id="mainCntr">
                <div class="container">

                        @yield('content')

        		</div>
        		<!--  \ content container / -->
                <div class="thnk_32"></div>

        	</div>
        	<!--  \ main container / -->

        </div>
        <!--  \ wrapper / -->

        <footer id="footer">
            <div class="container">
                <div class="f4c">
                    <h3>@lang('mainTemplate.footer.by')</h3>
                </div>
                <div class="f4c">
                    <a href="http://www.thnk.org" target="_blank">
                        <img src="{{asset('media/images/thnk_footer.png')}}">
                    </a>
                </div>
                <div class="f4c">
                    <a href="http://www.globalgoodfund.org" target="_blank">
                        <img src="{{asset('media/images/ggf_footer.png')}}">
                    </a>
                </div>
                <div class="f4c">
                    <div id="f4c_mail">
                        <a href="mailto:feedback@thnk.org" target="_blank">
                            <img id="f4c_img_mail"src="{{asset('media/images/email_icon_32x32.png')}}">
                            <span id="f4c_txt_mail">@lang('mainTemplate.footer.feedback')</span>
                        </a>
                    </div>
                </div>
            </div>
        </footer>

    </body>
</html>
