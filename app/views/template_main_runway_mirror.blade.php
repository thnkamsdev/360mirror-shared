<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>

    <title>SCALEUPNATION MIRROR</title>

    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="keywords" content="THNK, Global Good Fund, Mirror, assessment tool, 360, assessment, feedback, self assessment, report, creative leadership, school of creative leadership" />
    <meta name="description" content="ScaleUpNation Mirror is a comprehensive leadership development tool developed by ScaleUpNation, to provide insights into an individual’s creative leadership skill for social entrepreneurs. In only a few steps you’ll learn about your strengths and learning edges, resulting in the ultimate chance to enhance your effectiveness as a leader in your enterprise. ScaleUpNation Mirror is comprehensive, provided in two parts: Self-assessment Evaluation and 360° Peer Feedback." />
    <meta name="robots" content="index, follow" />

        <link rel="shortcut icon" href="{{asset('media/icons/favicon.ico')}}" type="image/x-icon">
        <link rel="icon" href="{{asset('media/icons/favicon.ico')}}" type="image/x-icon">
        
        <link href="{{asset('media/fonts/NeuBauGrotesk/stylesheet.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('media/css/stylesheet.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('media/css/global.css')}}" rel="stylesheet" type="text/css" />        
        <link href="{{asset('media/javascript/themes/base/jquery.ui.all.css')}}" rel="stylesheet" type="text/css" />        
        <link href="{{asset('media/javascript/fancybox/jquery.fancybox-1.3.1.css')}}" rel="stylesheet" type="text/css" />
              
        <script src="{{asset('media/javascript/custom-form-elements.js')}}"></script>
        <script src="{{asset('media/javascript/jquery-1.4.2.min.js')}}"></script>        
        <script src="{{asset('media/javascript/jquery.validate.js')}}"></script>
        <script src="{{asset('media/javascript/custom-sugarfree.js')}}"></script>     
                
        <script type="text/javascript">
            $(document).ready(function() { 

                $("#commentForm").validate();
            });
        </script>
        <script>
            if (jQuery.cookie('cc_cookie_accept') == "cc_cookie_accept") {
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-35807799-1']);
                _gaq.push(['_trackPageview']);
                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            }
            $(document).ready(function () {
                $.cookieCuttr({
                    cookieAnalytics: false,
                    cookiePolicyPage: true,
                    cookieDeclineButton: true,
                    cookiePolicyPageMessage: 'We uses cookies to tailor your experience and understand how you and other visitors use our website.<br><br>You can <a href="https://www.thnk.org/privacy-policy/#cookies" target= "_blank" title="read about our cookies">read about cookies here</a>.<br><br>'
                });    
            });
        </script>

        <style type="text/css">
            #commentForm {  }
            #commentForm { }
            #commentForm label { }
            #commentForm input.error { border: 1px solid #ff0000 }
            #commentForm label.error, #commentForm input.submit { margin-left: 10px; }
            .validateForm {  }
            .validateForm { }
            .validateForm label { }
            .validateForm input.error { border: 1px solid #ff0000 }
            .validateForm label.error, .validateForm input.submit { margin-left: 10px; }
        </style>
</head>

<body>
    <header id="header">
        <div class="container">
            <hgroup class="branding_main">
            <?php
                /**
                 * No logo on survey selector
                 */
                if(Route::current()->getName() == 'surveys'){
                    echo    '<a id="no-logo" href="">
                                <h2>ScaleUpNation Mirror</h2>
                            </a>';
                }else{
                    echo    '<a id="logoRM" href="/scaleupnation">
                                <h2>ScaleUpNation Mirror</h2>
                            </a>';
                }
            ?>                
            </hgroup>
        </div>
    </header>

    <div class="pusher" id="wrapper">
        <div class="content-top" id="mainCntr">
            <div class="container">

                    @yield('content')

            </div>
            <!--  \ content container / -->
            <div class="thnk_32"></div>

        </div>
        <!--  \ main container / -->

    </div>
    <!--  \ wrapper / -->

    <footer id="footer">
        <div class="container">
            <div class="f3c">
                <h3>BROUGHT TO YOU BY:</h3>
            </div>
            <div class="f3c">
                <a href="http://www.thnk.org" target="_blank">
                    <img src="{{asset('media/images/thnk_footer.png')}}">
                </a>
            </div>
            <div class="f3c">
                <div id="f3c_mail">
                    <a href="mailto:feedback@thnk.org" target="_blank">
                        <img id="f3c_img_mail"src="{{asset('media/images/email_icon_32x32.png')}}">
                        <span id="f3c_txt_mail">Feedback</span>
                    </a>
                </div>
            </div>
        </div>
    </footer>

</body>
</html>
