<?php

class table {
    public static function lijstscherm( $titel, $kolommen, $list, $component, $controller, $sort_column = 1,
                                        $action = 2, $custom_method_delete = 'delete', $custom_method_edit = 'edit', $extraParameter = ""
                                        ,$extraHTML  = "", $extraHTML2 = "", $extraIcon = "", $extraMethod = "", $extraController = "", $extraAttribute = "", $extraTarget = "") {

    	$locale = \THNK\Mirror\Localization\Facades\Localization::current();
    ?>

    <div class="lijstscherm">
        <?php
            $loggedInUser = Auth::user();
        ?>
        <table summary="<?= $titel ?>" class="sortable-onload-<?= $sort_column ?>" width="100%">
            
            <thead>
                <tr>
                    <?php foreach ($kolommen as $kolom) {
                        echo "<th scope='col'></th>";
                    }
                      if ($extraIcon  != "") {
                          $action++;
                      }
                        if ($action != 0)
                            echo "<th scope='col' colspan='$action'></th>";

                        if ($extraHTML != "")
                            echo "<th scope='col'></th>";
                    ?>
                </tr>
                <tr>
                    <?php foreach ($kolommen as $attribute => $header) {

                        if(is_array($header)) {
                            // A special type to sort
                            if ($header['type'] == 'boolean_with_empty') {
                                echo "<th scope='col' class='sortable-text'><a href='" . $attribute . "'>" . $header['naam'] . "</a></th>";
                            } else {
                                echo "<th scope='col' class='sortable-" . $header['type'] . "'><a href='" . $attribute . "'>" . $header['naam'] . "</a></th>";
                            }
                        } else {
                            echo "<th scope='col' class='sortable-text'><a href='" . $attribute . "'>" . $header . "</a></th>";
                        }

                    }
                        if ($action != 0)
                            echo "<th scope='col' colspan='$action'>Action</th>";

                        if ($extraHTML != "")
                            echo "<th scope='col'></th>";
                    ?>
                </tr>
            </thead>
        
            <tbody>
                <div class="table_body">
                    <?php
                    foreach ($list as $item) {
                        echo "<tr>";
                        foreach ($kolommen as $attribute => $header) {
                            echo "<td id='table-cols'>";
                                $userId = User::find($item->id);
                                if ($attribute == 'edit_profile') {
                                    
                                    if( $userId->hasRoleInClient(Role::USER_PARTICIPANT, $titel, $userId->id) )
                                        echo "<a href='/" . $locale->getURLPrefix() . "/test/myprofile/" . $titel . "/" . $item->id . "' title='Access participant details'><img src='".asset("media/images")."/user.png'  alt='user' class='access-user-details' /></a>";
                                    else
                                        echo "<img src='".asset("media/images")."/no-user.png'  alt='no-user' class='access-user-details' id='no-user-details' title='Not a participant' />";
                                
                                } elseif ($attribute == 'edit_client') {
                                    echo "<a href='/" . $locale->getURLPrefix() . "/clients/edit/" . $item->id . "'>" . trans('table.body.client') . "</a>";
                                } elseif ($attribute == 'test_status') {

                                    $test = Test::getLatestTestByUser($item->id, $titel);

                                    $respTests = DB::table('tests')
                                        ->select('*')
                                        ->where('test_id', '=', $test->id)
                                        ->where('teststatus_id', '=', 7)
                                        ->get();

                                    $respondents =      DB::table('respondents')
                                            ->join('tests', 'tests.respondent_id', '=', 'respondents.id')
                                            ->select('respondents.*')
                                            ->where('tests.test_id', '=', $test->id)
                                            ->orderby('respondentgroup_id')
                                            ->get();

                                    echo "<p id='table-resps'>".count($respTests)." / ".count($respondents). trans('table.body.respondents') . "</p>";

                                } elseif ($attribute == 'resend_participant_credentials') {
                                    echo "<a href='/" . $locale->getURLPrefix() . "/clients/getResendParticipantCredentials/" . $titel . "/" . $item->id . "' title='" . trans('table.body.credentials') . "'><img src='".asset("media/images")."/reset.png'  alt='user' class='access-user-details' /></a>";
                                } elseif ($attribute == 'resend_coach_credentials') {
                                    echo "<a href='/" . $locale->getURLPrefix() . "/clients/getResendCoachCredentials/" . $titel . "/" . $item->id . "' title='" . trans('table.body.credentials') . "'><img src='".asset("media/images")."/reset.png'  alt='user' class='access-user-details' /></a>";
                                } elseif ($attribute == 'participantRole') {

                                        $result1 = DB::table('users_roles')
                                        ->select('*')
                                        ->where('client_id', $titel)
                                        ->where('users_roles.user_id', $item->id)
                                        ->join('users', 'users_roles.user_id', '=', 'users.id')
                                        ->where('users_roles.role_id', '=', Role::USER_PARTICIPANT)
                                        ->get();
                                        $num_rows1 = count($result1);
                                        
                                        if ( $num_rows1 > 0 ) {
                                            echo "<input type='text' value='YES' name='participantRoleText' style='display:none;'>";
                                            echo "<input type='checkbox' name='participantRole' value='1' checked class='ajaxCheckBox'>";
                                        }else{
                                            echo "<input type='text' value='NO' name='participantRoleText' style='display:none;'>";
                                            echo "<input type='checkbox' name='participantRole' value='0' class='ajaxCheckBox'>";
                                        }                                   
                                    
                                } elseif ($attribute == 'coachRole') {
 
                                        $result2 = DB::table('users_roles')
                                        ->select('*')
                                        ->where('client_id', $titel)
                                        ->where('users_roles.user_id', $item->id)
                                        ->join('users', 'users_roles.user_id', '=', 'users.id')
                                        ->where('users_roles.role_id', '=', Role::USER_CLIENT_COACH)
                                        ->get();
                                        $num_rows2 = count($result2);

                                        if ( $num_rows2 > 0 ) {
                                            echo "<input type='text' value='YES' name='coachRoleText' style='display:none;'>";
                                            echo "<input type='checkbox' name='coachRole' value='1' checked class='ajaxCheckBox'>";
                                        }else{
                                            echo "<input type='text' value='NO' name='coachRoleText' style='display:none;'>";
                                            echo "<input type='checkbox' name='coachRole' value='0' class='ajaxCheckBox'>";
                                        }
                                                                        
                                } elseif ($attribute == 'clientRole') {

                                        $result3 = DB::table('users_roles')
                                        ->select('*')
                                        ->where('client_id', $titel)
                                        ->where('users_roles.user_id', $item->id)
                                        ->join('users', 'users_roles.user_id', '=', 'users.id')
                                        ->where('users_roles.role_id', '=', Role::USER_CLIENT_ADMIN)
                                        ->get();
                                        $num_rows3 = count($result3);

                                       if ( $num_rows3 > 0 ) {
                                            echo "<input type='text' value='YES' name='clientRoleText' style='display:none;'>";
                                            echo "<input type='checkbox' name='clientRole' value='1' checked class='ajaxCheckBox'>";
                                        }else{
                                            echo "<input type='text' value='NO' name='clientRoleText' style='display:none;'>";
                                            echo "<input type='checkbox' name='clientRole' value='0' class='ajaxCheckBox'>";
                                        }
                                   
                                } else {
                                    echo
                                        '
                                        <span class="hidden">'.$item->$attribute.'</span>
                                        <span class="hidden">'.App::environment().'</span>
                                        <span class="hidden">'.$item->id.'</span>
                                        <span class="hidden">'.$titel.'</span>
                                        <input type="text" value="'.$item->$attribute.'" name="'.$attribute.'" class="fdTableTtrTd ajaxChange" />';
                                }
                            echo "</td>";
                        }
                        echo "</tr>";
                    }
                    ?>
                </div>
            </tbody>

            <tfoot>
                <tr>
                    <?php foreach ($kolommen as $kolom) {
                        echo "<th scope='col'></th>";
                       }
                       if ($action != 0)
                            echo "<th scope='col' colspan='$action'></th>";

                        if ($extraHTML != "")
                            echo "<th scope='col'></th>";
                    ?>
                </tr>
                <tr>
                    <?php foreach ($kolommen as $kolom) {
                            echo "<th scope='col'></th>";
                        }

                       if ($action != 0)
                            echo "<th scope='col' colspan=" . $action . "></th>";

                        if ($extraHTML != "")
                            echo "<th scope='col'></th>";
                    ?>
                </tr>
            </tfoot>

        </table>
    </div>
        <script>
            $(document).ready(function () {
                $('.ajaxChange').bind('blur', function () {
                    $(this).removeClass('highlighted');
                });

                $('.ajaxChange').bind('focus', function () {
                    $(this).addClass('highlighted');
                });

                $('.ajaxChange').keypress(function (e) {
                    var key = e.which;
                    if(key == 13)  // the enter key code
                    {
                        var TH = $(this);

                        var env = TH.prev().prev().prev().text();
                        var userId = TH.prev().prev().text();
                        var clientId = TH.prev().text();
                        var typeField = TH.attr('name');
                        var thisValue = TH.val();


                        $.ajax({
                            type: "POST",
                            url: window.location.origin + "/ajax.php",
                            async: false,
                            data: { ajaxUserId: userId, ajaxThisVal: thisValue, ajaxTypeField: typeField, ajaxEnv: env, ajaxClientId: clientId },
                            success: function(data) {
                                console.log(data);
                            },
                            error: function(xhr) { // if error occured
                               alert("@lang('table.ajaxError') " + xhr);
                            },
                            dataType: 'text'
                        });

                        TH.blur();
                    }
                });

            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.ajaxCheckBox').change(function(){

                    var TH = $(this);

                    var chkval = 0;
                    if(TH.attr('checked')){
                        chkval  = 1;
                    }

                    var env = TH.parent().parent().children().eq(0).children('span').eq(1).text();
                    var userId = TH.parent().parent().children().eq(0).children('span').eq(2).text();
                    var clientId = TH.parent().parent().children().eq(0).children('span').eq(3).text();
                    var role = TH.attr('name');

                    $.ajax({
                        type: "POST",
                        url: window.location.origin + "/ajax.php",
                        async: false,
                        data:{ checkboxvalue: chkval, ajaxUserId: userId, ajaxEnv: env, ajaxRole: role , ajaxClientId: clientId},
                        success:function(returndata){
                            console.log(returndata);
                        },
                        error:function(errordata){
                            alert("Error occured. Please try again " + errordata);
                        },
                        dataType: 'text'
                    });
                    location.reload();
                });
            });
        </script>

        <script type="text/javascript">
            $( window ).load(function() {
               var numCols = $(".lijstscherm table thead tr:nth-child(2) th").length;

               if(numCols == 9){
                $('.lijstscherm table thead tr:nth-child(2) th:nth-child(7)').append( "<img src='/media/images/info.png' alt='info' class='info-help help-icon-table' id='manage-users-info' title='@lang('table.roles.participant')'>" );
                $('.lijstscherm table thead tr:nth-child(2) th:nth-child(8)').append( "<img src='/media/images/info.png' alt='info' class='info-help help-icon-table' id='manage-users-info' title='T@lang('table.roles.coach')'>" );
                $('.lijstscherm table thead tr:nth-child(2) th:nth-child(9)').append( "<img src='/media/images/info.png' alt='info' class='info-help help-icon-table' id='manage-users-info' title='@lang('table.roles.admin')'>" );
               }else if(numCols == 7){
                $('.lijstscherm table thead tr:nth-child(2) th:nth-child(5)').append( "<img src='/media/images/info.png' alt='info' class='info-help help-icon-table' id='manage-users-info' title='@lang('table.roles.participant')'>" );
                $('.lijstscherm table thead tr:nth-child(2) th:nth-child(6)').append( "<img src='/media/images/info.png' alt='info' class='info-help help-icon-table' id='manage-users-info' title='@lang('table.roles.coach')'>" );
                $('.lijstscherm table thead tr:nth-child(2) th:nth-child(7)').append( "<img src='/media/images/info.png' alt='info' class='info-help help-icon-table' id='manage-users-info' title='@lang('table.roles.admin')'>" );
               }
                
                // Tooltip only Text
                $('.help-icon-table').hover(function(){
                        // Hover over code
                        var title = $(this).attr('title');
                        $(this).data('tipText', title).removeAttr('title');
                        $('<p class="tooltip"></p>')
                        .text(title)
                        .appendTo('body')
                        .fadeIn('slow');
                }, function() {
                        // Hover out code
                        $(this).attr('title', $(this).data('tipText'));
                        $('.tooltip').remove();
                }).mousemove(function(e) {
                        var mousex = e.pageX + 20; //Get X coordinates
                        var mousey = e.pageY + 10; //Get Y coordinates
                        $('.tooltip')
                        .css({ top: mousey, left: mousex })
                });
            });
        </script>
    <?php
    }
}
?>
