<?php

use Illuminate\Support\Collection;
use THNK\Mirror\Localization\Locale;

if ( ! function_exists('lists'))
{
	/**
	 * Return an associative array with the id as the key and the $key as the value.
	 *
	 * @param array|Collection $entities
	 * @param string $key
	 *
	 * @return array
	 */
	function lists($entities, $key)
	{
		if ($entities instanceof Collection)
		{
			$entities = $entities->all();
		}

		$list = [];

		foreach ($entities as $entity)
		{
			$list[ $entity->id ] = $entity->$key;
		}

		return $list;
	}
}

if ( ! function_exists('translate_url'))
{
	/**
	 * @param string $url
	 * @param null|string|\THNK\Mirror\Localization\Locale $locale
	 *
	 * @return string
	 */
	function translate_url($url, $locale = null)
	{
		if ($locale && is_string($locale))
		{
			$locale = new Locale($locale);
		}

		/** @var \THNK\Mirror\Localization\Routes\RouteTranslator $urlTranslator */
		$urlTranslator = app('THNK\Mirror\Localization\Routes\RouteTranslator');

		return $urlTranslator->translate($url, $locale);
	}
}