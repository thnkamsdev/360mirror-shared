<?php

function setting($pdf){
    // convert TTF font to TCPDF format and store it on the fonts folder
    $GLOBALS['leicht'] = TCPDF_FONTS::addTTFfont('media/fonts/NeubauGrotesk-35Leicht.ttf', 'TrueTypeUnicode', '', 96);
    $GLOBALS['mager'] = TCPDF_FONTS::addTTFfont('media/fonts/NeubauGrotesk-45Mager.ttf', 'TrueTypeUnicode', '', 96);
    $GLOBALS['normal'] = TCPDF_FONTS::addTTFfont('media/fonts/NeubauGrotesk-55Normal.ttf', 'TrueTypeUnicode', '', 96);
    $GLOBALS['halbfett'] = TCPDF_FONTS::addTTFfont('media/fonts/NeubauGrotesk-65Halbfett.ttf', 'TrueTypeUnicode', '', 96);
    $GLOBALS['fett'] = TCPDF_FONTS::addTTFfont('media/fonts/NeubauGrotesk-75Fett.ttf', 'TrueTypeUnicode', '', 96);
    $GLOBALS['schwer'] = TCPDF_FONTS::addTTFfont('media/fonts/NeubauGrotesk-85Schwer.ttf', 'TrueTypeUnicode', '', 96);
    $GLOBALS['arnopro'] = TCPDF_FONTS::addTTFfont('media/fonts/arno.ttf', 'TrueTypeUnicode', '', 96);
    $GLOBALS['dinnetje'] = TCPDF_FONTS::addTTFfont('media/fonts/din.ttf', 'TrueTypeUnicode', '', 96);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('THNK');
    $pdf->SetTitle('THNK 360 Mirror Tool');

    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    //set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //set some language-dependent strings
    //$pdf->setLanguageArray($l);

    //set Globals
    $GLOBALS['textWidth'] = 80;
    $GLOBALS['textHeight'] = 10;
    $GLOBALS['numberPage'] = 2;
    $GLOBALS['titleSize'] = 12;
    $GLOBALS['titlePageSize'] = 30;
    $GLOBALS['descSize'] = 11;
    $GLOBALS['valueSize'] = 10;
    $GLOBALS['stDevSize'] = 8;
    $GLOBALS['rectTextX'] = 192.5;
    $GLOBALS['fontSizeTitle'] = 30;
    $GLOBALS['titleY'] = 26;
    $GLOBALS['colorText'] = array(0 => 0, 1 => 0, 2 => 0);
    $GLOBALS['colorDesc'] = array(0 => 147, 1 => 149, 2 => 152);
    $GLOBALS['colorMainRect'] = $GLOBALS['respLegend'][0]['colorBar'];
    $GLOBALS['rectX'] = 88.6;
    $GLOBALS['rectHeight'] = 6;
    $GLOBALS['TextX'] = 6;
    $GLOBALS['borderColor'] = array(0 => 257, 1 => 128, 2 => 16);
    $GLOBALS['numCategories'] = 4;
    $imageSize = getimagesize(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName']);

    $GLOBALS['imageWidth'] = $imageSize[0];
    $GLOBALS['imageHeight'] = $imageSize[1];
    // set font
    $pdf->SetFont('times', 'BI', 20,'',false);
}

/**
 * @param $pdf
 * @param $test
 * @return array
 *
 * @param $cred array with participant name [0] and surname [1]
 */
function firstPages($pdf,$cred,$m,$pathLayout,$client_id,$user_id)
{
    //COVER
    $pdf->AddPage();

    $pdf->setSourceFile($pathLayout);
    $template = 1;
    $tplIdx = $pdf->importPage($template);
    $pdf->useTemplate($tplIdx);

    if($GLOBALS['imageHeight'] > 500){
        $newWidth40 = ($GLOBALS['imageWidth']*40)/$GLOBALS['imageHeight'];
        //CLIENT LOGO
        $pdf->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth40,40,'','','M',false,0,'',false);
    }else{
        $newWidth = ($GLOBALS['imageWidth']*30)/$GLOBALS['imageHeight'];
        //CLIENT LOGO
        $pdf->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,30,'','','M',false,0,'',false);
    }

    //PARTICIPANT NAME
    $pdf->SetFont($GLOBALS['mager'], '', 37,'',false);
    $pdf->SetXY(11, 192);
    $pdf->MultiCell(250, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'L', 0, 0, '', '', true, 0);

    //MONTH
    $pdf->SetXY(15, 222);
    $pdf->SetTextColor(148, 150, 153);
    $pdf->SetFont($GLOBALS['halbfett'], '', 20,'',false);
    $pdf->MultiCell(20, 0, strtoupper(date("M",mktime(0, 0, 0, $m+1, 0, 0))), 0, 'L', 0, 0, '', '', true, 0); //USE TIMESTAMP FROM TABLE???

    //YEAR
    $pdf->SetXY(26, 238);
    $pdf->MultiCell(20, 0, strtoupper(date("Y", strtotime('2014'))), 0, 'L', 0, 0, '', '', true, 0); //USE TIMESTAMP FROM TABLE???
    $pdf->SetTextColor(0, 0, 0);

    $client = User::find($client_id);
    $introduction = $client->getIntroPDF($client->id);
    if($introduction){
        $pdf->AddPage();

        $pdf->setSourceFile($pathLayout);
        $template = 2;
        $tplIdx = $pdf->importPage($template);
        $pdf->useTemplate($tplIdx);
        
        $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];

        //CLIENT LOGO
        $pdf->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);
        
        //PARTICIPANT NAME
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetXY(135, 288.2);
        $pdf->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

        //PAGE NUMBER
        $pdf->SetXY(197.3, 280.5);//1 DIGIT ONLY
        $pdf->MultiCell(5, 5, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
        $GLOBALS['numberPage']++;
    }
}

/**
 * @param $pdfPointer
 * @param $r
 * @param $g
 * @param $b
 */
function setRectColor($pdfPointer, $r,$g,$b){
    $pdfPointer->SetDrawColor($r, $g, $b);
    $pdfPointer->SetFillColor($r, $g, $b);
}

/**
 * @param $pdfPointer
 * @param $Y
 * @param $width
 * @param $TY
 * @param $text
 * @param $arrayUsed
 */
function fillQuestionRect($pdfPointer,$Y,$width,$TY,$text){
    //PERSONAL SCORE RECT
    setRectColor($pdfPointer, $GLOBALS['colorMainRect'][0],$GLOBALS['colorMainRect'][1],$GLOBALS['colorMainRect'][2]);
    $pdfPointer->Rect($GLOBALS['rectX'], $Y, $width, $GLOBALS['rectHeight'], 'DF', $GLOBALS['borderColor']);

    //PERSONAL SCORE NUMBER
    $pdfPointer->SetTextColor($GLOBALS['colorText'][0], $GLOBALS['colorText'][1],$GLOBALS['colorText'][2]);
    $pdfPointer->SetFont($GLOBALS['fett'], '', $GLOBALS['valueSize'],'',false);
    $pdfPointer->Text($GLOBALS['rectTextX'], $Y+1.6, number_format((double)$text, 2));
}

/**
 * @param $pdfPointer
 * @param $TY
 * @param $text
 * @param $subtext
 */
function fillText($pdfPointer,$TY,$text,$subtext){
    //QUESTION TITLE
    $pdfPointer->SetTextColor($GLOBALS['colorText'][0], $GLOBALS['colorText'][1],$GLOBALS['colorText'][2]);
    $pdfPointer->SetFont($GLOBALS['halbfett'], '', $GLOBALS['titleSize'],'',false);
    $pdfPointer->SetXY($GLOBALS['TextX'],$TY);
    $pdfPointer->setCellHeightRatio(1.25);
    $pdfPointer->setFontSpacing(0);
    $pdfPointer->MultiCell($GLOBALS['textWidth'], $GLOBALS['textHeight'], strtoupper($text), 0, 'L', 0, 0, '', '', true, 0);

    $try = $pdfPointer->getStringHeight($GLOBALS['textWidth'],strtoupper($text));

    //QUESTION DESCRIPTION
    $pdfPointer->SetTextColor($GLOBALS['colorDesc'][0],$GLOBALS['colorDesc'][1],$GLOBALS['colorDesc'][2]);
    $pdfPointer->SetXY($GLOBALS['TextX'],$TY+$try+1.5);
    $pdfPointer->setFontSpacing(0);
    $pdfPointer->setCellHeightRatio(1.05);
    $pdfPointer->SetFont($GLOBALS['arnopro'], '', $GLOBALS['descSize'],'',false);
    $pdfPointer->MultiCell($GLOBALS['textWidth']+0.75, $GLOBALS['textHeight']+30, $subtext, 0, 'L', 0, 0, '', '', true, 0);

    $try2 = $pdfPointer->getStringHeight($GLOBALS['textWidth'],$subtext);
}

/**
 * @param $pdfPointer
 * @param $title
 * @param $desc
 * @return int
 */
function fillTitle($pdfPointer,$title,$desc){
    //CATEGORY TITLE
    $pdfPointer->SetTextColor($GLOBALS['colorText'][0], $GLOBALS['colorText'][1],$GLOBALS['colorText'][2]);
    $pdfPointer->SetFont($GLOBALS['leicht'], '', $GLOBALS['titlePageSize'],'',false);
    $pdfPointer->SetXY($GLOBALS['TextX'],$GLOBALS['titleY']);
    $pdfPointer->setCellHeightRatio(1.25);
    $pdfPointer->setFontSpacing(0);
    $pdfPointer->MultiCell(188, 10, strtoupper($title), 0, 'L', 0, 0, '', '', true, 0);
    $pdfPointer->SetFont($GLOBALS['leicht'], '', $GLOBALS['titlePageSize'],'',false);
    $pdfPointer->setCellHeightRatio(1.25);
    $pdfPointer->setFontSpacing(0);
    $heightTitle = $pdfPointer->getStringHeight(188,strtoupper($title));

    //CATEGORY DESCRIPTION
    if($desc != false){
        $pdfPointer->Image(__DIR__.'/../../public_html/media/images/arrow0.png',$GLOBALS['TextX'] + 5,$GLOBALS['titleY']+$heightTitle+2,2,3,'','','M',false,0,'',false);
        $pdfPointer->SetTextColor($GLOBALS['colorDesc'][0], $GLOBALS['colorDesc'][1],$GLOBALS['colorDesc'][2]);
        $pdfPointer->SetFont($GLOBALS['arnopro'], '', $GLOBALS['descSize'],'',false);
        $pdfPointer->SetXY($GLOBALS['TextX']+15.1,$GLOBALS['titleY']+$heightTitle+2);
        $pdfPointer->setCellHeightRatio(1.25);
        $pdfPointer->setFontSpacing(0);
        $pdfPointer->MultiCell(173, 27,$desc, 0, 'L', 0, 0, '', '', true, 0);
    }
    $pdfPointer->SetFont($GLOBALS['arnopro'], '', $GLOBALS['valueSize'],'',false);
    $pdfPointer->setCellHeightRatio(1.25);
    $pdfPointer->setFontSpacing(0);
    if($desc != false)
        $heightDesc = $pdfPointer->getStringHeight(173,$desc);
    else
        $heightDesc = 0;

    return ($heightDesc+$heightTitle+$GLOBALS['titleY']);
}

/**
 * @param $pdfPointer
 * @param $pathTemplate
 * @param $page
 * @param $text
 * @param $title
 * @param $desc
 */
function generateNewPage($pdfPointer,$pathTemplate,$page,$personal,$title,$desc,$cred){
    $pdfPointer->AddPage();
    $pathTemplate = str_replace('full_','self_',$pathTemplate);
    $pdfPointer->setSourceFile($pathTemplate);
    $tplIdx = $pdfPointer->importPage(1);
    $pdfPointer->useTemplate($tplIdx);

    $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];

    //CLIENT LOGO
    $pdfPointer->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);

    //PAGE NUMBER
    setRectColor($pdfPointer, $GLOBALS['colorMainRect'][0],$GLOBALS['colorMainRect'][1],$GLOBALS['colorMainRect'][2]);
    $pdfPointer->Rect(88.8, 268, 3.7, 3.7, 'DF');
    $pdfPointer->SetFont($GLOBALS['mager'], '', 11,'',false);
    $pdfPointer->SetXY(193.5, 280.5);//2 DIGITS
    $pdfPointer->MultiCell(10, 10, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
    $GLOBALS['numberPage']++;

    //PARTICIPANT NAME
    $pdfPointer->SetTextColor(0, 0, 0);
    $pdfPointer->SetFont($GLOBALS['mager'], '', 11,'',false);
    $pdfPointer->SetAutoPageBreak(false);
    $pdfPointer->SetXY(135, 288.2);
    $pdfPointer->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

    $GLOBALS['pos'] = (fillTitle($pdfPointer,$title,$desc)) +7;
    $GLOBALS['pageSize'] = 255;
    $GLOBALS['pos'] = createHeader($pdfPointer,$personal);
}

/**
 * @param $pdfPointer
 * @param $numQuestions
 * @param $text
 * @return array
 */
function tempArray($pdfPointer,$numQuestions,$text){
    $temp = [];
    for($i=0;$i<=$numQuestions-1;$i++){
        $temp[$i]['write'] = false;
        $temp[$i]['height'] = calcHeightQuestion($pdfPointer,$text[$i]['title'],$text[$i]['desc']);
        if($temp[$i]['height'] < $text[$i]['heightBars'])
            $temp[$i]['height'] = $text[$i]['heightBars'];
        $temp[$i]['height'] += 5;
    }
    return $temp;
}

/**
 * @param $numQuestions
 * @return array
 */
function takeQuestion($questions){
    $text = [];
    for($i=0;$i<=count($questions)-1;$i++){
        $text[$i]['title'] = $questions[$i]['name'];
        $text[$i]['desc'] = $questions[$i]['explanation'];
        $text[$i]['numBars'] = 1;//non si sa...se serve
        $text[$i]['heightBars'] = ($text[$i]['numBars'] * 6) + $text[$i]['numBars'] + 5;
        $text[$i]['individualValue'] = $questions[$i]['personal_score'];
    }
    return $text;
}

/**
 * @param $numCategories
 * @return array
 */
function takeCategoriesForOverall($pdfArray){
    $category = [];
    for($i=0;$i<count($pdfArray);$i++){
        $category[$i]['title'] = $pdfArray[$i]['name'];
        $category[$i]['description'] =  $pdfArray[$i]['explanation'];
        $category[$i]['average'] = $pdfArray[$i]['personal_score'];
    }
    return $category;
}

/**
 * @param $pdfPointer
 * @param $text
 * @param $subtext
 * @return mixed
 */
function calcHeight($pdfPointer,$text,$fontT,$subtext,$fontD){
    $fontT = [];
    $fontT[0] = 1.25;
    $fontT[1] = 0.1;
    $fontD = [];
    $fontD[0] = 0.17;
    $fontD[1] = 0.75;

    $pdfPointer->SetFont($GLOBALS['halbfett'], '', $GLOBALS['titlePageSize'],'',false);
    $pdfPointer->setCellHeightRatio($fontT[0]);//1.25
    $pdfPointer->setFontSpacing($fontT[1]);//0.1
    $heightTitle = $pdfPointer->getStringHeight(80,strtoupper($text));
    $pdfPointer->setFontSpacing($fontD[0]);//0.17
    $pdfPointer->setCellHeightRatio($fontD[1]);//0.75
    $pdfPointer->SetFont($GLOBALS['arnopro'], '', $GLOBALS['descSize'],'',false);
    $heightDesc = $pdfPointer->getStringHeight(80.75,$subtext);
    return ($heightDesc+$heightTitle);
}

function calcHeightQuestion($pdfPointer,$text,$subtext){
    $pdfPointer->SetFont($GLOBALS['halbfett'], '', $GLOBALS['titleSize'],'',false);
    $pdfPointer->setCellHeightRatio(1.25);
    $pdfPointer->setFontSpacing(0);
    $heightTitle = $pdfPointer->getStringHeight(80,strtoupper($text));
    $pdfPointer->setFontSpacing(0);
    $pdfPointer->setCellHeightRatio(1.05);
    $pdfPointer->SetFont($GLOBALS['arnopro'], '', $GLOBALS['descSize'],'',false);
    $heightDesc = $pdfPointer->getStringHeight(80.75,$subtext);
    return ($heightDesc+$heightTitle);
}



/**
 * @param $pdfPointer
 * @param $text
 * @param $page
 * @return int
 */
function createHeader($pdfPointer,$personal){
    //GREY BOX
    for($x=1;$x<6;$x++){
        setRectColor($pdfPointer, 236, 235, 234);
        $pdfPointer->Rect((68.35+(($x*20)+$x/6)), $GLOBALS['pos'], 19.75,$GLOBALS['pageSize']- $GLOBALS['pos'], 'DF', $GLOBALS['borderColor']);
        $pdfPointer->SetTextColor($GLOBALS['colorText'][0], $GLOBALS['colorText'][1],$GLOBALS['colorText'][2]);
        $pdfPointer->SetFont($GLOBALS['fett'], '', 10,'',false);
        $pdfPointer->Text(77+(($x*20)+$x/6), $GLOBALS['pos']+2.5, $x);
    }

    $GLOBALS['pos'] += 12.5;

    //OVERALL SCORES
    $pdfPointer->SetFont($GLOBALS['schwer'], '', 14,'',false);
    $pdfPointer->SetXY(0,$GLOBALS['pos']);
    $pdfPointer->MultiCell(50, 10,trans('test.pdf.overall_score'), 0, 'R', 0, 0, '', '', true, 0);
    $pdfPointer->SetTextColor(0,0,0);

    //PERSONAL SCORE RECT
    setRectColor($pdfPointer, $GLOBALS['colorMainRect'][0],$GLOBALS['colorMainRect'][1],$GLOBALS['colorMainRect'][2]);
    $pdfPointer->Rect($GLOBALS['rectX'], $GLOBALS['pos'], $personal*20, $GLOBALS['rectHeight'], 'DF', $GLOBALS['borderColor']);

    //PERSONAL SCORE NUMBER
    $pdfPointer->SetTextColor($GLOBALS['colorText'][0], $GLOBALS['colorText'][1],$GLOBALS['colorText'][2]);
    $pdfPointer->SetFont($GLOBALS['fett'], '', $GLOBALS['valueSize'],'',false);
    $pdfPointer->Text($GLOBALS['rectTextX'], $GLOBALS['pos']+0.9, number_format((double)$personal, 2));

    $GLOBALS['pos'] += 8.5;
    $count = 0;

    //HR
    setRectColor($pdfPointer, $GLOBALS['colorDesc'][0],$GLOBALS['colorDesc'][1],$GLOBALS['colorDesc'][2]);
    $pdfPointer->Line($GLOBALS['TextX'],$GLOBALS['pos']+2.5,$GLOBALS['rectTextX']+12.5,$GLOBALS['pos']+2.5);

    return ($GLOBALS['pos']+7.5);
}

/**
 * @param $pdfPointer
 * @param $pathTemplate
 * @param $title
 * @param $desc
 * @param $questions
 * @param $comment
 * @internal param $numQuestionssetRectColor($pdfPointer, $GLOBALS['colorDesc'][0],$GLOBALS['colorDesc'][1],$GLOBALS['colorDesc'][2]);
 */
function createPage($pdfPointer,$pathTemplate,$title,$desc,$questions,$comment,$personal_score,$cred,$comments_title){
    $border_style_blue  = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0));
    $border_style_white  = array();
    $border_style_red   = array('all' => array('width' => 0, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0));
    $text = takeQuestion($questions);
    $temp = tempArray($pdfPointer,count($questions),$text);
    generateNewPage($pdfPointer,$pathTemplate,5,$personal_score,$title,$desc,$cred);
    for($i=0;$i<=count($questions)-1;$i++){
        fillText($pdfPointer,$GLOBALS['pos'],$text[$i]['title'],$text[$i]['desc']);
        fillQuestionRect($pdfPointer,$GLOBALS['pos'],$text[$i]['individualValue']*20,$GLOBALS['pos'],$text[$i]['individualValue']);
        $GLOBALS['pos'] += ($temp[$i]['height']);
        if($i == (count($questions)-1)){
            $totCommentHeight = calcHeight($pdfPointer,'','',$comment,'');
            if(($totCommentHeight/300) > 1){
                $divCom = round($totCommentHeight/300);
                $totCommentChar = strlen($comment);
                $commentArray = str_split($comment,$totCommentChar/$divCom);
                for($s = 0;$s<$divCom;$s++){
                    $pdfPointer->AddPage();
                    $pdfPointer->setSourceFile($pathTemplate);
                    $tplIdx = $pdfPointer->importPage(4);
                    $pdfPointer->useTemplate($tplIdx);

                    //COMMENTS PAGE
                    $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];
                    //CLIENT LOGO
                    $pdfPointer->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);

                    fillTitle($pdfPointer,$title,false);
                    $addY = 0;
                    if(strlen($title) > 30)
                        $addY = 15;
                    //IN ADDITION
                    $pdfPointer->Image(__DIR__.'/../../public_html/media/images/arrow0.png',$GLOBALS['TextX'] + 5,40+$addY,2,3,'','','M',false,0,'',false);
                    $pdfPointer->SetTextColor(0, 0, 0);
                    $pdfPointer->SetAutoPageBreak(false);
                    $pdfPointer->SetXY(7+15.1,40+$addY);
                    //PRINTING COMMENTS TITLE IF CUSTOM
                    if($comments_title != ''){
                        $pdfPointer->SetFont($GLOBALS['arnopro'], '', $GLOBALS['descSize'],'',false);
                        $pdfPointer->MultiCell(0, 0, $comments_title, 0, 'L', 0, 0, '', '', true, 0);
                    }else{
                        $pdfPointer->SetFont($GLOBALS['leicht'], '', $GLOBALS['descSize'],'',false);
                        $pdfPointer->MultiCell(0, 0, trans('test.pdf.in_addition'), 0, 'L', 0, 0, '', '', true, 0);
                    }

                    //PERSONAL COMMENT
                    $pdfPointer->Image(__DIR__.'/../../public_html/media/images/arrow0.png',$GLOBALS['TextX'] + 5,$GLOBALS['titleY']+34.8+$addY,2,3,'','','M',false,0,'',false);
                    $pdfPointer->SetTextColor(0, 0, 0);
                    $pdfPointer->SetFont($GLOBALS['arnopro'], '', $GLOBALS['descSize'],'',false);
                    $pdfPointer->SetAutoPageBreak(false);
                    $pdfPointer->SetXY(7+15.1,60);
                    $pdfPointer->MultiCell(0, 0, $commentArray[$s], 0, 'L', 0, 0, '', '', true, 0);
                }
            }else{
                $pdfPointer->AddPage();
                $pdfPointer->setSourceFile($pathTemplate);
                $tplIdx = $pdfPointer->importPage(4);
                $pdfPointer->useTemplate($tplIdx);

                //COMMENTS PAGE
                $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];
                //CLIENT LOGO
                $pdfPointer->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);

                fillTitle($pdfPointer,$title,false);
                $addY = 0;
                if(strlen($title) > 30)
                    $addY = 15;
                //IN ADDITION
                $pdfPointer->Image(__DIR__.'/../../public_html/media/images/arrow0.png',$GLOBALS['TextX'] + 5,40+$addY,2,3,'','','M',false,0,'',false);
                $pdfPointer->SetTextColor(0, 0, 0);
                $pdfPointer->SetAutoPageBreak(false);
                $pdfPointer->SetXY(7+15.1,40+$addY);
                //PRINTING COMMENTS TITLE IF CUSTOM
                if($comments_title != ''){
                    $pdfPointer->SetFont($GLOBALS['arnopro'], '', $GLOBALS['descSize'],'',false);
                    $pdfPointer->MultiCell(0, 0, $comments_title, 0, 'L', 0, 0, '', '', true, 0);
                }else{
                    $pdfPointer->SetFont($GLOBALS['leicht'], '', $GLOBALS['descSize'],'',false);
                    $pdfPointer->MultiCell(0, 0, trans('test.pdf.in_addition'), 0, 'L', 0, 0, '', '', true, 0);
                }

                //PERSONAL COMMENT
                $pdfPointer->Image(__DIR__.'/../../public_html/media/images/arrow0.png',$GLOBALS['TextX'] + 5,$GLOBALS['titleY']+34.8+$addY,2,3,'','','M',false,0,'',false);
                $pdfPointer->SetTextColor(0, 0, 0);
                $pdfPointer->SetFont($GLOBALS['arnopro'], '', $GLOBALS['descSize'],'',false);
                $pdfPointer->SetAutoPageBreak(false);
                $pdfPointer->SetXY(7+15.1,60+$addY);
                $pdfPointer->MultiCell(0, 0, $comment, 0, 'L', 0, 0, '', '', true, 0);
            }



            //PAGE NUMBER
            $pdfPointer->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdfPointer->SetXY(193.5, 280.5);//2 DIGITS
            $pdfPointer->MultiCell(10, 10, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
            $GLOBALS['numberPage']++;

            //PARTICIPANT NAME
            $pdfPointer->SetTextColor(0, 0, 0);
            $pdfPointer->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdfPointer->SetAutoPageBreak(false);
            $pdfPointer->SetXY(135,288.2);
            $pdfPointer->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

        }else{
            $tot = $GLOBALS['pos'] + $temp[$i+1]['height'];
            if($tot > $GLOBALS['pageSize']){
                generateNewPage($pdfPointer,$pathTemplate,5,$personal_score,$title,false,$cred);
            }
        }
    }
}

$rectRed = array(0 => 257, 1 => 128, 2 => 16);

/**
 * @param $pdf
 * @param $categories
 * @internal param $test
 * @return array
 */
function overallPage($pdf,$pdfArray,$cred,$pathLayoutOv)
{
// add the OVERALL OUTCOMES
    $pdf->AddPage();

    $pdf->setSourceFile($pathLayoutOv);
    $template = 1;
    $tplIdx = $pdf->importPage($template);
    $pdf->useTemplate($tplIdx);

    $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];
    //CLIENT LOGO
    $pdf->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);

    //PAGE TITLE
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetFont($GLOBALS['leicht'], '', $GLOBALS['titlePageSize'],'',false);
    $pdf->SetAutoPageBreak(false);
    $pdf->SetXY(7, 25);
    $pdf->MultiCell(0, 0, trans('test.pdf.overall_outcomes'), 0, 'L', 0, 0, '', '', true, 0);

    //PARTICIPANT NAME
    $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
    $pdf->SetXY(135, 288.2);
    $pdf->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

    //PAGE NUMBER
    $pdf->SetXY(197.3, 280.5);//1 DIGIT ONLY
    $pdf->MultiCell(5, 5, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
    $GLOBALS['numberPage']++;

    //GREY BOX
    for ($x = 1; $x < 6; $x++) {
        setRectColor($pdf, 236, 235, 234);
        $pdf->Rect((68.35 + (($x * 20) + $x / 6)), 43, 19.75, 222.5, 'DF', $GLOBALS['borderColor']);
        $pdf->SetTextColor($GLOBALS['colorText'][0], $GLOBALS['colorText'][1], $GLOBALS['colorText'][2]);
        $pdf->SetFont($GLOBALS['fett'], '', 10,'',false);
        $pdf->Text(77 + (($x * 19.75) + $x / 6), 45.5, $x);
    }

    //YOUR SCORE BOX
    setRectColor($pdf, $GLOBALS['colorMainRect'][0],$GLOBALS['colorMainRect'][1],$GLOBALS['colorMainRect'][2]);
    $pdf->Rect(88.8, 268, 3.7, 3.7, 'DF');

    $cat = takeCategoriesForOverall($pdfArray);

    $startY = 56;
    foreach ($cat as $x) {
        if (256 < $startY) {
            $pdf->AddPage();

            $pdf->setSourceFile($pathLayoutOv);
            $template = 3;
            $tplIdx = $pdf->importPage($template);
            $pdf->useTemplate($tplIdx);

            //PARTICIPANT NAME
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdf->SetAutoPageBreak(false);
            $pdf->SetXY(135, 288.2);
            $pdf->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

            //PAGE NUMBER
            $pdf->SetXY(197.3, 280.5);//1 DIGIT ONLY
            $pdf->MultiCell(5, 5, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
            $GLOBALS['numberPage']++;
            $startY = 56;
        }

        $count = 0;

        //TITLE CATEGORY
        $pdf->SetTextColor($GLOBALS['colorText'][0], $GLOBALS['colorText'][1], $GLOBALS['colorText'][2]);
        $pdf->SetFont($GLOBALS['halbfett'], '', $GLOBALS['titleSize'],'',false);
        $pdf->setCellHeightRatio(1.25);
        $pdf->setFontSpacing(0);
        $pdf->SetXY(7, $startY-1.2);
        $pdf->MultiCell(75, 20, strtoupper($x['title']), 0, 'L', 0, 0, '', '', true, 0);

        //PERSONAL SCORE RECT
        setRectColor($pdf, $GLOBALS['colorMainRect'][0], $GLOBALS['colorMainRect'][1], $GLOBALS['colorMainRect'][2]);
        $pdf->Rect($GLOBALS['rectX'], $startY, $x['average'] * 20, $GLOBALS['rectHeight'], 'DF', $GLOBALS['borderColor']);

        //PERSONAL SCORE NUMBER
        $pdf->SetTextColor($GLOBALS['colorText'][0], $GLOBALS['colorText'][1], $GLOBALS['colorText'][2]);
        $pdf->SetFont($GLOBALS['fett'], '', $GLOBALS['valueSize'],'',false);
        $pdf->Text($GLOBALS['rectTextX'], $startY+0.9, number_format((double)$x['average'], 2));

        $startY += calcHeight($pdf,'','',$x['title'],'');
    }
}

/**
 * @param $pdf
 * @param $test
 */
function lastPages($pdf,$cred,$pathLayout,$client_id,$user_id)
{
    $client = User::find($client_id);
    $lastPage = $client->getLastPDF($client->id);
    if($lastPage){
        //SCHEME
        $pdf->AddPage();

        $pdf->setSourceFile($pathLayout);
        $template = 5;
        $tplIdx = $pdf->importPage($template);
        $pdf->useTemplate($tplIdx);

        $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];

        //CLIENT LOGO
        $pdf->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);

        //PARTICIPANT NAME
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetXY(135, 288.2);
        $pdf->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

        //PAGE NUMBER
        $pdf->SetXY(193.5, 280.5);//2 DIGITS
        $pdf->MultiCell(10, 10, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
        $GLOBALS['numberPage']++;
    }

    $backPage = $client->getBackPDF($client->id);
    $backPageEndeavor = $client->getBackPDFEndeavor($client->id);
    if($backPage){
        //BACK COVER
        $pdf->AddPage();

        $pdf->setSourceFile($pathLayout);
        $template = 6;
        $tplIdx = $pdf->importPage($template);
        $pdf->useTemplate($tplIdx);

        $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];

        //CLIENT LOGO
        $pdf->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);

        //PARTICIPANT NAME
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetXY(135, 288.2);
        $pdf->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

        //PAGE NUMBER
        $pdf->SetXY(193.5, 280.5);//2 DIGITS
        $pdf->MultiCell(10, 10, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
        $GLOBALS['numberPage']++;

        if($backPageEndeavor){
            $endeavor0 = Endeavor::getEndeavorByUser($user_id);
            if ($endeavor0->isEmpty()) {
                return;
            }

            if( empty( $endeavor0 ) ){
                //THE USER DIDN'T FILL THE ENDEAVOR PROJECT DETAILS YET
            }else{
                $endeavor = Endeavor::find($endeavor0[0]->id);
                //PROJECT NAME
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
                $pdf->SetAutoPageBreak(false);
                $pdf->SetXY(5, 50);
                $pdf->MultiCell(0, 0, strtoupper($endeavor->name), 0, 'L', 0, 0, '', '', true, 0);

                //PROJECT TYPE
                $pdf->setCellHeightRatio(1.25);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
                $pdf->SetAutoPageBreak(false);
                switch ($endeavor->type_id) {
                    case 1:
                        $pdf->SetXY(7.2, 72.5);
                        break;
                    case 2:
                        $pdf->SetXY(70.9, 72.5);
                        break;
                    case 3:
                        $pdf->SetXY(7.2, 84);
                        break;
                    case 4:
                        $pdf->SetXY(70.9, 84);
                        break;
                }                
                $pdf->MultiCell(0, 0, strtoupper('x'), 0, 'L', 0, 0, '', '', true, 0);
                if($endeavor->type_id == 4){
                    //OTHER TYPE
                    $pdf->SetTextColor(0, 0, 0);
                    $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
                    $pdf->SetAutoPageBreak(false);
                    $pdf->SetXY(128, 84);
                    $pdf->MultiCell(0, 0, strtoupper($endeavor->other_type), 0, 'L', 0, 0, '', '', true, 0);
                }

                //PROJECT PEOPLE
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
                $pdf->SetAutoPageBreak(false);
                $pdf->SetXY(5, 109);
                $pdf->MultiCell(0, 0, strtoupper($endeavor->people), 0, 'L', 0, 0, '', '', true, 0);

                //PROJECT STAGE
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
                $pdf->SetAutoPageBreak(false);
                switch ($endeavor->stage_id) {
                    case 1:
                        $pdf->SetXY(7.2, 131);
                        break;
                    case 2:
                        $pdf->SetXY(7.2, 142.5);
                        break;
                    case 3:
                        $pdf->SetXY(7.2, 154);
                        break;
                    case 4:
                        $pdf->SetXY(7.2, 169);
                        break;
                }
                $pdf->MultiCell(0, 0, strtoupper('x'), 0, 'L', 0, 0, '', '', true, 0);
            }            
        }

        if($backPageEndeavor){

            //PROJECT DESC1
            $pdf->AddPage();

            $pdf->setSourceFile($pathLayout);
            $template = 7;
            $tplIdx = $pdf->importPage($template);
            $pdf->useTemplate($tplIdx);

            $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];

            //CLIENT LOGO
            $pdf->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);

            //PARTICIPANT NAME
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdf->SetAutoPageBreak(false);
            $pdf->SetXY(135, 288.2);
            $pdf->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

            //PAGE NUMBER
            $pdf->SetXY(193.5, 280.5);//2 DIGITS
            $pdf->MultiCell(10, 10, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
            $GLOBALS['numberPage']++;

            $endeavor = Endeavor::find($endeavor0[0]->id);

            //PROJECT DESC1
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdf->SetAutoPageBreak(false);
            $pdf->SetXY(5, 50);
            $pdf->setCellHeightRatio(1.92);
            $pdf->MultiCell(0, 0, $endeavor->descriptionWhat, 0, 'L', 0, 0, '', '', true, 0);
            //PROJECT DESC2
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdf->SetAutoPageBreak(false);
            $pdf->SetXY(5, 170);
            $pdf->setCellHeightRatio(1.92);
            $pdf->MultiCell(0, 0, $endeavor->descriptionHow, 0, 'L', 0, 0, '', '', true, 0);


            //PROJECT DESC3
            $pdf->AddPage();

            $pdf->setSourceFile($pathLayout);
            $template = 8;
            $tplIdx = $pdf->importPage($template);
            $pdf->useTemplate($tplIdx);

            $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];

            //CLIENT LOGO
            $pdf->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);

            //PARTICIPANT NAME
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdf->SetAutoPageBreak(false);
            $pdf->SetXY(135, 288.2);
            $pdf->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

            //PAGE NUMBER
            $pdf->SetXY(193.5, 280.5);//2 DIGITS
            $pdf->MultiCell(10, 10, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
            $GLOBALS['numberPage']++;

            $endeavor = Endeavor::find($endeavor0[0]->id);

            //PROJECT DESC3
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdf->SetAutoPageBreak(false);
            $pdf->SetXY(5, 50);
            $pdf->setCellHeightRatio(1.92);
            $pdf->MultiCell(0, 0, $endeavor->descriptionWho, 0, 'L', 0, 0, '', '', true, 0);
            //PROJECT DESC4
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdf->SetAutoPageBreak(false);
            $pdf->SetXY(5, 170);
            $pdf->setCellHeightRatio(1.92);
            $pdf->MultiCell(0, 0, $endeavor->descriptionWhy, 0, 'L', 0, 0, '', '', true, 0);


            //PROJECT DESC5
            $pdf->AddPage();

            $pdf->setSourceFile($pathLayout);
            $template = 9;
            $tplIdx = $pdf->importPage($template);
            $pdf->useTemplate($tplIdx);

            $newWidth = ($GLOBALS['imageWidth']*20)/$GLOBALS['imageHeight'];

            //CLIENT LOGO
            $pdf->Image(__DIR__.'/../../public_html/media/images/clientLogo/'.$GLOBALS['logoName'],3,6,$newWidth,20,'','','M',false,0,'',false);

            //PARTICIPANT NAME
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdf->SetAutoPageBreak(false);
            $pdf->SetXY(135, 288.2);
            $pdf->MultiCell(50, 0, strtoupper($cred[0] . " " . $cred[1]), 0, 'R', 0, 0, '', '', true, 0);

            //PAGE NUMBER
            $pdf->SetXY(193.5, 280.5);//2 DIGITS
            $pdf->MultiCell(10, 10, $GLOBALS['numberPage'], 0, 'R', 0, 0, '', '', true, 0);
            $GLOBALS['numberPage']++;

            $endeavor = Endeavor::find($endeavor0[0]->id);

            //PROJECT DESC5
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont($GLOBALS['mager'], '', 11,'',false);
            $pdf->SetAutoPageBreak(false);
            $pdf->SetXY(5, 50);
            $pdf->setCellHeightRatio(1.92);
            $pdf->MultiCell(0, 0, $endeavor->descriptionNext, 0, 'L', 0, 0, '', '', true, 0);
        }
    }
}

/**
 * @param $pdf
 */
function closePDF($pdf,$cred,$date)
{
    $pdf_path = $cred[0].$cred[1].'_selfReport_' . $date . ".pdf";
    $pdf->Output($pdf_path);
    exit();
}