<?php

return array(

	'title' => 'Client details',

	'errors' => [
		'title' => 'The following errors have occurred:',
		'description' => '',
	],

	'company' => 'Company',
	'companyPlaceholder' => 'Company name',

	'country' => 'Country',
	'countryPlaceholder' => 'Company country',

	'fname' => 'First name',
	'fnamePlaceholder' => 'Contact person first name',

	'lname' => 'Last name',
	'lnamePlaceholder' => 'Contact person last name',

	'email' => 'Email',
	'emailPlaceholder' => 'Contact person email',

	'buy_more_seats' => 'Contact email to buy more seats',
	'buy_more_seatsPlaceholder' => 'Contact person email',

	'pwdReset' => 'Reset Password',
	'pwdResetYes' => 'Yes',
	'pwdResetNo' => 'No',

	'colors' => [
		'self' => 'User color',
		'superior' => 'Superiors color',
		'peer' => 'Peers color',
		'subordinate' => 'Subordinates color',
		'other' => 'Others color',
	],

	'tests' => [
		'title' => 'Test per User',
		'single' => '1 test per user',
		'multiple' => 'Unlimited tests per user',
	],

	'seats' => 'Total number of seats',

	'bcc' => 'BCC of tests goes to: (seperated by comma)',

	'logo' => 'Upload your logo',

	'multilanguage' => [
		'title' => 'Enable multiple languages',
		'info' => 'Click NO to use English as the default and only language. By clicking YES a language option will be enabled on the navigation menu.',
		'yes' => 'Yes',
		'no' => 'No',
	],

	'type_of_report' => [
		'title' => 'Select the type of report',
		'info' => 'A competency based report will rate from Needs significant improvement to Outstanding strength. A behavior based report will rate from Hardly ever to Almost always.',
		'behavior' => 'Behavior',
		'competency' => 'Competency',
	],

	'splitRespondents' => [
		'title' => 'Split respondents in groups',
		'info' => 'Click NO to group all respondents into a single group. By clicking YES four groups will be displayed on the assessment: Superiors, Peers, Subordinates, Others.',
		'yes' => 'Yes',
		'no' => 'No',
	],

	'programs' => [
		'title' => 'Include programs',
		'info' => 'Click NO to use our co-branded GGF-THNK template. By clicking YES the group template will be activated and users will need to be assigned into program groups.',
		'yes' => 'Yes',
		'no' => 'No',
	],

	'custom_open_question' => [
		'title' => 'Use custom open questions',
		'info' => 'Click NO to use default open question text. Click YES to use custom open question text.',
		'yes' => 'Yes',
		'no' => 'No',
	],

	'THNKGGF_footer' => [
		'title' => 'Use GGF & THNK co-branded footer',
		'info' => 'Click NO to use THNK only branded footer. Click YES to use GGF & THNK co-branded footer.',
		'yes' => 'Yes',
		'no' => 'No',
	],

	'buttons' =>  [
		'template' => 'THNKTemplate',
		'preview' => 'Preview',
		'save' => 'Save',
		'back' => 'Back',
	],

	'admin' =>  [
		'title' => 'Manage client',
		'questions' => 'Manage questions',
		'coaches' => 'Manage coaches',
		'toDashboard' => 'Client dashboard',
	],
);
