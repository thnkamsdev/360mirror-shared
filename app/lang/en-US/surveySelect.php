<?php

return array(

	'errors' => [
		'title' => 'The following errors have occurred:',
	],

	'title' => 'Welcome :firstname',
	'description' => 'You have been invited to multiple surveys, please select the one you want to work on.',

	'select' => [
		'title' => 'Select your survey',
		'button' => 'Select your survey',
	],

);
