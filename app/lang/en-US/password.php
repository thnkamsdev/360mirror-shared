<?php

return array(

	'remind' => [
		'title' => 'Recover your password',
		'success' => 'Please check your email for the link to reset your password.',
		'email' => 'Email',
		'placeholder' => 'Your email',
		'button' => 'Send Password',
	],

	'reset' => [
		'title' => 'Reset your password',
		'success' => 'Your password has been reseted.',
		'email' => 'Email',
		'placeholder' => 'Your email',
		'password' => 'Password',
		'confirm' => 'Confirm password',
		'button' => 'Reset Password',
	],	
);
