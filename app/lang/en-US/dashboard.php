<?php

return array(

	'dashboard' => 'DASHBOARD',
	
	'sidebar' => [
		'title' => 'Your project',
		'buttons' => [
			'update' => 'update your project details',
			'new' => 'fill out your project details',
			'create' => 'create a new assessment',
		],
		'tests' => [
			'title' => 'assessments',
			'open' => 'open',
			'date' => 'Y-m-d H:i',
		],
	],

	'main' => [
		'sent' => 'Reminder sent',
		'deleted' => 'Assessment deleted',
		'title' => 'Your project',
		'welcome' => 'Welcome to your dashboard, from here you can track the progress of your respondents at any time. You can also check the status of your assessment and once finished, generate your report.',
		'progressTitle' => 'Track progress, remind &amp; reporting',
		'respondents' => 'Respondents',
		'addRespondents' => 'Add respondents',
		'warning' => '* Please, remember to add at least two respondents per group or the group will be exluded from the report.',
		'errors' => [
			'title' => 'The following errors have occurred:',
		],
		'tests' => [
			'completed' => 'Congratulations you have completed your assessment.',
			'pending' => 'Finish your assessment',
			'status' => [
				'completed' => 'Assessment completed',
				'pending' => 'Not started yet',
				'inProgress' => 'In progress',
			],
			'reminder' => 'Last reminder:',
		],
		'buttons' => [
			'reminder' => 'Send Reminder',
			'removeUser' => 'Remove user',
			'self' => 'view self assessment',
			'360' => 'view full report',
		],
		'reports' => [
			'title' => 'View your report',
			'status' => [
				'ready' => 'You can view your self assessment now!',
				'notReady' => 'You cannot view your self assessment now, it is not finished yet',
				'full' => 'You can view your full report now!',
				'missingRespondents' => 'There are not enough completed respondents yet',
			],
		],
		'help' => [
			'self' => 'Self Assessment excludes the scores of your respondents.',
			'360' => 'Full report includes your own scores and the scores of your respondents.',
		],
	],
);