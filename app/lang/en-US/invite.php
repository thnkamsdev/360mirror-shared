<?php

return array(

	'errors' => [
		'title' => 'The following errors have occurred:',

		'superior' => 'The superior group contains only 1 respondent, ideally every group should contain at least 2 respondents',
		'peer' => 'The peer group contains only 1 respondent, ideally every group should contain at least 2 respondents',
		'subordinate' => 'The subordinate group contains only 1 respondent, ideally every group should contain at least 2 respondents',
		'other' => 'The other group contains only 1 respondent, ideally every group should contain at least 2 respondents',
		'team_member' => 'The team member group contains only 1 respondent, ideally every group should contain at least 2 respondents',

		'totals' => [
			'You have invited',
			'respondents,<br/>the minimum required is',
			'respondents.<br/>You still need to invite',
			'more resondents<br/><br/>If it remains impossible to get',
			'respondents,<br/>please notify your coach.',
		],

		'missing' => [
			'superior' => 'In the superior group there is one or more name or email missing',
			'peer' => 'In the peer group there is one or more name or email missing',
			'subordinate' => 'In the subordinate group there is one or more name or email missing',
			'other' => 'In the others group there is one or more name or email missing',
			'team_member' => 'In the team member group there is one or more name or email missing',
			'manager' => 'In the manager group there is one or more name or email missing',
		],
	],

	'invites' => [
		'name' => 'NAME',
		'email' => 'Email',
		'customText' => 'Custom email text',
		'customTextExample' => 'Dear {NAME},<br/>I am participating in Global Good Fund - THNK leadership assessment. For this purpose I would like to ask you to give feedback on my strengths and weaknesses.<br/>Please click the link below to access the assessment tool. Answering the questions will take approximately 30 minutes. The answers will be sent back to me anonymously.<br/>I really appreciate your help!<br/>Best regards,<br/>{YOUR_NAME}',
		'defaultNoELPTextExample' => 'Dear {NAME},<br/>I am participating in a THNK leadership assessment. For this purpose I would like to ask you to give feedback on my strengths and weaknesses.<br/>Please click the link below to access the assessment tool. Answering the questions will take approximately 30 minutes. The answers will be sent back to me anonymously.<br/>I really appreciate your help!<br/>Best regards,<br/>{YOUR_NAME}',
	],
	
	'title' => 'PART 2 - FEEDBACK',
	'userIntro' => 'Dear',
	'description' => 'Get started now by choosing the type of email you’d like to send and adding the email addresses.',

	'emailType' => [
		'title' => 'Which type of email would you like to send to your respondents?',
		'custom' => 'Use custom email text.',
		'default' => [
			'Use',
			'default e-mail text',
		],
	],

	'buttons' => [
		'invite' => 'invite',
	],

	'alreadyInvited' => [
		'title' => 'Add more respondents',
	],
);
