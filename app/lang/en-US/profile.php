<?php

return array(

	'title' => 'Edit Account details',

	'errors' => [
		'title' => 'The following errors have occurred:',
		'description' => '',
	],

	'email' => 'Email',
	'emailPlaceholder' => 'Your email',

	'pwdReset' => 'Reset Password',
	'pwd' => 'Password',
	'pwdConfirm' => 'Confirm password',

	'fname' => 'First name',
	'fnamePlaceholder' => 'Your first name',

	'lname' => 'Last name',
	'lnamePlaceholder' => 'Your last name',

	'gender' => 'Gender',
	'genderMale' => 'Male',
	'genderFemale' => 'Female',

	'dob' => 'Date of birth',
	'dobFormat' => 'dd/mm/YYYY',

	'country' => 'Country',
	'countryPlaceholder' => 'Your country',

	'programPlaceholder' => 'Your program',

	'company' => 'Company',
	'companyPlaceholder' => 'Your company',

	'groups' => 'Enable user groups',

	'buttons' =>  [
		'save' => 'Save',
		'back' => 'Back',
	],

	'reports' => [
		'title' => 'Check reports',
		'noTests' => 'No tests completed',
		'test' => 'Test',
		'respondents' => 'respondents',
		'minimumRespondentsDescription' => 'Introduce the minimum ammount of required respondents to complete the assessment. In order to respect privacy, every assessments needs at least 2 respondents.',
		'buttons' => [
			'self' => 'view self assessment',
			'full' => 'view full report',
			'update' => 'update minimum',
		],
		'titleEndeavors' => 'Check Endeavor Project',
		'noEndeavors' => 'No Scans completed',
		'endeavors' => 'Open Endeavor Project',
	],

	'adminFix' => [
		'title' => 'FIX FOR SUPER-ADMIN<br/><br/>Respondents #',
		'tests' => [
			'completed' => 'Test completed',
			'notStarted' => 'Not started yet<br/><br/>Last reminder:<br/>',
			'inProgress' => 'In progress<br/><br/>Last reminder:<br/>',
		],
		'buttons' => [
			'reminder' => 'Send Reminder',
			'delete' => 'Remove user',
		],
	],
);
