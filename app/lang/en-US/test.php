<?php

return array(

	'intro' => [
		'title1' => 'THE 360 MIRROR',
		'description1' => 'You are looking at your 360 Mirror. Looking in a mirror means looking at yourself. What do you see?  And how do others see you? This assesment tells you about your leadership skills, perceived by yourself and by others. It shows your strengths and development potential, or gifts and learning opportunities. This assessment will help you to consciously identify and develop the aspects of your leadership you’d like to focus on as a social entrepreneur.',
		'title2' => 'SOCIAL ENTERPRISE LEADERSHIP',
		'description2' => 'We believe that social entrepreneurship is the future of a society that effectively manages resources to meet the needs of its citizens while tackling society’s most challenging issues. We see leadership development as a vehicle for social impact.  The leadership skills of today’s and tomorrow’s leaders are developed along four distinctive areas of competency, as indicated in the model on the right. This assessment is meant to gauge your current level of competency and guide you as you define your leadership development goals.<br/><br/>Each of the four competencies (Manages self, Leads the team, Builds the enterprise, and Embodies personal values) will be explained individually at the beginning of each section that follows.<br/><br/>Leadership development is an ongoing journey throughout your career and we hope the 360 Mirror becomes a routine part of your personal journey.',
		'title3' => 'HOW TO READ',
		'description3' => 'The idea of the 360 Mirror is to get a sense of where in this model you have natural strengths and where your areas of improvement lie. We call these your Leadership Gifts and your Learning Opportunities. Every person has a unique set of gifts and opportunities.  This assessment allows you to discover yours. What should you look for? Look for where your self-scoring significantly differs from how other people rated you and then consider why these ratings are different. Take special note of areas where many respondents seem to agree strongly. Also take notice of where you score notably high or low.<br/><br/>Start with the big picture, your score over the major competencies. Then zoom in on each competency. The “In Addition” section features comments or concrete examples of how you display leadership competencies as provided by your raters. At the end of this document, you will find a table that helps you reflect on the 360 Mirror. We invite you to list the results that please you and displease or surprise you, which will help make your insights actionable.',
		'button' => 'Start your assessment',
	],

	'restricted' => [
		'title' => 'Not allowed',
		'description' => 'You are not allowed to create a new test.',
	],

	'header' => 'Progress',

	'competency_scores' => [
		'1' => 'Needs significant<br />improvement',
		'2' => 'Needs some<br />improvement',
		'3' => 'Competent',
		'4' => 'Strength',
		'5' => 'Outstanding<br />strength',
		'canNotRate' => 'Cannot<br />rate',
	],
	'behavior_scores' => [
		'1' => 'Hardly<br/>ever',
		'2' => 'Rare<br/>',
		'3' => 'Sometimes',
		'4' => 'Often',
		'5' => 'Always /<br/>Almost always',
		'canNotRate' => 'Cannot<br />rate',
	],

	'comment' => 'Please provide additional information (e.g. examples)',

	'errors' => [
		'required' => 'Required field.',
		'select' => 'Please choose one.',
	],

	'buttons' => [
		'back' => 'Back',
		'save' => 'Save',
		'next' => 'Save and next step',
		'finish' => 'Finish',
		'newEndeavor' => 'Fill in your endeavor',
		'oldEndeavor' => 'Update your endeavor',
	],

	'thanks' => [
		'title' => 'Thank you!',
		'description' => 'Congratulations! You have finished the test!',
		'button' => 'Take the MIRROR survey yourself',
	],

	'respondents' => [
		'Superior' => 'Superior',
		'Peer' => 'Peer',
		'Subordinate' => 'Subordinate',
		'Other' => 'Other',
		'Co-worker' => 'Co-worker',
		'Mentor' => 'Mentor',
		'Manager' => 'Manager',
		'Team member' => 'Team member',
	],

	'pdf' => [
		'overall_score' => 'OVERALL SCORE',
		'overall_outcomes' => 'OVERALL OUTCOME',
		'in_addition' => 'IN ADDITION',
		'personal_comments' => 'Personal comments',
		'respondents_comments' => 'Respondents comments',
	],
);
