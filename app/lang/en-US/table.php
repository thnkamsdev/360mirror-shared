<?php

return array(

	'ajaxError' => 'Error occured. Please try again ',
	
	'roles' => [
		'participant' => 'The Participant role allows the user to answer assessments.',
		'coach' => 'The Coach role allows the user to supervise Participants assessments.',
		'admin' => 'The Admin role allows the user to assign roles to users.',
	],

	'headers' => [
		'company' => 'Company',
		'fname' => 'First name',
		'lname' => 'Last name',
		'email' => 'Email',
		'client' => 'Edit client',
		'status' => 'Last Survey Status',
		'user' => 'User Surveys',
		'pwd' => 'Password Reset',
		'roles' => [
			'participant' => 'Participant',
			'coach' => 'Coach',
			'admin' => 'Admin',
		],
	],

	'body' => [
		'participant' => 'Access participant details',
		'noParticipant' => 'Not a participant',
		'client' => 'Edit client',
		'respondents' => ' respondents',
		'credentials' => 'Resend access link',
	],


);
