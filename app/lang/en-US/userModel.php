<?php

return array(

	'exception' => 'The user status entered does not exist',

	'getMailInvite' => [
		'coach' => 'Coach your participants',
		'reminder' => 'No action from respondents in past 2 weeks',
		'respondentDone' => 'You received input on your 360 Mirror',
		'allDone' => 'Your 360 Mirror is completed',
		'general' => [
			'Please review',
			'Leadership skills',
		],
		'default' => 'Create your 360 Mirror',
	],

	'getCustomText' => [
		'default' => 'Dear,\n\nI am participating in Global Good Fund - THNK leadership assessment. For this purpose I would like to ask you to give feedback on my strengths and weaknesses.\nPlease click the link below to access the assessment tool. Answering the questions will take approximately 30 minutes. The answers will be sent back to me anonymously.\nI really appreciate your help!\n\nBest regards,',
		'defaultNoELPTextExample' => 'Dear,\n\nI am participating in a THNK leadership assessment. For this purpose I would like to ask you to give feedback on my strengths and weaknesses.\n\nPlease click the link below to access the assessment tool. Answering the questions will take approximately 30 minutes. The answers will be sent back to me anonymously.\n\nI really appreciate your help!\n\nBest regards,\n\n',
	],

	'getBoxInvite' => [
		'intro' => 'Congratulations you are entering the second part of your 360 Mirror, known as 360º feedback.<br><br>In this step, you should invite at least 6 people you work with or have worked with in the past to provide feedback on your leadership skills. This is a crucial part of your learning and development process where you will receive comprehensive and unfiltered input.',
		'intro5' => 'Congratulations you are entering the second part of your 360 Mirror, known as 360º feedback.<br><br>In this step, you should invite at least 5 people you work with or have worked with in the past to provide feedback on your leadership skills. This is a crucial part of your learning and development process where you will receive comprehensive and unfiltered input.',
		'intro7' => 'Congratulations you are entering the second part of your 360 Mirror, known as 360º feedback.<br><br>In this step, you should invite at least 7 people you work with or have worked with in the past to provide feedback on your leadership skills. This is a crucial part of your learning and development process where you will receive comprehensive and unfiltered input.',
		'title' => 'What to expect?',
		'instructions' => 'Your respondents will be asked to reflect on your leadership skills by answering the same questions you just answered before. Your respondents will also be given the opportunity to add additional comments. This will be included in your final report.',
		'minimumRespondents' => [
			'title' => 'Respondents should ideally include at least:',
			'superiors' => '2 superiors',
			'peers' => '2 peers',
			'subordinates' => '2 subordinates',
			'one_superior' => 'superior',
			'num_superiors' => 'superiors',
			'one_manager' => 'manager',
			'num_managers' => 'managers',
			'num_peers' => 'peers',
			'num_subordinates' => 'subordinates',
			'num_teamMembers' => 'team members',
		],
	],

	'getInviteBoxes' => [
		'name' => 'NAME',
		'email' => 'Email',
		'customText' => 'Custom email text',
	],

	'getExtraMsgText' => [
		'alert' => [
			'* If you lack superiors, subordinates or peers, you can add more from other category. If you need, you can also include family and friends.<br><br>If it remains impossible to get',
			'respondents, please notify your coach.',
		],
	],
);
