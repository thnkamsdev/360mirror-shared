<?php

return array(

	'head' => [
		'keywords' => 'THNK, Global Good Fund, Mirror, assessment tool, 360, assessment, feedback, self assessment, report, creative leadership, school of creative leadership',
		'description' => '360 Mirror is a comprehensive leadership development tool developed by THNK School of Creative Leadership in partnership with Global Good Fund, to provide insights into an individual’s creative leadership skill for social entrepreneurs. In only a few steps you’ll learn about your strengths and learning edges, resulting in the ultimate chance to enhance your effectiveness as a leader in your enterprise. 360 Mirror is comprehensive, provided in two parts: Self-Assessment Evaluation and 360° Peer Feedback.',
	],

	'title' => '360 Mirror',
	
	'navigation' => [
		'admin' => 'THNK Admin Dashboard',
		'client' => 'Manage Client',
		'participant' => 'Participant Menu',
		'dashboard' => 'Dashboard',
		'profile' => 'My Profile',
		'welcome' => 'Welcome',
		'logout' => 'Logout',
	],

	'footer' => [
		'by' => 'BROUGHT TO YOU BY:',
		'feedback' => 'Feedback',
	],

	//REMOVE FROM HERE
	'deleteRespondent' => 'Are you sure you want to remove the selected respondent?',
	'emptyReport' => 'Your report is empty',

);
