<?php

return array(

	'allFinished' => [
		'dear' => 'Dear',
		'message' => 'We would like to inform you that all your respondents have finished giving feedback.',
		'report' => 'Please see the attached PDF file for your full report.',
	],

	'authReminder' => [
		'appTitle' => '360 Mirror',
		'title' => 'Password Reset',
		'message' => [
			'To reset your password, please visit this link:',
			'This link will expire in',
			'minutes',
		],
	],

	'default' => [
		'appTitle' => '360 Mirror',
		'footer' => [
			'360Mirror is a product of',
			'THNK School of Creative Leadership',
			'and',
			'The Global Good Fund',
		],
	],

	'inviteCustom' => [
		'message' => 'Visit my 360 MIRROR and provide feedback',
	],

	'inviteGeneral' => [
		'dear' => 'Dear',
		'intro' => 'I value your input and would like to ask you for help. I am reflecting on my Leadership skills using an online tool called 360 MIRROR. This includes 360º feedback from my peers, people I work with, and have worked for. This feedback is specifically on my leadership skills, strengths, and weaknesses. ',
		'action' => 'Please click the link below to send me your feedback. Answering these questions will take approximately 30 minutes, and the answers will be sent back to me anonymously. I appreciate your help!',
		'thanks' => 'Thank you!',
		'message' => 'Visit my 360 MIRROR and provide feedback',
	],

	'newCoach' => [
		'dear' => 'Dear',
		'message' => [
			'You are invited as a coach in a 360 leadership assessment. Please visit',
			'to start this process.',
		],
		'thanks' => [
			'default' => 'Regards',
			'extra' => [
				'Login with your e-mail and',
				'as your password',
			],
		],
	],

	'newSeat' => [
		'dear' => 'Dear',
		'message' => [
			'You are invited to partake in a 360 leadership assessment. Please visit',
			'to start this process.',
		],
		'thanks' => [
			'default' => 'Regards',
			'extra' => [
				'Login with your e-mail and',
				'as your password',
			],
		],
	],

	'appReminder' => [
		'dear' => 'Dear',
		'message' => 'We are mailing because you have an open test, but one or more respondents did not have any activity during the last two weeks. Could you send them a reminder?',
		'thanks' => 'Thank you!',
	],

	'respondentDone' => [
		'dear' => 'Dear',
		'message' => 'We would like to inform you that a respondent just finished providing feedback on your assessment via 360 MIRROR.',
	],
);
