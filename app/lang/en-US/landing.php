<?php

return array(

	'introduction' => [
		'title' => 'Discover your leadership qualities!',
		'description' => [
			'Fast, simple, clear, and completely online',
			'Get immediate clarity on your leadership skills',
			'Comprehensive 360° peer feedback',
			'Easy to understand report with results and next steps',
		],
	],

	'buttons' => [
		'start' => 'Start assessment',
		'learn' => 'Learn More',
	],

	'loginBox' => [
		'title' => 'Login',
		'email' => 'Email',
		'pwd' => 'Password',
		'pwdLink' => 'Forgot your password?',
		'button' => 'Login',
	],

	'learnMore' =>  [
		'description' => '360 Mirror is a comprehensive 360° leadership development tool. It is co-developed by THNK and Global Good Fund to provide insights into your creative leadership skills. In only a few steps you’ll learn about your strengths and learning edges, resulting in the ultimate chance to enhance your unique set of gifts.',
		'quoteTitle' => 'Ask yourself if you’re able to:',
		'quotes' => [
			'think big and bold when it comes to your mission.',
			'articulate clearly the triple bottom line impact of your mission.',
			'surround yourself with team members who are more gifted than yourself.',
			'channel conflict in a team to unlock new, creative solutions.',
			'formulate a robust business model to underpin your mission.',
			'use ambiguity in realizing your vision instead of seeing it as a blocker.',
			'articulate today what your legacy to the world will be.',
			'articulate how your legacy will build on your personal values, passions and strengths.',
		],
	],

	'explanation' =>  [
		'title' => 'How it works',
		'steps' => [
			'one' => [
				'title' => 'Step 1: Self-assessment',
				'description' => 'Complete this yourself by answering a series of questions that will help you reflect on your leadership skills.',
			],
			'two' => [
				'title' => 'Step 2: 360° Peer Feedback',
				'description' => 'Ask at least 6 people to provide anonymous feedback on your leadership skills, adding depth to your self assessment.',
			],
			'three' => [
				'title' => 'Step 3: Get your report',
				'description' => 'When completed, you can download or print the results. This will help you to gain insights into your inner leader.',
			],
		],
	],

);
