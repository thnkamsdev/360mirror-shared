<?php

return array(

	'title' => 'Questions',
	'description' => 'Here you can create and manage new and existing questions and categories.
    <br><br>
    Each tab correspond to a Category, you can navigate through the categories by clicking on each tab. If you want to create new categories, click on the NEW CATEGORY button. You can activate or deactivate categories, however if you deactivate a new category it will be removed from the account, only default categories will remain even if they are deactivated.
    <br><br>
    For each category you can create new questions, or use the default ones from the default categories. The order of the activated questions will correspond with their position on the assessment, you can change the order by dragding and dropping a question. Remember that the category where they belong needs to be activated first. Unlike deactivated categories, questions will remain even if they are deactivated.
    <br><br>
    Remember to click the SAVE button or your changes will be lost.',

	'buttons' => [
		'category' => 'New category',
		'question' => 'New question',
		'save' => 'Save',
		'edit' => '[EDIT]',
	],

	'category' => [
		'title' => 'User-specific Category',
		'name' => 'Name',
		'description' => 'Description',
		'customtext' => 'Custom open question text',
		'behavior_custom_open_question' => '1st Person custom open question text',
		'button' => 'Add to customer',
	],

	'question' => [
		'title' => 'User-specific Question',
		'name' => 'Name',
		'description' => 'Description',
		'behavior_desc' => '1st Person Description',
		'button' => 'Add to customer',
	],
);
