<?php

return array(

	'titleClient' => 'DASHBOARD',

	'hasPrograms' => [
		'title' => 'Filter by program',
		'button' => 'Filter by program',
	],

	'title' => 'OVERVIEW',

	'seats' => [
		'title' => 'Total seats',
		'used' => 'Total seats used',
		'free' => 'Total seats available',
		'buy' => [
			'button' => 'Buy new seats',
		],
	],

	'export' => [
		'program' => 'Program',
		'users' => 'Users',
		'usersHowTo' => 'Drag specific users to the right column',
		'usersAll' => 'All users',
		'usersSelected' => 'Selected users',
		'title' => 'Do you want to export all your participants data?',
		'how' => 'Learn how',
		'info' => 'Please select the program data you want to export.<br/>The data will be exported in a csv file.',
		'totals' => 'Export only totals',
		'completed' => 'Export only completed assessments',
		'yes' => 'yes',
		'no' => 'no',
		'buttonDefault' => 'Export data',
	],

	'errors' => [
		'title' => 'The following errors have occurred:',
		'success' => 'Success',
		'credentials' => 'New credentials have been sent.',
		'profile' => 'Participant profile has been updated.',
		'featureDisabled' => 'The selected feature is temporarily unavailable',
	],

	'enroll' => [
		'title' => 'INVITE PARTICIPANTS',
		'fname' => 'Participant first name',
		'lname' => 'Participant last name',
		'email' => 'Participant email',
		'program' => 'Participant program',
		'csv' => [
			'title' => 'Do you want to upload a .csv file?',
			'how' => 'Learn how',
			'infoCustom' => 'Please upload a .csv file with participant first name, last name, email and program id, separated by commas.',
			'infoDefault' => 'Please upload a .csv file with participant first name, last name and email, separated by commas.',
			'infoExtra' => 'Each participant needs to be in a single row.',
			'multiple' => 'Multiple participants (CSV upload)',
			'button' => 'Invite participants',
		],
	],

	'participants' => [
		'title' => 'MANAGE PARTICIPANTS',
		'info' => 'By unchecking every role, the user will be removed from your client account.',
		'search' => [
			'title' => 'Search',
			'name' => 'Name:',
			'namePlaceholder' => 'Participant name',
			'button' => 'Search',
		]
	],
);