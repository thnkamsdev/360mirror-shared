<?php

return array(

	'buttonNew' => 'New client',
	'buttonRemind' => 'Remind Inactivity',

	'msgBox' => [
		'title' => 'Success',
		'messages' => [
			'new' => 'New client has been created.',
			'update' => 'Client has been successfully updated.',
		],
	],

	'searchTable' => [
		'title' => 'Search',
		'name' => 'Name:',
		'placeholder' => 'Client name',
		'button' => 'Search client',
	],
);
