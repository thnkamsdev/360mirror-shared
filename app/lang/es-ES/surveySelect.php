<?php

return array(

	'errors' => [
		'title' => 'Se han producido los siguientes errores:',
	],

	'title' => 'Bienvenido :firstname',
	'description' => 'Has sido invitado para tomar varias encuestas, por favor, seleccione las que deseas trabajar.',

	'select' => [
		'title' => 'Seleccione tu encuesta',
		'button' => 'Seleccione tu encuesta',
	],

);
