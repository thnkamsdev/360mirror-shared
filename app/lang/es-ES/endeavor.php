<?php

return array(

	'title' => 'Editar detalles del proyecto',
	
	'name' => 'EL NOMBRE DE TU ENDEAVOR PROjECT',
	'namePlaceholder' => 'nombre de tu endeavor project',

	'descriptionWhatTitle' => 'DESCRIPTION OF THE "WHAT"',
	'descriptionWhat' => 'Describe what societal challenge and (emerging) user needs are you solving that are really relevant and urgent? What user group(s) is your solution primarily designed for, and what are the most compelling user needs and insights? What solution are your providing to your target users, as opposed to how they are solving the problem now, and what makes your solution truly delightful and memorable? What is the (intended) positive impact you will have on society?',
	'descriptionPlaceholderWhat' => 'Description of the WHAT',
	
	'descriptionHowTitle' => 'DESCRIPTION OF THE "HOW"',
	'descriptionHow' => 'What is your unique approach to customer interaction, and what should you be excellent at, in order to be able to deliver the value proposition?
What distinctive assets, resources, or partnerships are needed for your value proposition, the economic model and the delivery model?',
	'descriptionPlaceholderHow' => 'Description of the HOW',
	
	'descriptionWhoTitle' => 'DESCRIPTION OF THE "WHO"',
	'descriptionWho' => 'What does your team look like (central / de-central, competences, relationships)? What binds you together as a cohesive unit (core values, rituals, shared vision)? What is the (legal) enterprise structure, and how will your organization scale? Can your enterprise become a community, and if so, how are you inspiring people to become part of your community? What partnerships do you have or need to be successful, and how might build trust and leverage capabilities? Describe the wider ecosystem you are operating in.',
	'descriptionPlaceholderWho' => 'Description of the WHO',
	
	'descriptionWhyTitle' => 'DESCRIPTION OF THE "WHY"',
	'descriptionWhy' => 'Describe the possible better future that you wish to make happen through your Endeavour. What trends or movements are in your favor, to ensure your market entry strategy is successful? What short term activities you are preparing, or presently involved with, to drive progress. How do you make people accountable, track and evaluate progress, and have them learn? What are the one or two next big bet options for the longer term? How do you finance your growth and protect your bottom line at the same time?',
	'descriptionPlaceholderWhy' => 'Description of the WHY',

	'descriptionNextTitle' => 'Which parts of your endeavour need further development in your view?',
	'descriptionPlaceholderNext' => 'Please specify',

	'type' => 'TIPO DE PROYECTO',
	'otherType' => 'ESPECIFICA TU TIPO DE PROYECTO',
	'otherTypePlaceholder' => 'tu tipo de proyecto',

	'size' => 'NÚMERO DE PERSONAS INVOLUCRADAS EN EL DESARROLLO DE TU ENDEAVOR PROJECT',

	'stage' => 'PERIODO DE DESARROLLO',

	'buttons' => [
		'save' => 'Guardar',
		'back' => 'Volver ',
		'feedback' => 'Empieza tu feedback',
	],
);
