<?php

return array(

	'title' => 'Editar información de tu cuenta',

	'errors' => [
		'title' => 'Se han producido los siguientes errores:',
		'description' => '',
	],

	'email' => 'Correo electrónico',
	'emailPlaceholder' => 'Tu correo electrónico',

	'pwdReset' => 'Restablecer contraseña',
	'pwd' => 'Contraseña',
	'pwdConfirm' => 'Confirmar contraseña',

	'fname' => 'Nombre',
	'fnamePlaceholder' => 'Tu nombre',

	'lname' => 'Apellido',
	'lnamePlaceholder' => 'Tu apellido',

	'gender' => 'Género',
	'genderMale' => 'Hombre',
	'genderFemale' => 'Mujer',

	'dob' => 'Fecha de nacimiento',
	'dobFormat' => 'dd/mm/YYYY',

	'country' => 'País',
	'countryPlaceholder' => 'Tu país',

	'programPlaceholder' => 'Tu programa',

	'company' => 'Empresa',
	'companyPlaceholder' => 'Tu empresa',

	'groups' => 'Activar grupos de usuarios',

	'buttons' =>  [
		'save' => 'Guardar',
		'back' => 'Atrás',
	],

	'reports' => [
		'title' => 'Revisar informes',
		'noTests' => 'No hay pruebas completadas',
		'test' => 'prueba',
		'respondents' => 'encuestados',
		'minimumRespondentsDescription' => 'Introduce la cantidad minima requerida de los encuestados para computer la evaluación. Para respetar la privacidad, cada evaluación necesita por lo menos 2 encuestados.',
		'buttons' => [
			'self' => 'ver tu auto evaluación',
			'full' => 'ver tu informe completo',
			'update' => 'actualizar la cantidad minima',
		],
		'titleEndeavors' => 'Revisar Proyecto Endeavor',
		'noEndeavors' => 'No hay Scans completados',
		'endeavors' => 'Abrir Proyecto Endeavor',
	],

	'adminFix' => [
		'title' => 'FIX FOR SUPER-ADMIN<br/><br/>Respondents #',
		'tests' => [
			'completed' => 'Prueba completado',
			'notStarted' => 'No has empezado<br/><br/>Último recordatorio:<br/>',
			'inProgress' => 'En progreso<br/><br/>Último recordatorio:<br/>',
		],
		'buttons' => [
			'reminder' => 'Enviar recordatorio',
			'delete' => 'Eliminar usuario',
		],
	],
);
