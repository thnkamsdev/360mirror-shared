<?php

return array(

	'allFinished' => [
		'dear' => 'Estimado',
		'message' => 'Nos gustaría informarte sobre el hecho de que todos los encuestados han terminado de enviar sus comentarios.',
		'report' => 'Por favor consulte el archivo PDF adjunto para ver tu informe completo.',
	],

	'authReminder' => [
		'appTitle' => 'Espejo 360',
		'title' => 'Restablecer contraseña',
		'message' => [
			'Para restablecer tu contraseña, por favor visite este enlace:',
			'Este enlace expirará en',
			'minutos',
		],
	],

	'default' => [
		'appTitle' => 'Espejo 360',
		'footer' => [
			'Espejo 360 es un producto de',
			'THNK School of Creative Leadership',
			'y',
			'The Global Good Fund',
		],
	],

	'inviteCustom' => [
		'message' => 'Visite mi Espejo 360 y de feedback',
	],

	'inviteGeneral' => [
		'dear' => 'Estimado',
		'intro' => 'Valoro tu opinion y quisiera pedirte ayuda. Yo estoy reflexionando sobre mis habilidades de liderazgo usando una herramienta online llamada el ESPEJO 360. Esto incluye 360º feedback de mis colegas, la gente con la que he trabajado. Éste feedback está basado específicamente en mis habilidades de liderazgo, mis fortalezas y mis debilidades.',
		'action' => 'Por favor, dale clic en el enlace para enviarme tus comentarios. Respondiendo a estas preguntas te tomará aproximadamente 30 minutos, y las respuestas se enviarán a mí anónimamente. ¡Te agradezco tu ayuda!',
		'thanks' => '¡Gracias!',
		'message' => 'Visita mi Espejo 360 y envíame tu feedback',
	],

	'newCoach' => [
		'dear' => 'Estimado',
		'message' => [
			'Tú estás invitado como entrenador en una evaluación 360 de liderazgo. Por favor visite',
			'para iniciar este proceso.',
		],
		'thanks' => [
			'default' => 'Saludos',
			'extra' => [
				'Iniciar sesión con tu correo electrónico y',
				'como tu contraseña',
			],
		],
	],

	'newSeat' => [
		'dear' => 'Estimado',
		'message' => [
			'Estás invitado para participar en una evaluación 360 de liderazgo. Por favor visite',
			'para iniciar este proceso.',
		],
		'thanks' => [
			'default' => 'Saludos',
			'extra' => [
				'Iniciar sesión con tu correo electrónico y',
				'con tu contraseña',
			],
		],
	],

	'appReminder' => [
		'dear' => 'Estimado',
		'message' => 'Te estamos mandando este correo porque tienes una prueba abierta, pero uno o más de los encuestados no ha tenido ninguna actividad durante las últimas dos semanas. ¿Quieres enviarles un recordatorio?',
		'thanks' => 'Gracias',
	],

	'respondentDone' => [
		'dear' => 'Estimado',
		'message' => 'Nos gustaría informate que un encuestado acaba de dar un feedback a tu evaluación a través del ESPEJO 360.',
	],
);
