<?php

return array(

	'ajaxError' => 'Se ha producido un error. Por favor, vuelve a intentar',
	
	'roles' => [
		'participant' => 'El papel de Participante le permite a los usuarios responder los informes.',
		'coach' => 'El papel de Coach le permite supervisar los informes de los participantes.',
		'admin' => 'El papel de Administrador le permite asignar roles a los usuarios.',
	],

	'headers' => [
		'company' => 'La Empresa',
		'fname' => 'Nombre',
		'lname' => 'Apellido',
		'email' => 'Correo electrónico',
		'client' => 'Editar cliente',
		'status' => 'El estado de la ultima encuesta',
		'user' => 'Encuestas del usuario',
		'pwd' => 'Restablecimiento de su contraseña',
		'roles' => [
			'participant' => 'Participante',
			'coach' => 'Entrenador',
			'admin' => 'Admin',
		],
	],

	'body' => [
		'participant' => 'Accesar los detalles del participante',
		'noParticipant' => 'No es participante',
		'client' => 'Editar cliente',
		'respondents' => 'Encuestados',
		'credentials' => 'Vuelve a enviar el enlace de acceso',
	],


);
