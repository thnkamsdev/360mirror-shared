<?php

return array(

	'errors' => [
		'title' => 'Se han producido los siguientes errores:',

		'superior' => 'El grupo de superiores tiene sólo 1 encuestado, idealmente cada grupo debe tener al menos 2 encuestados',
		'peer' => 'El grupo de pares tiene sólo 1 encuestado, idealmente cada grupo debe tener al menos 2 encuestados',
		'subordinate' => 'El grupo subordinados tiene sólo 1 encuestado, idealmente cada grupo debe tener al menos 2 encuestados',
		'other' => 'El grupo de otros tiene sólo 1 encuestado, idealmente cada grupo debe tener al menos 2 encuestados',
		'team_member' => 'El grupo de miembros de equipo tiene sólo 1 encuestado, idealmente cada grupo debe tener al menos 2 encuestados',

		'totals' => [
			'Tu has invitado',
			'encuestados,<br/>el mínimo requerido es',
			'encuestados.<br/>Tú todavía necesitas invitar a',
			'más encuestados<br/><br/>Si aún es difícil de obtener',
			'encuestados,<br/>por favor notifique a tu entrenador.',
		],

		'missing' => [
			'superior' => 'En el grupo de superiores, falta el correo electrónico o nombre de uno o más',
			'peer' => 'En el grupo de pares, falta el correo electrónico o nombre de uno o más',
			'subordinate' => 'En el grupo de subordinados, falta el correo electrónico o nombre de uno o más',
			'other' => 'En el grupo de otros, falta el correo electrónico o nombre de uno o más',
			'team_member' => 'En el grupo de miembros de equipo, falta el correo electrónico o nombre de uno o más',
			'manager' => 'En el grupo de mánagers, falta el correo electrónico o nombre de uno o más',
		],
	],

	'invites' => [
		'name' => 'NOMBRE',
		'email' => 'Correo electrónico',
		'customText' => 'Texto personalizado',
		'customTextExample' => 'Estimado {NOMBRE},<br/>Estoy participando en el Global Good Fund - THNK evaluación de liderazgo. Por esta razón me gustaría pedirte tu opinión sobre mis fortalezas y debilidades.<br/>Por favor haz clic en el siguiente enlace para llegar a la herramienta de evaluación. Responder a las preguntas te tomará aproximadamente 30 minutos. Las respuestas se enviarán a mí anónimamente.<br/>¡Aprecio mucho tu ayuda!<br/>Saludos,<br/>{TÚ_NOMBRE}',
		'defaultNoELPTextExample' => 'Estimado {NOMBRE},<br/>Estoy participando en una evaluación de liderazgo de THNK School of Creative Leadership. Por esta razón me gustaría pedirte tu opinión sobre mis fortalezas y debilidades.<br/>Por favor haz clic en el siguiente enlace para llegar a la herramienta de evaluación. Responder a las preguntas te tomará aproximadamente 30 minutos. Las respuestas se enviarán a mí anónimamente.<br/>¡Aprecio mucho tu ayuda!<br/>Saludos,<br/>{TÚ_NOMBRE}',
	],
	
	'title' => 'PARTE 2 - FEEDBACK',
	'userIntro' => 'Estimado',
	'description' => 'Empieza ahora eligiendo el tipo de correo electrónico que deseas enviar y agrega las direcciones de los correos electrónicos.',

	'emailType' => [
		'title' => '¿Qué tipo de correo electrónico quieres enviar a tus encuestados?',
		'custom' => 'Usar texto personalizado.',
		'default' => [
			'Usar',
			'texto predeterminado',
		],
	],

	'buttons' => [
		'invite' => 'invitar',
	],

	'alreadyInvited' => [
		'title' => 'Añadir más encuestados',
	],
);
