<?php

return array(

	'head' => [
		'keywords' => 'THNK, Global Good Fund, El Espejo 360, herramienta de evaluación, feedback, auto evaluación, informe, liderazgo creativo, colegio de liderazgo creativo, creative leadership',
		'description' => 'El Espejo 360 es una herramienta designada para el desarrollo del liderazgo, hecho por THNK School de Creative Leadership en colaboración con el Global Good Fund, para darles a los empresarios sociales un conocimiento de sus habilidades de liderazgo creativo. En sólo unos pocos pasos aprenderás sobre tus fuerzas, dando lugar a la oportunidad para mejorar tu eficiencia como líder en tu empresa. El Espejo 360 es una evaluación completa, esta dividida en dos partes: El Auto-Evaluación y El 360° Feedback entre tus colegas.',
	],

	'title' => '360 Mirror',
	
	'navigation' => [
		'admin' => 'Menú de Administrador',
		'client' => 'Administrar Cliente',
		'participant' => 'Menú de Usuario',
		'dashboard' => 'Panel',
		'profile' => 'Mi Perfil',
		'welcome' => 'Bienvenido',
		'logout' => 'Cerrar Sesión',
	],

	'footer' => [
		'by' => 'TRAÍDO A USTED POR:',
		'feedback' => 'Feedback',
	],

	//REMOVE FROM HERE
	'deleteRespondent' => '¿Estás seguro que deseas eliminar el encuestado seleccionado?',
	'emptyReport' => 'Tu informe está vacío',

);
