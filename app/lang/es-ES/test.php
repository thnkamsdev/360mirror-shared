<?php

return array(

	'intro' => [
		'title1' => 'EL ESPEJO 360',
		'description1' => 'Estás viendo tu Espejo 360. Mirando a un espejo significa que estás mirando a ti mismo. ¿Qué ves? ¿Y como otros te ven a ti? Este informe trata de tus habilidades de liderazgo percibidos por ti y otra gente. Este informe te va a enseñar tus fortalezas y tu potencial para el desarrollo, o tus dones y oportunidades de aprendizaje. Tu Espejo 360 te va a ayudar a identificar y desarrollar los aspectos de tu liderazgo en los que quieres enfocarte como un empresario social.',
		'title2' => 'LIDERAZGO EMPRESARIAL SOCIAL',
		'description2' => 'Nosotros creemos que un emprendedor social es el futuro de una sociedad que eficientemente maneja sus recursos para satisfacer las necesidades de sus habitantes, y simultáneamente abordando los temas sociales más difíciles. Nosotros creemos que el desarrollo de liderazgo es un vehículo para el impacto social. Las habilidades de un líder de hoy están indicadas en el módulo en la derecha. Este informe está hecha para ver tu nivel de habilidad y para guiarte en como tú defines tus objetivos de liderazgo.<br/><br/>Cada una de las cuatro habilidades (Auto manejo, dirigir un equipo, construir la empresa , e incorpora valores personales) será explicado individualmente en los principios de cada sección que sigue.<br/><br/>El desarrollo del liderazgo es un viaje largo que continúa por toda tu carrera y nosotros esperamos que el Espejo 360 se convierta en una parte rutinaria de tu viaje personal.',
		'title3' => 'CÓMO LEER',
		'description3' => 'La idea del Espejo 360 es para que tú puedas entender dónde en este modelo tú tienes tus fortalezas naturales y cuáles son tus áreas en las que necesitas mejorar. Nosotros llamamos a estos tus Dones de Liderazgo y tus Oportunidades de Aprendizaje. Cada persona tiene dones y oportunidades únicas. Este informe te va a ayudar a descubrir las tuyas. ¿Qué debes buscar? Busca dónde tu auto-calificación es significativamente diferente de cómo otras personas te han clasificado y después considera por qué estos niveles son tan diferentes. Toma notas de las áreas donde muchos encuestados parecen que están de acuerdo, también ten en cuenta en donde tú calificación es alta o baja.<br/><br/>Empieza por el tema principal, que son tus calificaciones en las habilidades principales. Después enfócate en cada habilidad. La sección de “Además" te da comentarios o ejemplos de cómo tú expones las habilidades como un líder acordé con tus calificaciones. Al final de este documento, te vas a encontrar con una tabla que te ayudará a reflejar en tu Espejo 360. Te invitamos a que escribas una lista de los resultados que no te complacieron y te sorprendieron, esto te ayudará a tu percepción.',

		'button' => 'Comienza tu informe',
	],

	'restricted' => [
		'title' => 'Prohibido',
		'description' => 'No puedes crear un nuevo examen.',
	],

	'header' => 'Progreso',

	'competency_scores' => [
		'1' => 'Necesita<br />mejorar',
		'2' => 'Necesita algo<br />de mejora',
		'3' => 'Algo<br/>competente<br />',
		'4' => 'Muy<br />competente',
		'5' => 'Excelentemente<br />competente',
		'canNotRate' => 'NS/NC<br/>',
	],
	'behavior_scores' => [
		'1' => 'Casi<br/>nunca',
		'2' => 'Pocas<br/>veces',
		'3' => 'A<br/>veces',
		'4' => 'Con<br/>frecuencia',
		'5' => 'Siempre /<br/>Casi siempre',
		'canNotRate' => 'NS/NC<br/>',
	],

	'comment' => 'Por favor, proporcionar información adicional (ejemplos)',

	'errors' => [
		'required' => 'Obligatorio.',
		'select' => 'Por favor elija uno.',
	],

	'buttons' => [
		'back' => 'Atrás',
		'save' => 'Guardar',
		'next' => 'Guardar y sigue adelante',
		'finish' => 'Terminar',
	],

	'thanks' => [
		'title' => '¡Gracias!',
		'description' => '¡Felicitaciones, tú has terminado el examen!',
		'button' => 'Toma la prueba MIRROR tú mismo',
	],

	'respondents' => [
		'Superior' => 'Superior',
		'Peer' => 'Par',
		'Subordinate' => 'Subordinado',
		'Other' => 'Otro',
		'Co-worker' => 'Colega',
		'Mentor' => 'Mentor',
		'Manager' => 'Mánager',
		'Team member' => 'Miembro de equipo',
	],

	'pdf' => [
		'overall_score' => 'PUNTUACIÓN MEDIA',
		'overall_outcomes' => 'RESULTADO GLOBAL',
		'in_addition' => 'COMENTARIOS',
		'personal_comments' => 'Comentarios personales',
		'respondents_comments' => 'Comentarios encuestados',
	],
);
