<?php

return array(

	'dashboard' => 'PANEL',
	
	'sidebar' => [
		'title' => 'Tu proyecto',
		'buttons' => [
			'update' => 'actualize los detalles de tu proyecto',
			'new' => 'rellena los detalles de tu proyecto',
			'create' => 'crear un nuevo informe',
		],
		'tests' => [
			'title' => 'informes',
			'open' => 'abrir',
			'date' => 'Y-m-d H:i',
		],
	],

	'main' => [
		'sent' => 'Recordatorio enviado',
		'deleted' => 'Informe eliminado',
		'title' => 'Tu proyecto',
		'welcome' => 'Bienvenido a tu panel de control, desde aquí puedes seguir el progreso de tus respuestas en cualquier momento. También puedes revisar el estado de tu evaluación y una vez que termines, generar tu informe',
		'progressTitle' => 'Guardar su progreso, recordatorios &amp; informes',
		'respondents' => 'encuestados',
		'addRespondents' => 'Añadir encuestados',
		'warning' => '* Por favor, recuerda añadir al menos 2 encuestados por grupo o de lo contrario el grupo será excluido del informe.',
		'errors' => [
			'title' => 'Han ocurrido los siguientes errores:',
		],
		'tests' => [
			'completed' => 'Felicitaciones, has completado tu informe.',
			'pending' => 'Termina tu informe',
			'status' => [
				'completed' => 'Informe completado',
				'pending' => 'Todavía no ha empezado',
				'inProgress' => 'En progreso',
			],
			'reminder' => 'Último recordatorio:',
		],
		'buttons' => [
			'reminder' => 'Enviar recordatorio',
			'removeUser' => 'Eliminar usuario',
			'self' => 'Ver auto-informe',
			'360' => 'Ver informe completo',
		],
		'reports' => [
			'title' => 'Ver tu informe',
			'status' => [
				'ready' => 'Ahora puedes ver tu auto-informe!',
				'notReady' => 'No puedes ver tu auto-informe, no esta completo',
				'full' => 'Ahora puedes ver tu informe completo!',
				'missingRespondents' => 'No hay suficientes encuestados completados',
			],
		],
		'help' => [
			'self' => 'Auto-informe excluye los resultados de us encuestados.',
			'360' => 'El informe completo incluye tus propios resultados y los resultados de tus encuestados.',
		],
	],
);