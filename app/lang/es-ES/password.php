<?php

return array(

	'remind' => [
		'title' => 'Recuperar tu contraseña',
		'success' => 'Por favor revise tu correo electrónico para restablecer tu contraseña.',
		'email' => 'Correo Electrónico',
		'placeholder' => 'Tu correo electrónico',
		'button' => 'Enviar Contraseña',
	],

	'reset' => [
		'title' => 'Restablecer contraseña',
		'success' => 'Tu contraseña ha sido restablecida.',
		'email' => 'Correo Electrónico',
		'placeholder' => 'Tu Contraseña',
		'password' => 'Contraseña',
		'confirm' => 'Confirmar contraseña',
		'button' => 'Restablecer contraseña',
	],	
);
