<?php

return array(

	'buttonNew' => 'Nuevo cliente',
	'buttonRemind' => 'Recordar de Inactividad',

	'msgBox' => [
		'title' => 'Éxito',
		'messages' => [
			'new' => 'Nuevo cliente hecho.',
			'update' => 'Cliente actualizado correctamente.',
		],
	],

	'searchTable' => [
		'title' => 'Buscar',
		'name' => 'Nombre:',
		'placeholder' => 'Nombre de cliente',
		'button' => 'Buscar cliente',
	],
);
