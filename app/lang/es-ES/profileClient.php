<?php

return array(

	'title' => 'Datos del cliente',

	'errors' => [
		'title' => 'Han ocurrido los siguientes errores:',
		'description' => '',
	],

	'company' => 'Empresa',
	'companyPlaceholder' => 'Nombre de empresa',

	'country' => 'País',
	'countryPlaceholder' => 'País de empresa',

	'fname' => 'Nombre',
	'fnamePlaceholder' => 'Nombre',

	'lname' => 'Apellido',
	'lnamePlaceholder' => 'Apellido',

	'email' => 'Correo electrónico',
	'emailPlaceholder' => 'Correo electrónico',

	'buy_more_seats' => 'Email de contact para comprar nuevos asientos',
	'buy_more_seatsPlaceholder' => 'Correo electrónico',

	'pwdReset' => 'Restablecer contraseña',
	'pwdResetYes' => 'Si',
	'pwdResetNo' => 'No',

	'colors' => [
		'self' => 'Color de usuario',
		'superior' => 'Color de superior',
		'peer' => 'Color de par',
		'subordinate' => 'Color de subordinado',
		'other' => 'Color de otros',
	],

	'tests' => [
		'title' => 'Prueba por usuario',
		'single' => '1 Prueba por usuario',
		'multiple' => 'Ilimitado pruebas por usuario',
	],

	'seats' => 'Número total de asientos',

	'bcc' => 'BCC de pruebas va a: (separados por comas)',

	'logo' => 'Sube tu logo',

	'multilanguage' => [
		'title' => 'Activar múltiples lenguajes',
		'info' => 'Clicka NO para establecer el Inglés como el único lenguaje por defecto. Clicka en SÍ para activar el menú multi-lenguaje en la barra de navegación.',
		'yes' => 'Yes',
		'no' => 'No',
	],

	'type_of_report' => [
		'title' => 'Selecciona el tipo de informe',
		'info' => 'Un informe basado en competencias evaluará de Necesita mejorar a Extraordinariamente diestro. Un informe basado en comportamiento evaluará de Apenas nunca a Casi siempre.',
		'behavior' => 'Behavior',
		'competency' => 'Competency',
	],

	'splitRespondents' => [
		'title' => 'Dividir los participantes en grupos',
		'info' => 'Haga clic en NO para juntar todas las respuestas en un solo grupo. Cuando escojas SI cuatro groups se van a mostrar en la evaluación: Superiores, Pares, Subordinados, Otros.',
		'yes' => 'Si',
		'no' => 'No',
	],

	'programs' => [
		'title' => 'Incluir programas',
		'info' => 'Clic NO para utilizar nuestro co-branded GGF-THNK template. By clicking YES the group template will be activated and users will need to be assigned into program groups.',
		'yes' => 'Si',
		'no' => 'No',
	],

	'custom_open_question' => [
		'title' => 'Usar texto personalizado en las preguntas abiertas',
		'info' => 'Selecciona NO para usar el texto por defecto en las preguntas abiertas. Selecciona SÍ para usar el texto personalizado en las preguntas abiertas.',
		'yes' => 'Sí',
		'no' => 'No',
	],

	'THNKGGF_footer' => [
		'title' => 'Usar footer GGF & THNK co-diseñado',
		'info' => 'Clicka NO para usar footer sólo THNK. Clicka SÍ para usar footer GGF & THNK co-diseñado.',
		'yes' => 'Sí',
		'no' => 'No',
	],

	'buttons' =>  [
		'template' => 'THNKTemplate',
		'preview' => 'Preview',
		'save' => 'Guardar',
		'back' => 'Atrás',
	],

	'admin' =>  [
		'title' => 'Administrar cliente',
		'questions' => 'Administrar preguntas',
		'coaches' => 'Administrar coaches',
		'toDashboard' => 'Plantilla de cliente',
	],
);
