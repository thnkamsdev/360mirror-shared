<?php

return array(

	'exception' => 'El estatus de usuario introducida no existe',

	'getMailInvite' => [
		'coach' => 'Entrenar tu participantes',
		'reminder' => 'Ninguna acción de los encuestados en últimas 2 semanas',
		'respondentDone' => 'Recibiste consejo en tu Espejo 360',
		'allDone' => 'Tu Espejo 360 esta completado',
		'general' => [
			'Por favor, revise',
			'Habilidades de liderazgo',
		],
		'default' => 'Crear tu Espejo 360',
	],

	'getCustomText' => [
		'default' => 'Querido,\n\nEstoy participando en el Global Good Fund - THNK evaluación de liderazgo. Me gustaría pedirle su opinión sobre mis fortalezas y debilidades.\nPor favor haz clic en el siguiente enlace para usar a la herramienta de evaluación. Respondiendo a las preguntas tardará aproximadamente 30 minutos. Las respuestas me van a llegar a mí anónimamente.\n¡Gracias por tu ayuda!\n\nSaludos,',
		'defaultNoELPTextExample' => 'Estimada/o,\n\nEstoy participando en una evaluación de liderazgo de THNK School of Creative Leadership. Me gustaría pedirle su opinión sobre mis fortalezas y debilidades.\n\nPor favor haga click en el siguiente enlace para usar a la herramienta de evaluación. Responder a las preguntas le llevará aproximadamente 30 minutos. Sus respuestas serán enviadas de forma anónima.\n\n¡Gracias por su colaboración!\n\nSaludos,\n\n',
	],

	'getBoxInvite' => [
		'intro' => 'Felicitaciones estás entrando a la segunda parte de tu Espejo 360, conocido como 360º feedback. En este paso tienes que invitar por lo menos 6 personas que han trabajado contigo para darte consejos en tu habilidades de liderazgo. Esto es una parte crucial de tu proceso de aprendizaje y desarrollo, dónde vas a recibir sugerencias extensivas de tu colegas.',
		'intro5' => 'Felicitaciones estás entrando a la segunda parte de tu Espejo 360, conocido como 360º feedback. En este paso tienes que invitar por lo menos 5 personas que han trabajado contigo para darte consejos en tu habilidades de liderazgo. Esto es una parte crucial de tu proceso de aprendizaje y desarrollo, dónde vas a recibir sugerencias extensivas de tu colegas.',
		'title' => '¿Qué puedes esperar?',
		'instructions' => 'Tu encuestados serán interrogados para considerar tus habilidades de liderazgo al responder las mismas preguntas que tú has respondido. Le dará la oportunidad a tus encuestados para agregar comentarios adicionales. Esto va estar incluido en tu informe final.',
		'minimumRespondents' => [
			'title' => 'idealmente encuestados tienen que incluir por lo menos:',
			'superiors' => '2 superiores',
			'peers' => '2 pares',
			'subordinates' => '2 subordinados',
			'one_superior' => 'superior',
			'num_superiors' => 'superiores',
			'one_manager' => 'mánager',
			'num_managers' => 'mánagers',
			'num_peers' => 'pares',
			'num_subordinates' => 'subordinados',
			'num_teamMembers' => 'miembros de equipo',
		],
	],

	'getInviteBoxes' => [
		'name' => 'NOMBRE',
		'email' => 'Correo Electrónico',
		'customText' => 'Mensaje de correo electrónico personalizado',
	],

	'getExtraMsgText' => [
		'alert' => [
			'* Si the falta superiores, subordinados o pares, puedes agregar más de otra categoría. Si lo necesitas, también puedes incluir tu familia y amigos.<br><br>Si sigue imposible obtener',
			'encuestados, por favor avisa tu entrenador.',
		],
	],
);
