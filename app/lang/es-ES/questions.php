<?php

return array(

	'title' => 'Preguntas',
	'description' => 'Aquí puedes crear y manejar nuevas preguntas y categorías.
    <br><br>
    Cada pestaña corresponde a una categoría, tú puedes navegar las categorías haciendo clic en cada pestaña. Si quieres crear categorías nuevas, tienes que hacer clic en el botón de Nueva Categoría. Puedes activar o desactivar categorías, sin embargo si quieres desactivar una nueva categoría, se va a eliminar de la cuenta. Solamente las categorías predeterminadas se quedan aunque estén desactivadas.
    <br><br>
    Por cada categoría puedes crear preguntas nuevas, o puedes usar las preguntas predeterminadas. El orden de las preguntas activadas corresponde con su posición en la evaluación. Para cambiar el orden tienes que arrastrar y colocar cada pregunta. Recuerde que la categoría a la cual esa pregunta pertenece tiene que estar activada. Al contrario de las categorías desactivadas, estas preguntas se quedan aunque estén desactivadas.
    <br><br>
    Recuerda de darle clic al botón Guardar o los cambios se perderán.',

	'buttons' => [
		'category' => 'Nueva categoría',
		'question' => 'Nueva pregunta',
		'save' => 'Guardar',
		'edit' => '[EDIT]',
	],

	'category' => [
		'title' => 'Categoría específica del usuario',
		'name' => 'Nombre',
		'description' => 'Descripción',
		'customtext' => 'Texto personalizado para la pregunta abierta',
		'behavior_custom_open_question' => 'Texto personalizado en 1a persona para la pregunta abierta',
		'button' => 'Añadir a cliente',
	],

	'question' => [
		'title' => 'Pregunta específica del usuario',
		'name' => 'Nombre',
		'description' => 'Descripción',
		'behavior_desc' => 'Descripción en 1a persona',
		'button' => 'Añadir a cliente',
	],
);
