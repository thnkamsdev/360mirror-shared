<?php

return array(

	'introduction' => [
		'title' => '¡Descubre tus cualidades de liderazgo!',
		'description' => [
			'Rápido, simple, claro y totalmente en la red',
			'Obtener claridad inmediata sobre tus habilidades de liderazgo',
			'360° feedback completo de tus colegas',
			'Una manera fácil de entender el informe con los resultados y los pasos a seguir',
		],
	],

	'buttons' => [
		'start' => 'Comience evaluación',
		'learn' => 'Aprende más',
	],

	'loginBox' => [
		'title' => 'Iniciar sesión',
		'email' => 'Correo electrónico',
		'pwd' => 'Contraseña',
		'pwdLink' => '¿Olvidó su contraseña?',
		'button' => 'Iniciar sesión',
	],

	'learnMore' =>  [
		'description' => 'El Espejo 360 es una herramienta líder de desarrollo 360°. Implementada en conjunto por THNK y Global Good Fund para proveer visión a tus habilidades de liderazgo creativas. En unos pocos pasos, vas a aprender dónde están tus fortalezas, resultando en una oportunidad para resaltar tus dones.',
		'quoteTitle' => 'Pregúntate si eres capaz de:',
		'quotes' => [
			'pensar grande y audazmente cuando se trata de tu misión.',
			'articular claramente el impacto de tu misión.',
			'rodearte con miembros del equipo que tienen mas experiencia que tú.',
			'enfrentar el conflicto en un equipo para desbloquear nuevas soluciones creativas.',
			'formular un modelo de negocio robusto para sostener tu misión.',
			'utilizar la ambigüedad como una realización de tu misión en lugar de verlo como un bloqueo.',
			'expresar hoy lo que será tu legado al mundo.',
			'articular cómo tu legado se basará en tus valores personales, pasiones y fortalezas.',
		],
	],

	'explanation' =>  [
		'title' => 'Cómo funciona',
		'steps' => [
			'one' => [
				'title' => 'Paso 1: auto-evaluación',
				'description' => 'Vas a responder una series de preguntas que te ayudara a reflexionar sobre tus habilidades de liderazgo.',
			],
			'two' => [
				'title' => 'Paso 2: 360° Feedback de tus Colegas',
				'description' => 'Pregunta por lo menos a 6 personas para dar comentarios anónimos en tus habilidades de liderazgo, añadiendo profundidad a tu auto evaluación.',
			],
			'three' => [
				'title' => 'Step 3: Obtener tu informe',
				'description' => 'Cuando termines, puedes descargar o imprimir tus resultados. Esto te ayudará a obtener una idea de tu líder interior.',
			],
		],
	],

);
