<?php

return array(

	'titleClient' => 'PANEL',

	'hasPrograms' => [
		'title' => 'Filtrar por programa',
		'button' => 'Filtrar por programa',
	],

	'title' => 'RESUMEN',

	'seats' => [
		'title' => 'Cantidad total de asientos',
		'used' => 'Cantidad total de asientos usadas',
		'free' => 'Cantidad total de asientos disponibles',
		'buy' => [
			'button' => 'Compra nuevo asientos',
		],
	],

	'export' => [
		'program' => 'Programa',
		'users' => 'Usarios',
		'usersHowTo' => 'Arrastra usarios específicos a la columna de la derecha',
		'usersAll' => 'Todos los usarios',
		'usersSelected' => 'Usarios seleccionados',
		'title' => 'Deseas exportar todos los datos de tus participantes?',
		'how' => 'Aprender cómo',
		'info' => 'Por favor seleccione los datos del programa que deseas exportar.<br/>Los datos se can a exportar en un archivo csv.',
		'totals' => 'Solamente exportar totales',
		'completed' => 'Solamente exportar informes completos',
		'yes' => 'si',
		'no' => 'no',
		'buttonDefault' => 'Exportar datos',
	],

	'errors' => [
		'title' => 'Se han producido los siguientes errores:',
		'success' => 'Éxito',
		'credentials' => 'Credenciales nuevas han sido enviadas.',
		'profile' => 'El perfil del participante se ha actualizado.',
		'featureDisabled' => 'La función seleccionada temporalmente no está disponible',
	],

	'enroll' => [
		'title' => 'INVITAR PARTICIPANTES',
		'fname' => 'El nombre del participante',
		'lname' => 'El apellido del participante',
		'email' => 'Correo electrónico del participante',
		'program' => 'Programa de participantes',
		'csv' => [
			'title' => 'Quieres subir un archivo .csv?',
			'how' => 'Aprender cómo',
			'infoCustom' => 'Por favor, sube un archivo .csv con el nombre, apellido, correo electrónico y programa ID, separados por comas, del participante.',
			'infoDefault' => 'Por favor, sube un archivo .csv con el nombre, apellido, correo electrónico y programa ID, separados por comas, del participante.',
			'infoExtra' => 'Cada participante necesita estar en una fila.',
			'multiple' => 'Varios participantes (subir CSV)',
			'button' => 'Invite participantes',
		],
	],

	'participants' => [
		'title' => 'MANEGAR PARTICIPANTES',
		'info' => 'En desmarcando cada papel, el usuario se va a eliminar de tu cuenta de cliente.',
		'search' => [
			'title' => 'Busca',
			'name' => 'Nombre:',
			'namePlaceholder' => 'Nombre de participante',
			'button' => 'Busca',
		]
	],
);