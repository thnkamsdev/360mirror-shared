<?php

namespace THNK\Mirror\DataAnalysis;

use Test;
use Teststatus;
use THNK\Mirror\DataAnalysis\TestAnalysis\QuestionAnalysis\QuestionAnalysis;
use THNK\Mirror\DataAnalysis\TestAnalysis\TestAnalysis;

class DataAnalysis
{
	/**
	 * @var array|TestAnalysis[]
	 */
	private $testAnalyses;

	/**
	 * @param \Test[] $tests
	 */
	public function __construct(array $tests)
	{
		$this->testAnalyses = $this->makeAnalysesForTests($tests);
	}

	/**
	 * @return array|\THNK\Mirror\DataAnalysis\TestAnalysis\TestAnalysis[]
	 */
	public function getTestAnalyses()
	{
		return $this->testAnalyses;
	}

	/**
	 * @param array $tests
	 *
	 * @return TestAnalysis[]
	 */
	private function makeAnalysesForTests(array $tests)
	{
		$questionAnalysis = new QuestionAnalysis(array_pluck($tests, 'id'));

		$testAnalyses = array_map(function (Test $test) use ($questionAnalysis)
		{
			return new TestAnalysis($test, $questionAnalysis);
		}, $tests);

		return $this->sortTestAnalyses($testAnalyses);
	}

	/**
	 * @param \THNK\Mirror\DataAnalysis\QueryArguments $arguments
	 *
	 * @return \THNK\Mirror\DataAnalysis\DataAnalysis
	 */
	public static function createForQuery(QueryArguments $arguments)
	{
		$query = Test::where('tests.client_id', '=', $arguments->getClientId())
		             ->with('user', 'respondent', 'parent')
		             ->select('tests.*')
		             ->join('users', function ($join)
		             {
			             $join->on('tests.user_id', '=', 'users.id');
		             })
		             ->orderBy('users.lastname', 'asc');

		if ($arguments->hasUserIds())
		{
			$query->whereIn('tests.user_id', $arguments->getUserIds());
		}

		if ($arguments->getProgramId())
		{
			$query->where('users.THNKProgram', '=', $arguments->getProgramId());
		}

		if ($arguments->onlyCompleted())
		{
			$query->where('teststatus_id', '=', Teststatus::TEST_FULLY_FINISHED);
		}

		return new self(self::mergeChildTestsFor($query->get()->all()));
	}

	/**
	 * Fetch the child tests for the supplied tests and merge them into the array.
	 *
	 * @param array $tests
	 *
	 * @return array
	 */
	private static function mergeChildTestsFor(array $tests)
	{
		// Fetch the respondents as well to prevent a lot of extra queries
		$childTests = Test::whereIn('test_id', array_pluck($tests, 'id'))->with('respondent')->get()->all();

		return array_merge($tests, $childTests);
	}

	/**
	 * Pretty massive sorting function. Cannot be done in the database easily
	 * because the data is spread and polluted across many tables.
	 *
	 * @param array|TestAnalysis[] $testAnalyses
	 *
	 * @return array
	 */
	private function sortTestAnalyses(array $testAnalyses)
	{
		$testAnalyses = array_merge([], $testAnalyses);
		usort($testAnalyses, function (TestAnalysis $a, TestAnalysis $b)
		{
			if ($a->getUserIdentifier() !== $b->getUserIdentifier())
			{
				return $a->getUserIdentifier() > $b->getUserIdentifier() ? 1 : - 1;
			}

			if ($a->getRootTest()->id !== $b->getRootTest()->id)
			{
				return $a->getRootTest()->id > $b->getRootTest()->id ? 1 : - 1;
			}

			if ($a->isSelfAssessment())
			{
				return - 1;
			}

			if ($b->isSelfAssessment())
			{
				return 1;
			}

			if ($a->getRespondentGroupId() !== $b->getRespondentGroupId())
			{
				return $a->getRespondentGroupId() > $b->getRespondentGroupId() ? 1 : - 1;
			}

			if ($a->getRespondentIdentifier() !== $b->getRespondentIdentifier())
			{
				return $a->getRespondentIdentifier() > $b->getRespondentIdentifier() ? 1 : - 1;
			}

			return 0;
		});

		return $testAnalyses;
	}
}