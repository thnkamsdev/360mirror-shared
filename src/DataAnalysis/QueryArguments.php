<?php

namespace THNK\Mirror\DataAnalysis;

class QueryArguments
{
	/**
	 * @var int
	 */
	private $clientId;

	/**
	 * @var int
	 */
	private $programId;

	/**
	 * @var array
	 */
	private $userIds = [];

	/**
	 * @var bool
	 */
	private $onlyCompletedTests = false;

	/**
	 * @var bool
	 */
	private $onlyTotals = false;

	/**
	 * @param int $clientId
	 *
	 * @throws \THNK\Mirror\DataAnalysis\InvalidQueryArgumentsValue
	 */
	public function __construct($clientId)
	{
		$this->assertValidId($clientId, 'clientId');

		$this->clientId = (int) $clientId;
	}

	/**
	 * @return int
	 */
	public function getClientId()
	{
		return $this->clientId;
	}

	/**
	 * @return int
	 */
	public function getProgramId()
	{
		return $this->programId;
	}

	/**
	 * @param int $programId
	 *
	 * @throws \THNK\Mirror\DataAnalysis\InvalidQueryArgumentsValue
	 */
	public function setProgramId($programId)
	{
		$this->assertValidId($programId, 'programId');
		$this->programId = $programId;
	}

	/**
	 * @return array
	 */
	public function getUserIds()
	{
		return $this->userIds;
	}

	/**
	 * @return bool
	 */
	public function hasUserIds()
	{
		return count($this->getUserIds()) > 0;
	}

	/**
	 * @param array $userIds
	 *
	 * @return bool
	 * @throws \THNK\Mirror\DataAnalysis\InvalidQueryArgumentsValue
	 */
	public function setUserIds(array $userIds)
	{
		$userIds = array_map(function ($userId)
		{
			$userId = (int) $userId;
			if ($userId === 0)
			{
				throw new InvalidQueryArgumentsValue('User IDs array can only be positive integers.');
			}

			return $userId;
		}, $userIds);

		$this->userIds = $userIds;
	}

	/**
	 * @param null|bool $value
	 *
	 * @return bool
	 * @throws \THNK\Mirror\DataAnalysis\InvalidQueryArgumentsValue
	 */
	public function onlyCompleted($value = null)
	{
		if ($value !== null)
		{
			if ( ! is_bool($value))
			{
				throw new InvalidQueryArgumentsValue('onlyCompleted accepts only booleans');
			}

			$this->onlyCompletedTests = $value;
		}

		return $this->onlyCompletedTests;
	}

	/**
	 * @param null|bool $value
	 *
	 * @return bool
	 * @throws \THNK\Mirror\DataAnalysis\InvalidQueryArgumentsValue
	 */
	public function onlyTotals($value = null)
	{
		if ($value !== null)
		{
			if ( ! is_bool($value))
			{
				throw new InvalidQueryArgumentsValue('onlyTotals accepts only booleans');
			}

			$this->onlyTotals = $value;
		}

		return $this->onlyTotals;
	}

	/**
	 * @param int $id
	 * @param string $name
	 *
	 * @throws \THNK\Mirror\DataAnalysis\InvalidQueryArgumentsValue
	 */
	private function assertValidId($id, $name = 'id')
	{
		if ( ! is_int($id) || $id <= 0)
		{
			throw new InvalidQueryArgumentsValue(sprintf('Invalid query argument %s.', $name));
		}
	}

	public function toArray()
	{
		return [
			'clientId'           => $this->clientId,
			'programId'          => $this->programId,
			'userIds'            => $this->userIds,
			'onlyCompletedTests' => $this->onlyCompletedTests,
			'onlyTotals'         => $this->onlyTotals,
		];
	}
}