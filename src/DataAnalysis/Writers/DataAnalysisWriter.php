<?php

namespace THNK\Mirror\DataAnalysis\Writers;

use THNK\Mirror\DataAnalysis\DataAnalysis;
use THNK\Mirror\DataAnalysis\QueryArguments;

interface DataAnalysisWriter
{

	/**
	 * @param \THNK\Mirror\DataAnalysis\DataAnalysis $analysis
	 * @param \THNK\Mirror\DataAnalysis\QueryArguments $usedArguments
	 *
	 * @return \SplFileInfo
	 */
	public function writeToDisk(DataAnalysis $analysis, QueryArguments $usedArguments);
}