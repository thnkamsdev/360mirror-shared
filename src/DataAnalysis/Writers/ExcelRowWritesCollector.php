<?php

namespace THNK\Mirror\DataAnalysis\Writers;

class ExcelRowWritesCollector
{
	/**
	 * @var array
	 */
	private $columns = [];

	/**
	 * @param int $index
	 * @param mixed $value
	 *
	 * @return $this
	 */
	public function column($index, $value)
	{
		$this->columns[ $index ] = $value;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getColumns()
	{
		return $this->columns;
	}

}