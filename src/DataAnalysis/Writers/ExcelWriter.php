<?php

namespace THNK\Mirror\DataAnalysis\Writers;

use Category;
use DateTimeImmutable;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Worksheet;
use SplFileInfo;
use THNK\Mirror\DataAnalysis\DataAnalysis;
use THNK\Mirror\DataAnalysis\QueryArguments;
use THNK\Mirror\DataAnalysis\TestAnalysis\TestAnalysis;

class ExcelWriter implements DataAnalysisWriter
{
	const CATEGORY_COLUMN_OFFSET = 4;

	const HEADER_ROW_INDEX = 1;

	const SUB_HEADER_ROW_INDEX = 2;

	const DATA_ROW_INDEX = 3;

	/**
	 * @var array
	 */
	private $writtenCategories = [];

	/**
	 * @var \Illuminate\Filesystem\Filesystem
	 */
	private $filesystem;

	/**
	 * @var \PHPExcel_Style
	 */
	private $headerStyleIndex;

	/**
	 * @param \Illuminate\Filesystem\Filesystem $filesystem
	 */
	public function __construct(Filesystem $filesystem)
	{
		$this->filesystem = $filesystem;
	}

	/**
	 * @param \THNK\Mirror\DataAnalysis\DataAnalysis $analysis
	 * @param \THNK\Mirror\DataAnalysis\QueryArguments $usedArguments
	 *
	 * @return \THNK\Mirror\DataAnalysis\Writers\SplFileInfo
	 * @throws \PHPExcel_Reader_Exception
	 */
	public function writeToDisk(DataAnalysis $analysis, QueryArguments $usedArguments)
	{
		$excel = $this->writeAnalysisToExcel($analysis, $this->getTemplateExcel(), $usedArguments->onlyTotals());
		$path = $this->generateAndPreparePath($analysis);

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$objWriter->save($path);

		return new SplFileInfo($path);
	}

	/**
	 * @param \THNK\Mirror\DataAnalysis\DataAnalysis $analysis
	 * @param PHPExcel $excel
	 * @param bool $onlyTotals
	 *
	 * @return \PHPExcel
	 */
	private function writeAnalysisToExcel(DataAnalysis $analysis, PHPExcel $excel, $onlyTotals = false)
	{
		$sheet = $excel->getActiveSheet();

		/** @var TestAnalysis $testAnalysis */
		foreach ($analysis->getTestAnalyses() as $index => $testAnalysis)
		{
			$this->writeRow($sheet, self::DATA_ROW_INDEX + $index, function ($row) use ($testAnalysis, $sheet, $onlyTotals)
			{
				/**
				 * Write test meta data
				 */
				$row->column(0, '#' . $testAnalysis->getRootTest()->id);
				$row->column(1, $testAnalysis->getUserIdentifier());
				$row->column(2, $testAnalysis->isSelfAssessment() ? '-' : $testAnalysis->getRespondentIdentifier());
				$row->column(3, $testAnalysis->isSelfAssessment() ? '-' : $testAnalysis->getRespondentGroupLabel());

				/**
				 * Write category labels and question values
				 */
				foreach ($testAnalysis->getCategories() as $index => $category)
				{
					if ( ! $this->hasWrittenCategory($category))
					{
						$this->writeCategoryHeaders($category, $testAnalysis, $sheet, $onlyTotals);
					}

					// Scores
					$scores = $testAnalysis->getScoresForTestAndCategory($testAnalysis->getTestId(), $category->id);
					$columnOffset = $this->getColumnOffsetForCategory($category, $testAnalysis, $onlyTotals);

					// Write the average
					$row->column($columnOffset, $testAnalysis->getAverageOfScores($scores));

					// Should we stop at the averages?
					if ($onlyTotals)
					{
						continue;
					}

					// And the individual values
					$index = 0;
					foreach ($scores as $questionId => $score)
					{
						$row->column(1 + $columnOffset + $index, $score ? $score : '-');
						$index ++;
					}
				}
			});
		}

		return $excel;
	}

	/**
	 * @return PHPExcel
	 * @throws \PHPExcel_Reader_Exception
	 */
	private function getTemplateExcel()
	{
		$excel = PHPExcel_IOFactory::createReader('Excel2007');

		return $excel->load(app_path('helpers/dataAnalysis/template.xlsx'));
	}

	/**
	 * @param \THNK\Mirror\DataAnalysis\DataAnalysis $analysis
	 *
	 * @return string
	 */
	private function getFilePath(DataAnalysis $analysis)
	{
		$now = new DateTimeImmutable();
		$fileName = sprintf('DataAnalysis_%s_%s.xlsx', $now->format('Y-m-d_H-i'), uniqid());

		return 'data-analyses/' . $fileName;
	}

	/**
	 * @param \THNK\Mirror\DataAnalysis\DataAnalysis $analysis
	 *
	 * @return string
	 */
	private function generateAndPreparePath(DataAnalysis $analysis)
	{
		$path = storage_path() . '/' . $this->getFilePath($analysis);

		$dirPath = dirname($path);
		if ( ! $this->filesystem->exists($dirPath))
		{
			$this->filesystem->makeDirectory($dirPath);
		}

		return $path;
	}

	/**
	 * @param \PHPExcel_Worksheet $sheet
	 * @param int $rowIndex
	 * @param \Closure $columnValues
	 *
	 * @throws \PHPExcel_Exception
	 */
	private function writeRow(PHPExcel_Worksheet $sheet, $rowIndex, \Closure $columnValues)
	{
		$columnWrites = new ExcelRowWritesCollector();
		$columnValues($columnWrites);

		foreach ($columnWrites->getColumns() as $colIndex => $value)
		{
			$sheet->getCellByColumnAndRow($colIndex, $rowIndex)->setValue($value);
		}
	}

	/**
	 * @param \Category $category
	 *
	 * @return bool
	 */
	private function hasWrittenCategory(Category $category)
	{
		return in_array($category, $this->writtenCategories);
	}

	/**
	 * @param \Category $category
	 * @param \THNK\Mirror\DataAnalysis\TestAnalysis\TestAnalysis $testAnalysis
	 * @param \PHPExcel_Worksheet $sheet
	 * @param bool $onlyTotals
	 *
	 * @throws \PHPExcel_Exception
	 */
	private function writeCategoryHeaders(Category $category, TestAnalysis $testAnalysis, PHPExcel_Worksheet $sheet, $onlyTotals = false)
	{
		if ($this->hasWrittenCategory($category))
		{
			return;
		}

		if ( ! $this->headerStyleIndex)
		{
			$this->headerStyleIndex = $sheet->getCellByColumnAndRow(self::CATEGORY_COLUMN_OFFSET, self::HEADER_ROW_INDEX)
			                                ->getXfIndex();
		}

		$start = $this->getColumnOffsetForCategory($category, $testAnalysis, $onlyTotals);
		$questionCount = $onlyTotals ? 0 : $this->getCategoryColumnCount($category, $testAnalysis);

		// Merge cells
		$sheet->mergeCellsByColumnAndRow($start, self::HEADER_ROW_INDEX, $start + $questionCount, self::HEADER_ROW_INDEX);

		// Write values and style them
		$sheet->getCellByColumnAndRow($start, self::HEADER_ROW_INDEX)
		      ->setXfIndex($this->headerStyleIndex)
		      ->setValue($category->name);

		// Write overall score label
		$sheet->getCellByColumnAndRow($start, self::SUB_HEADER_ROW_INDEX)
		      ->setValue('Overall Score')
		      ->getStyle()
		      ->getFont()
		      ->setBold(true);

		// Write question labels
		if ($questionCount > 0)
		{
			foreach (range(1, $questionCount) as $questionIndex)
			{
				$sheet->getCellByColumnAndRow($start + $questionIndex, self::SUB_HEADER_ROW_INDEX)
				      ->setValue('Q' . $questionIndex)
				      ->getStyle()
				      ->getFont()
				      ->setBold(true);
			}
		}

		$this->writtenCategories[] = $category;
	}

	/**
	 * @param \Category $category
	 * @param \THNK\Mirror\DataAnalysis\TestAnalysis\TestAnalysis $testAnalysis
	 *
	 * @return int
	 */
	private function getCategoryColumnCount(Category $category, TestAnalysis $testAnalysis)
	{
		return count($testAnalysis->getQuestionsInCategory($category->id));
	}

	/**
	 * @param \Category $category
	 * @param \THNK\Mirror\DataAnalysis\TestAnalysis\TestAnalysis $testAnalysis
	 *
	 * @param bool $onlyTotals
	 *
	 * @return int
	 */
	private function getColumnOffsetForCategory(Category $category, TestAnalysis $testAnalysis, $onlyTotals = false)
	{
		$offset = self::CATEGORY_COLUMN_OFFSET;

		foreach ($this->writtenCategories as $writtenCategory)
		{
			if ($writtenCategory->id === $category->id)
			{
				return $offset;
			}

			$offset += 1 + ($onlyTotals ? 0 : $this->getCategoryColumnCount($writtenCategory, $testAnalysis));
		}

		return $offset;
	}

}