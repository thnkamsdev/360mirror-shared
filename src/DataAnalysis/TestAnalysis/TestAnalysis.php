<?php

namespace THNK\Mirror\DataAnalysis\TestAnalysis;

use Category;
use Test;
use THNK\Mirror\DataAnalysis\TestAnalysis\QuestionAnalysis\QuestionAnalysis;

class TestAnalysis
{
	/**
	 * @var \User
	 */
	private $user;

	/**
	 * @var \Respondent
	 */
	private $respondent;

	/**
	 * @var \Test
	 */
	private $test;

	/**
	 * @var \THNK\Mirror\DataAnalysis\TestAnalysis\QuestionAnalysis\QuestionAnalysis
	 */
	private $questionAnalysis;

	/**
	 * @param \Test $test
	 * @param \THNK\Mirror\DataAnalysis\TestAnalysis\QuestionAnalysis\QuestionAnalysis $questionAnalysis
	 */
	public function __construct(Test $test, QuestionAnalysis $questionAnalysis)
	{
		$this->test = $test;
		$this->questionAnalysis = $questionAnalysis;
	}

	/**
	 * @return int
	 */
	public function getTestId()
	{
		return $this->test->id;
	}

	/**
	 * @return \Test
	 */
	public function getRootTest()
	{
		return $this->test->getRootTest();
	}

	/**
	 * @return array|\Category[]
	 */
	public function getCategories()
	{
		$testId = $this->test->isSelfAssessment() ? $this->test->id : $this->test->test_id;

		return $this->questionAnalysis->getCategoriesForTest($testId);
	}

	/**
	 * @param int $categoryId
	 *
	 * @return array|\Question[]
	 */
	public function getQuestionsInCategory($categoryId)
	{
		$client = $this->getUser()->getClient();
		$clientId = $client ? $client->getId() : null;

		return $this->questionAnalysis->getQuestionsForCategory($categoryId, $clientId);
	}

	/**
	 * @param int $testId
	 * @param int $categoryId
	 *
	 * @return array
	 */
	public function getScoresForTestAndCategory($testId, $categoryId)
	{
		return $this->questionAnalysis->getScoresForTestAndCategory($testId, $categoryId);
	}

	/**
	 * @return bool
	 */
	public function isSelfAssessment()
	{
		return $this->test->isSelfAssessment();
	}

	/**
	 * @return string|bool
	 */
	public function getUserIdentifier()
	{
		if ( ! $this->getUser())
		{
			return false;
		}

		// Construct the name and filter the empty ones
		$name = join(', ', array_filter([
			$this->getUser()->lastname,
			$this->getUser()->firstname,
		]));

		return sprintf('%s (#%s)', $name, $this->getUser()->id);
	}

	/**
	 * @return string
	 */
	public function getUserCompany()
	{
		if ( ! $this->getUser())
		{
			return '';
		}

		return $this->getUser()->company;
	}

	/**
	 * @return string
	 */
	public function getRespondentIdentifier()
	{
		if ( ! $this->getRespondent())
		{
			return '';
		}

		return sprintf('%s (#%s)', $this->getRespondent()->name, $this->getRespondent()->id);
	}

	/**
	 * @return int|null
	 */
	public function getRespondentGroupId()
	{
		if ( ! $this->getRespondent())
		{
			return null;
		}

		return (int) $this->getRespondent()->respondentgroup_id;
	}

	/**
	 * @return string
	 */
	public function getRespondentGroupLabel()
	{
		if ( ! $this->getRespondent())
		{
			return 'Unknown';
		}

		/** @var \THNK\Mirror\DataAnalysis\TestAnalysis\RespondentGroups\RespondentGroups $respondentGroups */
		$respondentGroups = app('THNK\Mirror\DataAnalysis\TestAnalysis\RespondentGroups\RespondentGroups');

		return $respondentGroups->getGroupById($this->getRespondent()->respondentgroup_id);
	}

	/**
	 * @return \Respondent
	 */
	private function getRespondent()
	{
		if ( ! $this->respondent)
		{
			$this->respondent = $this->test->respondent;
		}

		return $this->respondent;
	}

	/**
	 * @return \User
	 */
	private function getUser()
	{
		if ( ! $this->user)
		{
			$this->user = $this->getRootTest()->user;
		}

		return $this->user;
	}

	/**
	 * @param array $scores
	 *
	 * @return float|int
	 */
	public function getAverageOfScores(array $scores)
	{
		// Filter out 0 and null values
		$filteredScores = array_filter($scores);

		if (count($filteredScores) === 0)
		{
			return '-';
		}

		return round(array_sum($filteredScores) / count($filteredScores), 1);
	}

}