<?php

namespace THNK\Mirror\DataAnalysis\TestAnalysis\QuestionAnalysis;

use RuntimeException;

class NoTestsFoundException extends RuntimeException
{

}