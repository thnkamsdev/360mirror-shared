<?php

namespace THNK\Mirror\DataAnalysis\TestAnalysis\QuestionAnalysis;

use Category;
use Closure;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Question;

class QuestionAnalysis
{

	/**
	 * @var array
	 */
	private $testQuestionData;

	/**
	 * @var array
	 */
	private $loadedEntityCollections = [];

	/**
	 * QuestionTestRepository constructor.
	 *
	 * @param array $testIds
	 *
	 * @throws \Exception
	 */
	public function __construct(array $testIds)
	{
		$this->testQuestionData = $this->createTestCategoryStructure($testIds);

//		if ( ! count($this->testQuestionData))
//		{
//			throw new NoTestsFoundException('Cannot analyse for zero tests.');
//		}
	}

	/**
	 * @param int $testId
	 * @param int $categoryId
	 *
	 * @return array
	 */
	public function getScoresForTestAndCategory($testId, $categoryId)
	{
		if ( ! isset($this->testQuestionData[ $testId ][ $categoryId ]))
		{
			return [];
		}

		if ( ! count($this->testQuestionData[ $testId ][ $categoryId ]))
		{
			// Using Fluent instead of Eloquent for performance reasons
			$rows = DB::table('category_question_user_test')
			          ->select('test_id', 'question_id', 'value')
			          ->whereIn('test_id', array_keys($this->testQuestionData))
			          ->whereIn('question_id', array_pluck($this->getQuestionsForCategory($categoryId), 'id'))
			          ->get();

			foreach ($rows as $ids)
			{
				// Filter out 0 and 9 values as these are not actual scores
				$value = $ids->value === 0 || $ids->value === 9 ? null : $ids->value;
				$this->testQuestionData[ $ids->test_id ][ $categoryId ][ $ids->question_id ] = $value;
			}
		}

		return $this->testQuestionData[ $testId ][ $categoryId ];
	}

	/**
	 * @param int $categoryId
	 * @param int|null $userId
	 *
	 * @return array|\Question[]
	 */
	public function getQuestionsForCategory($categoryId, $userId = null)
	{
		$questionsPerCategory = $this->lazyLoadEntities([$categoryId], 'questionsPerCategory', function ($categoryIdInArray)
		{
			$fakeEntity = (object) [
				'id'           => $categoryIdInArray[0],
				'question_ids' => DB::table('category_question_user')
				                    ->select('question_id', 'user_id')
				                    ->whereIn('category_id', $categoryIdInArray)
				                    ->get()
			];

			return new Collection([$fakeEntity]);
		});

		// Extract the question ids
		$questionsInSelectedCategory = $questionsPerCategory[ $categoryId ]->question_ids;

		// Filter out the categories that are not relevant to this user
		if ($userId)
		{
			$questionsInSelectedCategory = array_filter($questionsInSelectedCategory, function ($row) use ($userId)
			{
				return $row->user_id == $userId;
			});
		}

		$questionIds = $questionsInSelectedCategory = array_unique(array_pluck($questionsInSelectedCategory, 'question_id'));

		return $this->lazyLoadEntities($questionIds, 'questions', function ($missing)
		{
			return Question::whereIn('id', $missing)->get();
		});
	}

	/**
	 * @param int $testId
	 *
	 * @return array|\Category[]
	 */
	public function getCategoriesForTest($testId)
	{
		if ( ! isset($this->testQuestionData[ $testId ]))
		{
			// If this happens, not categories were assigned to the test
			return [];
		}

		$categoryIds = array_keys($this->testQuestionData[ $testId ]);

		return $this->lazyLoadEntities($categoryIds, 'categories', function ($missing)
		{
			return Category::whereIn('id', $missing)->get();
		});
	}

	/**
	 * Convert the raw data to a structure like:
	 *
	 * [
	 *    <testId> => [
	 *     <categoryId> => [],
	 *   ],
	 *   <otherTestId> => []
	 * ]
	 *
	 * @param array $testIds
	 *
	 * @return array
	 */
	private function createTestCategoryStructure(array $testIds)
	{
		// Using Fluent instead of Eloquent for performance reasons
		$rows = DB::table('category_question_user_test')
		          ->select('test_id', 'category_id')
		          ->whereIn('test_id', $testIds)
		          ->get();

		$tests = [];

		foreach ($rows as $ids)
		{
			if ( ! isset($tests[ $ids->test_id ]))
			{
				$tests[ $ids->test_id ] = [];
			}

			// Skip zero-ish category ids
			if ( ! $ids->category_id)
			{
				continue;
			}

			if ( ! isset($tests[ $ids->test_id ][ $ids->category_id ]))
			{
				$tests[ $ids->test_id ][ $ids->category_id ] = [];
			}
		}

		return $tests;
	}

	/**
	 * @param string $cacheName
	 *
	 * @return array|mixed
	 */
	private function getLoadedEntityCollection($cacheName)
	{
		if ( ! isset($this->loadedEntityCollections[ $cacheName ]))
		{
			return [];
		}

		return $this->loadedEntityCollections[ $cacheName ];
	}

	/**
	 * @param array $ids
	 * @param string $collectionName
	 * @param \Closure $fetchMissing
	 *
	 * @return array
	 */
	private function lazyLoadEntities(array $ids, $collectionName, Closure $fetchMissing)
	{
		$idsAsKeys = array_flip($ids);
		$unavailableIds = array_diff_key($idsAsKeys, $this->getLoadedEntityCollection($collectionName));

		// Check if we still need to retrieve some categories
		if (count($unavailableIds))
		{
			$newEntities = $fetchMissing(array_flip($unavailableIds));
			$this->storeLazyLoadedEntities($newEntities, $collectionName);
		}

		return array_intersect_key($this->getLoadedEntityCollection($collectionName), $idsAsKeys);
	}

	/**
	 * @param \Illuminate\Support\Collection $entities
	 * @param string $collectionName
	 */
	private function storeLazyLoadedEntities(Collection $entities, $collectionName)
	{
		$entities = $entities->keyBy('id')->all();

		// Use the + operator instead of array_merge to preserve numeric keys
		$this->loadedEntityCollections[ $collectionName ] = $this->getLoadedEntityCollection($collectionName) + $entities;
	}
}