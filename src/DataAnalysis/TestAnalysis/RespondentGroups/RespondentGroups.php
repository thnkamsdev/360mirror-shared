<?php

namespace THNK\Mirror\DataAnalysis\TestAnalysis\RespondentGroups;

use Illuminate\Support\Collection;
use Respondentgroup;

class RespondentGroups
{

	/**
	 * @var \Illuminate\Support\Collection
	 */
	private $groups;

	public function getGroupById($id)
	{
		if ( ! $this->groups)
		{
			$this->groups = Respondentgroup::all();
		}

		$group = $this->groups->first(function ($index, $group) use ($id)
		{
			return $group->id == $id;
		});

		if ( ! $group)
		{
			return 'Unknown';
		}

		return $group->name;
	}
}