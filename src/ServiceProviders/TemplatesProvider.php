<?php

namespace THNK\Mirror\ServiceProviders;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class TemplatesProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
	}

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		View::composer('*', 'THNK\Mirror\Templates\ViewComposers\LocaleComposer');
	}
}