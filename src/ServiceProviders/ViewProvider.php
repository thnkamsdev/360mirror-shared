<?php

namespace THNK\Mirror\ServiceProviders;

use Illuminate\View\ViewServiceProvider;
use THNK\Mirror\Templates\ViewFinder;

class ViewProvider extends ViewServiceProvider
{
	/**
	 * Register the view finder implementation.
	 *
	 * @return void
	 */
	public function registerViewFinder()
	{
		$this->app->bindShared('view.finder', function ($app)
		{
			$paths = $app['config']['view.paths'];

			return new ViewFinder($app['files'], $paths);
		});
	}
}