<?php

namespace THNK\Mirror\ServiceProviders;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use THNK\Mirror\Localization\Facades\Localization;
use THNK\Mirror\Localization\InvalidLocalePrefixException;
use THNK\Mirror\Localization\Locale;

class RoutesProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadLocaleBySegment();
	}

	private function loadLocaleBySegment()
	{
		// Default to English for console commands
		if ($this->app->runningInConsole())
		{
			return Localization::set(Localization::defaultLocale());
		}

		$firstSegment = Request::segment(1);
		if ( ! in_array($firstSegment, Localization::getURLPrefixes()))
		{
			return;
		}

		try
		{
			Localization::set(Locale::fromURLPrefix($firstSegment));
		}
		catch (InvalidLocalePrefixException $e)
		{

		}
	}
}