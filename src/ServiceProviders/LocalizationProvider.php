<?php

namespace THNK\Mirror\ServiceProviders;

use Illuminate\Routing\RouteCollection;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use THNK\Mirror\Localization\Locale;

class LocalizationProvider extends ServiceProvider
{
	/**
	 * @var \THNK\Mirror\Localization\Localization
	 */
	protected $localization;

	/**
	 * @var bool
	 */
	protected $updatedOnce;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('THNK\Mirror\Localization\Localization');
		$this->app->singleton('THNK\Mirror\Localization\RouteTranslator');
	}

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		/** @var \THNK\Mirror\Localization\Localization localization */
		$this->localization = $this->app->make('THNK\Mirror\Localization\Localization');

		$this->registerFilters();
		$this->registerListeners();
		$this->registerHelpers();

		// Set the initial language for php
		$this->updatePHPLocale($this->localization->current());
	}

	/**
	 *
	 */
	protected function registerFilters()
	{
		Route::filter('locale.force_prefix', 'THNK\Mirror\Http\Filters\Localization\ForcePrefixFilter');
		Route::filter('locale.set_through_get', 'THNK\Mirror\Http\Filters\Localization\SetThroughGetFilter');
		Route::filter('locale.default_to_english', 'THNK\Mirror\Http\Filters\Localization\DefaultToEnglishFilter');
	}

	/**
	 * Do not enable LC_ALL or LC_MONEY since this conflicts with the NumberFormatting class.
	 *
	 * @param \THNK\Mirror\Localization\Locale $locale
	 */
	private function updatePHPLocale(Locale $locale)
	{
		if (defined('LC_MESSAGES'))
		{
			setlocale(LC_MESSAGES, $locale->toString() . '.utf-8');
		}

		setlocale(LC_TIME, $locale->toString() . '.utf-8');
	}

	/**
	 * Reload the routes file. The current routes will be overridden.
	 */
	private function reloadRoutes()
	{
		//		$this->resetRoutes();

		//		$routesProvider = new RouteServiceProvider($this->app);
		//		$routesProvider->map($this->getRouter());
	}

	/**
	 *
	 */
	private function resetRoutes()
	{
		$newRoutes = new RouteCollection();
		$this->getRouter()->setRoutes($newRoutes);
	}

	/**
	 * @return \Illuminate\Routing\Router
	 */
	private function getRouter()
	{
		return $this->app->make('Illuminate\Routing\Router');
	}

	private function registerListeners()
	{
		// Listen for locale changes
		Event::listen('locale.changed', function ()
		{
			$locale = new Locale($this->app->getLocale());

			// Call the internal Locale change again to make sure it is updated,
			// also when someone calls App::setLocale
			$this->localization->set($locale);
			$this->updatePHPLocale($locale);
			$this->reloadRoutes();
		});
	}

	private function registerHelpers()
	{

		if ( ! function_exists('trans'))
		{
			/**
			 * Translate the given message.
			 *
			 * @param  string $id
			 * @param  array $parameters
			 * @param  string $domain
			 * @param  string $locale
			 *
			 * @return string
			 */
			function trans($id, $parameters = array(), $domain = 'messages', $locale = null)
			{
				return app('translator')->trans($id, $parameters, $domain, $locale);
			}
		}
	}
}