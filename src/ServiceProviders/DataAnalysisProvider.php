<?php

namespace THNK\Mirror\ServiceProviders;

use Illuminate\Support\ServiceProvider;

class DataAnalysisProvider extends ServiceProvider
{

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('THNK\Mirror\DataAnalysis\TestAnalysis\RespondentGroups\RespondentGroups');
	}
}