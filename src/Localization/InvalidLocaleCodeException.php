<?php

namespace THNK\Mirror\Localization;

use InvalidArgumentException;

class InvalidLocaleCodeException extends InvalidArgumentException
{

}