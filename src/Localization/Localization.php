<?php

namespace THNK\Mirror\Localization;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class Localization
{
	/**
	 * @var \THNK\Mirror\Localization\Locale $locale ;
	 */
	private $locale;

	/**
	 * @var \THNK\Mirror\Localization\Locale locale
	 */
	private $default;

	/**
	 *
	 */
	public function __construct()
	{
		$this->locale = new Locale(App::getLocale());
	}

	/**
	 * @return \THNK\Mirror\Localization\Locale
	 */
	public function defaultLocale()
	{
		if ( ! $this->default)
		{
			$this->default = new Locale(Config::get('app.locale'));
		}

		return $this->default;
	}

	/**
	 * Get or set the locale
	 *
	 * @param \THNK\Mirror\Localization\Locale|string $locale
	 *
	 * @throws \THNK\Mirror\Localization\InvalidLocaleCodeException
	 *
	 * @return bool
	 */
	public function set($locale)
	{
		if ( ! ($locale instanceof Locale))
		{
			$locale = new Locale($locale);
		}

		if ($locale->equals($this->current()))
		{
			return false;
		}

		$this->locale = $locale;

		// Update Laravel code
		App::setLocale($locale->getCode());

		return true;
	}

	/**
	 * @return \THNK\Mirror\Localization\Locale[]
	 */
	public function all()
	{
		return array_map(function ($localeCode)
		{
			return new Locale($localeCode);

		}, array_keys(Locale::$validLocales));
	}

	/**
	 * @return \THNK\Mirror\Localization\Locale
	 */
	public function get()
	{
		return $this->locale;
	}

	/**
	 * @return \THNK\Mirror\Localization\Locale
	 */
	public function current()
	{
		return $this->get();
	}

	/**
	 * @return string[]
	 */
	public function getURLPrefixes()
	{
		return array_map(function ($localeProperties)
		{
			return $localeProperties['urlPrefix'];
		}, Locale::$validLocales);
	}
}