<?php

namespace THNK\Mirror\Localization;

use THNK\Mirror\Localization\Facades\Localization as LocalizationFacade;

trait LocalizedModel
{

	/**
	 * Retrieve the localized version of a certain attribute.
	 *
	 * @param string $key
	 *
	 * @return mixed
	 */
	protected function getLocalized($key)
	{
		if ( ! isset($this->attributes[ $this->getLocalizedKey($key) ]))
		{
			return null;
		}

		return $this->attributes[ $this->getLocalizedKey($key) ];
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 */
	protected function setLocalized($key, $value)
	{
		$this->attributes[ $this->getLocalizedKey($key) ] = $value;
	}

	/**
	 * @param string $key
	 *
	 * @return string
	 */
	private function getLocalizedKey($key)
	{
		$locale = LocalizationFacade::current();

		return $key . '_' . str_replace('-', '', $locale->getCode());
	}

}