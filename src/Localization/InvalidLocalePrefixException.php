<?php

namespace THNK\Mirror\Localization;

use InvalidArgumentException;

class InvalidLocalePrefixException extends InvalidArgumentException
{

}