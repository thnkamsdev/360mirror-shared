<?php

namespace THNK\Mirror\Localization\Facades;

use Illuminate\Support\Facades\Facade;
use THNK\Mirror\Localization\Locale;

/**
 * @method static current() : \THNK\Mirror\Localization\Locale
 * @method static default() : \THNK\Mirror\Localization\Locale
 * @method static set(Locale|string $locale)
 * @method static getURLPrefixes() : array
 */
class Localization extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 *
	 * @throws \RuntimeException
	 */
	protected static function getFacadeAccessor()
	{
		return 'THNK\Mirror\Localization\Localization';
	}
}