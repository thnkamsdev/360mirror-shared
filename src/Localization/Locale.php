<?php

namespace THNK\Mirror\Localization;

class Locale
{
	/**
	 * Helper constants
	 */
	const ENGLISH = 'en-US';

	const SPANISH = 'es-ES';

	/**
	 * List of all valid languages and their properties
	 *
	 * @var array
	 */
	public static $validLocales = [
		'en-US' => [
			'urlPrefix'  => 'en',
			'label'      => 'English',
			'localLabel' => 'English',
		],
		'es-ES' => [
			'urlPrefix'  => 'es',
			'label'      => 'Spanish',
			'localLabel' => 'Español',
		],
	];

	/**
	 * @var string
	 */
	protected $code;

	/**
	 * @param string $code
	 */
	public function __construct($code)
	{
		$this->code = $this->parseAndValidate($code);
	}

	private function parseAndValidate($code)
	{
		if ( ! isset(self::$validLocales[ $code ]))
		{
			throw new InvalidLocaleCodeException();
		}

		return $code;
	}

	/**
	 * @param string $prefix
	 *
	 * @return \THNK\Mirror\Localization\Locale
	 */
	public static function fromURLPrefix($prefix)
	{
		$codes = array_keys(array_filter(self::$validLocales, function ($properties) use ($prefix)
		{
			return $properties['urlPrefix'] === $prefix;
		}));

		if (count($codes) === 0)
		{
			throw new InvalidLocalePrefixException();
		}

		return new self($codes[0]);
	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->getLocaleProperty('label');
	}

	/**
	 * @return string
	 */
	public function getLocalLabel()
	{
		return $this->getLocaleProperty('localLabel');
	}

	/**
	 * @return string
	 */
	public function getURLPrefix()
	{
		return $this->getLocaleProperty('urlPrefix');
	}

	/**
	 * @return string
	 */
	public function getDatabaseSuffix()
	{
		return '_' . str_replace('-', '', $this->getCode());
	}

	/**
	 * @param \THNK\Mirror\Localization\Locale $locale
	 *
	 * @return bool
	 */
	public function equals(Locale $locale)
	{
		return $this->code === $locale->getCode();
	}

	/**
	 * @return string
	 */
	public function toString()
	{
		return $this->getCode();
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->toString();
	}

	/**
	 * @param string $propertyName
	 *
	 * @return mixed
	 */
	private function getLocaleProperty($propertyName)
	{
		return self::$validLocales[ $this->getCode() ][ $propertyName ];
	}

}