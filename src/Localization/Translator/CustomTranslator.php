<?php

namespace THNK\Mirror\Localization\Translator;

use Illuminate\Support\Facades\Auth;
use Illuminate\Translation\Translator;

class CustomTranslator extends Translator
{

	/**
	 * Get the translation for the given key. First see if there's a logged in user
	 * and if their client has overridden the default translation.
	 *
	 * @param  string $key
	 * @param  array $replace
	 * @param  string $locale
	 *
	 * @return string
	 */
	public function get($key, array $replace = array(), $locale = null)
	{
		/** @var \User $user */
		$user = Auth::user();
		if ( ! $user)
		{
			return parent::get($key, $replace, $locale);
		}

		$client = $user->getClient();
		if ( ! $client)
		{
			return parent::get($key, $replace, $locale);
		}

		$prefixedKey = $client->getFolder() . '/' . $key;
		$overwrittenTranslation = parent::get($prefixedKey, array(), $locale);
		if ($overwrittenTranslation !== $prefixedKey)
		{
			return $overwrittenTranslation;
		}

		return parent::get($key, $replace, $locale);
	}
}