<?php

namespace THNK\Mirror\Localization\Routes;

use Illuminate\Support\Facades\Request;
use THNK\Mirror\Localization\Locale;
use THNK\Mirror\Localization\Localization;

class RouteTranslator
{
	/**
	 * @var \THNK\Mirror\Localization\Localization
	 */
	private $localization;

	/**
	 * @param \THNK\Mirror\Localization\Localization $localization
	 */
	public function __construct(Localization $localization)
	{
		$this->localization = $localization;
	}

	/**
	 * @param string $url
	 * @param \THNK\Mirror\Localization\Locale $locale
	 *
	 * @return string
	 */
	public function translate($url, Locale $locale = null)
	{
		if ( ! $locale)
		{
			$locale = $this->localization->current();
		}

		$urlParts = $this->parseURL($url);
		$segments = explode('/', ltrim($urlParts['path'], '/'));

		if ( ! in_array($segments[0], $this->localization->getURLPrefixes()))
		{
			array_unshift($segments, '');
		}

		// Overwrite the locale prefix
		$segments[0] = $locale->getURLPrefix();
		$urlParts['path'] = '/' . join('/', $segments);

		return $this->reconstructURL($urlParts);
	}

	/**
	 * @param array $parsedURL
	 *
	 * @return string
	 */
	private function reconstructURL(array $parsedURL)
	{
		return http_build_url($parsedURL);
	}

	/**
	 * @param string $url
	 *
	 * @return array
	 */
	private function parseURL($url)
	{
		$defaults = [
			'scheme' => 'http',
			'host'   => $this->getHost(),
			'path'   => '',
		];

		return array_merge($defaults, parse_url($url));
	}

	/**
	 * @return string
	 */
	private function getHost()
	{
		return preg_replace('#https?://#', '', Request::root());
	}
}