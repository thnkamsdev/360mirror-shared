<?php

namespace THNK\Mirror\Http\Filters\Localization;

use Illuminate\Support\Facades\Input;
use THNK\Mirror\Localization\Facades\Localization;
use THNK\Mirror\Localization\Locale;

class SetThroughGetFilter
{

	public function filter()
	{
		$customLocale = Input::get('locale');
		if ( ! $customLocale)
		{
			return;
		}

		Localization::set(new Locale($customLocale));
	}
} 
