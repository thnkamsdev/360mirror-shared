<?php

namespace THNK\Mirror\Http\Filters\Localization;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use THNK\Mirror\Localization\Facades\Localization;

class ForcePrefixFilter
{

	/**
	 * Make sure the current locale is the first segment of the URL
	 */
	public function filter()
	{
		// If the first segment is not a valid language, it probably has no language at all
		if ( ! in_array(Request::segment(1), Localization::getURLPrefixes()))
		{

			// Prepend the default locale
			$path = Localization::current()->getURLPrefix() . '/' . Request::path();
			if (Request::getQueryString())
			{
				$path .= '?' . Request::getQueryString();
			}

			// Keep the flashed stuff
			Session::reflash();

			// And redirect there
			return Redirect::to($path);
		}
	}
} 
