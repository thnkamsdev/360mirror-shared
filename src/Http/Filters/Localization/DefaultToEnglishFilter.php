<?php

namespace THNK\Mirror\Http\Filters\Localization;

use THNK\Mirror\Localization\Facades\Localization;
use THNK\Mirror\Localization\Locale;

class DefaultToEnglishFilter
{
	public function filter()
	{
		Localization::set(new Locale(Locale::ENGLISH));
	}
}