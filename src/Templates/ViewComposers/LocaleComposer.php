<?php

namespace THNK\Mirror\Templates\ViewComposers;

use Illuminate\View\View;
use THNK\Mirror\Localization\Localization;
use THNK\Mirror\Localization\Routes\RouteTranslator;

class LocaleComposer
{
	/**
	 * @var \THNK\Mirror\Localization\Localization
	 */
	private $localization;

	/**
	 * @var \THNK\Mirror\Localization\Routes\RouteTranslator
	 */
	private $routeTranslator;

	/**
	 * @param \THNK\Mirror\Localization\Localization $localization
	 * @param \THNK\Mirror\Localization\Routes\RouteTranslator $routeTranslator
	 */
	public function __construct(Localization $localization, RouteTranslator $routeTranslator)
	{
		$this->localization = $localization;
		$this->routeTranslator = $routeTranslator;
	}

	/**
	 * @param \Illuminate\View\View $view
	 */
	public function compose(View $view)
	{
		$view->with([
			'locale'           => $this->localization->current(),
			'availableLocales' => $this->localization->all(),
			'urlTranslator'    => $this->routeTranslator,
		]);
	}
}