<?php

namespace THNK\Mirror\Templates;

use Illuminate\View\FileViewFinder;
use THNK\Mirror\Localization\Facades\Localization;

class ViewFinder extends FileViewFinder
{
	/**
	 * Overrides the default find method to handle the special email case.
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	public function find($name)
	{
		if (strpos($name, 'emails.') === 0)
		{
			$name .= '.' . $this->getLocaleBaseFilename();
		}

		return parent::find($name);
	}

	/**
	 * @return string
	 */
	private function getLocaleBaseFilename()
	{
		$locale = Localization::current();

		return $locale->getCode();
	}
}