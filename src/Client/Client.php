<?php

namespace THNK\Mirror\Client;

use Illuminate\Support\Facades\File;
use THNK\Mirror\Localization\Facades\Localization;
use User;

class Client
{
	/**
	 * @var User
	 */
	private $user;

	/**
	 * @param int|object $clientIdOrUserObject
	 *
	 * @throws \THNK\Mirror\Client\InvalidClientIDException
	 */
	public function __construct($clientIdOrUserObject)
	{
		$this->user = $clientIdOrUserObject;

		if ( ! is_object($this->user))
		{
			$this->user = User::find($clientIdOrUserObject);

			// Valid "client" users must have total seats defined
			if ( ! $this->user || ! $this->user->total_seats)
			{
				throw new InvalidClientIDException();
			}
		}
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->user->id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->user->company;
	}

	/**
	 * @return string
	 */
	public function getFolder()
	{
		if ( ! $this->user->slug)
		{
			$this->user->slug = self::generateSlugFromName($this->user->company);
			$this->user->save();
		}

		return $this->user->slug;
	}

	/**
	 * Get PDF path
	 *
	 * @param  string $pdfType full or self
	 *
	 * @return string           PDF path
	 */
	public function getPDFPath($pdfType)
	{
		$basePath = app_path('helpers/pdf');
		$fileName = sprintf('%s_%s.pdf', $pdfType, Localization::current()->getCode());

		// Search for a client override
		$clientSpecificPath = $basePath . '/' . $this->getFolder() . '/' . $fileName;

		if (File::exists($clientSpecificPath))
		{
			return $clientSpecificPath;
		}

		return $basePath . '/default/' . $fileName;
	}

	/**
	 * We can create a default client. This is basically a null object.
	 *
	 * @return \THNK\Mirror\Client\Client
	 * @throws \THNK\Mirror\Client\InvalidClientIDException
	 */
	public static function createDefault()
	{
		return new self((object) [
			'id'      => 'default',
			'company' => 'Default',
			'slug'    => 'default',
			'company' => 'default',
		]);
	}

	/**
	 * @param string $name
	 *
	 * @see http://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
	 * @return bool|mixed|string
	 */
	public static function generateSlugFromName($name)
	{
		// replace non letter or digits by -
		$name = preg_replace('~[^\pL\d]+~u', '-', $name);

		// transliterate
		$name = iconv('utf-8', 'us-ascii//TRANSLIT', $name);

		// remove unwanted characters
		$name = preg_replace('~[^-\w]+~', '', $name);

		// trim
		$name = trim($name, '-');

		// remove duplicate -
		$name = preg_replace('~-+~', '-', $name);

		// lowercase
		$name = strtolower($name);

		if (empty($name))
		{
			return 'n-a';
		}

		return $name;
	}
}