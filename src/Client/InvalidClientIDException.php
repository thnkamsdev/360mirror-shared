<?php

namespace THNK\Mirror\Client;

use InvalidArgumentException;

class InvalidClientIDException extends InvalidArgumentException
{

}